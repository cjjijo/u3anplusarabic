package com.mawaqaa.u3an.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.mawaqaa.u3an.appconstants.AppConstants;

public class Country {


	public String cityId;
	public String cityName;
	public ArrayList<SingleAreaCountry> areas;

	public Country( JSONObject jsonObject) {
		try {
			if (jsonObject != null) {
				if (!jsonObject.isNull(AppConstants.area_by_city_id))

					cityId = jsonObject.getString(AppConstants.area_by_city_id);

				if (!jsonObject.isNull(AppConstants.area_by_city_name))
					cityName = jsonObject
							.getString(AppConstants.area_by_city_name);
				if (!jsonObject.isNull(AppConstants.areas_all_areas)) {
					JSONArray jsonArray = jsonObject
							.getJSONArray(AppConstants.areas_all_areas);
					areas = new ArrayList<SingleAreaCountry>(jsonArray.length());
					for (int i = 0; i < jsonArray.length(); i++) {
						if (!jsonArray.isNull(i)) {
							JSONObject object = jsonArray.getJSONObject(i);
						

							areas.add(new SingleAreaCountry(object));
							
							//commenting the db insertion
							//SingleAreaCountry singleAreaCountry = new SingleAreaCountry(area, cityId, cityName);
							//baseActivity.insertSingleAreaCountry(singleAreaCountry);
						}
					}
				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}


}
