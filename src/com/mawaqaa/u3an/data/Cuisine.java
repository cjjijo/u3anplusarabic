package com.mawaqaa.u3an.data;

import org.json.JSONObject;

import android.util.Log;

import com.mawaqaa.u3an.appconstants.AppConstants;

public class Cuisine {

	public String strCuisineId;
	public String strCuisineName;

	public Cuisine() {
	}

	public Cuisine(JSONObject jsonObject) {

		Log.d("Cuisine", "jsonObject" + jsonObject);
		if (jsonObject != null) {
			if (!jsonObject.isNull(AppConstants.get_cusines_country_id))
				strCuisineId = jsonObject
						.optString(AppConstants.get_cusines_country_id);
			
			if (!jsonObject.isNull(AppConstants.get_cusines_country_name)) {
				strCuisineName = jsonObject
						.optString(AppConstants.get_cusines_country_name);
				// Capitalise First letter
		//Jijo
				if(strCuisineName.equals("")||strCuisineName == null){
										
				}else{
					strCuisineName = strCuisineName.substring(0, 1).toUpperCase()
							+ strCuisineName.substring(1);
				}
				
			}
		}
	}

}
