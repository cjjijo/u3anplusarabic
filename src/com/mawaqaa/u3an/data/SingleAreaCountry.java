package com.mawaqaa.u3an.data;

import org.json.JSONException;
import org.json.JSONObject;

import com.mawaqaa.u3an.appconstants.AppConstants;

public class SingleAreaCountry {

	public String areaId;
	//Anju
	public String areaName=null;
	public boolean isCityName;

	public SingleAreaCountry() {
		// TODO Auto-generated constructor stub
	}

	public SingleAreaCountry(JSONObject jsonObject) {
		// Retrieve JSON Objects
		try {
			if (jsonObject != null) {
				if (!jsonObject.isNull(AppConstants.areas_area_id))
					areaId = jsonObject.getString(AppConstants.areas_area_id);
				if (!jsonObject.isNull(AppConstants.areas_area_name)) {
					areaName = jsonObject
							.getString(AppConstants.areas_area_name);
					// Capitalise First letter
					areaName = areaName.substring(0, 1).toUpperCase()
							+ areaName.substring(1);
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

}
