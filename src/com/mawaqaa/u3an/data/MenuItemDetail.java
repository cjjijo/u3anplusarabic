package com.mawaqaa.u3an.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

public class MenuItemDetail {

	public String description;
	public String discount;
	public String ItemId;
	public int sectionId;
	public String name;
	public String price;
	public String rating;
	public String itemImgUrl;
	public ArrayList<MenuItemChoiceDetails> MenuItemChoiceDetails;
	

	public static final String MENU_LIST = "MenuDetails";
	private static final String DESCRIPTION = "Description";
	private static final String DISCOUNT = "Discount";
	private static final String ITEM_ID = "MenuItemId";
	private static final String SECTION_ID = "MenuSectionId";
	private static final String NAME = "Name";
	private static final String PRICE = "Price";
	private static final String RATING = "Rating";
	private static final String IMAGE_URL = "Thumbnail";
	public static final String MENUITEMCHOISE = "MenuItemChoices";

	public MenuItemDetail(JSONObject jsonObject) {
		
		if (jsonObject != null && !jsonObject.isNull(DESCRIPTION))
			description = jsonObject.optString(DESCRIPTION);
		if (jsonObject != null && !jsonObject.isNull(DISCOUNT))
			discount = jsonObject.optString(DISCOUNT);
		if (jsonObject != null && !jsonObject.isNull(ITEM_ID))
			ItemId = jsonObject.optString(ITEM_ID);
		if (jsonObject != null && !jsonObject.isNull(SECTION_ID))
			sectionId = jsonObject.optInt(SECTION_ID);
		if (jsonObject != null && !jsonObject.isNull(NAME))
			name = jsonObject.optString(NAME);
		if (jsonObject != null && !jsonObject.isNull(PRICE))
			price = jsonObject.optString(PRICE);
		if (jsonObject != null && !jsonObject.isNull(RATING))
			rating = jsonObject.optString(RATING);
		if (jsonObject != null && !jsonObject.isNull(IMAGE_URL))
			itemImgUrl = jsonObject.optString(IMAGE_URL);
		if (jsonObject != null && !jsonObject.isNull(MENUITEMCHOISE)) {
			JSONArray jsonArray = jsonObject.optJSONArray(MENUITEMCHOISE);
			int Arratlength = jsonArray.length();
			if (jsonArray != null && jsonArray.length() > 0) {
				MenuItemChoiceDetails = new ArrayList<MenuItemChoiceDetails>(Arratlength);
				for (int i = 0; i < jsonArray.length(); i++) {
					Log.e("Array....", ""+jsonArray.optJSONObject(i).toString());
					MenuItemChoiceDetails menuItemChoiceDetails = new MenuItemChoiceDetails(jsonArray.optJSONObject(i));
					MenuItemChoiceDetails.add(menuItemChoiceDetails);
				}
			}
		}
	}

	public class MenuItemChoiceDetails {
		public String minItem;
		public String maxItem;
		public static final String MenuItemChoic = "MenuItemChoiceDetails";
		public static final String MINM_COUNT = "MinItem";
		public static final String MAXIMUM_COUNT = "MaxItem";
		public ArrayList<MenuItemChoiceDetailsArray> MenuItemChoicearray;

		public MenuItemChoiceDetails(JSONObject jsonObject) {
			if (jsonObject != null && !jsonObject.isNull(MINM_COUNT))
				minItem = jsonObject.optString(MINM_COUNT);
			if (jsonObject != null && !jsonObject.isNull(MAXIMUM_COUNT))
				maxItem = jsonObject.optString(MAXIMUM_COUNT);
			
			Log.e(".....................................................", minItem+".."+maxItem);
			
			if (jsonObject != null && !jsonObject.isNull(MenuItemChoic)) {
				JSONArray jsonArray = jsonObject.optJSONArray(MenuItemChoic);
				int Arratlength = jsonArray.length();
				if (jsonArray != null && jsonArray.length() > 0) {
					MenuItemChoicearray = new ArrayList<MenuItemChoiceDetailsArray>(Arratlength);
					for (int i = 0; i < jsonArray.length(); i++) {
						Log.e("Array....2", ""+jsonArray.optJSONObject(i).toString());
						MenuItemChoiceDetailsArray menuItemChoiceDetails = new MenuItemChoiceDetailsArray(
								jsonArray.optJSONObject(i));						
						MenuItemChoicearray.add(menuItemChoiceDetails);
					}
				}
			}
		}
	}

	public class MenuItemChoiceDetailsArray {
		public String ItemTd;
		public String name;
		public String price;

		public static final String MenuItemChoic = "MenuItemChoiceDetails";
		public static final String ITEMID = "ItemId";
		public static final String NAME = "Name";
		public static final String PRICE = "Price";

		public MenuItemChoiceDetailsArray(JSONObject jsonObject) {			
			if (jsonObject != null && !jsonObject.isNull(NAME))
				name = jsonObject.optString(NAME);
			if (jsonObject != null && !jsonObject.isNull(PRICE))
				price = jsonObject.optString(PRICE);
			if (jsonObject != null && !jsonObject.isNull(ITEMID))
				ItemTd = jsonObject.optString(ITEMID);
		}
	}
}
