package com.mawaqaa.u3an.data;

import org.json.JSONObject;

public class OrderDetails {
	public String ItemChoiceId;
	public String ItemChoiceName;
	public String ItemChoicePrice;
	public String ItemChoiceQty;
	
	public static final String ORDERITEMCHOICE    = "OrderItemChoices";
	private static final String ITEMCHOICEID      = "ItemChoiceId";
	private static  final String ITEMCHOICENAME   ="ItemChoiceName" ;
	private static final String  ITEMCHOICEPRICE  = "ItemChoicePrice";
	private static final String ITEMCHOICEQTY     = "ItemChoiceQty";
	
	public OrderDetails(JSONObject jsonObject)
	{
		if (jsonObject!=null && !jsonObject.isNull(ITEMCHOICEID)) 
			ItemChoiceId = jsonObject.optString("ItemChoiceId");
		    if (jsonObject!=null && !jsonObject.isNull(ITEMCHOICENAME))
		    	ItemChoiceName=jsonObject.optString("ItemChoiceName");
		    if (jsonObject!=null && !jsonObject.isNull(ITEMCHOICEPRICE))
		    ItemChoicePrice  = jsonObject.optString("ItemChoicePrice");
		    if(jsonObject!=null && !jsonObject.isNull(ITEMCHOICEQTY))
		    ItemChoiceQty =jsonObject.optString("ItemChoiceQty");
	}
	

}
