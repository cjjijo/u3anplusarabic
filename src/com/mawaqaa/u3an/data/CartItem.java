package com.mawaqaa.u3an.data;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import com.mawaqaa.u3an.appconstants.AppConstants;

public class CartItem {

	public int itemId;
	public String itemName, itemPrice, itemQty, itemSpecialRequest;
	public ArrayList<CartItemChoice> cartItemChoices;

	CartItem(JSONObject jsonObject) {
		
		if (jsonObject != null) {
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_ID))
				itemId = jsonObject.optInt(AppConstants.ORDERED_ITEM_ID);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_NAME))
				itemName = jsonObject.optString(AppConstants.ORDERED_ITEM_NAME);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_PRICE))
				itemPrice = jsonObject
						.optString(AppConstants.ORDERED_ITEM_PRICE);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_QUANTITY))
				itemQty = jsonObject
						.optString(AppConstants.ORDERED_ITEM_QUANTITY);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_SPECIAL_REQUEST))
				itemSpecialRequest = jsonObject
						.optString(AppConstants.ORDERED_ITEM_SPECIAL_REQUEST);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_CHOICES)) {
				JSONArray jsonArray = jsonObject
						.optJSONArray(AppConstants.ORDERED_ITEM_CHOICES);
				if (jsonArray != null && jsonArray.length() > 0) {
					int arrayLen = jsonArray.length();
					cartItemChoices = new ArrayList<CartItemChoice>(arrayLen);
					for (int i = 0; i < arrayLen; i++) {
						if (!jsonArray.isNull(i)) {
							CartItemChoice cartItemChoice = new CartItemChoice(
									jsonArray.optJSONObject(i));
							cartItemChoices.add(cartItemChoice);
						}
					}
				}
			}
		}
	}

	public class CartItemChoice {
		int itemChoiceId, itemChoiceQnty;
		String itemChoiceName;
		double itemChoicePrice;

		CartItemChoice(JSONObject jsonObject) {
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_CHOICE_ID))
				itemChoiceId = jsonObject
						.optInt(AppConstants.ORDERED_ITEM_CHOICE_ID);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_CHOICE_NAME))
				itemChoiceName = jsonObject
						.optString(AppConstants.ORDERED_ITEM_CHOICE_NAME);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_CHOICE_PRICE))
				itemChoicePrice = jsonObject
						.optDouble(AppConstants.ORDERED_ITEM_CHOICE_PRICE);
			if (!jsonObject.isNull(AppConstants.ORDERED_ITEM_CHOICE_QUANTITY))
				itemChoiceQnty = jsonObject
						.optInt(AppConstants.ORDERED_ITEM_CHOICE_QUANTITY);
		}
	}

}
