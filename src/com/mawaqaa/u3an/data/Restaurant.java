package com.mawaqaa.u3an.data;

import org.json.JSONObject;

public class Restaurant {

	public static final String RESTAURANT_DETAILS = "RestaurantDetails";

	private static final String ACCEPTS_CC = "AcceptsCC";
	private static final String ACCEPTS_CASH = "AcceptsCash";
	private static final String ACCEPTS_KNET = "AcceptsKNET";
	private static final String COVERAGE_TIMING = "CoverageAreaTimingDetails";
	private static final String CUISINE = "Cuisine";
	private static final String DELIVERY_CHARGE = "DeliveryCharges";
	private static final String DELIVERY_TIME = "DeliveryTime";
	private static final String MIN_AMOUNT = "MinAmount";
	private static final String MINIMUM_AMOUNT = "MinimumAmount";
	private static final String RATIING = "Rating";
	private static final String RESTAURANT_ID = "RestaurantId";
	private static final String RESTAURANT_LOGO = "RestaurantLogo";
	private static final String RESTAURANT_NAME = "RestaurantName";
	private static final String RESTAURANT_STATUS = "RestaurantStatus";
	private static final String SUMMARY = "Summary";
	private static final String WORKING_HOUR = "Workinghour";
	private static final String U3AN_CHARGE = "u3anCharge";

	public boolean acceptsCC = false;
	public boolean acceptsCash = false;
	public boolean acceptsKNet = false;
	public String coverageTiming = "0:00 AM TO 12:00PM";
	public String cuisine = "";
	public double deliveryCharge = 0.00;
	public String deliveryTime = "0";
	public String minAmount = "0";
	public String minimumAmount = "0";
	public String rating = "0";
	public int restaurantId = 0;
	public String restaurantLogoUrl = "";
	public String restaurantName = "name";
	public String restaurantStatus = "CLOSED";
	public String summary = "";
	public String workingHour = "0";
	public int u3anCharge = 0;
	
	public Restaurant() {
	
	}
	
	 
	public Restaurant(JSONObject jsonObject) {
		if (jsonObject != null) {

			if (!jsonObject.isNull(ACCEPTS_CC))
				acceptsCC = jsonObject.optBoolean(ACCEPTS_CC);
			if (!jsonObject.isNull(ACCEPTS_CASH))
				acceptsCash = jsonObject.optBoolean(ACCEPTS_CASH);
			if (!jsonObject.isNull(ACCEPTS_KNET))
				acceptsKNet = jsonObject.optBoolean(ACCEPTS_KNET);

			if (!jsonObject.isNull(COVERAGE_TIMING))
				coverageTiming = jsonObject.optString(COVERAGE_TIMING);
			if (!jsonObject.isNull(CUISINE))
				cuisine = jsonObject.optString(CUISINE);

			if (!jsonObject.isNull(DELIVERY_CHARGE))
				deliveryCharge = jsonObject.optDouble(DELIVERY_CHARGE);

			if (!jsonObject.isNull(DELIVERY_TIME))
				deliveryTime = jsonObject.optString(DELIVERY_TIME);
			if (!jsonObject.isNull(MIN_AMOUNT))
				minAmount = jsonObject.optString(MIN_AMOUNT);
			if (!jsonObject.isNull(MINIMUM_AMOUNT))
				minimumAmount = jsonObject.optString(MINIMUM_AMOUNT);
			if (!jsonObject.isNull(RATIING))
				rating = jsonObject.optString(RATIING);

			if (!jsonObject.isNull(RESTAURANT_ID))
				restaurantId = jsonObject.optInt(RESTAURANT_ID);

			if (!jsonObject.isNull(RESTAURANT_LOGO))
				restaurantLogoUrl = jsonObject.optString(RESTAURANT_LOGO);
			if (!jsonObject.isNull(RESTAURANT_NAME))
				restaurantName = jsonObject.optString(RESTAURANT_NAME);
			if (!jsonObject.isNull(RESTAURANT_STATUS))
				restaurantStatus = jsonObject.optString(RESTAURANT_STATUS);
			if (!jsonObject.isNull(SUMMARY))
				summary = jsonObject.optString(SUMMARY);
			if (!jsonObject.isNull(WORKING_HOUR))
				workingHour = jsonObject.optString(WORKING_HOUR);
			if (!jsonObject.isNull(U3AN_CHARGE))
				u3anCharge = jsonObject.optInt(U3AN_CHARGE);
		}
	}
}
