package com.mawaqaa.u3an.data;

import java.util.HashMap;

public class SelectedChoiceItems {
	public int minItem;
	public int maxItem;
	
	public HashMap<Integer,String> addredItems;

	public SelectedChoiceItems() {
		minItem = 0;
		maxItem =0;
		addredItems = new HashMap<Integer,String>();
	}
}
