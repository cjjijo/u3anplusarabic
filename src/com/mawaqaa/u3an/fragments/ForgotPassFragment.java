package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.Utilities;

public class ForgotPassFragment extends DialogFragment {
	private String TAG = "Forgotpwd Fragment";
	EditText emailEdittext;
	Button ok, cancel;
	private String userNameString;
	private EditText errorEditText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, 0);
		if (getDialog() == null)
			return;
		getDialog().getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			// fragListener = (FragListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}

	}

	@Override
	public void onStart() {
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.dimAmount = 0.5f; // dim only a little bit
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");
		View view = inflater.inflate(R.layout.forgot_passwd_prompt, container,
				false);
		if (getActivity() instanceof BaseActivity)
			((BaseActivity) getActivity()).setupUI(view, false);
		emailEdittext = (EditText) view.findViewById(R.id.forgot_pwd);
		ok = (Button) view.findViewById(R.id.done);
		cancel = (Button) view.findViewById(R.id.cancel);
		getDialog().setCanceledOnTouchOutside(false);
		ok.setOnClickListener(submitListener);
		cancel.setOnClickListener(cancelListener);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);
		// getDialog().getWindow().getAttributes().windowAnimations =
		// R.style.dialogueAnim;

	};

	@Override
	public void onResume() {
		super.onResume();
		// emailEdittext.setText("");

	}

	OnClickListener cancelListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			if (getDialog() != null && getDialog().isShowing())
				getDialog().dismiss();

		}
	};
	OnClickListener submitListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			userNameString = emailEdittext.getText().toString();
			Boolean isValidMail = false;
			isValidMail = Utilities.doemailValidation(userNameString);
			Log.d("Forget Password.............", ""+userNameString);
			if (isValidMail == true) {
				
				Utilities.hide_keyboard(getActivity());
				new doForgetPassword().execute();
			}

			else {
				emailEdittext
						.setError(Html
								.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_email)+"</font>"));
				errorEditText = emailEdittext;
				emailEdittext.requestFocus();
				emailEdittext.addTextChangedListener(registerInputTextWatcher);
			}

		}
	};
	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (errorEditText != null)
				errorEditText.setError(null);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};

	public class doForgetPassword extends AsyncTask<Void, Void, Void> {

		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		String fName;

		InputStream istream;
		private ProgressDialog mProgressDialog;

		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			mProgressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
			mProgressDialog.setCancelable(false);
			mProgressDialog
					.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();

			try {
				JSONObject objJson = new JSONObject();
				objJson.accumulate("email", userNameString);
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}

				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						AppConstants.forget_password_url);
				StringEntity entity = new StringEntity(objJson.toString());
				Log.i("Login Request", ""+objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					Log.e("the result", ""+this.result);

					try {
						String vtype = jObj.getString("login_status");
					} catch (Exception e) {
						Log.e("The result", ""+e.getMessage());
					}
				} else {
					result = null;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			mProgressDialog.dismiss();
			String status = null;
			//Log.e("The result", this.result);

			if (this.result.contains("success")) {
				//mProgressDialog.dismiss();
				if (getDialog() != null && getDialog().isShowing())
					getDialog().dismiss();
				Toast.makeText(getActivity(), R.string.forgot_pwd_sent,
						Toast.LENGTH_SHORT).show();
			} else if (this.result.contains("Enter")) {
				
					getDialog().dismiss();
				emailEdittext.setError(Html.fromHtml("<font color='white'>"
						+ getString(R.string.registered_emailid) + "</font>"));
				errorEditText = emailEdittext;
				emailEdittext.requestFocus();
				emailEdittext.addTextChangedListener(registerInputTextWatcher);
				Toast.makeText(getActivity(), R.string.wrong_mail,
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getActivity(), R.string.enter_valid_email, Toast.LENGTH_SHORT)
						.show();
			}
//Jijo
			
			// Toast.makeText(getActivity(), this.result, Toast.LENGTH_SHORT).show();

			//
			/*
			 * if (jObj.length() > 0) {
			 * 
			 * try { JSONArray jsonarray = jObj
			 * .getJSONArray("forgot_password_status");
			 * 
			 * for (int i = 0; i < jsonarray.length(); i++) { jObj =
			 * jsonarray.getJSONObject(i);
			 * 
			 * // Retrieve JSON Objects status = jObj.getString("user_email"); }
			 * 
			 * mProgressDialog.dismiss(); } catch (Exception e) {
			 * 
			 * }
			 * 
			 * if (status.equals("Successfull")) { mProgressDialog.dismiss(); if
			 * (getDialog() != null && getDialog().isShowing())
			 * getDialog().dismiss(); Toast.makeText(getActivity(),
			 * R.string.forgot_pwd_sent, Toast.LENGTH_SHORT).show(); } else {
			 * emailEdittext.setError(Html.fromHtml("<font color='white'>" +
			 * getString(R.string.wrong_mail) + "</font>")); errorEditText =
			 * emailEdittext; emailEdittext.requestFocus(); emailEdittext
			 * .addTextChangedListener(registerInputTextWatcher);
			 * Toast.makeText(getActivity(), R.string.wrong_mail,
			 * Toast.LENGTH_SHORT).show();
			 * 
			 * }
			 * 
			 * } else { Toast.makeText(getActivity(), R.string.failed,
			 * Toast.LENGTH_SHORT).show(); }
			 */

		}
	}
}
