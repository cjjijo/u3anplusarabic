 package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpinnerAdapter;
import com.mawaqaa.u3an.adapters.SpinnerAreaAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.SingleAreaCountry;
import com.mawaqaa.u3an.db.OpenHelper;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

@SuppressWarnings("deprecation")
public class ConfirmOrderDeliveryDetailsFragment extends
		HeaderViewControlFragment implements OnClickListener {
	ImageButton villaButton, buildingButton, officeButton;
	Boolean villaFlag = true, buildingFlag = false, officeFlag = false;
	String address_Type = "0";

	TextView villaTextView, officeTextView, buildingTextView;
	String userName = null;
	TextView areaSelectedTextView;
	ArrayList<HashMap<String, String>> arraylist_Address_data;

	EditText errorEditTxt;
	String profileName;

	Button addNewAdddressButton;
	Spinner areaSpinner;
	String restaurantID;
	int maxLength = 25;

	private ArrayList<SingleAreaCountry> areaList;
	Button saveAddressButton;
	Boolean loginBoolean = true;
	LinearLayout apartmentLayout, floorLayout;

	String block, streetString, juddaString, buildinString, floorString,
			apartmentString, directionString, firstName, email, addressType,
			areaId, areaName, buildingNumber, housePhone, lastName,
			mobileNumber, workphone, company;

	String areaString;

	EditText blockEditText, streetEditText, juddaEditText, buildingEditText,
			floorEditText, apartmentEditText, directionEditText, emailEdittext;

	EditText firstNameEditText, lastNameEditText, MobileEditText;

	Boolean changeFlag = false;

	Button nextButton;
	OpenHelper db_Obj;

	LinearLayout firstNameLinearLayout, lastNameLinearLayout,
			emailLinearLayout, mobileLinearLayout;

	String FILENAMEAREA = "areaName.txt";

	ArrayList<String> address = new ArrayList<String>();

	Spinner addressSpiner;

	int spinnerPosition;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	SharedPreferences sharedpreferences;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(
				R.layout.fragment_confirmorder_deliveryinfo, gp, false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.confirm_order_heading));
		try {
			userName = getArguments().getString("UserName");
			restaurantID = getArguments().getString("RestaurantID");
			Log.e("UserName",""+ userName);
			Log.e("restaurantID............",""+ restaurantID);
		} catch (Exception e) {
		}

		if (Utilities.doemailValidation(userName)) {
			loginBoolean = true;
			getValuesFromService();
			// emailEdittext.setText(userName);
		} else {
			loginBoolean = false;
			changeFlag = true;
			new GetAreas().execute();
		}
		areaName = readFile();
		//Log.e("areaName........delivery spinner....", areaName);
		init(rootView);
		fixUI();
		return rootView;
	}

	private void getValuesFromService() {
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		FormatJsonRequest();
	}

	private String readFile() {
		String data = null;
		try {
			FileInputStream fis = Activity.openFileInput(FILENAMEAREA);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			data = br.readLine();
		} catch (Exception e) {
		}
		return data;
	}

	private void FormatJsonRequest() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", userName);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadAboutUsData(jsonObject);
	}

	private void loadAboutUsData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.getCustomerAddressURL,
					jsonObject);
		}
	}

	@Override
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
		super.getCustomerAddressSuccess(jsonObject);
		pareseData(jsonObject);
		Log.e("The data received", ""+jsonObject.toString());
	}

	@Override
	public void getCustomerAddressFail(JSONObject jsonObject) {
		super.getCustomerAddressFail(jsonObject);
		Activity.stopSpinWheel();
	}

	private void pareseData(JSONObject jsonObject) {
		Log.e("The data received",""+ jsonObject.toString());
		Activity.stopSpinWheel();
		JSONObject jsonobject = new JSONObject();
		jsonobject = jsonObject;
		JSONArray jsonarray;
		int selected_Position = 0;
		try {
			// Create an array
			arraylist_Address_data = new ArrayList<HashMap<String, String>>();
			try {
				// Locate the array name in JSON
				Log.e("Cust Address Json",""+ jsonobject.toString());
				jsonarray = jsonobject
						.getJSONArray(AppConstants.getCustomerAddressJSONObj);
				for (int j = 0; j < jsonarray.length(); j++) {
					HashMap<String, String> map = new HashMap<String, String>();
					JSONObject dummyJsonObject = jsonarray.getJSONObject(j);
					String status = dummyJsonObject.getString("IsPrimary");
					Log.e("Entered",""+ status);
					if (status.equals("True")) {
						selected_Position = j;
					}
					Log.e("The data is",""+ status);
					// Retrieve JSON Objects.....
					map.put(AppConstants.getCustomerAddressType,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressType));
					map.put(AppConstants.getCustomerAddressAreaId,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressAreaId));
					map.put(AppConstants.getCustomerAddressBlock,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressBlock));
					map.put(AppConstants.getCustomerAddressBuildingNo,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressBuildingNo));
					map.put(AppConstants.getCustomerAddressExtraDirections,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressExtraDirections));
					map.put(AppConstants.getCustomerfirstName, dummyJsonObject
							.getString(AppConstants.getCustomerfirstName));
					map.put(AppConstants.getCustomerAddressFloor,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressFloor));
					map.put(AppConstants.getCustomerhousePhone, dummyJsonObject
							.getString(AppConstants.getCustomerhousePhone));
					map.put(AppConstants.getCustomerAddressJudda,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressJudda));
					map.put(AppConstants.getCustomerlastName, dummyJsonObject
							.getString(AppConstants.getCustomerlastName));
					map.put(AppConstants.getCustomermobile, dummyJsonObject
							.getString(AppConstants.getCustomermobile));
					map.put(AppConstants.getCustomerAddressStreet,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressStreet));
					map.put(AppConstants.getCustomerAddressWorkphone,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressWorkphone));
					map.put(AppConstants.getCustomerAddressId, dummyJsonObject
							.getString(AppConstants.getCustomerAddressId));
					map.put(AppConstants.getCustomerAddressIsPrimary,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressIsPrimary));
					map.put(AppConstants.getCustomerAddressProfileName,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressProfileName));
					map.put(AppConstants.getCustomerAddressSuite,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressSuite));
					map.put(AppConstants.getCustomerlastName, dummyJsonObject
							.getString(AppConstants.getCustomerlastName));
					map.put(AppConstants.getCustomeremail, dummyJsonObject
							.getString(AppConstants.getCustomeremail));
					map.put(AppConstants.areaName,
							dummyJsonObject.getString(AppConstants.areaName));
					map.put(AppConstants.getCustomerAddressHousePhone,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressHousePhone));
					map.put(AppConstants.getCustomerAddressCompany,
							dummyJsonObject
									.getString(AppConstants.getCustomerAddressCompany));
					address.add(dummyJsonObject
							.getString(AppConstants.areaName));
					// Set the JSON Objects into the array
					arraylist_Address_data.add(map);
					try {
						addressSpiner.setAdapter(new SpinnerAdapter(Activity,
								address));
					} catch (Exception e) {
						Log.e("Data",""+ e.getMessage());
					}

					addressSpiner.setSelection(selected_Position);
					addressSpiner
							.setOnItemSelectedListener(new OnItemSelectedListener() {
								@Override
								public void onItemSelected(
										AdapterView<?> parent, View view,
										int position, long id) {
									HashMap<String, String> mapsingledata = arraylist_Address_data
											.get(position);
									block = mapsingledata
											.get(AppConstants.getCustomerAddressBlock);
									streetString = mapsingledata
											.get(AppConstants.getCustomerAddressStreet);
									juddaString = mapsingledata
											.get(AppConstants.getCustomerAddressJudda);
									buildinString = mapsingledata
											.get(AppConstants.getCustomerAddressBuildingNo);
									floorString = mapsingledata
											.get(AppConstants.getCustomerAddressFloor);
									apartmentString = mapsingledata
											.get(AppConstants.getCustomerAddressSuite);
									directionString = mapsingledata
											.get(AppConstants.getCustomerAddressExtraDirections);
									firstName = mapsingledata
											.get(AppConstants.getCustomerfirstName);
									email = mapsingledata
											.get(AppConstants.getCustomeremail);
									firstName = mapsingledata
											.get(AppConstants.getCustomerfirstName);
									areaId = mapsingledata
											.get(AppConstants.getCustomerAddressAreaId);
									areaName = mapsingledata
											.get(AppConstants.areaName);
									buildingNumber = mapsingledata
											.get(AppConstants.getCustomerAddressBuildingNo);
									housePhone = mapsingledata
											.get(AppConstants.getCustomerhousePhone);
									lastName = mapsingledata
											.get(AppConstants.getCustomerlastName);
									mobileNumber = mapsingledata
											.get(AppConstants.getCustomermobile);
									workphone = mapsingledata
											.get(AppConstants.getCustomerAddressWorkphone);
									company = mapsingledata
											.get(AppConstants.getCustomerAddressCompany);
									address_Type = mapsingledata
											.get(AppConstants.getCustomerAddressType);

									profileName = mapsingledata
											.get(AppConstants.getCustomerAddressProfileName);

									blockEditText.setText(block);
									blockEditText
											.setFocusableInTouchMode(false);

									streetEditText.setText(streetString);
									streetEditText
											.setFocusableInTouchMode(false);
									juddaEditText.setText(juddaString);
									juddaEditText
											.setFocusableInTouchMode(false);
									buildingEditText.setText(buildinString);
									buildingEditText
											.setFocusableInTouchMode(false);
									floorEditText.setText(floorString);
									floorEditText
											.setFocusableInTouchMode(false);
									apartmentEditText.setText(apartmentString);
									apartmentEditText
											.setFocusableInTouchMode(false);
									directionEditText.setText(directionString);
									directionEditText
											.setFocusableInTouchMode(false);
									areaSpinner.setClickable(false);

									if ((address_Type.startsWith("V"))
											|| (address_Type.startsWith("v"))) {
										ClickAction();

									} else if ((address_Type.startsWith("B"))
											|| (address_Type.startsWith("b"))) {
										ClickActionB();
									} else {
										ClickActionO();
									}
								}

								@Override
								public void onNothingSelected(
										AdapterView<?> parent) {
								}
							});
				}
			} catch (JSONException e) {
				Log.e("Error",""+ e.getMessage());
				e.printStackTrace();
			}
		} catch (Exception e) {
		}
	}

	private void init(View view) {
		villaButton = (ImageButton) view.findViewById(R.id.villaRadioBtnOrder);
		villaButton.setOnClickListener(this);

		officeButton = (ImageButton) view
				.findViewById(R.id.officeRadioBtnOrder);
		officeButton.setOnClickListener(this);

		buildingButton = (ImageButton) view
				.findViewById(R.id.buildingRadioBtnOrder);
		buildingButton.setOnClickListener(this);

		villaTextView = (TextView) view.findViewById(R.id.villaTextvieworder);
		villaTextView.setOnClickListener(this);
		buildingTextView = (TextView) view
				.findViewById(R.id.buildingTextviewOrder);
		buildingTextView.setOnClickListener(this);
		officeTextView = (TextView) view.findViewById(R.id.officeTextviewOrder);
		officeTextView.setOnClickListener(this);
		addNewAdddressButton = (Button) view
				.findViewById(R.id.addAnotherAddress);
		addNewAdddressButton.setOnClickListener(this);
		areaSpinner = (Spinner) view.findViewById(R.id.areaSpinner);
		areaSelectedTextView = (TextView) view.findViewById(R.id.areaSelected);
		if(areaName!=null){
			areaSelectedTextView.setText(areaName);
		}else{
			areaSelectedTextView.setText("");
		}
		apartmentLayout = (LinearLayout) view.findViewById(R.id.appartment);
		floorLayout = (LinearLayout) view.findViewById(R.id.floor);
		apartmentLayout.setVisibility(View.GONE);
		floorLayout.setVisibility(View.GONE);

		ArrayList<String> dummy = new ArrayList<String>();
		dummy.add(areaName);
		try {
			areaSpinner.setAdapter(new SpinnerAdapter(Activity, dummy));
		} catch (Exception e) {

		}
		blockEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_block);
		blockEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				blockEditText));
		blockEditText.setOnFocusChangeListener(new FocusChangedListener());
		blockEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });

		streetEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Street);
		streetEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				streetEditText));
		streetEditText.setOnFocusChangeListener(new FocusChangedListener());
		streetEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		juddaEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Judda);
		juddaEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		buildingEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Building);
		buildingEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		floorEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_floor);
		floorEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				floorEditText));
		floorEditText.setOnFocusChangeListener(new FocusChangedListener());
		floorEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		apartmentEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_apartment);

		directionEditText = (EditText) view
				.findViewById(R.id.directionEdittext);
		directionEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		emailEdittext = (EditText) view
				.findViewById(R.id.Edittext_cnfrm_order_email);
		emailEdittext.addTextChangedListener(new AddRestaurantTextwatcher(
				emailEdittext));
		emailEdittext.setOnFocusChangeListener(new FocusChangedListener());

		if (loginBoolean) {
			emailEdittext.setText(userName);
		}

		firstNameEditText = (EditText) view
				.findViewById(R.id.Edittext_confirmorder_firstname);
		firstNameEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				firstNameEditText));
		firstNameEditText.setOnFocusChangeListener(new FocusChangedListener());

		lastNameEditText = (EditText) view
				.findViewById(R.id.Edittext_cnfrm_order_lastname);
		MobileEditText = (EditText) view
				.findViewById(R.id.Edittext_cnfrm_order_mobile);
		MobileEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				MobileEditText));
		MobileEditText.setOnFocusChangeListener(new FocusChangedListener());

		nextButton = (Button) view.findViewById(R.id.nextConfirmOrder);
		nextButton.setOnClickListener(this);
		firstNameLinearLayout = (LinearLayout) view
				.findViewById(R.id.linlay_firstname);
		lastNameLinearLayout = (LinearLayout) view
				.findViewById(R.id.linlay_lastname);
		emailLinearLayout = (LinearLayout) view.findViewById(R.id.linlayemail);
		mobileLinearLayout = (LinearLayout) view
				.findViewById(R.id.linlaymobile);
		saveAddressButton = (Button) view.findViewById(R.id.save_edits);
		saveAddressButton.setOnClickListener(this);
		saveAddressButton.setVisibility(View.VISIBLE);

		addressSpiner = (Spinner) view.findViewById(R.id.addressSpinner);
		addressSpiner.setVisibility(View.VISIBLE);

		if (!Utilities.doemailValidation(userName)) {
			firstNameLinearLayout.setVisibility(View.VISIBLE);
			lastNameLinearLayout.setVisibility(View.VISIBLE);
			emailLinearLayout.setVisibility(View.VISIBLE);
			mobileLinearLayout.setVisibility(View.VISIBLE);
			saveAddressButton.setVisibility(View.VISIBLE);
			addressSpiner.setVisibility(View.GONE);
			addNewAdddressButton.setVisibility(View.GONE);
		} else {
			firstNameLinearLayout.setVisibility(View.GONE);
			lastNameLinearLayout.setVisibility(View.GONE);
			emailLinearLayout.setVisibility(View.GONE);
			mobileLinearLayout.setVisibility(View.GONE);
			addNewAdddressButton.setVisibility(View.VISIBLE);
		}
		if (changeFlag) {
		} else {

			blockEditText.setFocusableInTouchMode(false);
			streetEditText.setFocusableInTouchMode(false);
			juddaEditText.setFocusableInTouchMode(false);
			buildingEditText.setFocusableInTouchMode(false);
			floorEditText.setFocusableInTouchMode(false);
			apartmentEditText.setFocusableInTouchMode(false);
			directionEditText.setFocusableInTouchMode(false);
			if (villaFlag == true) {
				buildingFlag = false;
				officeFlag = false;
			}
			if (buildingFlag == true) {
				villaFlag = false;
				officeFlag = false;
			}
			if (officeFlag == true) {
				villaFlag = false;
				buildingFlag = false;
			}
		}
	}

	private void fixUI() {
		if (villaFlag == true) {
			villaButton.setBackgroundResource(R.drawable.villahouse2);
		} else {
			villaButton.setBackgroundResource(R.drawable.villahouse1);
		}
		if (officeFlag == true) {
			officeButton.setBackgroundResource(R.drawable.office2);
		} else {
			officeButton.setBackgroundResource(R.drawable.office1);
		}
		if (buildingFlag == true) {
			buildingButton.setBackgroundResource(R.drawable.building2);
		} else {
			buildingButton.setBackgroundResource(R.drawable.building1);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.villaRadioBtnOrder:
			if (changeFlag) {
				ClickActionForVilla();
			} else {
			}
			break;
		case R.id.buildingRadioBtnOrder:
			if (changeFlag) {
				clickActionForBuilding();
			} else {
			}
			break;
		case R.id.officeRadioBtnOrder:
			if (changeFlag) {
				clickActionForOffice();
			} else {
			}
			break;
		case R.id.villaTextvieworder:
			if (changeFlag) {
				ClickActionForVilla();
			} else {
			}
			break;
		case R.id.buildingTextviewOrder:
			if (changeFlag) {
				clickActionForBuilding();
			} else {
			}
			break;
		case R.id.officeTextviewOrder:
			if (changeFlag) {
				clickActionForOffice();
			} else {
			}
			break;
		case R.id.addAnotherAddress:
			addAnotherAddress();
			break;
		case R.id.nextConfirmOrder:
			clearFields();
			Fragment fragment = new ConfirmOrderDeliveryDetailsFragment();
			Bundle data = new Bundle();
			data.putString("UserName", userName);
			data.putString("RestaurantID", restaurantID);
			fragment.setArguments(data);
			Activity.pushFragments(fragment, false, true);
			break;
//*****************************************************************************************			
		case R.id.save_edits:
			if (Utilities.doemailValidation(userName)) {
				SaveAddress();
			} else {
				ValidateGuestData();
			}
			break;
		default:
			break;
		}
	}

	private void SaveAddress() {
		ValidateDataAndGoToNextStage(false);
	}

	private void AccumulateJsonSave() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonobject = new JSONObject();

		try {
			jsonobject.accumulate("areaId", areaId);
			jsonobject
					.accumulate("authKey", PrefUtil.getAuthkey(getActivity()));
			jsonobject.accumulate("block", blockEditText.getText().toString());
			jsonobject.accumulate("buildingNo", buildingEditText.getText()
					.toString());
			jsonobject.accumulate("extraDirections", directionEditText
					.getText().toString());
			jsonobject.accumulate("isPrimary", null);
			jsonobject.accumulate("judda", juddaEditText.getText().toString());
			if(sDefSystemLanguage.equals("en")){
				jsonobject.accumulate("locale", "en-US");
			}else{
				jsonobject.accumulate("locale", "ar-KW");
			}
			jsonobject.accumulate("profileName", null);
			jsonobject
					.accumulate("street", streetEditText.getText().toString());
			jsonobject.accumulate("suite", null);
			jsonobject.accumulate("type", address_Type);
			jsonobject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("conforddelivdetfrag req json", "" + jsonobject.toString());
		LoadData(jsonobject);
	}

	private void clearFields() {
		firstNameEditText.setText("");
		lastNameEditText.setText("");
		emailEdittext.setText("");
		MobileEditText.setText("");
		blockEditText.setText("");
		streetEditText.setText("");
		juddaEditText.setText("");
		buildingEditText.setText("");
		floorEditText.setText("");
		apartmentEditText.setText("");
		directionEditText.setText("");
	}

	private void LoadData(JSONObject jsonobject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("conforddelivdetfrag req URL",
					""+AppConstants.deleveryinformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.deleveryinformation,
					jsonobject);
		}
	}

	@Override
	public void LoaddeleverySuccessfully(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.LoaddeleverySuccessfully(jsonObject);
		Log.e("json response", "" + jsonObject.toString());
		goToNextstage();
	}

	@Override
	public void Loaddeleveryfail(JSONObject jsonObject) {
		super.Loaddeleveryfail(jsonObject);
		Log.e("json response Failed", "" + jsonObject.toString());
	}

	private void goToNextstage() {
		if (Utilities.doemailValidation(userName)) {
			ValidateDataAndGoToNextStage(true);
		} else {
			ValidateGuestData();
		}
	}

	private void ValidateGuestData() {
		boolean isvalid = DoFirstNameValidation();
		if (!isvalid)
			return;
		isvalid = doEmailIdValidation();
		if (!isvalid)
			return;

		isvalid = DoPhoneNumberValidation();
		if (!isvalid)
			return;

		isvalid = DoBlockValidation();
		if (!isvalid)
			return;

		isvalid = DoStreetValidation();
		if (!isvalid)
			return;

		isvalid = doFloorValidation();
		if (!isvalid)
			return;
		else {
			GoToNextStageGuest();
		}
	}

	private void GoToNextStageGuest() {
		firstName = firstNameEditText.getText().toString();
		block = blockEditText.getText().toString();
		buildinString = juddaEditText.getText().toString();
		directionString = directionEditText.getText().toString();
		floorString = floorEditText.getText().toString();
		housePhone = " ";
		juddaString = juddaEditText.getText().toString();
		lastName = lastNameEditText.getText().toString();
		mobileNumber = MobileEditText.getText().toString();
		streetString = streetEditText.getText().toString();
		workphone = " ";
		email = emailEdittext.getText().toString();
		apartmentString = apartmentEditText.getText().toString();

		Fragment fragment = new ConfirmOrderSecondStage();
		Bundle data = new Bundle();
		data.putString("address_Type", address_Type);
		data.putString("areaID", areaId);
		data.putString("block", block);
		data.putString("buildinString", buildinString);
		data.putString("directionString", directionString);
		data.putString("firstName", firstName);
		data.putString("floorString", floorString);
		data.putString("housePhone", housePhone);
		data.putString("juddaString", juddaString);
		data.putString("lastName", lastName);
		data.putString("mobile", mobileNumber);
		data.putString("streetString", streetString);
		data.putString("workPhone", workphone);
		data.putString("UserName", userName);
		data.putString("email", email);
		data.putString("apartmentString", apartmentString);
		data.putString("Company", company);
		fragment.setArguments(data);
		Activity.pushFragments(fragment, false, true);
	}

	private Boolean DoFirstNameValidation() {
		String fName = firstNameEditText.getText().toString();
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();	
				
		if (fName.length() == 0) {
			firstNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.first_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				firstNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a first name to continue</font>"));
			}else{
				firstNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£ÙˆÙ„ Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
			
			errorEditTxt = firstNameEditText;
			firstNameEditText.addTextChangedListener(registerInputTextWatcher);
			// firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private void ValidateDataAndGoToNextStage(boolean type) {
		boolean isvalid = DoBlockValidation();
		Log.e("DoBlockValidation...", "" + isvalid);
		if (!isvalid)
			return;
		/*
		 * isvalid = doEmailIdValidation(); Log.e("doEmailIdValidation...", "" +
		 * isvalid); if (!isvalid) return;
		 */
		Log.e("Address Type", ""+address_Type + addressType);

		isvalid = DoStreetValidation();
		Log.e("DoStreetValidation...", "" + isvalid);
		if (!isvalid)
			return;
		if (address_Type == "0") {

		}

		else {
			isvalid = doFloorValidation();
		}
		Log.e("DoFloorValidation...", "" + isvalid);
		if (!isvalid)
			return;
		// isvalid = DoPho                   neNumberValidation();
		// Log.e("DoPhoneNumberValidation...", ""+isvalid);
		// if (!isvalid)
		// return;

		else {
			if (type) {
				Log.e("type", "" + type);
				goToNextStage();
			} else if (changeFlag) {
				AccumulateJsonSave();
			} else {
				goToNextStage();
			}
		} 
	}

	private boolean DoPhoneNumberValidation() {
		String phoneString = MobileEditText.getText().toString();
		boolean chkLan = PrefUtil.isAppLanEnglish(getActivity());
		if (phoneString.length() == 0) {
			MobileEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(chkLan){
				MobileEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				MobileEditText
				.setError(Html
						.fromHtml("<font color='white'>ÙŠØ±Ø¬Ù‰ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ§Ù„Ø­ Ù…Ø¹ 5 Ø¥Ù„Ù‰ 15 Ø±Ù‚Ù…Ø§</font>"));
			}*/
			errorEditTxt = MobileEditText;
			MobileEditText.addTextChangedListener(registerInputTextWatcher);
			// mobileEditText.requestFocus();
			return false;
		}
		if (!Utilities.isValidPhoneNumber(phoneString)) {
			MobileEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(chkLan){
				MobileEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				MobileEditText
				.setError(Html
						.fromHtml("<font color='white'>ÙŠØ±Ø¬Ù‰ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ§Ù„Ø­ Ù…Ø¹ 5 Ø¥Ù„Ù‰ 15 Ø±Ù‚Ù…Ø§</font>"));
			}*/
			errorEditTxt = MobileEditText;
			MobileEditText.addTextChangedListener(registerInputTextWatcher);
			// mobileEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private void goToNextStage() {
		email = emailEdittext.getText().toString();
		block = blockEditText.getText().toString();
		streetString = streetEditText.getText().toString();
		juddaString = juddaEditText.getText().toString();
		buildinString = juddaEditText.getText().toString();
		floorString = floorEditText.getText().toString();
		apartmentString = apartmentEditText.getText().toString();
		Fragment fragment = new ConfirmOrderSecondStage();
		Bundle data = new Bundle();
		data.putString("address_Type", address_Type);
		data.putString("areaID", areaId);
		data.putString("block", block);
		data.putString("buildinString", buildinString);
		data.putString("directionString", directionString);
		data.putString("firstName", firstName);
		data.putString("floorString", floorString);
		data.putString("housePhone", housePhone);
		data.putString("juddaString", juddaString);
		data.putString("lastName", lastName);
		data.putString("mobile", mobileNumber);
		data.putString("streetString", streetString);
		data.putString("workPhone", workphone);
		data.putString("UserName", userName);
		data.putString("email", email);
		data.putString("apartmentString", apartmentString);
		fragment.setArguments(data);
		Activity.pushFragments(fragment, false, true);
	}

	private Boolean DoStreetValidation() {
		String sString = streetEditText.getText().toString();
		/*Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();*/
		boolean chkLan = PrefUtil.isAppLanEnglish(getActivity());
		if (sString.length() == 0) {
			streetEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_street)+"</font>"));
			/*if(chkLan){
				streetEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a Street name to continue</font>"));
			}else{
				streetEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø¥Ø¯Ø®Ø§Ù„ Ø§Ø³Ù… Ø§Ù„Ø´Ø§Ø±Ø¹ Ù„ Ù…ÙˆØ§ØµÙ„Ø©</font>"));
			}*/
			errorEditTxt = streetEditText;
			streetEditText.addTextChangedListener(registerInputTextWatcher);
			// firstNameEditText.requestFocus();
			return false;
		} else {
			Log.e("sString", "" + sString);
			return true;
		}
	}

	private Boolean doFloorValidation() {
		String sString = floorEditText.getText().toString();
		/*Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();*/
		boolean chkLan = PrefUtil.isAppLanEnglish(getActivity());
		if (sString.length() == 0) {
			if (address_Type == "0") {
				Log.e("dfkd", "msg" + address_Type);
				return true;
			} else {
				floorEditText
				.setError(Html
						.fromHtml("<font color='white'>"+getString(R.string.enter_floor)+"</font>"));
				/*if(chkLan){
					floorEditText
							.setError(Html
									.fromHtml("<font color='white'>Enter floor to continue</font>"));
				}else{
					floorEditText
					.setError(Html
							.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø¯ÙˆØ± Ù…ÙˆØ§ØµÙ„Ø©</font>"));
				}*/
				errorEditTxt = floorEditText;
				floorEditText.addTextChangedListener(registerInputTextWatcher);
				// firstNameEditText.requestFocus();
				return false;
			}
		} else {
			return true;
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		arraylist_Address_data = new ArrayList<HashMap<String, String>>();
		address = new ArrayList<String>();
	}

	private Boolean DoBlockValidation() {
		String blck = blockEditText.getText().toString();
		/*Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();*/
		boolean chkLan = PrefUtil.isAppLanEnglish(getActivity());
		if (blck.length() == 0) {
			blockEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_block)+"</font>"));
			/*if(chkLan){
				blockEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter Block to continue</font>"));
			}else{
				blockEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ ÙƒØªÙ„Ø© Ù„Ù…ÙˆØ§ØµÙ„Ø©</font>"));
			}*/
			errorEditTxt = blockEditText;
			blockEditText.addTextChangedListener(registerInputTextWatcher);
			// firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private void addAnotherAddress() {
		new GetAreas().execute();
		changeFlag = true;
		;
		blockEditText.setFocusableInTouchMode(true);
		streetEditText.setFocusableInTouchMode(true);
		juddaEditText.setFocusableInTouchMode(true);
		buildingEditText.setFocusableInTouchMode(true);
		floorEditText.setFocusableInTouchMode(true);
		apartmentEditText.setFocusableInTouchMode(true);
		directionEditText.setFocusableInTouchMode(true);
		blockEditText.setText("");
		streetEditText.setText("");
		juddaEditText.setText("");
		buildingEditText.setText("");
		floorEditText.setText("");
		apartmentEditText.setText("");
		directionEditText.setText("");
		saveAddressButton.setVisibility(View.VISIBLE);
	}

	// Address type 0,1,2 for villa ,building and office
	private void clickActionForOffice() {
		villaFlag = false;
		buildingFlag = false;
		officeFlag = true;
		address_Type = "2";
		clearFields();
		fixUI();
		visibilityControl("office");
	}

	private void clickActionForBuilding() {
		villaFlag = false;
		buildingFlag = true;
		officeFlag = false;
		address_Type = "1";
		clearFields();
		fixUI();
		visibilityControl("building");
	}

	private void ClickAction() {
		villaFlag = true;
		buildingFlag = false;
		officeFlag = false;
		address_Type = "0";
		fixUI();

		visibilityControl("villa");
	}

	private void ClickActionB() {
		villaFlag = false;
		buildingFlag = true;
		officeFlag = false;
		address_Type = "1";
		fixUI();
		visibilityControl("building");
	}

	private void ClickActionO() {
		villaFlag = false;
		buildingFlag = false;
		officeFlag = true;
		address_Type = "2";
		fixUI();
		visibilityControl("office");
	}

	private void ClickActionForVilla() {
		villaFlag = true;
		buildingFlag = false;
		officeFlag = false;
		address_Type = "0";
		clearFields();
		fixUI();
		visibilityControl("villa");
	}

	private void visibilityControl(String string) {
		switch (string) {
		case "villa":
			apartmentLayout.setVisibility(View.GONE);
			floorLayout.setVisibility(View.GONE);
			break;
		case "building":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;
		case "office":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
	}

	// AsyncTask for loading data....
	public class GetAreas extends AsyncTask<Void, String,

	Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;
		InputStream istream;
		boolean saveEdit = false;

		public GetAreas() {
		}

		public GetAreas(boolean sedit) {
			this.saveEdit = sedit;
		}

		@Override
		protected void onPreExecute() {
			Activity.startSpinwheel(false, true);
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				objJson.accumulate("restId", "6");
//Jijo
				/*final int CONN_WAIT_TIME = 3000;
				final int CONN_DATA_WAIT_TIME = 2000;
				
				HttpParams httpParams = new BasicHttpParams();      
				HttpConnectionParams.setConnectionTimeout(httpParams, CONN_WAIT_TIME);
				HttpConnectionParams.setSoTimeout(httpParams, CONN_DATA_WAIT_TIME);*/
//			
				
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						AppConstants.GET_AREA_BY_RESTAURANT_URL);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);

				} else {
					result = null;
				}
			} catch (JSONException e) {
				Log.e(TAB_TAG, "JSONException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				Log.e(TAB_TAG,
						"UnsupportedEncodingException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				Log.e(TAB_TAG,
						"ClientProtocolException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				Log.e(TAB_TAG, "IOException Error:" + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.w("GET_AREA_BY_RESTAURANT_URL===",""+result);
			Activity.stopSpinWheel();
			try {
				parseData(jObj);

			} catch (Exception e) {
				Log.e(TAB_TAG, "Error:" + e.getMessage());
			}
		}
	}

	private void parseData(JSONObject jsonObject) {
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.area_by_json_obj)) {
				JSONArray jsonArray = jsonObject
						.getJSONArray(AppConstants.area_by_json_obj);
				if (jsonArray != null && jsonArray.length() > 0) {
					areaList = new ArrayList<SingleAreaCountry>();
					for (int i = 0; i < jsonArray.length(); i++) {

						if (!jsonArray.isNull(i)) {
							JSONObject jobj = jsonArray.getJSONObject(i);
							if (jobj != null) {
								SingleAreaCountry singleAreaCountry = new SingleAreaCountry(
										jobj);
								if (singleAreaCountry.areaName != null)
									areaList.add(singleAreaCountry);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Log.e("Arraylist Spinner vcv", "kjkjk");
		populateSpinner();
	}

	private void populateSpinner() {
		SpinnerAreaAdapter spinAdapter = new SpinnerAreaAdapter(Activity,
				areaList);
		areaSpinner.setClickable(true);
		areaSpinner.setAdapter(spinAdapter);

		int position = areaSpinner.getSelectedItemPosition();
//Anju		
		if(areaList.size()!=0){
			areaString = areaList.get(position).areaName;
			areaSelectedTextView.setText(areaString);
			areaSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					areaName = areaList.get(position).areaName;
					areaSelectedTextView.setText(areaName);
	
					areaId = areaList.get(position).areaId;
					spinnerPosition = position;
				}
	
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});
		}else{
			 Toast.makeText(getActivity(),getResources().getString(R.string.nodata), Toast.LENGTH_SHORT).show();
			 Activity.onBackPressed();
		}
	}

	private boolean doEmailIdValidation() {
		String Email = emailEdittext.getText().toString().trim();
		//Anju
		boolean chkLan = PrefUtil.isAppLanEnglish(getActivity());
		if (Email == null || Email.length() < 1) {
			emailEdittext
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_email)+"</font>"));
			/*if(chkLan){
				emailEdittext
						.setError(Html
								.fromHtml("<font color='white'>Enter email id to proceed</font>"));
			}else{
				emailEdittext
						.setError(Html
								.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø¨Ø±ÙŠØ¯ Ø§Ù„Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠ</font>"));
			}*/
			errorEditTxt = emailEdittext;
			emailEdittext.addTextChangedListener(registerInputTextWatcher);
			// emailEditText.requestFocus();
			return false;
		}
		if (!Utilities.doemailValidation(Email)) {
			emailEdittext
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_email)+"</font>"));
			/*if(chkLan){
				emailEdittext
						.setError(Html
								.fromHtml("<font color='white'>Enter a Valid Email id</font>"));
			}else{
				emailEdittext
				.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø¨Ø±ÙŠØ¯ Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠ ØµØ­ÙŠØ­</font>"));
			}*/
			errorEditTxt = emailEdittext;
			emailEdittext.addTextChangedListener(registerInputTextWatcher);
			// emailEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private class AddRestaurantTextwatcher implements TextWatcher {

		private EditText editText;

		AddRestaurantTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	}

	private class FocusChangedListener implements OnFocusChangeListener {
		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {
				case R.id.Edittext_confirmorder_firstname:
					DoFirstNameValidation();
					break;
				case R.id.Edittext_cnfrm_order_email:
					doEmailIdValidation();
					break;
				case R.id.Edittext_cnfrm_order_mobile:
					DoPhoneNumberValidation();
					break;
				case R.id.Edittext_register2_block:
					DoBlockValidation();
					break;
				case R.id.Edittext_register2_Street:
					DoStreetValidation();
					break;
				case R.id.Edittext_register2_floor:
					doFloorValidation();
					break;

				default:
					break;
				}
			}
		}
	}

}
