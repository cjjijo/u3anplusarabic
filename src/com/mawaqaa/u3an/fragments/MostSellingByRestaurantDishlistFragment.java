package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MostSellingByRestaurantDishDataAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MostSellingByRestaurantDishlistFragment extends
		HeaderViewControlFragment {
	GridView dishGridView;
	String restaurant_ID, restrant_name, restaurant_logo;
	JSONArray jsonarray;
	TextView errorTextView;

	ArrayList<HashMap<String, String>> arraylist_mostselling_dishes_data;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_byrestaurant_dishlist, gp,
				false);
		setupUI(v, false);
		initView(v);
		setTitleForPage(getResources().getString(R.string.most_selling_dishes));
		
		restaurant_ID = getArguments().getString(
				AppConstants.all_restaurants_id);
		restrant_name = getArguments().getString(
				AppConstants.all_restaurants_resturant_name);
		restaurant_logo = getArguments().getString(
				AppConstants.all_restaurants_resturant_logo);
		AccumulateJSonObject();
// Jijo 29-12-15	
		dishGridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {				
				HashMap<String, String> map = new HashMap<String, String>();
				map = arraylist_mostselling_dishes_data.get(position);				
				Log.e("The Most selling dishes data is", ""+map.toString());
				String staString = map
						.get(AppConstants.most_selling_dishes_rastaurant_status);
				if(PrefUtil.getAppLanguage(Activity).equals("en")){
					switch (staString) {
					case AppConstants.OPEN:
						callMostSellingDishesDetails(position);
						break;
					default:
						break;
					}
				}else{
					if(staString.equalsIgnoreCase(getResources().getString(R.string.open))){
						callMostSellingDishesDetails(position);
					}
				}
				
			}
		});
		return v;
	}

	private void callMostSellingDishesDetails(int position) {
		Fragment mostSellingDishesDetailsFragment = new MostSellingRestaurantsDetails();
		Log.d("callMostSellingDishesDetails:", "url:"+restaurant_logo);
		Bundle params1 = new Bundle();
		params1.putString("Header", AppConstants.MOST_SELLING_TAG);
		params1.putString(AppConstants.all_restaurants_id, restaurant_ID);
		params1.putString(AppConstants.all_restaurants_resturant_name,
				restrant_name);
		params1.putString(AppConstants.all_restaurants_resturant_logo,
				restaurant_logo);
		mostSellingDishesDetailsFragment.setArguments(params1);
		Activity.pushFragments(mostSellingDishesDetailsFragment, false, true);
	}

	private void initView(View view) {
		dishGridView = (GridView) view.findViewById(R.id.gridView_dish_list);
		errorTextView = (TextView)view.findViewById(R.id.errorTextview);
	}

	private void AccumulateJSonObject() {
		Activity.startSpinwheel(true, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectRestaurants = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectRestaurants = new JSONObject();
			jsonObjectRestaurants.accumulate("RestaurantId", restaurant_ID);
			if(sDefSystemLanguage.equals("en")){
				jsonObjectRestaurants.accumulate("locale", "en-US");
			}else{
				jsonObjectRestaurants.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryData(jsonObjectRestaurants);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.most_selling_dishes_data, jsonObject);
		}
	}

	@Override
	public void onMostSellingDishesDataLoadedSuccessfull(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.onMostSellingDishesDataLoadedSuccessfull(jsonObject);
		parseData(jsonObject);
	}

	private void parseData(JSONObject jObj) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObj;
		// Create an array
		arraylist_mostselling_dishes_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL
		// address.....all_restaurants_id
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.most_selling_dishes_jobj);
			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);
				// Retrieve JSON Objects
				map.put(AppConstants.most_selling_dishes_dishname, jsonObject
						.getString(AppConstants.most_selling_dishes_dishname));
				map.put(AppConstants.most_selling_dishes_restaurantname,
						jsonObject
								.getString(AppConstants.most_selling_dishes_restaurantname));
				map.put(AppConstants.most_selling_dishes_rastaurant_status,
						jsonObject
								.getString(AppConstants.most_selling_dishes_rastaurant_status));
				map.put(AppConstants.most_selling_dishes_image, jsonObject
						.getString(AppConstants.most_selling_dishes_image));
				map.put(AppConstants.most_selling_dishes_id, jsonObject
						.getString(AppConstants.most_selling_dishes_id));
				// Set the JSON Objects into the array
				arraylist_mostselling_dishes_data.add(map);
			}
			populateGridData();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("Got exception",""+ e.getMessage());
		}
	}

	private void populateGridData() {	
		Log.e("The data", arraylist_mostselling_dishes_data.size()+"");
		if (arraylist_mostselling_dishes_data.size()>0) {
			MostSellingByRestaurantDishDataAdapter adapter = new MostSellingByRestaurantDishDataAdapter(
					Activity, arraylist_mostselling_dishes_data);
			dishGridView.setAdapter(adapter);
		}else{
			errorTextView.setVisibility(View.VISIBLE);
			errorTextView.setText(R.string.no_data_availabale);			
		}	
	}
}