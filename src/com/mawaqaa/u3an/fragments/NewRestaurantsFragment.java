package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.NewResturantsDataAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.DataHandlingUtilities;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class NewRestaurantsFragment extends HeaderViewControlFragment {
	GridView gridView;
	TextView errorTextView;
	ArrayList<HashMap<String, String>> arraylist_newrestaurants_data;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info
				.inflate(R.layout.fragment_newrestaurant, gp, false);
		setupUI(rootView, false);
		initView(rootView);
		setTitleForPage(getResources().getString(R.string.new_restaurent));
		PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.new_restaurent));
		loadRestaurants();
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				HashMap<String, String> map = new HashMap<String, String>();
				map = arraylist_newrestaurants_data.get(position);
				Fragment allRestaurantsDetailsFragment = new MostSellingRestaurantsDetails();
				Bundle params1 = new Bundle();
				params1.putString("Header", AppConstants.NEW_RESTAURANTS_TAG);
				params1.putString(AppConstants.all_restaurants_id,
						map.get(AppConstants.all_restaurants_id));
				params1.putString(AppConstants.all_restaurants_resturant_name,
						map.get(AppConstants.all_restaurants_resturant_name));
				params1.putString(AppConstants.all_restaurants_resturant_logo,
						map.get(AppConstants.all_restaurants_resturant_logo));
				allRestaurantsDetailsFragment.setArguments(params1);
				Activity.pushFragments(allRestaurantsDetailsFragment, false,
						true);
			}
		});

		return rootView;
	}

	private void initView(View view) {
		gridView = (GridView) view.findViewById(R.id.gridView_newresturants);
		errorTextView = (TextView) view.findViewById(R.id.emptyview);
	}

	private void loadRestaurants() {
		JSONObject jObj = new JSONObject();
		loadSubCategoryData(jObj);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		showProgressbar();
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.new_restaurant_url,
					jsonObject);
		}
	}

	@Override
	public void onNewRestaurantsLoadedSuccessfully(JSONObject jsonObject) {
		super.onNewRestaurantsLoadedSuccessfully(jsonObject);
		hideProgressbar();
		Log.d("testinggggggggggg",""+jsonObject);
		parseData(jsonObject);
	}

	@Override
	public void onNewRestaurantsLoadedFailed(JSONObject jsonObject) {
		super.onNewRestaurantsLoadedFailed(jsonObject);
		hideProgressbar();
		errorTextView.setVisibility(View.VISIBLE);
	}

	private void parseData(JSONObject jsonObject) {
		JSONObject jObj = new JSONObject();
		jObj = jsonObject;
		arraylist_newrestaurants_data = new ArrayList<HashMap<String, String>>();
		try {
			JSONArray jArray = jObj
					.getJSONArray(AppConstants.most_selling_restaurants_jsonobj);
			for (int i = 0; i < jArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jObj = jArray.getJSONObject(i);

				// Retrieve JSON Objects
				map.put(AppConstants.all_restaurants_rating,
						jObj.getString(AppConstants.all_restaurants_rating));
				map.put(AppConstants.all_restaurants_id,
						jObj.getString(AppConstants.all_restaurants_id));

				map.put(AppConstants.all_restaurants_resturant_logo, jObj
						.getString(AppConstants.all_restaurants_resturant_logo));
				map.put(AppConstants.all_restaurants_resturant_name, jObj
						.getString(AppConstants.all_restaurants_resturant_name));

				map.put(AppConstants.all_restaurants_resturant_status,
						jObj.getString(AppConstants.all_restaurants_resturant_status));

				// Set the JSON Objects into the array
				arraylist_newrestaurants_data.add(map);
			}

			populateGridview();
		} catch (Exception e) {
			Log.e("New Restaurant Frag", "Parsing Error");
			e.printStackTrace();
		}
	}

	private void populateGridview() {
		hideProgressbar();
		if (arraylist_newrestaurants_data.size() > 0) {
			NewResturantsDataAdapter adapter = new NewResturantsDataAdapter(
					Activity, arraylist_newrestaurants_data);
			gridView.setAdapter(adapter);
		} else {
			errorTextView.setVisibility(View.VISIBLE);
		}
	}

	protected void showProgressbar() {
		Activity.startSpinwheel(false, true);
	}

	protected void hideProgressbar() {
		Activity.stopSpinWheel();
	}

	@Override
	public void onResume() {
		super.onResume();
		DataHandlingUtilities listenerHideView = (DataHandlingUtilities) Activity;
		listenerHideView.hideToolBar();
		listenerHideView.newResturantsBottomBar();
		;
	}

}
