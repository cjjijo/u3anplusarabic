package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class U3AnFoodCartFragment extends HeaderViewControlFragment implements
		OnClickListener {
	String userName;
	ArrayList<HashMap<String, String>> arraylist_cart_data;
	ArrayList<HashMap<String, String>> arraylist_item_data = new ArrayList<HashMap<String, String>>();
	JSONObject jsonobject;
	JSONArray jsonarray;
	LogoutListener listener;

	float restaurantTotal = 0, grandTotal = 0;
	String imageString, minimumAmountString, restaurantNameString;
	int number_Of_Restaurant = 0;
	LinearLayout containerLayout;
	LinearLayout sub_item_Container;
	JSONObject dataFromServer = new JSONObject();
	TextView grantTotalTextView;
	Button nextButton, placr_Oder_Button;
	String restaurantID;
	String restaurantName;
	String spl_NAme;
	
	Boolean minimumOrderFlag = false;
	String restauratName_Low, minimumAmount_;
	float special_minl;

	String area_ID, restaurant_Logo, menuSectionId, menuSectionName,
			restaurant_ID, restaurant_Name, restaurant_rating, minimum_Amount;
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	DecimalFormat df = (DecimalFormat)nf;
	boolean flag = false;
	int countOnResumeAttempt = 0;
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		
		View v = info.inflate(R.layout.fragment_foodcart, gp, false);
		setupUI(v, false);
		init(v);
		flag = true;
		Log.d("U3AN foodcart.....Jijo", "initializeContainer");
		//setTitleForPage("Cart");
		//Anju
		setTitleForPage(getResources().getString(R.string.cart));
		Activity.stopSpinWheel();
		userName = getArguments().getString("UserName");
		restaurantID = getArguments().getString("RestaurantID");

		return v;
	}

	private void init(View view) {
		df.applyPattern("###,##0.000");
		containerLayout = (LinearLayout) view.findViewById(R.id.restdata_main);
		grantTotalTextView = (TextView) view
				.findViewById(R.id.grand_total_Textview);
		nextButton = (Button) view.findViewById(R.id.next_Cart);
		nextButton.setOnClickListener(this);
		placr_Oder_Button = (Button) view.findViewById(R.id.place_order_Cart);
		placr_Oder_Button.setOnClickListener(this);
	}

	//
	public class getCartInformation extends AsyncTask<Void, Void, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		JSONObject objJson;
		String fName;
		InputStream istream;
		private ProgressDialog mProgressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
			mProgressDialog.setCancelable(false);
			mProgressDialog
					.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				objJson.accumulate("userId", userName);
				
				
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost;
				if (Utilities.doemailValidation(userName)) {
					httppost = new HttpPost(AppConstants.GET_CART_INFO_URL);
					Log.e("The UUUUUUUUURRRRRRRRRRLLLLLLLLLLL",
							""+AppConstants.GET_CART_INFO_URL);
				} else {
					httppost = new HttpPost(AppConstants.GET_TEMP_CART_INFO_URL);
					Log.e("The UUUUUUUUURRRRRRRRRRLLLLLLLLLLL",
							""+AppConstants.GET_TEMP_CART_INFO_URL);
				}
				StringEntity entity = new StringEntity(objJson.toString());
				Log.i("Login Request",""+ objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);

				} else {
					result = null;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			Log.e("U3AnFooCartFragment","Posting json data :"+objJson.toString());
			mProgressDialog.dismiss();
			parseAndGroupData(jObj);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.place_order_Cart:
			Activity.onBackPressed();
			break;
		case R.id.next_Cart:
			if (minimumOrderFlag == true) {
				df.applyPattern("###,##0.000");
				Log.d("jijo Cart Min Amt:-", special_minl+"");
				Utilities.show(Activity,getString( R.string.min_order_amount)
						+" "+ spl_NAme + " is KD " + df.format(special_minl)
						, getString(R.string.proceed_shopping),
						okClicked, cancelClicked);
			} else {
				Fragment fragment1 = new ConfirmOrderDeliveryDetailsFragment();
				Bundle data = new Bundle();
				data.putString("UserName", userName);
				data.putString("RestaurantID", restaurantID);
				fragment1.setArguments(data);
				Activity.pushFragments(fragment1, false, true);
			}
			break;
		default:
			break;
		}
	}

	private DialogInterface.OnClickListener okClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			Activity.popFragments();
		}
	};

	private DialogInterface.OnClickListener cancelClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
		}
	};;

	private void parseAndGroupData(JSONObject jsonObject1) {
		Log.e("U3AnFooCartFragment.........parseAndGroupData","Posting json data :"+jsonObject1.toString());
		String restaurant_Name = "noname";
		jsonobject = new JSONObject();
		jsonobject = jsonObject1;

		try {
			// Create an array
			arraylist_cart_data = new ArrayList<HashMap<String, String>>();
			try {
				// Locate the array name in JSON
				jsonarray = jsonobject
						.getJSONArray(AppConstants.get_cart_info_jsonobj);
				if ((jsonarray.length() > 0)) {

					for (int i = 0; i < jsonarray.length(); i++) {
						jsonobject = jsonarray.getJSONObject(i);
						JSONArray jsonArray = jsonobject
								.getJSONArray(AppConstants.get_cart_info_sub_jsonobj);
						for (int j = 0; j < jsonArray.length(); j++) {
							HashMap<String, String> map = new HashMap<String, String>();
							JSONObject dummyJsonObject = jsonArray
									.getJSONObject(j);
							// Retrieve JSON Objects
							map.put(AppConstants.get_cart_info_area_ID,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_area_ID));
							map.put(AppConstants.get_cart_info_ItemChoiceIds,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ItemChoiceIds));
							map.put(AppConstants.get_cart_info_ItemId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ItemId));
							map.put(AppConstants.get_cart_info_ItemName,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ItemName));
							map.put(AppConstants.get_cart_info_OrderDate,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_OrderDate));
							map.put(AppConstants.get_cart_info_Quantity,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_Quantity));
							map.put(AppConstants.get_cart_info_RestaurantId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_RestaurantId));

							map.put(AppConstants.get_cart_info_ShoppingCartId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ShoppingCartId));

							map.put(AppConstants.get_cart_info_price,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_price));
							// Set the JSON Objects into the array
							arraylist_item_data.add(map);

						}
						imageString = jsonobject
								.getString(AppConstants.get_cart_info_Thumbnail);

						// Minimum amount is not available right now, So replace
						// it
						// with, when available(Top priority)
						minimumAmountString = jsonobject
								.getString(AppConstants.get_cart_info_MinimumAmount);
						restaurantName = jsonobject
								.getString(AppConstants.get_cart_info_RestaurantName);
				
						AddView();
					}
					
//Jijo				//anju	
					df.applyPattern("###,##0.000");
					String test = df.format(grandTotal);
					Log.d("U3AnFoodCartFragment", "Decimal UnFormat :"+grandTotal);
					
					if(countOnResumeAttempt<=1){
						if(PrefUtil.getTotal(Activity).equals("")){
							PrefUtil.setTotal(Activity, test.toString());
							Log.d("If in If", ""+test.toString());
						}
						else{
							PrefUtil.setTotal(Activity, test.toString());
							grantTotalTextView.setText(getResources().getString(R.string.KD)+ " "+PrefUtil.getTotal(Activity));
							Log.d("else in If", ""+test.toString());
						}
					}else{
						grantTotalTextView.setText(getResources().getString(R.string.KD)+" "+ PrefUtil.getTotal(Activity));
						//grantTotalTextView.setText(getResources().getString(R.string.KD)+ test);
					}
						
					
//Jijo
				} else {
					Log.e("The data", "Reached here");
					Activity.onBackPressed();
					if (Activity.readFile().equals("Guest")) {
						Activity.WriteDataToFile("");
					}
				}
			
			} catch (JSONException e) {
				Log.e("Error",""+ e.getMessage());
				Log.e("The data", "Reached here");
				Activity.onBackPressed();
				if (Activity.readFile().equals("Guest")) {
					Activity.WriteDataToFile("");
				}
				e.printStackTrace();
				Activity.onBackPressed();
				Toast.makeText(Activity, R.string.no_cart_item, Toast.LENGTH_SHORT)
						.show();
			}
		} catch (Exception e) {
		}
		Log.e("The result",""+ arraylist_cart_data.toString());
		Log.e("", number_Of_Restaurant + "");
//Jijo
		if (minimumOrderFlag) {
			//getNextButton();
			if(PrefUtil.getAppLanguage(Activity).equals(AppConstants.STR_EN)){
				nextButton.setBackgroundResource(R.drawable.next_btn_inac);
			}else{
				nextButton.setBackgroundResource(R.drawable.next_btn_inac_ar);
			}
			
		} else {
			Log.d("jijoooooooooooo", "entered here");
			if(PrefUtil.getAppLanguage(Activity).equals(AppConstants.STR_EN)){
				
				nextButton.setBackgroundResource(R.drawable.next_btn);	
			}else{
				nextButton.setBackgroundResource(R.drawable.next_btn_ar);	
			}
					
		}
	}

//Jijo
	private void AddView() {
		if(flag){
			LayoutInflater layoutInflater = (LayoutInflater) Activity
					.getBaseContext().getSystemService(
							Context.LAYOUT_INFLATER_SERVICE);
			final View addView = layoutInflater.inflate(R.layout.cart_row, null);
			ImageView imageView = (ImageView) addView.findViewById(R.id.cart_image);
			TextView min_amnt_textView = (TextView) addView
					.findViewById(R.id.minimum_order_amnt);
			sub_item_Container = (LinearLayout) addView
					.findViewById(R.id.list_dishdata);
			Picasso.with(Activity).load(imageString).into(imageView);
			containerLayout.addView(addView);
			//code edit by Anju
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			//anju
			float min = Float.parseFloat(minimumAmountString);
			Log.d("Min in Head value:", ""+min);
			df.applyPattern("###,##0.000");
			String minStr = df.format(min);
			min_amnt_textView.setText(getResources().getString(R.string.mini_order_amt)
					+" "+ df.format(min)+" " + getResources().getString(R.string.KD));
			/*if(sDefSystemLanguage.equals("en")){
				min_amnt_textView.setText(getResources().getString(R.string.mini_order_amt)
						+" "+ minStr+" " + getResources().getString(R.string.KD));
			}else{			
				min_amnt_textView.setText( getResources().getString(R.string.KD)
						+" "+ minStr +" "+
						getResources().getString(R.string.mini_order_amt));		
			}*/

			for (int i = 0; i < arraylist_item_data.size(); i++) {
				LayoutInflater layoutInflater1 = (LayoutInflater) Activity
						.getBaseContext().getSystemService(
								Context.LAYOUT_INFLATER_SERVICE);
				final View subItemView = layoutInflater1.inflate(
						R.layout.cart_list_singleitem, null);
				ImageView itemImageView = (ImageView) subItemView
						.findViewById(R.id.item_id);
				TextView quantityTextView = (TextView) subItemView
						.findViewById(R.id.textquantity);
				TextView itemNameTextView = (TextView) subItemView
						.findViewById(R.id.item_name);
				itemNameTextView.setSelected(true);
				TextView priceTextView = (TextView) subItemView
						.findViewById(R.id.textview_price);
				final ImageButton closeButton = (ImageButton) subItemView
						.findViewById(R.id.close_image_Button);
				HashMap<String, String> singleData = arraylist_item_data.get(i);
				String itemName = singleData
						.get(AppConstants.get_cart_info_ItemName);
				String price = singleData.get(AppConstants.get_cart_info_price);
				closeButton.setId(Integer.parseInt(singleData
						.get(AppConstants.get_cart_info_ShoppingCartId)));
				
	//**************************************************************************************************************			
				
				quantityTextView.setText(singleData
						.get(AppConstants.get_cart_info_Quantity));
				try {
					float Total = (Float.parseFloat(price))
							* (Integer.parseInt(singleData
									.get(AppConstants.get_cart_info_Quantity)));
					restaurantTotal = restaurantTotal + Total;
				} catch (Exception e) {
					Log.e("Exception While Parsing integer", restaurantTotal + "");
				}

				itemNameTextView.setText(itemName);

				float prices = (Float.parseFloat(price))
						* (Integer.parseInt(singleData
								.get(AppConstants.get_cart_info_Quantity)));
				//anju
				df.applyPattern("###,##0.000");
				String test = df.format(prices);
				priceTextView.setText(test + "");
				sub_item_Container.addView(subItemView);
				
				closeButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						accumulateJSONObject(closeButton.getId());
					}
				});
			}
					
			LayoutInflater layoutInflater2 = (LayoutInflater) Activity
					.getBaseContext().getSystemService(
							Context.LAYOUT_INFLATER_SERVICE);
			final View addView1 = layoutInflater2.inflate(R.layout.total_price_row,
					null);
			TextView restaurant_pricee = (TextView) addView1
					.findViewById(R.id.textview_price_restaurant_cart);
							
			//anju
			df.applyPattern("###,##0.000");
			String test = df.format(restaurantTotal);
			restaurant_pricee.setText(test);
				
	// Jijo
			Log.d("jijo", "Total:"+restaurantTotal);
			float minAmnt = Float.parseFloat(minimumAmountString);

			if (minAmnt > restaurantTotal) {
				minimumOrderFlag = true;
				special_minl = minAmnt;
				spl_NAme = restaurantName;
			} else {
				Log.e(".............................................", minAmnt
						+ "..." + restaurantTotal);
				//minimumOrderFlag = false;
				restauratName_Low = restaurantName;
			}
			grandTotal = grandTotal + restaurantTotal;
			
			
			restaurantTotal = 0;
			sub_item_Container.addView(addView1);
			
			arraylist_item_data = new ArrayList<HashMap<String, String>>();
		}
		else{
			Log.d("jijooo..U3ANFoodCart....","entered in pushFragments4LanSwitch()");
			pushFragments4LanSwitch();
		}
		
	}

	@Override
	public void onResume() {
		super.onResume();
		grandTotal = 0;
		Log.d("U3AN foodcart.....Jijo", "onResume()");
		countOnResumeAttempt++;
		if(countOnResumeAttempt>1){
			grantTotalTextView.setText(getResources().getString(R.string.KD)+PrefUtil.getTotal(Activity));
			Log.d("Jijoooo.......Total", ""+PrefUtil.getTotal(Activity));
			flag = false;
		}
			if (Utilities.isNetworkAvailable(Activity)) {
				userName = Activity.readFile();
				if (userName == null) {
					userName = Secure.getString(Activity.getContentResolver(),
							Secure.ANDROID_ID);
					new getCartInformation().execute();
				} else if (Utilities.doemailValidation(userName)) {
					new getCartInformation().execute();
				} else {
					userName = Secure.getString(Activity.getContentResolver(),
							Secure.ANDROID_ID);
					new getCartInformation().execute();
				}
			} else {
					
			}
		
	
	}

	private void accumulateJSONObject(int id) {
		Activity.startSpinwheel(false, true);
		JSONObject jsonObjectRemoveCartItems = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectRemoveCartItems = new JSONObject();
			jsonObjectRemoveCartItems.accumulate("ShoppingCartId", id);
			jsonObjectRemoveCartItems.accumulate("Username", userName);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryForgetPassword(jsonObjectRemoveCartItems);
	}

	private void loadSubCategoryForgetPassword(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			if (Utilities.doemailValidation(userName)) {
				commandFactory.sendPostCommand(
						AppConstants.REMOVE_CART_ITEM_URL, jsonObject);
			} else {
				commandFactory.sendPostCommand(
						AppConstants.REMOVE_TEMP_CART_ITEM_URL, jsonObject);
			}
		}
	}

	@Override
	public void removeCartItemSuccess(JSONObject jsonObject) {
		super.removeCartItemSuccess(jsonObject);
		Activity.stopSpinWheel();

		Log.e("The data",""+ jsonObject.toString());

		if (jsonObject.toString().contains("Success")) {
			//Anju
			Toast.makeText(Activity, getResources().getString(R.string.cart_item_remove),
					Toast.LENGTH_SHORT).show();
			Log.e("Entered into removed ", "Token");
			Activity.onBackPressed();
			Fragment cartFragment = new U3AnFoodCartFragment();
			Bundle data = new Bundle();
			data.putString("UserName", userName);
			cartFragment.setArguments(data);
			Activity.pushFragments(cartFragment, false, true);
		} else {
			Toast.makeText(Activity, getString(R.string.failed_to_remove),
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void removetempCartItemSuccess(JSONObject jsonObject) {
		super.removetempCartItemSuccess(jsonObject);
		if (jsonObject.toString().contains("Success")) {
			Toast.makeText(Activity, getResources().getString(R.string.cart_item_remove),
					Toast.LENGTH_SHORT).show();
			Log.e("Entered into removed ", "Token");
			Activity.onBackPressed();
			Fragment cartFragment = new U3AnFoodCartFragment();
			Bundle data = new Bundle();
			data.putString("UserName", userName);
			cartFragment.setArguments(data);
			Activity.pushFragments(cartFragment, false, true);
		} else {
			Toast.makeText(Activity, getString(R.string.failed_to_remove),
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void removetempCartItemFail(JSONObject jsonObject) {
		super.removetempCartItemFail(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity, getString(R.string.failed_to_remove),
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void removeCartItemFail(JSONObject jsonObject) {
		super.removeCartItemFail(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity, getString(R.string.failed_to_remove),
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
		Log.d("U3AN foodcart.....Jijo", "onActivityCreated()");
//
		
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			// Activity.popFragments();
			userName = Secure.getString(Activity.getContentResolver(),
					Secure.ANDROID_ID);

			Activity.onBackPressed();
			Fragment cartFragment = new U3AnFoodCartFragment();
			Bundle data = new Bundle();
			data.putString("UserName", userName);
			cartFragment.setArguments(data);
			Activity.pushFragments(cartFragment, false, true);
		}
	}
}
