package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MyAccountLandingPage extends HeaderViewControlFragment implements
		OnClickListener {

	TextView account_infoTextView, my_order_TextView, my_favorites_TextView,
			creadit_textview, userName , myPoints_textview;
	ImageButton account_infoButton, my_orderButton, my_favButton;
	LogoutListener listener;
	ArrayList<HashMap<String, String>> AddressList;
	String  username;
	String firstNameString, lastNameString, phoneString, resi_phoneString,
			firstname, lastname, mobilePhone, resi_PhNo, gender, sms,
			newsletter, occupation, uancreadit , u3anpoints;
	public String FILENAMEAUTHKEY = "authentication_key.txt";

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_myaccount, gp, false);
		setupUI(rootView, false);
		
		setTitleForPage(getResources().getString(R.string.my_account_heading));

		uancreadit = PrefUtil.getU3Ancredit(getActivity());
		u3anpoints = PrefUtil.getU3Anpoints(getActivity());
		
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		
		Log.e("username in landing page", ""+username);
		init(rootView);

		return rootView;
	}

	private void init(View view) {
		AccumulateDataforGetAddress();
		account_infoTextView = (TextView) view
				.findViewById(R.id.account_infoTextview);
		userName = (TextView) view.findViewById(R.id.textviewUsername);
		userName.setSelected(true);	
		creadit_textview = (TextView) view.findViewById(R.id.textviewcredit);
		creadit_textview.setText(uancreadit);
		myPoints_textview = (TextView) view.findViewById(R.id.textviewpoints);
		myPoints_textview.setText(u3anpoints);
		account_infoTextView.setOnClickListener(this);
		account_infoButton = (ImageButton) view
				.findViewById(R.id.btn_acount_info);
		account_infoButton.setOnClickListener(this);

		my_order_TextView = (TextView) view
				.findViewById(R.id.my_account_my_order);
		my_order_TextView.setOnClickListener(this);
		my_orderButton = (ImageButton) view.findViewById(R.id.my_order_button);
		my_orderButton.setOnClickListener(this);

		my_favorites_TextView = (TextView) view
				.findViewById(R.id.my_favourite_my_account);
		my_favorites_TextView.setOnClickListener(this);
		my_favButton = (ImageButton) view.findViewById(R.id.my_fav_button);
		my_favButton.setOnClickListener(this);

	}

	private void AccumulateDataforGetAddress() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		JSONObject jsonObject = new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LoadDataforGetAddress(jsonObject);

	}

	private void LoadDataforGetAddress(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.getdeleveryinformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.getdeleveryinformation,
					jsonObject);
		}
	}

	@Override
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.getCustomerAddressSuccess(jsonObject);
		Log.w("jsonObject....MyAccountLandingPage","=="+jsonObject);
		parse(jsonObject);
	}

	private void parse(JSONObject jsonObject) {
		AddressList = new ArrayList<HashMap<String, String>>();
		try {
			//Anju
			uancreadit = jsonObject.getString("U3ANCredit");
			if(uancreadit==null){
				uancreadit = "0.00";
			}
			Log.e("uancreadit........MyAccount", "" + uancreadit);
			PrefUtil.setU3ancredit(getActivity(), uancreadit);
			
			u3anpoints = jsonObject.getString("U3ANPoints");
			if(u3anpoints==null){
				u3anpoints = "0.00";
			}
			Log.e("u3anpoints........MyAccount", "" + u3anpoints);
			PrefUtil.setU3anpoints(getActivity(), u3anpoints);
			
			JSONArray jsonArray = jsonObject.getJSONArray("CustomerDetails");
			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);

				map.put(AppConstants.AreaId,
						jsonObject.getString(AppConstants.AreaId));
				map.put(AppConstants.Areaname,
						jsonObject.getString(AppConstants.Areaname));
				map.put(AppConstants.Block,
						jsonObject.getString(AppConstants.Block));
				map.put(AppConstants.Buildingno,
						jsonObject.getString(AppConstants.Buildingno));
				map.put(AppConstants.Extradirections,
						jsonObject.getString(AppConstants.Extradirections));
				map.put(AppConstants.floor,
						jsonObject.getString(AppConstants.floor));
				map.put(AppConstants.id, jsonObject.getString(AppConstants.id));
				map.put(AppConstants.LastName,
						jsonObject.getString(AppConstants.LastName));
				map.put(AppConstants.FirstName,
						jsonObject.getString(AppConstants.FirstName));
				map.put(AppConstants.getCustomermobile,
						jsonObject.getString(AppConstants.getCustomermobile));
				map.put(AppConstants.Res_Ph,
						jsonObject.getString(AppConstants.Res_Ph));
				map.put(AppConstants.Gender,
						jsonObject.getString(AppConstants.Gender));
				map.put(AppConstants.isprimary,
						jsonObject.getString(AppConstants.isprimary));
				map.put(AppConstants.judda,
						jsonObject.getString(AppConstants.judda));
				map.put(AppConstants.profileName,
						jsonObject.getString(AppConstants.profileName));
				map.put(AppConstants.street,
						jsonObject.getString(AppConstants.street));
				map.put(AppConstants.suite,
						jsonObject.getString(AppConstants.suite));
				map.put(AppConstants.subscribed_to_newsletter, jsonObject
						.getString(AppConstants.subscribed_to_newsletter));
				map.put(AppConstants.subscribed_to_sms,
						jsonObject.getString(AppConstants.subscribed_to_sms));
				map.put(AppConstants.occupation,
						jsonObject.getString(AppConstants.occupation));
				AddressList.add(map);
				Log.e("firstname", "" + map.get(AppConstants.FirstName));

				firstname = map.get(AppConstants.FirstName);
				lastname = map.get(AppConstants.LastName);
				mobilePhone = map.get(AppConstants.getCustomermobile);
				resi_PhNo = map.get(AppConstants.Res_Ph);
				gender = map.get(AppConstants.Gender);
				sms = map.get(AppConstants.subscribed_to_sms);
				newsletter = map.get(AppConstants.subscribed_to_newsletter);
				occupation = map.get(AppConstants.occupation);
		
			}
			Log.e("occupation..........................", "" + occupation);
			PrefUtil.setFirstname(getActivity(), firstname);
			PrefUtil.setLastname(Activity, lastname);
			PrefUtil.setPhonenum(getActivity(), mobilePhone);
			PrefUtil.setredsi_Phonenum(getActivity(), resi_PhNo);
			PrefUtil.setGender(getActivity(), gender);
			PrefUtil.setNewsletter(getActivity(), newsletter);
			PrefUtil.setsms(getActivity(), sms);
			PrefUtil.setOccupation(getActivity(), occupation);
			
			
			
			
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		username = PrefUtil.getFirstname(getActivity());
		userName.setText(username);
		creadit_textview.setText(uancreadit);
		myPoints_textview.setText(u3anpoints);
		Log.e("username......in landing", "" + username);
		Log.e("uancreadit", "" + AddressList);
		//myPoints.setText(mypoints);
		//Log.e("mypoints", "" + mypoints);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.account_infoTextview:
			onMyAccountClicked();
			break;
		case R.id.btn_acount_info:
			onMyAccountClicked();
			break;
		case R.id.my_account_my_order:
			onMyOrderClicked();
			break;
		case R.id.my_order_button:
			onMyOrderClicked();
			break;
		case R.id.my_favourite_my_account:
			onMyFavouriteClicked();
			break;
		case R.id.my_fav_button:
			onMyFavouriteClicked();
			break;
		default:
			break;
		}
	}

	private void onMyAccountClicked() {
		Fragment fragment = new AccountInformationFragment();
		Activity.pushFragments(fragment, false, true);
	}

	private void onMyOrderClicked() {
		Fragment fragment = new MyOrdersFragment();
		Activity.pushFragments(fragment, false, true);
	}

	private void onMyFavouriteClicked() {
		Fragment fragment = new MyFavouriteFragment();
		Activity.pushFragments(fragment, false, true);
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!PrefUtil.isUserSignedIn(Activity)) {
			Activity.popFragments();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
