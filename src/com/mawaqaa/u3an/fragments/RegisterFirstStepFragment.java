package com.mawaqaa.u3an.fragments;

import java.util.Locale;

import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.db.OpenHelper;
import com.mawaqaa.u3an.utilities.Utilities;

public class RegisterFirstStepFragment extends HeaderViewControlFragment
		implements OnClickListener {

	ImageButton nextButton;

	String emailString, passwordString, confirmPasswordString, firstNameString,
			lastNameString, phoneString, homePhoneString;
	int maxLength= 25;
	EditText emailEditText, passwordEditText, confirmPasswordEditText,
			firstNameEditText, lastNameEditText, mobileEditText,
			homePhoneEditText;
	EditText errorEditTxt;

	OpenHelper db_Obj;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_register, gp, false);
		init(rootView);		
		View converterView = rootView.findViewById(R.id.linlay_parent);
		setupUI(converterView, false);
		setTitleForPage(getResources().getString(R.string.sign_up_heading));
		db_Obj = new OpenHelper(Activity);
		return rootView;
	}

	private void init(View view) {
		nextButton = (ImageButton) view
				.findViewById(R.id.signup_first_stage_next);
		nextButton.setOnClickListener(this);

		emailEditText = (EditText) view
				.findViewById(R.id.Edittext_register_Email);
		emailEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				emailEditText));
		emailEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		passwordEditText = (EditText) view
				.findViewById(R.id.Edittext_register_Password);		
		passwordEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				confirmPasswordEditText));
		passwordEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		confirmPasswordEditText = (EditText) view
				.findViewById(R.id.Edittext_register_ConfirmPassword);
		confirmPasswordEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				confirmPasswordEditText));
		confirmPasswordEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		
		firstNameEditText = (EditText) view
				.findViewById(R.id.Edittext_register_FirstName);
		firstNameEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				firstNameEditText));
		firstNameEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		
		lastNameEditText = (EditText) view
				.findViewById(R.id.Edittext_register_LastName);
		
		lastNameEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				lastNameEditText));
		lastNameEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		/*firstNameEditText.setFilters(new InputFilter[] {
			    new InputFilter() {
			        public CharSequence filter(CharSequence src, int start,
			                int end, Spanned dst, int dstart, int dend) {
			            if(src.equals("")){ // for backspace
			                return src;
			            }
			            if(src.toString().matches("[a-zA-Z ]+")){
			                return src;
			            }
			            return "";
			        }
			    }
			});*/
		/*firstNameEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});*/
		/*lastNameEditText.setFilters(new InputFilter[] {
			    new InputFilter() {
			        public CharSequence filter(CharSequence src, int start,
			                int end, Spanned dst, int dstart, int dend) {
			            if(src.equals("")){ // for backspace
			                return src;
			            }
			            if(src.toString().matches("[a-zA-Z ]+")){
			                return src;
			            }
			            return "";
			        }
			    }
			});*/
	/*	lastNameEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});*/
		mobileEditText = (EditText) view
				.findViewById(R.id.Edittext_register_Mobile);
		mobileEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				mobileEditText));
		mobileEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		
		homePhoneEditText = (EditText) view
				.findViewById(R.id.Edittext_register_HousePhone);
		homePhoneEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				homePhoneEditText));
		homePhoneEditText
		.setOnFocusChangeListener(new FocusChangedListener());
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.signup_first_stage_next:
			DataValidation();
			break;

		default:
			break;
		}
	}
	
	

	private void DataValidation() {
		boolean isvalid = doEmailIdValidation();
		if (!isvalid)
			return;

		isvalid = passwordValidation();
		if (!isvalid)
			return;
		isvalid = confirmpasswordValidation();
		if (!isvalid)
			return;
		isvalid = DoFirstNameValidation();
		if (!isvalid)
			return;

		isvalid = DoLastNameValidation();
		if (!isvalid)
			return;

		isvalid = DoPhoneNumberValidation();
		if (!isvalid)
			return;

		isvalid = DoHomeNumberValidation();
		if (isvalid) {
			firstNameString = firstNameEditText.getText().toString();
			phoneString=mobileEditText.getText().toString();
			lastNameString=lastNameEditText.getText().toString();
			homePhoneString=homePhoneEditText.getText().toString();
			emailString=emailEditText.getText().toString().trim();
			passwordString=passwordEditText.getText().toString();
			
			db_Obj.DetailsInsertion(emailString, passwordString,
					firstNameString, lastNameString, phoneString,
					homePhoneString);
			Fragment registerSecondStage = new RegisterSecondStageFragment();
			Activity.pushFragments(registerSecondStage, false, true);
		}
		else {
			return;
		}
	}
	private Boolean DoFirstNameValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		String fNameString = firstNameEditText.getText().toString();
		if (fNameString.length() == 0) {
			firstNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.first_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				firstNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a first name to continue</font>"));
			}else{
				firstNameEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£ÙˆÙ„ Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
				errorEditTxt = firstNameEditText;
			firstNameEditText.addTextChangedListener(registerInputTextWatcher);
			//firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean DoHomeNumberValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		String hPhoneString = homePhoneEditText.getText().toString();
		if (hPhoneString.length() <= 0) {
			homePhoneEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				homePhoneEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				homePhoneEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			errorEditTxt = homePhoneEditText;
			homePhoneEditText.addTextChangedListener(registerInputTextWatcher);
		//	homePhoneEditText.requestFocus();
			return false;
		}
		if (!Utilities.isValidPhoneNumber(hPhoneString)) {
			homePhoneEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				homePhoneEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				homePhoneEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			errorEditTxt = homePhoneEditText;
			homePhoneEditText.addTextChangedListener(registerInputTextWatcher);
			//homePhoneEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean DoPhoneNumberValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		String pString = mobileEditText.getText().toString();
		if (pString.length() == 0) {
			mobileEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				mobileEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				mobileEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			errorEditTxt = mobileEditText;
			mobileEditText.addTextChangedListener(registerInputTextWatcher);
		//	mobileEditText.requestFocus();
			return false;
		}
		if (!Utilities.isValidPhoneNumber(pString)) {
			mobileEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				mobileEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				mobileEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			errorEditTxt = mobileEditText;
			mobileEditText.addTextChangedListener(registerInputTextWatcher);
			//mobileEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean DoLastNameValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		String lNameString = lastNameEditText.getText().toString();
		if (lNameString.length() == 0) {
			lastNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.last_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				lastNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a last name to continue</font>"));
			}else{
				lastNameEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£Ø®ÙŠØ± Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
			errorEditTxt = lastNameEditText;
			lastNameEditText.addTextChangedListener(registerInputTextWatcher);
			//lastNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private boolean doEmailIdValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		String emString = emailEditText.getText().toString().trim();
		if (emString == null || emString.length() < 1) {
			emailEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_email)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				emailEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter email id to proceed</font>"));
			}else{
				emailEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø¨Ø±ÙŠØ¯ Ø§Ù„Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠ</font>"));
			}*/
			errorEditTxt = emailEditText;
			emailEditText.addTextChangedListener(registerInputTextWatcher);
			//emailEditText.requestFocus();
			return false;
		}
		if (!Utilities.doemailValidation(emString)) {
			emailEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_valid_email)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				emailEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a Valid Email id</font>"));
			}else{
				emailEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø¨Ø±ÙŠØ¯ Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠ ØµØ­ÙŠØ­</font>"));
			}*/
			errorEditTxt = emailEditText;
			emailEditText.addTextChangedListener(registerInputTextWatcher);
			//emailEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean passwordValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		String pwdString = passwordEditText.getText().toString();
		
		if ((pwdString.length() == 0) || (pwdString.length() < 7)) {
			passwordEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.pw_min_7_chara)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				passwordEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a password with minimum 7 character</font>"));
			}else{
				passwordEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ ÙƒÙ„Ù…Ø© Ù…Ø±ÙˆØ± Ø¨Ù€ 7  Ø£Ø­Ø±Ù� Ø¹Ù„Ù‰ Ø§Ù„Ø£Ù‚Ù„</font>"));
			}*/
			errorEditTxt = passwordEditText;
			passwordEditText.addTextChangedListener(registerInputTextWatcher);
			//passwordEditText.requestFocus();
			return false;
		}else{
			return true;
		}
		
	}
	private Boolean confirmpasswordValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		String coPasswordString = confirmPasswordEditText.getText().toString();
		/*if((confirmPasswordString.length() == 0) || (confirmPasswordString.length() < 7))
		{
			confirmPasswordEditText
					.setError(Html
				.fromHtml("<font color='white'>Enter a password with minimum 7 character</font>"));
			errorEditTxt = confirmPasswordEditText;
			confirmPasswordEditText.addTextChangedListener(registerInputTextWatcher);
		//passwordEditText.requestFocus();
			return false;
		}
		 else*/ if (!coPasswordString.equals(passwordEditText.getText().toString())) {
			 confirmPasswordEditText.setError(Html
						.fromHtml("<font color='white'>"+getString(R.string.pw_mismach)+"</font>"));
			 /*if(sDefSystemLanguage.equals("en")){
				confirmPasswordEditText.setError(Html
						.fromHtml("<font color='white'>Password mismatch</font>"));
			 }else{
				 confirmPasswordEditText.setError(Html
							.fromHtml("<font color='white'>ÙƒÙ„Ù…Ø© Ø§Ù„Ø³Ø± ØºÙŠØ± Ù…Ø·Ø§Ø¨Ù‚Ø© </font>")); 
			 }*/
			errorEditTxt = confirmPasswordEditText;
			confirmPasswordEditText.addTextChangedListener(registerInputTextWatcher);
			//passwordEditText.requestFocus();
			//passwordEditText.setText("");
			//confirmPasswordEditText.setText("");
			return false;
		} 
		else {
			
			return true;
		}
	}
	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	
	private class AddRestaurantTextwatcher implements TextWatcher {

		private EditText editText;

		AddRestaurantTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {
			/*
			 * switch (editText.getId()) { case R.id.firstname:
			 * DoFirstNameValidation();
			 * 
			 * break; case R.id.lastname: DoLastNameValidation(); break; case
			 * R.id.restaurant_name: DoRestaurantNameValidation(); break; case
			 * R.id.restaurant_address: DoRestaurantAddressValidation(); break;
			 * case R.id.telephone: DoPhoneNumberValidation(); break; case
			 * R.id.email: doEmailIdValidation(); break;
			 * 
			 * default: break; }
			 */
		}

	}

	private class FocusChangedListener implements OnFocusChangeListener {

		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {

				case R.id.Edittext_register_Email:
					doEmailIdValidation();
					break;
				case R.id.Edittext_register_Password:
					passwordValidation();
					break;
				case R.id.Edittext_register_ConfirmPassword:
					confirmpasswordValidation();
					break;
				case R.id.Edittext_register_FirstName:
					DoFirstNameValidation();
					break;
				case R.id.Edittext_register_LastName:
					DoLastNameValidation();
					break;
				case R.id.Edittext_register_Mobile:
					DoPhoneNumberValidation();
					break;
				case R.id.Edittext_register_HousePhone:
					DoHomeNumberValidation();
					break;
				
				default:
					break;
				}
			}
		}

	}
	
	
	
}
