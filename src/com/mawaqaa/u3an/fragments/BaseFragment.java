package com.mawaqaa.u3an.fragments;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mawaqaa.u3an.BaseActivity;

public abstract class BaseFragment extends Fragment {
	private String TAG = "BaseFragment";
	protected boolean backPress;
	public static String TAB_TAG = "Restaurantdetails";
	BaseActivity Activity;

	private ProgressDialog mProgressDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Activity = (BaseActivity) this.getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	public void setupUI(View view, boolean isHeading) {
		if (Activity != null)
			Activity.setupUI(view, isHeading);
		else
			Log.e(TAG, "Activity not created");
	}

	// Cusines fetching
	public void onCusinesDataLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onCusinesLoadingFailed(JSONObject jsonObject) {
	}

	// Country fetching
	public void onCountryLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onCountryLoadingFailed(JSONObject jsonObject) {
	}

	// Most selling dishes Data fetching
	public void onMostSellingDishesLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onMostSellingDishesLoadingFailed(JSONObject jsonObject) {
	}

	// All restaurants
	public void onAllRestaurantsDataLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onAllresturantDataLoadingFailed(JSONObject jsonObject) {
	}

	// Most selling dishes by restaurants
	public void onMostSellingDishesByRestaurantsLoadedSuccessfully(
			JSONObject jsonObject) {
	}

	public void onMostSellingDishesByRestaurantsLoadingFailed(
			JSONObject jsonObject) {
	}

	// GetAreaByRestaurants
	public void onGetAreaByRestaurantsLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onGetAreaByRestaurantsLoadingFailed(JSONObject jsonObject) {
	}

	// GetRestaurantDetails
	public void onRestaurantDetailsLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onRestaurantDetailsLoadingFailed(JSONObject jsonObject) {
	}

	// Login data
	public void onLoginDataLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onLoginDataLoadingFailed(JSONObject jsonObject) {
	}

	// Forget password
	public void onForgetPasswordDataLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onForgetPasswordDataLoadingFailed(JSONObject jsonObject) {
	}

	// Most Selling Dishes details(By restaurant)
	public void onMostSellingDishesDataLoadedSuccessfull(JSONObject jsonObject) {
	}

	public void onMostSellingdishesDataLoadingFailed(JSONObject jsonObject) {
	}

	// Most Selling By cuisines Dishes List
	public void onMostSellingDishesByCuisinesLoadedSuccessfully(
			JSONObject jsonObject) {
	}

	public void onMostSellingDishesByCuisinesLoadingFailed(JSONObject jsonObject) {
	}

	// Get New Restaurants..
	public void onNewRestaurantsLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onNewRestaurantsLoadedFailed(JSONObject jsonObject) {
	}

	// Get Restaurant By Area List
	public void onGotResponseByArea(JSONObject jsonObject) {
	}

	public void onGettingResponseByAreaFailed(JSONObject jsonObject) {
	}

	// Get Advance Search Restaurant
	public void advanceSearchRestarants(JSONObject jsonObject) {
	}

	public void onAdvanceSeachRestaurantFailed(JSONObject jsonObject) {
	}

	public void onAdvanceSeachRestaurantSuccessFully(JSONObject jsonObject) {
	}

	// Get restaurant menus
	public void onRestaurantMenuLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onRestaurantMenuLoadingFailed(JSONObject jsonObject) {
	}

	// Most selling dishes
	public void onMostSellingdishesLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onMostSellingdishesLoadingFailed(JSONObject jsonObject) {
	}

	// Special Offers
	public void onSpecialOffersLoadedSuccessfull(JSONObject jsonObject) {
	}

	public void SignupSuccess(JSONObject jsonObject) {
	}

	public void SignupFail(JSONObject jsonObject) {
	}

	// getorderdetails
	public void onOrderdetailSuccesfully(JSONObject jsonObject) {
	}

	public void onOrderdetailFail(JSONObject jsonObject) {
	}

	public void addToCartSuccess(JSONObject jsonObject) {
	}

	public void addToCartFail(JSONObject jsonObject) {
	}

	/* Cart Fragment */
	public void cartLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void cartLoadingFailed(JSONObject jsonObject) {
	}

	/* Cart Fragment */
	public void PlaceQuickSuccessfully(JSONObject jsonObject) {
	}

	public void PlaceQuickFailed(JSONObject jsonObject) {
	}

	@Override
	public void onResume() {
		super.onResume();
		((BaseActivity) getActivity()).baseFragment = this;
	}

	public void onRestaurantMenuSectionsLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onRestaurantMenuSectionsLoadingFailed(JSONObject jsonObject) {
	}

	public void addingRestaurantFail(JSONObject jsonObject) {
	}

	public void restaurantAddedSuccessfully(JSONObject jsonObject) {
	}

	// get review
	public void onNewReviewSuccessfully(JSONObject jsonObject) {
	}

	public void onNewReviewFail(JSONObject jsonObject) {
	}

	public void onOrderCountGot(JSONObject jsonObject) {
	}

	public void onOrderCountGettingFail(JSONObject jsonObject) {
	}

	public void pageLoadSuccess(JSONObject jsonObject) {
	}

	public void pageLoadFail(JSONObject jsonObject) {
	}

	public void removeCartItemSuccess(JSONObject jsonObject) {
	}

	public void removeCartItemFail(JSONObject jsonObject) {
	}

	// get Promotions
	public void LoadPromotionsSuccessfully(JSONObject jsonObject) {
	}

	public void removetempCartItemSuccess(JSONObject jsonObject) {
	}

	public void removetempCartItemFail(JSONObject jsonObject) {
	}

	public void LoadPromotionsFaill(JSONObject jsonObject) {
	}

	// get promotion by restaurant
	public void LoadPromotionsbyresturentSuccessfully(JSONObject jsonObject) {
	}

	public void LoadPromotionsbyresturentfail(JSONObject jsonObject) {
	}

	public void FeedbackSumbitFail(JSONObject jsonObject) {
	}

	// Get Customer Adderess For CheckOut
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
	}

	public void getCustomerAddressFail(JSONObject jsonObject) {
	}

	// u3an credit
	public void LoadcrediuanSuccessfully(JSONObject jsonObject) {

	}

	public void Loadcrediuanfail(JSONObject jsonObject) {

	}

	public void MyFavouriteLoadedSuccessfully(JSONObject jsonObject) {

	}

	public void MyFavouriteLoadingFail(JSONObject jsonObject) {

	}

	public void removedFromFavouriteSuccessfully(JSONObject jsonObject) {

	}

	public void removedFromFavouriteFail(JSONObject jsonObject) {

	}

	public void onMyOrdersLoadedSuccessfully(JSONObject jsonObject) {

	}

	public void onMyOrdersLoadFail(JSONObject jsonObject) {

	}

	public void onItemorRestaurantratingSuccessfully(JSONObject jsonObject) {

	}

	public void onItemorRestaurantratingLoadFail(JSONObject jsonObject) {

	}

	// Get GiftVoucher
	public void LoadGiftVoucherSuccesfully(JSONObject jsonObject) {

	}

	public void LoadGiftVoucherFail(JSONObject jsonObject) {

	}

	public void LoadBuyGiftVoucherSuccesfully(JSONObject jsonObject) {

	}

	public void LoadBuyGiftVoucherFail(JSONObject jsonObject) {

	}

	// Getu3ancreditintervals
	public void LoadcreditintealsuanSuccessfully(JSONObject jsonObject) {
	}

	public void Loadcreditintealuanfail(JSONObject jsonObject) {
	}

	// delevery information
	public void LoaddeleverySuccessfully(JSONObject jsonObject) {
	}

	public void Loaddeleveryfail(JSONObject jsonObject) {
	}

	public void LoadgetdeleverySuccessfully(JSONObject jsonObject) {
	}

	public void Loadgetdeleveryfail(JSONObject jsonObject) {
	}

	public void changePasswordSuccessfully(JSONObject jsonObject) {
	}

	public void changePasswordfail(JSONObject jsonObject) {
	}

	public void EditAccountSuccessfully(JSONObject jsonObject) {
	}

	public void EditAccountfail(JSONObject jsonObject) {
	}

	public void MakeAsPrimarySuccess(JSONObject jsonObject) {
	}

	public void MakeAsPrimaryFail(JSONObject jsonObject) {
	}

	public void onAddressRemovalSuccess(JSONObject jsonObject) {
	}

	public void onAddressRemovalFail(JSONObject jsonObject) {
	}

	public void placeOrderSuccess(JSONObject jsonObject) {
	}

	public void placeOrderFail(JSONObject jsonObject) {
	}

	public void editAddressSuccess(JSONObject jsonObject) {
	}

	public void editAddressFail(JSONObject jsonObject) {
	}

	public void cartCountSuccess(JSONObject jsonObject) {
	}

	public void cartCountFail(JSONObject jsonObject) {
	}

	public void tempCartCountSuccess(JSONObject jsonObject) {
	}

	public void tempCartCountFail(JSONObject jsonObject) {
	}

	public void onAdvertisementLoadedSuccessfully(JSONObject jsonObject) {
	}

	public void onAdvertisementLoadingFailed(JSONObject jsonObject) {
	}

	/*
	 * public void HideKeyBoard(View view) {
	 * 
	 * Log.e("Entered into the ", "Base fragment");
	 * 
	 * // Set up touch listener for non-text box views to hide keyboard. if
	 * ((view instanceof LinearLayout)) {
	 * 
	 * view.setOnTouchListener(new OnTouchListener() {
	 * 
	 * @Override public boolean onTouch(View v, MotionEvent event) {
	 * Utilities.hide_keyboard(getActivity()); return false; }
	 * 
	 * }); }
	 * 
	 * // If a layout container, iterate over children and seed recursion. if
	 * (view instanceof ViewGroup) {
	 * 
	 * for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
	 * 
	 * View innerView = ((ViewGroup) view).getChildAt(i);
	 * 
	 * setupUI(innerView); } } }
	 */

	public void FeedbackSumbitSuccess(JSONObject jsonObject) {
	}

	public abstract void pushFragments4LanSwitch();

	public abstract void LanSwitchTrigered();
}
