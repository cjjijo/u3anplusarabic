package com.mawaqaa.u3an.fragments;

import java.io.FileOutputStream;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class LoginFragment extends HeaderViewControlFragment implements
		OnClickListener {

	private EditText nameEditText, passwordEditText;
	private Button loginButton, forgetPasswordButton, createAccountButton;
	private String usernameString, passwordString;
	private EditText errorEditText;
	private ImageButton checkbox_Button;
	private TextView checkbox_TextView;
	private Boolean keepMeLoggedInFlag = true;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	String FILENAMELOG = "keeplogin.txt";
	SharedPreferences sp;
	String Auth_key;
	public String name;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_login, gp, false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.login_heading));
		initView(rootView);
		
		writeData("keeplogin");		
		onCheckboxClickChange();
		Activity.stopSpinWheel();
		return rootView;
	}

	private void initView(View view) {
		nameEditText = (EditText) view.findViewById(R.id.editTextnamelogin);
		passwordEditText = (EditText) view
				.findViewById(R.id.editTextpasswordlogin);
		loginButton = (Button) view.findViewById(R.id.loginButton);
		forgetPasswordButton = (Button) view
				.findViewById(R.id.forgotpasswordButton);
		createAccountButton = (Button) view
				.findViewById(R.id.createaccountButton);
		checkbox_Button = (ImageButton) view.findViewById(R.id.btn_chk_box);
		checkbox_TextView = (TextView) view
				.findViewById(R.id.texview_keep_me_logged_in);

		loginButton.setOnClickListener(this);
		forgetPasswordButton.setOnClickListener(this);
		createAccountButton.setOnClickListener(this);
		checkbox_Button.setOnClickListener(this);
		checkbox_TextView.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.loginButton:
			doVerification();
			break;
		case R.id.forgotpasswordButton:
			Utilities.showForgotPasswordFrag(getChildFragmentManager());
			break;
		case R.id.createaccountButton:
			Fragment registerFragment = new RegisterFirstStepFragment();
			Activity.pushFragments(registerFragment, false, true);
			break;
		case R.id.btn_chk_box:
			if (keepMeLoggedInFlag) {
				keepMeLoggedInFlag = false;
			} else {
				keepMeLoggedInFlag = true;
			}
			onCheckboxClickChange();
			break;
		case R.id.texview_keep_me_logged_in:
			if (keepMeLoggedInFlag) {
				keepMeLoggedInFlag = false;
			} else {
				keepMeLoggedInFlag = true;
			}
			onCheckboxClickChange();
			break;
		default:
			break;
		}
	}

	private void onCheckboxClickChange() {
		if (keepMeLoggedInFlag) {
			checkbox_Button.setBackgroundResource(R.drawable.tick_box1);
			writeData("keeplogin");
		} else {
			checkbox_Button.setBackgroundResource(R.drawable.tick_box2);
			writeData("logout");
		}
	}

	private void AccumulateJSONObjectForgetPassword() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectCuisines = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectCuisines = new JSONObject();
			jsonObjectCuisines.accumulate("email", usernameString);
			if(sDefSystemLanguage.equals("en")){
				jsonObjectCuisines.accumulate("locale", "en-US");
			}else{
				jsonObjectCuisines.accumulate("locale", "ar-KW");
			}
				
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryForgetPassword(jsonObjectCuisines);
	}

	private void loadSubCategoryForgetPassword(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.login_url, jsonObject);
		}
	}

	@Override
	public void onForgetPasswordDataLoadedSuccessfully(JSONObject jsonObject) {
		super.onForgetPasswordDataLoadedSuccessfully(jsonObject);
		Activity.onBackPressed();
		Activity.stopSpinWheel();
	}

	private void doVerification() {
		usernameString = nameEditText.getText().toString();
		passwordString = passwordEditText.getText().toString();
		boolean isvalid = doNameValidation();
		if (!isvalid)
			return;
		isvalid = doPasswordValidation();
		if (!isvalid) {
			return;
		} else {
			AccumulateJSONObject();
		}
	}

	private void AccumulateJSONObject() {
		Activity.startSpinwheel(true, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectCuisines = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectCuisines = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObjectCuisines.accumulate("locale", "en-US");
			}else{
				jsonObjectCuisines.accumulate("locale", "ar-KW");
			}
			jsonObjectCuisines.accumulate("password", passwordString);
			jsonObjectCuisines.accumulate("username", usernameString);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryDataLogin(jsonObjectCuisines);
	}

	private void loadSubCategoryDataLogin(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.login_url, jsonObject);
		}
	}

	@Override
	public void onLoginDataLoadedSuccessfully(JSONObject jsonObject) {
		super.onLoginDataLoadedSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("The data receiced is", ""+jsonObject.toString());
		parseData(jsonObject);
	}

	@Override
	public void onLoginDataLoadingFailed(JSONObject jsonObject) {
		super.onLoginDataLoadingFailed(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity, R.string.login_failed,
				Toast.LENGTH_SHORT).show();
	}

	private void parseData(JSONObject jsonObject) {
		String result = jsonObject.toString();
		try {
			if (result.contains("success")) {
				JSONObject JObj = jsonObject.getJSONObject("LoginResult");
				Auth_key = JObj.getString("Authkey");
				Log.e("auth_Key", "" + Auth_key);
				PrefUtil.setAuthkey(getActivity(), Auth_key);
				Toast.makeText(Activity, R.string.login_success, Toast.LENGTH_SHORT)
						.show();
				PrefUtil.setUserSignedIn(Activity);				

				Activity.WriteDataToFile(usernameString);
				Activity.onBackPressed();
				Activity.popFragments(new RegisterFirstStepFragment());
				Activity.popFragments(new RegisterSecondStageFragment());
				Activity.popFragments(new RegisterThirdStageFragment());			
			} else

				if (jsonObject != null
						&& jsonObject.getJSONObject("LoginResult") != null
						&& !jsonObject.isNull("LoginResult")) {
					JSONObject object = jsonObject.getJSONObject("LoginResult");
					if (object != null && !object.isNull("Status"))
						Log.e("object.getString====", ""+object.getString("Status"));
					   if( object.getString("Status").equals("Username Is Invalid")){
							Toast.makeText(Activity, getResources().getString(R.string.username_is_invalid),
									Toast.LENGTH_SHORT).show();
					   }else if (object.getString("Status").equals("Password Is Invalid")) {
						   Toast.makeText(Activity, getResources().getString(R.string.password_is_invalid),
									Toast.LENGTH_SHORT).show();
					   }
			}
			try {
				Log.e("Authentication keyword", ""+Auth_key);
			} catch (Exception e) {
				Log.e("im here", "here..........");
				e.printStackTrace();
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
	}

	/*
	 * private void loadSavedPreferences() {
	 * 
	 * SharedPreferences sharedPreferences =
	 * PreferenceManager.getDefaultSharedPreferences(Activity);	 
	 * name = sharedPreferences.getString("storedName", Auth_key);	  
	 * }
	 * 
	 * private void savePreferences(String key, String value) {
	 * 
	 * SharedPreferences sharedPreferences =
	 * PreferenceManager.getDefaultSharedPreferences(Activity);
	 * 
	 * Editor editor = sharedPreferences.edit();
	 * 
	 * editor.putString(key, value);
	 * 
	 * editor.commit();
	 * 
	 * }
	 */
	/*
	 * // Writing value to the file private void writeData(String key) {
	 * 
	 * Log.e("The key value is", key);
	 * 
	 * FileOutputStream fosdummyy; try { fosdummyy =
	 * Activity.openFileOutput(FILENAMEAUTHKEY, Context.MODE_PRIVATE);
	 * fosdummyy.write(key.getBytes()); fosdummyy.close(); } catch (Exception e)
	 * { Log.e("Data Written", e.getMessage()); } }
	 */

	// Name validation
	private Boolean doNameValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		if (usernameString.length() < 1) {
			nameEditText.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				nameEditText.setError(Html
						.fromHtml("<font color='white'>Enter a valid name</font>"));
			}else{
				nameEditText.setError(Html
						.fromHtml("<font color='white'>أدخل اسم صحيح</font>"));
			}*/
			errorEditText = nameEditText;
			nameEditText.requestFocus();
			nameEditText.addTextChangedListener(registerInputTextWatcher);
			return false;
		} else {
			return true;
		}
	}

	// Password validation
	private Boolean doPasswordValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		if (passwordString.length() < 1) {
			passwordEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_password)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				passwordEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter the password to continue</font>"));
			}else{
				passwordEditText
					.setError(Html
						.fromHtml("<font color='white'>أدخل كلمة المرو للاستمرار</font>"));
			}*/
			errorEditText = passwordEditText;
			passwordEditText.requestFocus();
			passwordEditText.addTextChangedListener(registerInputTextWatcher);
			return false;
		} else {
			return true;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (errorEditText != null)
				errorEditText.setError(null);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {

		}
	};
	
	// Writing file for the service. 
    private void writeData(String data){
    FileOutputStream fosdummyy;
    try {
        fosdummyy = Activity.openFileOutput(FILENAMELOG, Context.MODE_PRIVATE);
        fosdummyy.write(data.getBytes());   
        fosdummyy.close();
    } catch (Exception e) {

    }
	}
}
