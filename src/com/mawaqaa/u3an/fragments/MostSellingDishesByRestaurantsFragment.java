package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MostSellingDishesChoiceAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class MostSellingDishesByRestaurantsFragment extends
		HeaderViewControlFragment {
	String restaurant_ID, restaurant_Name, restaurant_logo;
	ArrayList<HashMap<String, String>> arraylist_mostselling_dishes_data;
	JSONArray jsonarray;
	GridView dishGridView;
	TextView restaurant_NameTextView;
	ImageView logoImageView;

	String areaID, restaurant_Rating, header, minimum_Amount;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_most_selling_dishes, gp, false);
		setupUI(v, false);
		init(v);
		
		restaurant_ID = getArguments().getString("restaurant_id");
		restaurant_Name = getArguments().getString("restaurant_name");
		restaurant_logo = getArguments().getString("restaurant_logo");

		areaID = getArguments().getString("Area_ID");
		restaurant_Rating = getArguments().getString("restaurant_rating");
		header = getArguments().getString("Header");
		minimum_Amount = getArguments().getString("minimum_Amount");

		restaurant_NameTextView.setText(restaurant_Name);
		if (restaurant_logo.startsWith("http")) {
			Picasso.with(Activity).load(restaurant_logo).into(logoImageView);
		} else {
			logoImageView.setBackgroundResource(R.drawable.imagenotavailable);
		}

		setTitleForPage(getResources().getString(R.string.most_selling_restaurant));
		if (Utilities.isNetworkAvailable(Activity)) {
			AccumulateJSONOject();
		} else {
			Toast.makeText(Activity, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT)
					.show();
		}
		Log.d("Most Selling Dishes...", ""+restaurant_ID);
		return v;
	}

	private void init(View view) {
		dishGridView = (GridView) view
				.findViewById(R.id.gridView_special_offer);
		restaurant_NameTextView = (TextView) view
				.findViewById(R.id.restaurant_name_Textview);
		logoImageView = (ImageView) view.findViewById(R.id.restaurant_logo);
	}

	private void AccumulateJSONOject() {
		Activity.startSpinwheel(true, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectRestaurants = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectRestaurants = new JSONObject();
			jsonObjectRestaurants.accumulate("RestaurantId", restaurant_ID);
			if(sDefSystemLanguage.equals("en")){
				jsonObjectRestaurants.accumulate("locale", "en-US");
			}else{
				jsonObjectRestaurants.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.w("Most Selling Dishes....", ""+jsonObjectRestaurants);
		loadSubCategoryData(jsonObjectRestaurants);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.most_selling_dishes_data, jsonObject);
		}
	}

	@Override
	public void onMostSellingDishesDataLoadedSuccessfull(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		Activity.stopSpinWheel();
		super.onMostSellingDishesDataLoadedSuccessfull(jsonObject);
		parseData(jsonObject);
	}

	private void parseData(JSONObject jObj) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObj;
		
		// Create an array
		arraylist_mostselling_dishes_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL
		// address.....all_restaurants_id
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.most_selling_dishes_jobj);
			for (int i = 0; i < jsonarray.length(); i++) {
				if (i < jsonarray.length()) {
					Log.e("Entered here", "arraylist_mostselling_dishes_data");
					HashMap<String, String> map = new HashMap<String, String>();
					jsonObject = jsonarray.getJSONObject(i);
					// Retrieve JSON Objects
					map.put(AppConstants.most_selling_dishes_dishname,
							jsonObject
									.getString(AppConstants.most_selling_dishes_dishname));
					map.put(AppConstants.most_selling_dishes_description,
							jsonObject
									.getString(AppConstants.most_selling_dishes_description));
					map.put(AppConstants.most_selling_dishes_restaurantname,
							jsonObject
									.getString(AppConstants.most_selling_dishes_restaurantname));
					map.put(AppConstants.most_selling_dishes_rastaurant_status,
							jsonObject
									.getString(AppConstants.most_selling_dishes_rastaurant_status));
					map.put(AppConstants.most_selling_dishes_image, jsonObject
							.getString(AppConstants.most_selling_dishes_image));
					map.put(AppConstants.most_selling_dishes_id, jsonObject
							.getString(AppConstants.most_selling_dishes_id));
					map.put(AppConstants.most_selling_dishes_price, jsonObject
							.getString(AppConstants.most_selling_dishes_price));
					
					if(	jsonObject.getString(AppConstants.most_selling_dishes_rastaurant_status).equals("HIDDEN")){
						
					}else{
					// Set the JSON Objects into the array
					arraylist_mostselling_dishes_data.add(map);
					}
				} else {

				}
			}
			populateGridData();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("Got exception", ""+e.getMessage());
		}
	}

	private void populateGridData() {		
		MostSellingDishesChoiceAdapter adapter = new MostSellingDishesChoiceAdapter(
				Activity, arraylist_mostselling_dishes_data, restaurant_ID,
				restaurant_logo, restaurant_Name, areaID, restaurant_Rating,
				header, minimum_Amount);
		dishGridView.setAdapter(adapter);
	}
}
