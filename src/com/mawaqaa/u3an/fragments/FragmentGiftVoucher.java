package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.GiftVoucherAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class FragmentGiftVoucher extends HeaderViewControlFragment implements
		OnItemClickListener {
	GridView gift;
	ArrayList<HashMap<String, String>> arrayList;
	String Ammount, giftvoucherId;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.giftvoucher, gp, false);
		setupUI(v, false);
		initView(v);
		AccumulateJson();
		return v;
	}

	private void initView(View v) {
		setTitleForPage(getResources().getString(R.string.gift_voucher_heading));
		gift = (GridView) v.findViewById(R.id.gridView_gifts);
		gift.setOnItemClickListener(this);
	}

	private void AccumulateJson() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_GiftVoucher,
					jsonObject);
		}
	}

	@Override
	public void LoadGiftVoucherSuccesfully(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.LoadGiftVoucherSuccesfully(jsonObject);
		ParseData(jsonObject);
	}

	@Override
	public void LoadGiftVoucherFail(JSONObject jsonObject) {
		super.LoadGiftVoucherFail(jsonObject);
	}

	private void ParseData(JSONObject jsonObject) {
		try {
			arrayList = new ArrayList<HashMap<String, String>>();

			JSONArray jsonArray = jsonObject.getJSONArray("GiftVouchersList");

			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);

				map.put(AppConstants.GiftVoucherImage,
						jsonObject.getString(AppConstants.GiftVoucherImage));
				map.put(AppConstants.amount,
						jsonObject.getString(AppConstants.amount));
				map.put(AppConstants.GiftVoucherId,
						jsonObject.getString(AppConstants.GiftVoucherId));
				arrayList.add(map);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		poppulateGridView();
	}

	private void poppulateGridView() {
		GiftVoucherAdapter giftVoucherAdapter = new GiftVoucherAdapter(
				Activity, arrayList);
		gift.setAdapter(giftVoucherAdapter);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		HashMap<String, String> map = new HashMap<String, String>();
		map = arrayList.get(position);
		Fragment giftvoucherform = new GiftVoucherFormFrag();
		Bundle data = new Bundle();
		data.putString(AppConstants.amount, map.get(AppConstants.amount));
		data.putString(AppConstants.GiftVoucherId,
				map.get(AppConstants.GiftVoucherId));
		giftvoucherform.setArguments(data);
		Activity.pushFragments(giftvoucherform, false, true);
	}
}
