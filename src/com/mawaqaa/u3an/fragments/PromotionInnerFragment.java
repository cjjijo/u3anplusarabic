package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.PromotionInnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class PromotionInnerFragment extends HeaderViewControlFragment implements OnItemClickListener {
	GridView promotionitem;
	TextView desc;
	TextView empty,title;
	ArrayList<HashMap<String, String>> arrayList;
	JSONArray jsonArray;
	String resturentid, resturentname, restorentlogo, decsription;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {

		View v = info.inflate(R.layout.promotioninnerpage, gp, false);
		setupUI(v, false);
		initview(v);
		return v;
	}

	private void initview(View v) {
		promotionitem = (GridView) v.findViewById(R.id.gridView_promotionitem);
		desc = (TextView) v.findViewById(R.id.desc);
		empty = (TextView) v.findViewById(android.R.id.empty);
		title=(TextView) v.findViewById(R.id.title);
		setTitleForPage(getResources().getString(R.string.promotions_heading));
		Bundle data = getArguments();
		resturentid = data.getString(AppConstants.RestaurantId);
		Log.e("RestaurantId", "" + resturentid);
		resturentname = data.getString(AppConstants.RestaurantName);
		title.setText(getResources().getString(R.string.promotional_item_form)+" : "+resturentname);
		
		accumulateData();
		promotionitem.setOnItemClickListener(this);
	}

	private void accumulateData() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject=new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("restId", resturentid);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("jsonrequest", ""+jsonObject.toString());
		LoadData(jsonObject);
		
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.get_pagedetails_url);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.get_restuarent_promotions, jsonObject);
		}
	}

	@Override
	public void LoadPromotionsbyresturentSuccessfully(JSONObject jsonObject) {
		super.LoadPromotionsbyresturentSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("jsonResponse .........", "" + jsonObject.toString());
		ParseData(jsonObject);
	}

	@Override
	public void LoadPromotionsbyresturentfail(JSONObject jsonObject) {
		super.LoadPromotionsbyresturentfail(jsonObject);
	}

	private void ParseData(JSONObject jsonObject) {
		arrayList = new ArrayList<HashMap<String, String>>();
		try {
			decsription=jsonObject.getString(AppConstants.RestaurantDescription);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		try {
			jsonArray = jsonObject
					.getJSONArray(AppConstants.PromotionItemsDetails);

			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);

				map.put(AppConstants.Description,
						jsonObject.getString(AppConstants.Description));
				map.put(AppConstants.Name,
						jsonObject.getString(AppConstants.Name));
				map.put(AppConstants.Price,
						jsonObject.getString(AppConstants.Price));
				map.put(AppConstants.Rating,
						jsonObject.getString(AppConstants.Rating));
				map.put(AppConstants.Thumbnail,
						jsonObject.getString(AppConstants.Thumbnail));
				arrayList.add(map);
			}
			Log.e("arraylist............", "" + arrayList.toString());
			Log.e("decsription", ""+decsription);
			PoppulateGridview();

		} catch (JSONException e) {
			
			e.printStackTrace();
		}

	}

	private void PoppulateGridview() {
		desc.setText(decsription);
		PromotionInnerAdapter promotionInnerAdapter = new PromotionInnerAdapter(
				Activity, arrayList);
		promotionitem.setAdapter(promotionInnerAdapter);
		promotionitem.setEmptyView(empty);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Fragment restuarentdetails=new MostSellingRestaurantsDetails();
		Bundle data=new Bundle();
		data.putString(AppConstants.RestaurantId, resturentid);
		data.putString(AppConstants.RestaurantName, resturentname);
		data.putString(AppConstants.RestaurantLogo, restorentlogo);
		restuarentdetails.setArguments(data);
		PrefUtil.setFragHeading(getActivity(),getResources().getString(R.string.promotions_heading));
		Activity.pushFragments(restuarentdetails, false, true);
		
	}

}
