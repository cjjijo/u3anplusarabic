package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class FeedBackFragment extends HeaderViewControlFragment {
	RadioGroup radiogroupOne, radiogroupTwo, radiogroupThree;
	EditText userName, userEmail, userComments, errorEditText;
	Button submit;
	String designFeedback, OrderProcess, usabilityFeedback;
	
	ArrayList<HashMap<String, String>> AddressList;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	String uancredit, username;
	String firstNameString, lastNameString, phoneString, resi_phoneString,
	firstname, lastname, mobilePhone, resi_PhNo, gender, sms,
	newsletter, occupation, uancreadit;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_feedback, gp, false);
		setupUI(v, false);
		setTitleForPage(getString(R.string.feedback_title));
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		
		Log.e("username in landing page", ""+username);
		AccumulateDataforGetAddress();
		initView(v);
		return v;
	}

	private void initView(View v) {
		radiogroupOne = (RadioGroup) v.findViewById(R.id.radioGroup1);
		radiogroupTwo = (RadioGroup) v.findViewById(R.id.radioGroup2);
		radiogroupThree = (RadioGroup) v.findViewById(R.id.radioGroup3);
		userName = (EditText) v.findViewById(R.id.userName);
		
		userEmail = (EditText) v.findViewById(R.id.userEmail);
		userComments = (EditText) v.findViewById(R.id.userComments);
		submit = (Button) v.findViewById(R.id.submit);
		submit.setOnClickListener(submitListner);
		
		try{
			if (Utilities.doemailValidation(FILENAMEAUTHKEY)) {
				userName.setText(PrefUtil.getFirstname(Activity));
				userEmail.setText(FILENAMEAUTHKEY);
			}
			
			}catch(Exception e){
					
			}
	}

	OnClickListener submitListner = new OnClickListener() {
		@Override
		public void onClick(View v) {
			doValidation();
		}
	};

	private void doValidation() {

		boolean isvalid = doNameValidation();
		if (!isvalid)
			return;

		isvalid = doEmailValidation();
		if (!isvalid)
			return;

		isvalid = doCommentValidation();
		if (!isvalid)
			return;

		if (isvalid) {
			// Call web service...
			doUploaddata();
		}

		else {
			return;
		}
	}

	private void doUploaddata() {
		Activity.startSpinwheel(false, true);
		JSONObject jsonObject = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObject = new JSONObject();
			jsonObject.accumulate(AppConstants.user_name, userName.getText()
					.toString());
			jsonObject.accumulate(AppConstants.user_Email, userEmail.getText()
					.toString());
			jsonObject.accumulate(AppConstants.user_comments, userComments
					.getText().toString());
			jsonObject.accumulate(AppConstants.DesignFeedback,
					((RadioButton) Activity.findViewById(radiogroupOne
							.getCheckedRadioButtonId())).getText().toString());
			jsonObject.accumulate(AppConstants.OrderProcess,
					((RadioButton) Activity.findViewById(radiogroupThree
							.getCheckedRadioButtonId())).getText().toString());
			jsonObject.accumulate(AppConstants.UsabilityFeedback,
					((RadioButton) Activity.findViewById(radiogroupTwo
							.getCheckedRadioButtonId())).getText().toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("Params", "" + jsonObject.toString());
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.Feedback_frm,
					jsonObject);
		}
	}

	/*
	 * private boolean doFirstRatingValidation() { if (radiogroupOne != null) {
	 * int selectedId = radiogroupOne.getCheckedRadioButtonId(); if (selectedId
	 * != -1) { return true; } else { return false; }
	 * 
	 * } return false; }
	 * 
	 * private boolean doSecondRatingValidation() { if (radiogroupTwo != null) {
	 * int selectedId = radiogroupTwo.getCheckedRadioButtonId(); if (selectedId
	 * != -1) { return true; } else { return false; }
	 * 
	 * } return false; }
	 * 
	 * private boolean doThirdRatingValidation() { if (radiogroupThree != null)
	 * { int selectedId = radiogroupThree.getCheckedRadioButtonId(); if
	 * (selectedId != -1) { return true; } else { return false; }
	 * 
	 * } return false; }
	 */
// Jijo
	public Boolean doEmailValidation() {

		Boolean isValidEmail = false;
		if (userEmail != null) {
			String emailString = userEmail.getText().toString().trim();
			isValidEmail = Utilities.doemailValidation(emailString);
		}

		if (!isValidEmail) {
			userEmail
				.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_email)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				userEmail
				.setError(Html
						.fromHtml("<font color='white'>Enter a valid email id</font>"));
			}else{
				userEmail
				.setError(Html
						.fromHtml("<font color='white'>أدخل بريد إلكتروني صحيح</font>"));
			}*/
			
			errorEditText = userEmail;
			userEmail.requestFocus();
			userEmail.addTextChangedListener(registerInputTextWatcher);
			return false;
		} else {
			return true;
		}
	}
// Jijo
	public Boolean doNameValidation() {

		String firstNameString = null;
		if (userName != null) {
			firstNameString = userName.getText().toString().trim();
		}

		if (firstNameString != null && firstNameString.length() < 1) {
			userName.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_name)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				userName.setError(Html
						.fromHtml("<font color='white'>Enter a valid name</font>"));
			}else{
				userName.setError(Html
						.fromHtml("<font color='white'>أدخل اسم صحيح</font>"));
			}*/
			errorEditText = userName;
			userName.requestFocus();
			userName.addTextChangedListener(registerInputTextWatcher);
			return false;
		} else {
			return true;
		}
	}

	private boolean doCommentValidation() {
		String carRegString = null;
		if (userComments != null) {
			carRegString = userComments.getText().toString().trim();
		}

		if (carRegString != null && carRegString.length() < 1) {
			userComments
				.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.valid_comment)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				userComments
				.setError(Html
						.fromHtml("<font color='white'>Enter a valid comments</font>"));
			}else{
				userComments
				.setError(Html
						.fromHtml("<font color='white'>Enter a valid comments</font>"));
			}*/
			errorEditText = userComments;
			userComments.requestFocus();
			userComments.addTextChangedListener(registerInputTextWatcher);
			return false;
		} else {
			return true;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (errorEditText != null)
				errorEditText.setError(null);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	@Override
	public void FeedbackSumbitFail(JSONObject jsonObject) {
		super.FeedbackSumbitFail(jsonObject);
		Activity.stopSpinWheel();
	};
	
	private void AccumulateDataforGetAddress() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoadDataforGetAddress(jsonObject);

	}

	private void LoadDataforGetAddress(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL", ""+AppConstants.getdeleveryinformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.getdeleveryinformation,
					jsonObject);
		}

	}

	@Override
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		Activity.stopSpinWheel();
		super.getCustomerAddressSuccess(jsonObject);
		parse(jsonObject);
	}

	private void parse(JSONObject jsonObject) {

		AddressList = new ArrayList<HashMap<String, String>>();
		try {
			JSONArray jsonArray = jsonObject.getJSONArray("CustomerDetails");
			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);

				map.put(AppConstants.AreaId,
						jsonObject.getString(AppConstants.AreaId));
				map.put(AppConstants.Areaname,
						jsonObject.getString(AppConstants.Areaname));
				map.put(AppConstants.Block,
						jsonObject.getString(AppConstants.Block));
				map.put(AppConstants.Buildingno,
						jsonObject.getString(AppConstants.Buildingno));
				map.put(AppConstants.Extradirections,
						jsonObject.getString(AppConstants.Extradirections));
				map.put(AppConstants.floor,
						jsonObject.getString(AppConstants.floor));
				map.put(AppConstants.id, jsonObject.getString(AppConstants.id));
				map.put(AppConstants.LastName,
						jsonObject.getString(AppConstants.LastName));
				map.put(AppConstants.FirstName,
						jsonObject.getString(AppConstants.FirstName));
				map.put(AppConstants.getCustomermobile,
						jsonObject.getString(AppConstants.getCustomermobile));
				map.put(AppConstants.Res_Ph,
						jsonObject.getString(AppConstants.Res_Ph));
				map.put(AppConstants.Gender,
						jsonObject.getString(AppConstants.Gender));
				map.put(AppConstants.isprimary,
						jsonObject.getString(AppConstants.isprimary));
				map.put(AppConstants.judda,
						jsonObject.getString(AppConstants.judda));
				map.put(AppConstants.profileName,
						jsonObject.getString(AppConstants.profileName));
				map.put(AppConstants.street,
						jsonObject.getString(AppConstants.street));
				map.put(AppConstants.suite,
						jsonObject.getString(AppConstants.suite));
				map.put(AppConstants.subscribed_to_newsletter, jsonObject
						.getString(AppConstants.subscribed_to_newsletter));
				map.put(AppConstants.subscribed_to_sms,
						jsonObject.getString(AppConstants.subscribed_to_sms));
				map.put(AppConstants.occupation,
						jsonObject.getString(AppConstants.occupation));
				AddressList.add(map);
				Log.e("firstname", "" + map.get(AppConstants.FirstName));

				firstname = map.get(AppConstants.FirstName);
				lastname = map.get(AppConstants.LastName);
				mobilePhone = map.get(AppConstants.getCustomermobile);
				resi_PhNo = map.get(AppConstants.Res_Ph);
				gender = map.get(AppConstants.Gender);
				sms = map.get(AppConstants.subscribed_to_sms);
				newsletter = map.get(AppConstants.subscribed_to_newsletter);
				occupation = map.get(AppConstants.occupation);

			}
			Log.e("occupation..........................", "" + occupation);
			PrefUtil.setFirstname(getActivity(), firstname);
			PrefUtil.setLastname(Activity, lastname);
			PrefUtil.setPhonenum(getActivity(), mobilePhone);
			PrefUtil.setredsi_Phonenum(getActivity(), resi_PhNo);
			PrefUtil.setGender(getActivity(), gender);
			PrefUtil.setNewsletter(getActivity(), newsletter);
			PrefUtil.setsms(getActivity(), sms);
			PrefUtil.setOccupation(getActivity(), occupation);
			uancreadit = jsonObject.getString("U3ANCredit");
			PrefUtil.setU3ancredit(getActivity(), uancreadit);
			Log.e("USer Name", ""+firstname);

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		username = PrefUtil.getFirstname(getActivity());
		userName.setText(username);
		Log.e("username......in landing", "" + username);
		Log.e("uancreadit", "" + AddressList);
	}


	@Override
	public void FeedbackSumbitSuccess(JSONObject jsonObject) {
		super.FeedbackSumbitSuccess(jsonObject);
		Activity.stopSpinWheel();
		Log.e("Response", "" + jsonObject.toString());
		try {
			
			Toast.makeText(Activity, R.string.feedback_submitted, Toast.LENGTH_SHORT).show();
			if (jsonObject.getString("Status").equals("success"));
			Activity.popFragments();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}
