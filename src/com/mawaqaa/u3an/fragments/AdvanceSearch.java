package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.CountryAreaSpinnerAdapter;
import com.mawaqaa.u3an.adapters.CuisineSpinnerAdapter;
import com.mawaqaa.u3an.adapters.FilterSearchAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Country;
import com.mawaqaa.u3an.data.Cuisine;
import com.mawaqaa.u3an.data.SingleAreaCountry;

public class AdvanceSearch extends DialogFragment {

	private String TAG = "U3An::AdvanceSearch";

	private Spinner m_spinnerFilter;
	private Spinner m_areaSpinner;
	private Spinner m_cuisineSpinner;
	private MultiAutoCompleteTextView m_restaurantNameEdit;
	private Button m_goBtn;
	private Button m_cancelBtn;
	private String strAreaId;
	private String strCuisineId;
	private BaseActivity baseActivity;
	private JSONObject areaJsonObj, cuisineJsonPbj;
	private ArrayList<Cuisine> arraylist_cusines_data;
	private ArrayList<SingleAreaCountry> arraylist_country_data;
	private List<String> filterListData;

	private int areaSpinnerPos;
	private int cuisineSpinnerPos;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// setStyle(STYLE_NO_FRAME, 0);
		if (getDialog() == null)
			return;
	}

	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if (activity instanceof BaseActivity)
			baseActivity = (BaseActivity) activity;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
	}

	@Override
	public void onStart() {
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.dimAmount = 0.5f;
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}

	@SuppressWarnings("unchecked")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");
		View view = inflater.inflate(R.layout.advance_search_dlg, container,
				false);
		if (getActivity() instanceof BaseActivity)
			((BaseActivity) getActivity()).setupUI(view, false);
		m_spinnerFilter = (Spinner) view.findViewById(R.id.spinner1_filter);
		m_areaSpinner = (Spinner) view.findViewById(R.id.spinner2_area);
		m_cuisineSpinner = (Spinner) view.findViewById(R.id.spinner3_cuisine);
		m_restaurantNameEdit = (MultiAutoCompleteTextView) view
				.findViewById(R.id.restaurant_name_edit);
		m_goBtn = (Button) view.findViewById(R.id.go_btn);
		m_cancelBtn = (Button) view.findViewById(R.id.cancel_btn);

		m_goBtn.setOnClickListener(btnClicked);
		m_cancelBtn.setOnClickListener(btnClicked);
		Log.d(TAG, "" + baseActivity.getAllRestaurantnames());
	//Jijo
		String[] filterDataArr = getResources().getStringArray(
				R.array.restaurant_filter);
		filterListData = Arrays.asList(filterDataArr);
		m_spinnerFilter.setAdapter(new FilterSearchAdapter(baseActivity, filterListData));
	//	
		m_restaurantNameEdit.setAdapter(new ArrayAdapter(baseActivity,
				android.R.layout.simple_list_item_1, baseActivity
						.getAllRestaurantnames()));
		m_restaurantNameEdit
				.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
		getDialog().setCanceledOnTouchOutside(false);
		return view;
	}

	private JSONObject readDataFromFile(String fileName) {
		JSONObject jsonObject = null;
		try {
			FileInputStream fis = baseActivity.openFileInput(fileName);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			String data = br.readLine();
			br.close();
			in.close();
			fis.close();
			if (data != null)
				jsonObject = new JSONObject(data);
		} catch (Exception e) {
		}
		return jsonObject;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);
		Bundle bundle = getArguments();
		if (bundle != null) {
			strAreaId = bundle.getString(AppConstants.area_id);
			Log.d(TAG, "strAreaId : " + strAreaId);
			strCuisineId = bundle.getString(AppConstants.cuisine_id);
			cuisineSpinnerPos = bundle.getInt("CUSINE_SPINNER_POS");
			areaSpinnerPos = bundle.getInt("AREA_SPINNER_POS");

		} else {
			Log.d(TAG, "Bundle Null");
		}
		areaJsonObj = readDataFromFile(HomeFragment.FILENAMEAREADATA);
		cuisineJsonPbj = readDataFromFile(HomeFragment.FILENAMECUISINEDATA);
		ParseCuisineData(cuisineJsonPbj);
		getDialog().getWindow().getAttributes().windowAnimations = R.style.AppTheme;
	};

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
	}

	private OnClickListener btnClicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.go_btn:
				AccumulateJsonObject();
				dismiss();
				break;
			case R.id.cancel_btn:
				dismiss();
				break;
			default:
				break;
			}
		}
	};

	private void AccumulateJsonObject() {
		try {
			String selectedFilter = "";
			try {
				String[] filterIds = getResources().getStringArray(
						R.array.restaurant_filter_ids);
				selectedFilter = filterIds[m_spinnerFilter
						.getSelectedItemPosition()];
			} catch (Exception exception) {
				Log.e(TAG,
						"Excetion in getting filter text: "
								+ exception.getMessage());
			}
			int pos = m_cuisineSpinner.getSelectedItemPosition();
			Cuisine cuisine = arraylist_cusines_data.get(pos);
			strCuisineId = cuisine.strCuisineId;

			pos = m_areaSpinner.getSelectedItemPosition();
			SingleAreaCountry country = arraylist_country_data.get(pos);
			strAreaId = country.areaId;
			if (strAreaId == null || strAreaId.equals(null)
					|| strAreaId.trim().equals("") || country.isCityName)
				strAreaId = "";
			
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();

			JSONObject jsonObject = new JSONObject();
			jsonObject.put(AppConstants.area_id, strAreaId);
			jsonObject.put(AppConstants.filter_by, selectedFilter);
			jsonObject.put(AppConstants.cuisine_id, strCuisineId);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.put(AppConstants.locale, "en-US");
			}else{
				jsonObject.put(AppConstants.locale, "ar-KW");
			}
			jsonObject.put(AppConstants.restuarent_name, m_restaurantNameEdit
					.getText().toString().trim());
			baseActivity.advanceSearchRestarants(jsonObject);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void ParseCuisineData(JSONObject jsonObject1) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jsonObject1;
		// Create an array
		arraylist_cusines_data = new ArrayList<Cuisine>();
		
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.get_cusines_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.get_cusines_json_obj);
				for (int i = 0; i < jsonarray.length(); i++) {

					if (!jsonarray.isNull(i)) {
						Cuisine cuisine = new Cuisine(
								jsonarray.optJSONObject(i));
						//Anju
						if(cuisine.strCuisineId.equals("")||cuisine.strCuisineName.equals("")){
							Log.w("cuisine.strCuisineName", "=="+cuisine.strCuisineName);
						}else{
							arraylist_cusines_data.add(cuisine);
						}
					}
				}
			}
			PopulateCusinesSpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("The error", ""+e.getMessage());
		}
	}

	private void PopulateCusinesSpinner() {
		if (arraylist_cusines_data != null) {
			Collections.sort(arraylist_cusines_data, new Comparator<Cuisine>() {
				@Override
				public int compare(Cuisine c1, Cuisine c2) {
					return c1.strCuisineName.compareTo(c2.strCuisineName);
				}
			});
		} else
			return;
		m_cuisineSpinner.setAdapter(new CuisineSpinnerAdapter(baseActivity,
				arraylist_cusines_data));
		m_cuisineSpinner.setSelection(cuisineSpinnerPos);
		ParseCountryData(areaJsonObj);
	}

	private void ParseCountryData(JSONObject jsonObject) {
		// Create an array
		arraylist_country_data = new ArrayList<SingleAreaCountry>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.areas_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.areas_json_obj);
				for (int i = 0; i < jsonarray.length(); i++) {
					if (!jsonarray.isNull(i)) {

						jsonObject = jsonarray.getJSONObject(i);
						Country country = new Country(jsonObject);
						addAreas2SpinnerArrayList(country);
						// Set the JSON Objects into the array arraylist_country_data.add(country);
					}
				}
			}
			PopulateCountrySpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		}
	}

	private void addAreas2SpinnerArrayList(Country country) {
		SingleAreaCountry areaCountry = new SingleAreaCountry();
		// for showing city Name in Spinner
		areaCountry.areaName = "-- " + country.cityName + " --";
		areaCountry.isCityName = true;
		arraylist_country_data.add(areaCountry);
		// adding all areas in the city
		arraylist_country_data.addAll(country.areas);
	}

	private void PopulateCountrySpinner() {
		m_areaSpinner.setAdapter(new CountryAreaSpinnerAdapter(baseActivity,
				arraylist_country_data));
		m_areaSpinner.setSelection(areaSpinnerPos);
	}
}
