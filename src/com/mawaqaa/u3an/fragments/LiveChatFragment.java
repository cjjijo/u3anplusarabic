package com.mawaqaa.u3an.fragments;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.mawaqaa.u3an.R;

public class LiveChatFragment extends HeaderViewControlFragment {

	private FrameLayout livechatlay;
	private WebView LiveChatwebview;
	String html, data;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_faq, gp, false);
		setupUI(v, false);
		initView(v);
		return v;
	}

	private void initView(View v) {
		livechatlay = (FrameLayout) v.findViewById(R.id.faq_web_container);		
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		setTitleForPage(getResources().getString(R.string.live_chat));
		showLiveChat();
	}

	private void showLiveChat() {
	
		LiveChatwebview = new WebView(Activity);
		LiveChatwebview.getSettings().setJavaScriptEnabled(true);
		LiveChatwebview.setInitialScale(0);// default,no zoom
		LiveChatwebview.getSettings().setSupportZoom(true);
		LiveChatwebview.getSettings().setBuiltInZoomControls(false);
		LiveChatwebview.getSettings().setLoadWithOverviewMode(false);
		LiveChatwebview.getSettings().setUseWideViewPort(false);
		LiveChatwebview.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
		/*Activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);*/
	//	webview WebView webview = new WebView(this, null, R.style.StyleWindowSoftInputModeAdjustResize);
		if (Build.VERSION.SDK_INT >= 11)
			LiveChatwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		this.LiveChatwebview.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				// TODO Auto-generated method stub
				Activity.startSpinwheel(false, false);
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				Activity.stopSpinWheel();
				LiveChatwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
				if (Build.VERSION.SDK_INT >= 11)
					LiveChatwebview.setLayerType(View.LAYER_TYPE_SOFTWARE,
							null);
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {

				Log.e("The url", ""+url);

				Log.e("Url.......", "" + url);
				view.loadUrl(url);
				return true;
			}
		});
		this.LiveChatwebview.setWebChromeClient(new WebChromeClient() {
			@Override
			public void onCloseWindow(WebView w) {
				super.onCloseWindow(w);
				Activity.popFragments();
			}
		});

		livechatlay.addView(LiveChatwebview);
		LiveChatwebview.loadUrl("http://www.u3an.com/livesupport/UserPreChat.aspx?ref=http%3a%2f%2fwww.u3an.com%2fDefault.aspx&d=&u=&bypass=");

		
		//LiveChatwebview.loadUrl("http://prototypes.jquerymaps.com/indianapolisintlairport/003/");
		
		// LiveChatwebview.loadUrl("http://www.u3an.com/livesupport/UserChat.aspx?userid=0&deptid=1");
		/*
		 * LiveChatwebview.setVerticalScrollBarEnabled(true);
		 * LiveChatwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
		 * LiveChatwebview.getSettings().setJavaScriptEnabled(true);
		 * LiveChatwebview.getSettings().setSupportZoom(false);
		 * LiveChatwebview.getSettings().setDefaultZoom(ZoomDensity.FAR);
		 * LiveChatwebview.getSettings().setBuiltInZoomControls(true);
		 * LiveChatwebview
		 * .setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
		 * //LiveChatwebview.getSettings().setUseWideViewPort(true);
		 * LiveChatwebview.getSettings().setSupportZoom(false);
		 * LiveChatwebview.getSettings().setBuiltInZoomControls(false);
		 * LiveChatwebview.getSettings().setDefaultZoom(ZoomDensity.FAR); if
		 * (Build.VERSION.SDK_INT >= 11)
		 * LiveChatwebview.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
		 * this.LiveChatwebview.setWebViewClient(new WebViewClient() {
		 * 
		 * @Override public void onPageFinished(WebView view, String url) {
		 * LiveChatwebview.setBackgroundColor(Color.parseColor("#FFFFFF")); if
		 * (Build.VERSION.SDK_INT >= 11)
		 * LiveChatwebview.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null); }
		 * }); int pad = com.mawaqaa.u3an.utilities.Utilities.convertDip2Pixels(
		 * getActivity(), 15); //LiveChatwebview.setPadding(pad, pad, pad, pad);
		 * livechatlay.addView(LiveChatwebview);
		 * 
		 * LiveChatwebview.loadDataWithBaseURL(
		 * "file:///android_asset/fonts/BREESERIF-REGULAR.OTF",
		 * com.mawaqaa.u3an.utilities.Utilities.getHtmlDatawithFont(
		 * getActivity(), data), "text/html", "utf-8", "about:blank");
		 * 
		 * LiveChatwebview.loadUrl("http://goo.gl/sbrCU8");
		 * 
		 * LiveChatwebview.loadUrl(
		 * "http://www.u3an.com/livesupport/UserPreChat.aspx?ref=http%3a%2f%2fwww.u3an.com%2fDefault.aspx&d=&u=&bypass="
		 * );
		 */

	}
}
