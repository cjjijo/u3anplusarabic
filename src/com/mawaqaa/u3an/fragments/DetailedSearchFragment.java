package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.DetailedResturantsAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Restaurant;
import com.mawaqaa.u3an.dialogfragment.DialogReadyU3AnVideo;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class DetailedSearchFragment extends HeaderViewControlFragment {

	private GridView gridView;
	private String strAreaId;
	private String strCuisineId;
	private ArrayList<Restaurant> restaurants;
	private DetailedResturantsAdapter detailedResturantsAdapter;
	private int cuisinePos, areaPos;
	private TextView emptyView;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		TAB_TAG = DetailedSearchFragment.class.getName();
		View rootView = info.inflate(R.layout.fragment_allrestaurants, gp,
				false);
		setupUI(rootView, false);
		initView(rootView);
		PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.detailed_search_heading));
		setTitleForPage(getResources().getString(R.string.detailed_search_heading));
		showFilterText(true);
		m_txtFilter.setOnClickListener(filterSelected);
		AccumulateJsonObject();
		gridView.setOnItemClickListener(gridItemSelected);
		
		showVideoDialog();
		
		return rootView;
	}

	private OnClickListener filterSelected = new OnClickListener() {
		@Override
		public void onClick(View v) {
			// showDialogue
			AdvanceSearch advanceSearch = new AdvanceSearch();
			Bundle bundle = new Bundle();
			bundle.putString(AppConstants.area_id, strAreaId);
			bundle.putString(AppConstants.cuisine_id, strCuisineId);
			bundle.putInt("AREA_SPINNER_POS", areaPos);
			bundle.putInt("CUSINE_SPINNER_POS", cuisinePos);
			advanceSearch.setArguments(bundle);
			Activity.showDialogueFragment(advanceSearch);
		}
	};
	
//******************************************************************************************************	
	OnItemClickListener gridItemSelected = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			String status = restaurants.get(position).restaurantStatus;
			Log.e("fdfdfdf", ""+status);
			if(PrefUtil.getAppLanguage(Activity).equals("en")){
				switch (status) {
				case AppConstants.OPEN:
					//getResources().getString(R.string.open)
					Fragment restaurantMenuFragment = new RestaurantMenuFragment();
									
					Bundle params1 = new Bundle();
					params1.putString("Area_ID", strAreaId);
					params1.putString("Header", AppConstants.ALL_RESTAURANT_TAG);
					params1.putString("restaurant_id", Integer.toString(restaurants
							.get(position).restaurantId));
					params1.putString("restaurant_logo",
							restaurants.get(position).restaurantLogoUrl);
					params1.putString("restaurant_name",
							restaurants.get(position).restaurantName);
					params1.putString("restaurant_rating",
							restaurants.get(position).rating);
					restaurantMenuFragment.setArguments(params1);
					Activity.pushFragments(restaurantMenuFragment, false, true);
					break;
				default:
					break;
				}
			}else{
				//Log.i("Anjuuuuuu", status+"");
				if(status.equalsIgnoreCase(getResources().getString(R.string.open))){
					Fragment restaurantMenuFragment = new RestaurantMenuFragment();
					
					Bundle params1 = new Bundle();
					params1.putString("Area_ID", strAreaId);
					params1.putString("Header", AppConstants.ALL_RESTAURANT_TAG);
					params1.putString("restaurant_id", Integer.toString(restaurants
							.get(position).restaurantId));
					params1.putString("restaurant_logo",
							restaurants.get(position).restaurantLogoUrl);
					params1.putString("restaurant_name",
							restaurants.get(position).restaurantName);
					params1.putString("restaurant_rating",
							restaurants.get(position).rating);
					restaurantMenuFragment.setArguments(params1);
					Activity.pushFragments(restaurantMenuFragment, false, true);
					
				}
			}
			
		}
	};

	private void initView(View view) {
		gridView = (GridView) view.findViewById(R.id.gridView_allresturants);
		emptyView = (TextView) view.findViewById(R.id.empty);
	}

	
	private void showVideoDialog() {
		try {
			DialogReadyU3AnVideo qtyDialog = new DialogReadyU3AnVideo();
			qtyDialog.show(getActivity().getSupportFragmentManager(), "DialogReadyU3AnVideo");
		} catch (NullPointerException ne) {
			// Log.e(TAG, "Null Pointer Exception in Sharing");
			ne.printStackTrace();
		}
	}
	
	@Override
	public void advanceSearchRestarants(JSONObject jsonObject) {
		super.advanceSearchRestarants(jsonObject);
		if (restaurants == null)
			restaurants = new ArrayList<>();
		else
			restaurants.clear();
		if (detailedResturantsAdapter != null)
			detailedResturantsAdapter.notifyDataSetChanged();
		emptyView.setVisibility(View.GONE);
		showProgressbar();
		Log.d(TAB_TAG, "Advance Search Obj: " + jsonObject);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.get_restaurant_advance_search_url, jsonObject);
		}
	}

	// Get Advance Search Restaurant
	@Override
	public void onAdvanceSeachRestaurantFailed(JSONObject jsonObject) {
		super.onAdvanceSeachRestaurantFailed(jsonObject);
		Log.d(TAB_TAG, "onAdvanceSeachRestaurantFailed: " + jsonObject);
		Toast.makeText(Activity, R.string.reastaurant_loading_failed,
				Toast.LENGTH_SHORT).show();
		hideProgressbar();
	}

	@Override
	public void onAdvanceSeachRestaurantSuccessFully(JSONObject jsonObject) {
		super.onAdvanceSeachRestaurantSuccessFully(jsonObject);
		Log.d(TAB_TAG, "onAdvanceSeachRestaurantSuccessFully: " + jsonObject);
		hideProgressbar();
		parseJson(jsonObject);
	}

	private void AccumulateJsonObject() {
		showProgressbar();
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		try {
			JSONObject jsonObject = new JSONObject();
			// Retrieve JSON Objects
			Bundle bundle = getArguments();
			if (bundle != null) {
				strAreaId = bundle.getString(AppConstants.area_id);
				cuisinePos = bundle.getInt("CUSINE_SPINNER_POS");
				areaPos = bundle.getInt("AREA_SPINNER_POS");
				jsonObject.put(AppConstants.area_id, strAreaId);

				strCuisineId = bundle.getString(AppConstants.cuisine_id);
				jsonObject.put(AppConstants.cuisine_id, strCuisineId);
				if(sDefSystemLanguage.equals("en")){
					jsonObject.put(AppConstants.locale, "en-US");
				}else{
					jsonObject.put(AppConstants.locale, "ar-KW");
				}
				jsonObject.put(AppConstants.restuarent_name, "");
				loadRestarantInDetail(jsonObject);
			}else{
				Log.w("bundle==", ""+bundle);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void loadRestarantInDetail(JSONObject jsonObject) {
		Log.i("Details   ....."+TAB_TAG,""+ jsonObject);
		emptyView.setVisibility(View.GONE);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_restuarant_by_area,
					jsonObject);
		}
	}

	@Override
	public void onGotResponseByArea(JSONObject jsonObject) {
		Log.d(TAB_TAG, "onGotResponseByArea");
		super.onGotResponseByArea(jsonObject);
		Log.w(TAB_TAG, ""+jsonObject);
		parseJson(jsonObject);
		hideProgressbar();
	}

	@Override
	public void onGettingResponseByAreaFailed(JSONObject jsonObject) {
		Log.d(TAB_TAG, "onGettingResponseByAreaFailed");
		super.onGettingResponseByAreaFailed(jsonObject);
		emptyView.setVisibility(View.VISIBLE);
		hideProgressbar();
	}

	private void parseJson(JSONObject jsonObject) {
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(Restaurant.RESTAURANT_DETAILS)) {				
				
				if (restaurants == null)
					restaurants = new ArrayList<>();
				else
					restaurants.clear();
				JSONArray jsonArray = jsonObject
						.getJSONArray(Restaurant.RESTAURANT_DETAILS);
				if (jsonArray != null && jsonArray.length() > 0) {
					for (int i = 0; i < jsonArray.length(); i++) {
						
						JSONObject jOBj = jsonArray
								.getJSONObject(i);
						String statusString  = jOBj.getString("RestaurantStatus");
						
						if (statusString.equals("HIDDEN")) {
							
						}else{
						restaurants.add(new Restaurant(jsonArray
								.getJSONObject(i)));						
						}						
					}
				}
				Activity.insertOrUpdateRestaurants(restaurants);
				setRestaurantAdapter();
				
				
			}
		} catch (JSONException e) {
			Log.e(TAB_TAG, "Exception In Restaurant parsing :" + e.getMessage());
			e.printStackTrace();
		}
	}

	private void setRestaurantAdapter() {
		Log.d(TAB_TAG, "setRestaurantAdapter");
		if (restaurants != null) {
			Log.d(TAB_TAG, "restaurants size  : " + restaurants.size());
			detailedResturantsAdapter = new DetailedResturantsAdapter(Activity,
					restaurants);
			gridView.setAdapter(detailedResturantsAdapter);
			gridView.setEmptyView(emptyView);
			emptyView.setVisibility(View.VISIBLE);
		} else
			Log.e(TAB_TAG, "no restaurant to show : ");
	}

	private void hideProgressbar() {
		Activity.stopSpinWheel();
	}

	private void showProgressbar() {
		Activity.startSpinwheel(false, true);
	}

}
