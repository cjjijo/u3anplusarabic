package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class ChangePasswordFragment extends HeaderViewControlFragment implements
		OnClickListener, OnFocusChangeListener {
	
	EditText oldPasswordEditText, newPasswordEditText, retypePasswordEditText;
	Button changeButton, cancelButton;
	String oldPasswordString, newPasswordString, retypePasswordString;
	EditText errorEditText;
	String oldpassword, newpassword, confirmpasswod, authkey;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	LogoutListener listener;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_changepassword, gp,
				false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.my_account_heading));
		init(rootView);
		
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		return rootView;
	}

	private void init(View view) {
		oldPasswordEditText = (EditText) view
				.findViewById(R.id.Edittext_my_Account_oldpassword);
		//oldPasswordEditText.setOnFocusChangeListener(this);
		newPasswordEditText = (EditText) view
				.findViewById(R.id.Edittext_my_Account_newpassword);
		//newPasswordEditText.setOnFocusChangeListener(this);
		retypePasswordEditText = (EditText) view
				.findViewById(R.id.Edittext_my_Account_retype_password);
		//retypePasswordEditText.setOnFocusChangeListener(this);
		changeButton = (Button) view.findViewById(R.id.chang_password);
		changeButton.setOnClickListener(this);
		cancelButton = (Button) view.findViewById(R.id.chang_password_cancel);
		cancelButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.chang_password:
			doDataValidation();
			break;
		case R.id.chang_password_cancel:
			Activity.onBackPressed();
			break;
		default:
			break;
		}
	}

	private void doDataValidation() {
		boolean isvalid = oldPasswordValidation();
		if (!isvalid)
			return;
		isvalid = doPasswordValidation();

		if (!isvalid) {
			return;
		}/*
		 * else {
		 * 
		 * }
		 */
	 isvalid = doOldnNewPasswordValidation();
		if (!isvalid) {
			return;
		}
		isvalid = doRePasswordValidation();

		if (!isvalid) {
			return;
		}
		if (isvalid) {
			DoSubmit();
		}
	}

	private boolean doRePasswordValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		if(retypePasswordEditText.getText().toString().equals(newPasswordEditText.getText().toString())){
			return true;
		}else{
			retypePasswordEditText
			.setError(Html
				.fromHtml("<font color='white'>"+getResources().getString(R.string.password_mismatch)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				retypePasswordEditText
					.setError(Html
						.fromHtml("<font color='white'>New password mismatch</font>"));
			}else{
				retypePasswordEditText
					.setError(Html
							.fromHtml("<font color='white'>كلمة السر غير مطابقة</font>"));
			}*/
			errorEditText = retypePasswordEditText;
			retypePasswordEditText
			.addTextChangedListener(registerInputTextWatcher);			
			return false;
		}
	}

	private boolean doOldnNewPasswordValidation() {
		if(!newPasswordEditText.getText().toString().equals(oldPasswordEditText.getText().toString())){
			return true;
		}else{
			newPasswordEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.nPassword_diff_oPassword)+"</font>"));
			errorEditText = newPasswordEditText;
			newPasswordEditText
			.addTextChangedListener(registerInputTextWatcher);			
			return false;
		}		
	}

	private Boolean doPasswordValidation() {
		String newPasword = newPasswordEditText.getText().toString();
		if (newPasword.length() >= 7) {
			return true;
		}
		 else {
			newPasswordEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.valid_password)+"</font>"));
			errorEditText = newPasswordEditText;
			newPasswordEditText
					.addTextChangedListener(registerInputTextWatcher);
			
			return false;
		}
	}

	private Boolean oldPasswordValidation() {
		String oldPasword = oldPasswordEditText.getText().toString();
		if (oldPasword.length() >= 7) {

			return true;
		} else {
			oldPasswordEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.invalid_old_password)+"</font>"));
			errorEditText = oldPasswordEditText;
			oldPasswordEditText
					.addTextChangedListener(registerInputTextWatcher);
			return false;
		}
	}

//	private Boolean newPasswordValidation() {
//		Log.e("The data", "msg");
//		String newPString = newPasswordEditText.getText().toString();
//		if (newPString.length() >= 7) {
//
//			return true;
//		} else {
//			newPasswordEditText
//					.setError(Html
//							.fromHtml("<font color='white'>Old password not valid</font>"));
//			errorEditText = newPasswordEditText;
//			newPasswordEditText
//					.addTextChangedListener(registerInputTextWatcher);
//			return false;
//		}
//	}

	private Boolean Repasswordvalidation() {
		Log.e("The data", "msg");
		retypePasswordString = retypePasswordEditText.getText().toString();
		if (retypePasswordString.length() >= 7) {

			return true;
		} else {
			retypePasswordEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.invalid_old_password)+"</font>"));
			errorEditText = retypePasswordEditText;
			retypePasswordEditText
					.addTextChangedListener(registerInputTextWatcher);
			return false;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private class ChangePasswordTextwatcher implements TextWatcher {
		private EditText editText;
		
		ChangePasswordTextwatcher(EditText editText) {
			this.editText = editText;
		}		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editText != null)
				editText.setError(null);
		}
		@Override
		public void afterTextChanged(Editable s) {
		}
	}

	private void DoSubmit() {
		oldPasswordString = oldPasswordEditText.getText().toString();
		newPasswordString = newPasswordEditText.getText().toString();
		retypePasswordString = retypePasswordEditText.getText().toString();
		AccumulateData();
	}

	private void AccumulateData() {
		authkey = PrefUtil.getAuthkey(getActivity());
		Log.e("authkey", "" + authkey);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("authKey", authkey);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("newPassword", newPasswordString);
			jsonObject.accumulate("oldPassword", oldPasswordString);
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.chnge_password,
					jsonObject);
		}
	}

	
	@Override
	public void changePasswordSuccessfully(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.changePasswordSuccessfully(jsonObject);
		parseData(jsonObject);
	
	}

	private void parseData(JSONObject jsonObject){
		String result = jsonObject.toString();
		if (result.contains("success")) {
			Toast.makeText(Activity, R.string.password_change, Toast.LENGTH_LONG).show();
			Activity.onBackPressed();
		}else{
			Toast.makeText(Activity, R.string.password_check, Toast.LENGTH_LONG).show();
			oldPasswordEditText.setText("");
			newPasswordEditText.setText("");
			retypePasswordEditText.setText("");
		}
	}
	
	@Override
	public void changePasswordfail(JSONObject jsonObject) {
		super.changePasswordfail(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		if (!hasFocus) {
			switch (v.getId()) {
			case R.id.Edittext_my_Account_oldpassword:
				oldPasswordValidation();
				break;
			case R.id.Edittext_my_Account_newpassword:
				oldPasswordValidation();
				break;
			case R.id.Edittext_my_Account_retype_password:
				doRePasswordValidation();
				break;
			default:
				break;
			}
		}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
