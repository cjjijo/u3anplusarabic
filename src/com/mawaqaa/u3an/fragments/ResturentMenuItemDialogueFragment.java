package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.MenuItemDetail.MenuItemChoiceDetails;
import com.mawaqaa.u3an.data.SelectedChoiceItems;
import com.mawaqaa.u3an.interfaces.OnItemCheckedCompleted;
import com.mawaqaa.u3an.utilities.PrefUtil;

public class ResturentMenuItemDialogueFragment extends DialogFragment {
	private ArrayList<MenuItemChoiceDetails> details;
	BaseActivity Activity;
	OnItemCheckedCompleted listenerComment;
	int canSelect;
	ArrayList<SelectedChoiceItems> selectItems;
	boolean shouldSelect;
	ToggleableRadioButton cb;
	Button done,cancel;

	public ResturentMenuItemDialogueFragment(BaseActivity baseActivity, OnItemCheckedCompleted listenerComment) {
		this.Activity = baseActivity;
		this.listenerComment = listenerComment;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onStart() {
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		Display display = Activity.getWindowManager().getDefaultDisplay();
		int width = display.getWidth() * 3 / 4;
		params.width = width;
		params.dimAmount = 0.5f; // dim only a little bit
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog mydiDialog = new Dialog(getActivity());
		mydiDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mydiDialog.setContentView(R.layout.menuitemdialogue);
		if (getActivity() instanceof BaseActivity)
			((BaseActivity) getActivity()).setupUI(
					mydiDialog.findViewById(R.id.dialogue_lyt), false);
		LinearLayout contnt = (LinearLayout) mydiDialog.findViewById(R.id.contentView);
		
		done = (Button) mydiDialog.findViewById(R.id.done);
		cancel = (Button) mydiDialog.findViewById(R.id.cancel);
		selectItems = new ArrayList<SelectedChoiceItems>(details.size());

		LayoutInflater vi = (LayoutInflater) Activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		for (int i = 0; i < details.size(); i++) {
			SelectedChoiceItems selItem = new SelectedChoiceItems();
			selectItems.add(selItem);
			selectItems.get(i).minItem = Integer.parseInt(details.get(i).minItem);
			selectItems.get(i).maxItem = Integer.parseInt(details.get(i).maxItem);
			
			
			Log.e("The minItem",  Integer.parseInt(details.get(i).minItem)+" ");
			Log.w("The minItem",  Integer.parseInt(details.get(i).minItem)+" ");
			Log.e("The maxitem",  Integer.parseInt(details.get(i).maxItem)+" ");
            
			if (Integer.parseInt(details.get(i).maxItem) == 1) {		
			View subViews = vi.inflate(R.layout.dynamic_checkbox_view, null);
			RadioGroup ll = (RadioGroup) subViews.findViewById(R.id.addView);
			for (int j = 0; j < details.get(i).MenuItemChoicearray.size(); j++) {
				selectItems.get(i).addredItems.put(j, "null");
				cb = new ToggleableRadioButton(Activity);
				cb.setId(j);				
// Jijo				
				if(details.get(i).MenuItemChoicearray.get(j).price.equals("0")){
					if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_EN)){
						cb.setText(details.get(i).MenuItemChoicearray.get(j).name);
					}else{
						/*cb.setPadding(0, 0, 20, 0);
						cb.setRotationY(180);
						cb.setTextDirection(cb.TEXT_DIRECTION_ANY_RTL);
						cb.setText(details.get(i).MenuItemChoicearray.get(j).name);*/
						cb.setButtonDrawable(new StateListDrawable());
						cb.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.abc_btn_radio_material, 0);
						if(Float.parseFloat((details.get(i).MenuItemChoicearray.get(j).price))!=0.0f){
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
						}else{
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name);
						}
						//cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
						
					}
		
					
				}else{
					if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_EN)){
						cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
					}else{

						cb.setButtonDrawable(new StateListDrawable());
						cb.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.abc_btn_radio_material, 0);
						Log.d("RestaurentMenuItemDialog..............", ""+Float.parseFloat(details.get(i).MenuItemChoicearray.get(j).price));
						if(Float.parseFloat(details.get(i).MenuItemChoicearray.get(j).price)!=0.0f){
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
						}else{
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name);
						}
						//cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
						/*cb.setPadding(0, 0, 20, 0);
						cb.setRotationY(180);
						cb.setTextDirection(cb.TEXT_DIRECTION_ANY_RTL);
						cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");*/
					}
							
				}
				ll.addView(cb);
				final int pos = i;
				final int posForj = j;
			
				cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						if (isChecked) {
							Log.e("True", "True");
							selectItems.get(pos).addredItems.put(posForj,
									details.get(pos).MenuItemChoicearray.get(posForj).ItemTd);
							Log.e("added.........." + pos,
									"......." + details.get(pos).MenuItemChoicearray.get(posForj).ItemTd);
						} else {
							Log.e("false", "false");
							selectItems.get(pos).addredItems.put(posForj, "null");
						}
					}
				});
			}
			contnt.addView(subViews);			
			}				
			else{
				View subViews = vi.inflate(R.layout.dynamic_checkbox_view, null);
				LinearLayout ll = (LinearLayout) subViews.findViewById(R.id.layout_container_radio_btn);
				for (int j = 0; j < details.get(i).MenuItemChoicearray.size(); j++) {
					selectItems.get(i).addredItems.put(j, "null");
					cb = new ToggleableRadioButton(Activity);
					cb.setId(j);				
					Log.e("The data.....", "data"+details.get(i).MenuItemChoicearray.get(j).price+details.get(i).MenuItemChoicearray.get(j).name);
										
					if(details.get(i).MenuItemChoicearray.get(j).price.equals("0")){
						if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_EN)){
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name);
						}else{
							cb.setButtonDrawable(new StateListDrawable());
							cb.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.abc_btn_radio_material, 0);
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name);
							/*cb.setPadding(0, 0, 20, 0);
							cb.setRotationY(180);
							cb.setTextDirection(subViews.TEXT_DIRECTION_RTL);
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name);*/
						}
												
					}else{	
						if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_EN)){
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
						}else{
							cb.setButtonDrawable(new StateListDrawable());
							cb.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.abc_btn_radio_material, 0);
							Log.d("RestaurentMenuItemDialog..............", ""+Float.parseFloat(details.get(i).MenuItemChoicearray.get(j).price));
							if(Float.parseFloat(details.get(i).MenuItemChoicearray.get(j).price)!=0.0f){
								cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");
							}else{
								cb.setText(details.get(i).MenuItemChoicearray.get(j).name);
							}
							/*cb.setPadding(0, 0, 20, 0);
							cb.setRotationY(180);
							cb.setTextDirection(subViews.TEXT_DIRECTION_RTL);
							cb.setText(details.get(i).MenuItemChoicearray.get(j).name+"("+details.get(i).MenuItemChoicearray.get(j).price+")");*/
						}
				
					}
					ll.addView(cb);
					final int pos = i;
					final int posForj = j;
				
					cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
							if (isChecked) {
								Log.e("True", "True");
								selectItems.get(pos).addredItems.put(posForj,
										details.get(pos).MenuItemChoicearray.get(posForj).ItemTd);
								Log.e("added.........." + pos,
										"......." + details.get(pos).MenuItemChoicearray.get(posForj).ItemTd);
							} else {
								Log.e("false", "false");
								selectItems.get(pos).addredItems.put(posForj, "null");
							}
						}
					});
				}
				contnt.addView(subViews);				
			}			
		}
		
		done.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (validate(selectItems)) {
					listenerComment.ItemCheckComple(selectItems,true);
					dismiss();
				} else {
				}
			}
		});
//		
		cancel.setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				dismiss();				
			}
		});
		
		mydiDialog.setCanceledOnTouchOutside(false);
		return mydiDialog;
	}

	private boolean validate(ArrayList<SelectedChoiceItems> selectItems2) {
		for (int i = 0; i < selectItems2.size(); i++) {
			ArrayList<String> data = new ArrayList<String>();
			for (int j = 0; j < selectItems2.get(i).addredItems.size(); j++) {
				if (!selectItems2.get(i).addredItems.get(j).equals("null")) {
					data.add(selectItems2.get(i).addredItems.get(j));
				}
			}
			if (selectItems2.get(i).minItem <= data.size()) {
				Log.e("row.........." + i, "......." + data.size());
				Log.e("rowmin Item.." + i, "..." + selectItems2.get(i).minItem);
				return true;
			} else {
				/*if(Integer.parseInt(details.get(i).minItem)){
					
				}*/
// Anju
				if(selectItems2.get(i).minItem == 1){
					Toast.makeText(Activity, R.string.atleast_one_item, Toast.LENGTH_LONG)
					.show();
				}else if (selectItems2.get(i).minItem == 2) {
					Toast.makeText(Activity, R.string.atleast_two_item, Toast.LENGTH_LONG)
					.show();
				}else{
					Toast.makeText(Activity, R.string.more_item, Toast.LENGTH_LONG)
					.show();
				}
				
				return false;
			}

		}
		return false;
	}

	public void setAdObj(ArrayList<MenuItemChoiceDetails> menuItemChoiceDetails) {
		this.details = menuItemChoiceDetails;
	}
	
	public class ToggleableRadioButton extends RadioButton {
	    public ToggleableRadioButton(Context context) {
			super(context);
			// TODO Auto-generated constructor stub
		}
		// Implement necessary constructors..... 
	    @Override
	    public void toggle() {
	        if(isChecked()) {	        
	        	setChecked(false);	         
	        } else {
	            setChecked(true);
	        }
	    }
	}
}
