package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MyOrdersthirdPageAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.interfaces.OnRatingbarChanged;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MyOrdersFragmentChildView2 extends HeaderViewControlFragment {
	int parentPos;
	int childPos;
	JSONObject obj;
	ListView orderListview;
	private TextView name, time;
	private ArrayList<HashMap<String, String>> arraylist;
	private MyOrdersthirdPageAdapter adapter;
	RatingBar rateBar;
	OnRatingbarChanged rateBarChecked;
	private String restaurantId;
	int raTing=0;
	LogoutListener listener;
	public MyOrdersFragmentChildView2(int pos, int position) {
		this.parentPos = pos;
		this.childPos = position;
	}

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.my_oreders_thirdpage, gp, false);
		setupUI(v, false);
		setTitleForPage(getString(R.string.my_accnt));
		initView(v);
		rateBarChecked = new OnRatingbarChanged() {

			@Override
			public void ItemRatingComple(String type, String id, String count) {
				doRateBarChangedUpdate(type, id, count);
			}
		};
		rateBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
				if(raTing==ratingBar.getRating()){
					Log.e("Same Value...........", ""+raTing+"................"+ratingBar.getRating());
				}else{
					doRateBarChangedUpdate("Restaurant",restaurantId,String.valueOf(rating));
					Log.e("different Value.....", ""+raTing+"................"+ratingBar.getRating());
				}
			}
		});
		fetData();
		return v;
	}

	private void initView(View v) {
		orderListview = (ListView) v.findViewById(R.id.orderListView);
		name = (TextView) v.findViewById(R.id.name);
		time = (TextView) v.findViewById(R.id.time);
		rateBar = (RatingBar) v.findViewById(R.id.ratingBar1);
		rateBar.setOnRatingBarChangeListener(null);
	}

	private void doRateBarChangedUpdate(String type, String id, String count) {

		Activity.startSpinwheel(false, true);
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.accumulate(AppConstants.ItemOrRestaurantId, id);
			jsonObject.accumulate("UserId", Activity.readFile());
			jsonObject.accumulate(AppConstants.Rating, count);
			jsonObject.accumulate(AppConstants.flag, type);
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.postRating, jsonObject);
			}
			Log.e("rating params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onItemorRestaurantratingLoadFail(JSONObject jsonObject) {
		super.onItemorRestaurantratingLoadFail(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity, R.string.failed, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onItemorRestaurantratingSuccessfully(JSONObject jsonObject) {
		super.onItemorRestaurantratingSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("rating response", "" + jsonObject.toString());
		try {
			if (jsonObject.getString("Status").equals("Success")) {
				fetData();
			} else {
				Toast.makeText(Activity, R.string.failed, Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMyOrdersLoadedSuccessfully(JSONObject jsonObject) {
		super.onMyOrdersLoadedSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("My order response", "" + jsonObject.toString());
		fetchmyOrders(jsonObject);
	}

	@Override
	public void onMyOrdersLoadFail(JSONObject jsonObject) {
		super.onMyOrdersLoadFail(jsonObject);
		Activity.stopSpinWheel();
	}

	private void fetData() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		try {
			JSONObject jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", Activity.readFile());
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.myOrdersUrl, jsonObject);
			}
			Log.e("My order params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void fetchmyOrders(JSONObject object) {

		try {
			arraylist = new ArrayList<HashMap<String, String>>();
			if (object.getString("Status").equals("Success")) {
				JSONArray array = object.getJSONArray("TransactionList");
				if (array.length() > parentPos) {
					JSONObject objectData = array.getJSONObject(parentPos);
					JSONArray newarray = objectData.getJSONArray("Orders");
					if (newarray.length() > childPos) {
						JSONObject childObj = newarray.getJSONObject(childPos);
						if (childObj != null) {

							if (!childObj.isNull(AppConstants.Restaurant)) {

								name.setText(childObj.getString(AppConstants.Restaurant));
								restaurantId=childObj.getString(AppConstants.all_restaurants_id);
							} else {

							}
							// if
							// (!objectData.isNull(AppConstants.delivery_time_json_obj))
							// {
							time.setText(objectData.getString(AppConstants.pDate) + "\n"
									+ (childObj.getString(AppConstants.delivery_time_json_obj)));
							// } else {
							raTing=Integer.parseInt(childObj.getString(AppConstants.Rating));
							rateBar.setRating(raTing);
							// }
							JSONArray oderArray = childObj.getJSONArray("Orders");
							for (int i = 0; i < oderArray.length(); i++) {
								HashMap<String, String> map = new HashMap<String, String>();
								JSONObject obj = oderArray.getJSONObject(i);
								map.put(AppConstants.get_cart_info_ItemId,
										obj.getString(AppConstants.get_cart_info_ItemId));
								map.put(AppConstants.get_cart_info_ItemName,
										obj.getString(AppConstants.get_cart_info_ItemName));
								map.put(AppConstants.ItemQuantity, obj.getString(AppConstants.ItemQuantity));
								map.put(AppConstants.Rating, obj.getString(AppConstants.Rating));
								map.put(AppConstants.SpecialRequest, obj.getString(AppConstants.SpecialRequest));
								map.put(AppConstants.ORDERED_ITEM_PRICE, obj.getString(AppConstants.ORDERED_ITEM_PRICE));
								arraylist.add(map);
							}
						}
						adapter = new MyOrdersthirdPageAdapter(Activity, arraylist, childObj, rateBarChecked);
						orderListview.setAdapter(adapter);
					}
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
