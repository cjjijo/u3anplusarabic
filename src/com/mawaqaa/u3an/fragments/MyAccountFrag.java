package com.mawaqaa.u3an.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.interfaces.LogoutListener;

public class MyAccountFrag extends HeaderViewControlFragment implements OnClickListener{
	Button changepassword,save,saveaddres,addAdress;
	RadioGroup gender;
	LogoutListener listener;
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		// TODO Auto-generated method stub
		View v=info.inflate(R.layout.myaccount, gp, false);
		setupUI(v, false);
		InitView(v);
		return v;
	}

	private void InitView(View v) {
		save=(Button) v.findViewById(R.id.save);
		changepassword=(Button) v.findViewById(R.id.changepassword);
		saveaddres=(Button) v.findViewById(R.id.saveadd);
		addAdress=(Button) v.findViewById(R.id.addadress);
		gender=(RadioGroup) v.findViewById(R.id.radiogroup);
		save.setOnClickListener(this);
		changepassword.setOnClickListener(this);
		saveaddres.setOnClickListener(this);
		addAdress.setOnClickListener(this);		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.save:
			Toast.makeText(Activity, R.string.click_save_button, Toast.LENGTH_LONG).show();
			break;
		case R.id.changepassword:
			Toast.makeText(Activity, R.string.click_change_password_button, Toast.LENGTH_LONG).show();
			break;
		case R.id.saveadd:
			Toast.makeText(Activity, R.string.click_address_button, Toast.LENGTH_LONG).show();
            break;
		case R.id.addadress:
			Toast.makeText(Activity, R.string.click_add_address, Toast.LENGTH_LONG).show();
		default:
			break;
		}	
	}
}
