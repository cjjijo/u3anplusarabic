package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class UanCreditFragment extends HeaderViewControlFragment implements
		OnClickListener {

	public static String crhtmlData;

	Spinner creditvalue;
	ImageButton submit;
	CheckBox newsletter, iaccept;
	boolean newsletterstatus;
	String auth_Kwy;
	String creditamount, url;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	ArrayList<String> creditvalues = new ArrayList<String>();
	SharedPreferences sp;

	Boolean accept_terms = false;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.uancreaditfragment, gp, false);
		setupUI(v, false);
		initView(v);
		PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.uan));
		setTitleForPage(getResources().getString(R.string.uan));

		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		if (Utilities.isNetworkAvailable(getActivity())) {
			Accumulate();
		} else {
		}

		return v;
	}

	private void Accumulate() {
		Activity.startSpinwheel(false, true);
		auth_Kwy = PrefUtil.getAuthkey(getActivity());
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsobject = new JSONObject();
		try {
			jsobject.accumulate("authKey", auth_Kwy);
			if(sDefSystemLanguage.equals("en")){
				jsobject.accumulate("locale", "en-US");
			}else{
				jsobject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LoadIntervl(jsobject);
	}

	private void LoadIntervl(JSONObject jsobject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.uancreditintervals,
					jsobject);
		}
	}

	@Override
	public void LoadcreditintealsuanSuccessfully(JSONObject jsObject) {
		super.LoadcreditintealsuanSuccessfully(jsObject);
		Log.e("intervals........", "" + jsObject.toString());
		parseIntervals(jsObject);
	}

	private void parseIntervals(JSONObject jsObject) {
		Activity.stopSpinWheel();
		try {
			JSONArray jsaArray = jsObject.getJSONArray(AppConstants.Intervals);
			for (int i = 0; i < jsaArray.length(); i++) {
				jsObject = jsaArray.getJSONObject(i);
				String interval = jsObject.getString(AppConstants.Intervals);
				creditvalues.add(interval);
			}
			Log.e("creditvalues", "" + creditvalues);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		poppulateList();
	}

	private void poppulateList() {
		SpinnerAdapter spinnerAdapter = new SpinnerAdapter(Activity,
				creditvalues);
		creditvalue.setAdapter(spinnerAdapter);
	}

	@Override
	public void Loadcreditintealuanfail(JSONObject jsonObject) {
		super.Loadcreditintealuanfail(jsonObject);
	}

	private void initView(View v) {
		creditvalue = (Spinner) v.findViewById(R.id.creditvaluespinner);
		submit = (ImageButton) v.findViewById(R.id.submit_Btn);
		newsletter = (CheckBox) v.findViewById(R.id.newsletter);
		alignCheckBox(newsletter);
		iaccept = (CheckBox) v.findViewById(R.id.iaccept);
		alignCheckBox2(iaccept);
		submit.setOnClickListener(this);
		new getCrFromTheservice(true).execute();

		iaccept.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                if(crhtmlData!=null){
				if (iaccept.isChecked()) {
					accept_terms = true;
					MyDialogFragmentu3ancredit crBanner = MyDialogFragmentu3ancredit
							.newInstance();
					crBanner.show(Activity.getFragmentManager(), "crBanner");
				} else {
					accept_terms = false;
				}
                }
			}
		});
	}

	private void alignCheckBox(CheckBox newsletter2) {
		final float scale = this.getResources().getDisplayMetrics().density;
		newsletter2.setPadding(newsletter.getPaddingLeft()
				+ (int) (10.0f * scale + 0.5f), newsletter.getPaddingTop(),
				newsletter2.getPaddingRight(), newsletter.getPaddingBottom());
	}

	private void alignCheckBox2(CheckBox newsletter2) {
		final float scale = this.getResources().getDisplayMetrics().density;
		newsletter2.setPadding(iaccept.getPaddingLeft()
				+ (int) (10.0f * scale + 0.5f), iaccept.getPaddingTop(),
				newsletter2.getPaddingRight(), iaccept.getPaddingBottom());
	}

	@Override
	public void onClick(View v) {
		if (PrefUtil.isUserSignedIn(Activity)) {
			if (accept_terms) {
				validateData();
			} else
				Toast.makeText(Activity, getResources().getString(R.string.agree_term_condition),
						Toast.LENGTH_SHORT).show();

		} else {
			Fragment fr = new LoginFragment();
			Toast.makeText(Activity, R.string.please_login, Toast.LENGTH_SHORT).show();
			Activity.pushFragments(fr, false, false);
		}
	}

	private void validateData() {
		Boolean isValid = creaditAmmount();
		if (!isValid) {
			Log.e("U3AnCreditFrag", "Invalid...");
			return;
		} else {
			if (Utilities.isNetworkAvailable(getActivity())) {
				dosubmition();
			} else {
				Toast.makeText(Activity,  getResources().getString(R.string.no_network),
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	private Boolean creaditAmmount() {
		creditamount = creditvalue.getSelectedItem().toString();
		if (creditamount == "5") {
			Toast.makeText(Activity, R.string.select_amount, Toast.LENGTH_SHORT)
					.show();
			return false;
		} else {
			return true;
		}
	}

	private void dosubmition() {
		newsletterstatus = newsletter.isChecked();
		auth_Kwy = PrefUtil.getAuthkey(getActivity());
		Log.e("auth_Kwy", "" + auth_Kwy);
		AcculateJSONoBJECT();
	}

	private void AcculateJSONoBJECT() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("amount", creditamount);
			jsonObject.accumulate("authKey", auth_Kwy);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("params........", "" + jsonObject.toString());
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.uancredit, jsonObject);
		}
	}

	@Override
	public void LoadcrediuanSuccessfully(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.LoadcrediuanSuccessfully(jsonObject);
		parseData(jsonObject);
		Log.e("jsonobject", "" + jsonObject.toString());
	}

	private void parseData(JSONObject jsonObject) {
		try {
			url = jsonObject.getString("Url");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		PoppulteData();
	}

	private void PoppulteData() {
		Fragment kentfragment = new KnetForGiftvoucher();
		Bundle data = new Bundle();
		data.putString("Url", url);
		kentfragment.setArguments(data);
		Activity.pushFragments(kentfragment, false, true);
	}

	@Override
	public void Loadcrediuanfail(JSONObject jsonObject) {
		super.Loadcrediuanfail(jsonObject);
	}

	// AsyncTask for loading data....
	public class getCrFromTheservice extends AsyncTask<Void, String, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;
		InputStream istream;

		public getCrFromTheservice(Boolean progressVisibilty) {
			this.progressVisibiltyflag = progressVisibilty;
		}

		@Override
		protected void onPreExecute() {
			// showProgressbar();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				objJson.accumulate("PageName", "terms_credit");
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}

				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						AppConstants.get_pagedetails_url);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();

					jObj = new JSONObject(result);

				} else {
					result = null;
				}

			} catch (JSONException e) {
				Log.e("Data", "JSONException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				Log.e("Data",
						"UnsupportedEncodingException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				Log.e("Data", "ClientProtocolException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				Log.e("Data", "IOException Error:" + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// hideProgressbar();

			try {
				if (jObj.getString("Status").equalsIgnoreCase("Success")) {
					if (jObj.getString("Data").equals("")) {
						crhtmlData = "<center>"
								+ getResources().getString(R.string.nodatahtml)
								+ "</center>";
					} else {
						crhtmlData = jObj.getString("Data");
					}
					// setWebView();
				} else {
					crhtmlData = "<h3>"+getResources().getString(R.string.nodata)+"</h3>";
					/*if(PrefUtil.getAppLanguage(getActivity())=="en"){
						crhtmlData = "<h3>Sorry... No Data Available</h3>";
					}else{
						crhtmlData = "<h3>لا يوجد نتائج</h3>";
					}*/
					// setWebView();
				}

			} catch (JSONException e) {
				e.printStackTrace();
			}

		}
	}

}
