package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.db.OpenHelper;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.squareup.picasso.Picasso;

public class GuestCartFragment extends HeaderViewControlFragment {
	LinearLayout containerLayout;
	TextView grantTotalTextView;
	OpenHelper db_Obj;
	ArrayList<HashMap<String, String>> cartData;
	ArrayList SortedRestaurantIDs = new ArrayList();
	ArrayList<HashMap<String, String>> singleRestaurantData = new ArrayList<HashMap<String, String>>();
	int actualDataLength, processedDataLength;
	String restaurant_ID, restaurant_Name, restaurant_Thumbnail;
	String minimum_Amount_String;

	float restaurantTotal;
	float grandTotal;

	String first_ID;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_foodcart, gp, false);
		setupUI(v, false);
		init(v);
		// setHeader();
		db_Obj = new OpenHelper(Activity);
		cartData = db_Obj.cart_Data();
		Log.e("The cart data", ""+cartData.toString());
		setTitleForPage(PrefUtil.getFragHeading(getActivity()));
		actualDataLength = cartData.size();
		first_ID = cartData.get(0).get("restaurant_ID");
		ParseDataAndFixUI();
		return v;
	}

	public void ParseDataAndFixUI() {
		String temp_ID, prevID = "0";
		for (int i = 0; i < cartData.size(); i++) {
			HashMap<String, String> map = cartData.get(i);
			HashMap<String, String> cartItem = new HashMap<String, String>();
			temp_ID = map.get("restaurant_ID");
			if (first_ID.equals(temp_ID)) {

				restaurant_ID = map.get("restaurant_ID");
				restaurant_Name = map.get("restaurant_name");
				restaurant_Thumbnail = map.get("restaurant_thumbnail");

				cartItem.put("ItemId", map.get("ItemId"));
				cartItem.put("dishname", map.get("dishname"));
				cartItem.put("Quantity", map.get("Quantity"));
				cartItem.put("price", map.get("price"));
				singleRestaurantData.add(cartItem);
			} else if (cartData.size() == 1) {
				restaurant_ID = map.get("restaurant_ID");
				restaurant_Name = map.get("restaurant_name");
				restaurant_Thumbnail = map.get("restaurant_thumbnail");

				cartItem.put("ItemId", map.get("ItemId"));
				cartItem.put("dishname", map.get("dishname"));
				cartItem.put("Quantity", map.get("Quantity"));
				cartItem.put("price", map.get("price"));
				singleRestaurantData.add(cartItem);

				AddView();
			} else {
				AddView();
				first_ID = temp_ID;
			}

			/*
			 * if (temp_ID.equals(prevID)) { Log.e("Entered here", "lfkg");
			 * 
			 * }else{ Log.e("Entered here", "else"); restaurant_ID =
			 * map.get("restaurant_ID"); restaurant_Name =
			 * map.get("restaurant_name"); restaurant_Thumbnail =
			 * map.get("restaurant_thumbnail"); minimum_Amount_String =
			 * map.get("minimumAmount");
			 * 
			 * prevID = temp_ID; AddView(); }
			 */

		}
	}

	private void AddView() {
		LinearLayout sub_item_Container;
		LayoutInflater layoutInflater = (LayoutInflater) Activity.getBaseContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		final View addView = layoutInflater.inflate(R.layout.cart_row, null);
		ImageView imageView = (ImageView) addView.findViewById(R.id.cart_image);
		TextView min_amnt_textView = (TextView) addView.findViewById(R.id.minimum_order_amnt);
		sub_item_Container = (LinearLayout) addView.findViewById(R.id.list_dishdata);
		Picasso.with(Activity).load(restaurant_Thumbnail).into(imageView);
		containerLayout.addView(addView);
		min_amnt_textView.setText("minimum Order Amount: " + minimum_Amount_String);
		for (int i = 0; i < singleRestaurantData.size(); i++) {
			LayoutInflater layoutInflater1 = (LayoutInflater) Activity.getBaseContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			final View subItemView = layoutInflater1.inflate(R.layout.cart_list_singleitem, null);
			ImageView itemImageView = (ImageView) subItemView.findViewById(R.id.item_id);
			TextView itemNameTextView = (TextView) subItemView.findViewById(R.id.item_name);
			itemNameTextView.setSelected(true);
			TextView priceTextView = (TextView) subItemView.findViewById(R.id.textview_price);
			final ImageButton closeButton = (ImageButton) subItemView.findViewById(R.id.close_image_Button);
			HashMap<String, String> singleData = singleRestaurantData.get(i);
			String itemName = singleData.get("dishname");
			String price = singleData.get("price");
			closeButton.setId(Integer.parseInt(singleData.get("ItemId")));
			String quantity = singleData.get("Quantity");
			int x = Integer.parseInt(quantity);

			try {
				float Total = Float.parseFloat(price) * x;
				restaurantTotal = restaurantTotal + Total;
			} catch (Exception e) {
				Log.e("Exception While Parsing integer", restaurantTotal + "");
			}

			itemNameTextView.setText(itemName);
			priceTextView.setText(price);
			sub_item_Container.addView(subItemView);

			closeButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// accumulateJSONObject(closeButton.getId());
					Toast.makeText(Activity, R.string.get_the_data, Toast.LENGTH_SHORT)
							.show();
				}
			});
		}
		LayoutInflater layoutInflater2 = (LayoutInflater) Activity.getBaseContext().getSystemService(
				Context.LAYOUT_INFLATER_SERVICE);
		final View addView1 = layoutInflater2.inflate(R.layout.total_price_row, null);
		TextView restaurant_price = (TextView) addView1.findViewById(R.id.textview_price_restaurant_cart);
		restaurant_price.setText(restaurantTotal + "");
		// float minAmnt = Float.parseFloat(minimumAmountString);

		grandTotal = grandTotal + restaurantTotal;
		restaurantTotal = 0;
		sub_item_Container.addView(addView1);
		singleRestaurantData = new ArrayList<HashMap<String, String>>();
	}

	public void init(View view) {
		containerLayout = (LinearLayout) view.findViewById(R.id.restdata_main);
		grantTotalTextView = (TextView) view.findViewById(R.id.grand_total_Textview);
	}

	@Override
	public void onResume() {		
		super.onResume();
	}
}
