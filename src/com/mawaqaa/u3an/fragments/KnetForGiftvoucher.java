package com.mawaqaa.u3an.fragments;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.utilities.PrefUtil;

public class KnetForGiftvoucher extends HeaderViewControlFragment {
	private FrameLayout faqlay;
	private WebView Faqwebview;
	String url;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		// TODO Auto-generated method stub
		View v = info.inflate(R.layout.fragment_faq, gp, false);
		setupUI(v, false);
		Bundle data = getArguments();
		url = data.getString("Url");
		Log.e("url in KnetForGiftvoucher", "" + url);
		initView(v);
		return v;

	}

	private void initView(View v) {
		setTitleForPage(PrefUtil.getFragHeading(getActivity()));
		faqlay = (FrameLayout) v.findViewById(R.id.faq_web_container);
		showdata();
	}

	private void showdata() {
		// TODO Auto-generated method stub

		Faqwebview = new WebView(Activity);
		Faqwebview.setVerticalScrollBarEnabled(false);
		Faqwebview.getSettings().setJavaScriptEnabled(true);
		Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));

		if (Build.VERSION.SDK_INT >= 11)
			Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		this.Faqwebview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
				if (Build.VERSION.SDK_INT >= 11)
					Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				// parenrLyt.setBackgroundDrawable(getResources().getDrawable(R.drawable.webview_bg));
			}
		});
		int pad = com.mawaqaa.u3an.utilities.Utilities.convertDip2Pixels(
				getActivity(), 20);
		Faqwebview.setPadding(pad, pad, pad, pad);
		faqlay.addView(Faqwebview);
		Faqwebview.loadUrl(url);

	}

}
