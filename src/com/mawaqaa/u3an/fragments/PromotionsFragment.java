package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.PromotionAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.DataHandlingUtilities;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class PromotionsFragment extends HeaderViewControlFragment implements
		OnItemClickListener {
	GridView promotionGridView;
	ArrayList<HashMap<String, String>> arraylist;
	JSONArray jsonArray;
	TextView emtyView;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.promotionfragment, gp, false);

		setupUI(rootView, false);
		initView(rootView);
		return rootView;
	}

	private void initView(View v) {
		promotionGridView = (GridView) v.findViewById(R.id.gridView_promotions);
		emtyView = (TextView) v.findViewById(android.R.id.empty);
		setTitleForPage(getResources().getString(R.string.promotions_heading));
		AccumulateData();
		promotionGridView.setOnItemClickListener(this);
	}

	private void AccumulateData() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("countryId", "1");
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL", ""+AppConstants.get_pagedetails_url);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.all_restuarent_promotions, jsonObject);
		}
	}

	@Override
	public void LoadPromotionsSuccessfully(JSONObject jsonObject) {
		super.LoadPromotionsSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("jsonResponse .........", "" + jsonObject.toString());
		ParseData(jsonObject);
	}

	private void ParseData(JSONObject jsonObject) {
		arraylist = new ArrayList<HashMap<String, String>>();

		try {
			jsonArray = jsonObject
					.getJSONArray(AppConstants.RestaurantDetailsWithItem);
			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);
				map.put(AppConstants.RestaurantId,
						jsonObject.getString(AppConstants.RestaurantId));
				map.put(AppConstants.RestaurantLogo,
						jsonObject.getString(AppConstants.RestaurantLogo));
				map.put(AppConstants.RestaurantName,
						jsonObject.getString(AppConstants.RestaurantName));
				map.put(AppConstants.status,
						jsonObject.getString(AppConstants.status));

				arraylist.add(map);
			}
			PoppulateGridview();
			Log.e("arraylist............", "" + arraylist.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void onResume() {
		super.onResume();
		DataHandlingUtilities listenerHideView = (DataHandlingUtilities) Activity;
		listenerHideView.hideToolBar();
		listenerHideView.allResturantsBottomBar();
	}

	private void PoppulateGridview() {

		PromotionAdapter promotionAdapter = new PromotionAdapter(Activity,
				arraylist);
		promotionGridView.setAdapter(promotionAdapter);
		promotionGridView.setEmptyView(emtyView);
	}

	@Override
	public void LoadPromotionsFaill(JSONObject jsonObject) {
		super.LoadPromotionsFaill(jsonObject);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		HashMap<String, String> map = new HashMap<String, String>();
		map = arraylist.get(position);
		String statusString = map.get(AppConstants.status);
		if(PrefUtil.getAppLanguage(Activity).equals("en")){
			switch (statusString) {
			case AppConstants.OPEN:
				Fragment fragment = new PromotionInnerFragment();
				Bundle params1 = new Bundle();

				params1.putString(AppConstants.RestaurantId,
						map.get(AppConstants.RestaurantId));
				params1.putString(AppConstants.RestaurantName,
						map.get(AppConstants.RestaurantName));
				params1.putString(AppConstants.all_restaurants_resturant_logo,
						map.get(AppConstants.all_restaurants_resturant_logo));
				Log.e("all_restaurants_resturant_logo",
						"" + map.get(AppConstants.all_restaurants_resturant_logo));
				params1.putString(AppConstants.Description,
						map.get(AppConstants.Description));
				Log.e("Description", "" + map.get(AppConstants.Description));
				fragment.setArguments(params1);
				Activity.pushFragments(fragment, false, true);
				break;

			default:
				break;
			}
		}else{
			if(statusString.equalsIgnoreCase(getResources().getString(R.string.open))){
				Fragment fragment = new PromotionInnerFragment();
				Bundle params1 = new Bundle();

				params1.putString(AppConstants.RestaurantId,
						map.get(AppConstants.RestaurantId));
				params1.putString(AppConstants.RestaurantName,
						map.get(AppConstants.RestaurantName));
				params1.putString(AppConstants.all_restaurants_resturant_logo,
						map.get(AppConstants.all_restaurants_resturant_logo));
				Log.e("all_restaurants_resturant_logo",
						"" + map.get(AppConstants.all_restaurants_resturant_logo));
				params1.putString(AppConstants.Description,
						map.get(AppConstants.Description));
				Log.e("Description", "" + map.get(AppConstants.Description));
				fragment.setArguments(params1);
				Activity.pushFragments(fragment, false, true);
			}
		}
		

	}
}
