package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class PersonalInformationFragment extends HeaderViewControlFragment
		implements OnClickListener {
	RadioButton maleRadioButton, femaleRadioButton;
	EditText referredEditText;
	private RadioGroup radioGroup;
	private RadioButton radioButton;
	Button submitButton;
	Spinner occupationEditText;
	CheckBox checkbox_newsletter, checkBox_SMS;
	Boolean newsletterstatus, SMSstatus;
	String genderString, occupationString;
	ArrayList<String> occupationArrayList = new ArrayList<String>();
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	String firstname, lastname, mobileNo, homehno, gender,newsletter,sms,occupation;
	LogoutListener listener;
	
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_personalinfo, gp, false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.my_account_heading));
		init(rootView);

		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		populateArraylist();
		SpinnerAdapter spinnerAdapter = new SpinnerAdapter(Activity,
				occupationArrayList);
		occupationEditText.setAdapter(spinnerAdapter);
		setOccupatation();

		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		occupationArrayList = new ArrayList<String>();
	}

	private void setOccupatation() {
		//Anju
		occupation = PrefUtil.getOccupation(getActivity());
		Log.e("occupation..........................",""+ occupation);
		if (occupation.equalsIgnoreCase("Select"))
		{
			occupationEditText.setSelection(0);
		}
		if (occupation.equalsIgnoreCase("Professional Services"))
		{
			occupationEditText.setSelection(0);
			Log.e("Entered Here"," msg");
			
		}
		else if (occupation.equalsIgnoreCase("Self employed/Owner"))
		{
			
			
			occupationEditText.setSelection(1);
		}
		else if (occupation.equalsIgnoreCase("Others"))
		{
			occupationEditText.setSelection(2);
		}
	}

	private void populateArraylist() {
		// TODO Auto-generated method stub
		occupationArrayList.add(getResources().getString(R.string.professional_service));
		occupationArrayList.add(getResources().getString(R.string.self_employed));
		occupationArrayList.add(getResources().getString(R.string.others));
	}

	private void init(View view) {
		radioGroup = (RadioGroup) view.findViewById(R.id.radiogroup);

		occupationEditText = (Spinner) view
				.findViewById(R.id.Edittext_register3_Occupation);
		checkbox_newsletter = (CheckBox) view.findViewById(R.id.promos);
		checkBox_SMS = (CheckBox) view.findViewById(R.id.keep_me_logged);
		alignCheckBox(checkBox_SMS);
		alignCheckBox(checkbox_newsletter);

		maleRadioButton = (RadioButton) view.findViewById(R.id.radioMale);
		femaleRadioButton = (RadioButton) view.findViewById(R.id.radioFemale);
		submitButton = (Button) view.findViewById(R.id.saveButton);
		submitButton.setOnClickListener(this);
		gender = PrefUtil.getgender(Activity);
		Log.e("gender", "" + gender);
		if (gender.equalsIgnoreCase("Female")) {
			Log.e("Gender in if....", "" + gender);
			femaleRadioButton.setChecked(true);
			;
		} else {
			maleRadioButton.setChecked(true);
		}
		
newsletter=PrefUtil.getnewsletter(getActivity());
Log.e("newsletter", "" + newsletter);
if (newsletter.equalsIgnoreCase("true")) 
	checkbox_newsletter.setChecked(true);
else
	checkbox_newsletter.setChecked(false);
	
sms=PrefUtil.getsms(getActivity());
if (sms.equalsIgnoreCase("true")) 
	checkBox_SMS.setChecked(true);
else
	checkBox_SMS.setChecked(false);
occupation=PrefUtil.getOccupation(getActivity());


	}

	private void alignCheckBox(CheckBox checkBox) {
		final float scale = this.getResources().getDisplayMetrics().density;
		checkBox.setPadding(checkBox.getPaddingLeft()
				+ (int) (10.0f * scale + 0.5f), checkBox.getPaddingTop(),
				checkBox.getPaddingRight(), checkBox.getPaddingBottom());

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.saveButton:
			validateData(v);
			break;

		default:
			break;
		}

	}

	private void validateData(View v) {
		Boolean isValid = isGenderValid(v);
		if (!isValid)
			return;

		isValid = isOccupationValid();
		if (!isValid)
			return;
		if (isValid)
			SubmitAction();
	}

	private Boolean isOccupationValid() {
		//Anju
		if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_AR)){
			if(occupationEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.professional_service))){
				occupationString = "Professional Services";
			}else if (occupationEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.self_employed))) {
				occupationString = "Self employed/Owner";
			}else if (occupationEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.others))) {
				occupationString = "Others";
			}
			
		}else{
			occupationString = occupationEditText.getSelectedItem().toString();
		}
		if (occupationString == "Select") {
			Toast.makeText(Activity, R.string.select_occupation, Toast.LENGTH_SHORT)
					.show();
			return false;
		} else {
			return true;
		}
	}

	private Boolean isGenderValid(View v) {
		/* gender=PrefUtil.getgender(getActivity()); */
		if (maleRadioButton.isChecked()) {
			genderString = "Male";
		} else if (femaleRadioButton.isChecked()) {
			genderString = "Female";
		} else {
			Log.e("not", "selected");

		}

		if (genderString.length() > 0) {
			return true;
		} else {
			return false;
		}
	}

	private void SubmitAction() {

		newsletterstatus = checkbox_newsletter.isChecked();
		SMSstatus = checkBox_SMS.isChecked();
		AccumulateData();
	}

	private void AccumulateData() {
		String authkey = PrefUtil.getAuthkey(getActivity());
		
		  String firstNameString=PrefUtil.getFirstname(getActivity()); String
		  resi_phoneString=PrefUtil.getreside_phonenum(getActivity()); String
		 astNameString=PrefUtil.getLastname(getActivity()); String
		 phoneString=PrefUtil.getphonenum(getActivity());
		 String gender=PrefUtil.getgender(getActivity());
		 Log.e("gender.....", ""+genderString);
		
//		Bundle data = getArguments();
//		firstname = data.getString("firstName");
//		lastname = data.getString("lastName");
//		homehno = data.getString("homePhNo");
//		mobileNo = data.getString("mobileNo");
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("authKey", authkey);
			jsonObject.accumulate("birthday", "");
			jsonObject.accumulate("company", "");
			jsonObject.accumulate("firstName", firstNameString);
			jsonObject.accumulate("gender", genderString);
			jsonObject.accumulate("housePhone", resi_phoneString);
			jsonObject.accumulate("howToContact", "");
			jsonObject.accumulate("lastName", astNameString);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("mobile", phoneString);
			jsonObject.accumulate("occupation", occupationString);
			jsonObject.accumulate("subscribedToNewsletter", newsletterstatus);
			jsonObject.accumulate("subscribedToSMS", SMSstatus);
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
			jsonObject.accumulate("workPhone", "");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoadData(jsonObject);
		
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.AccountInformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.AccountInformation,
					jsonObject);

		}

	}

	@Override
	public void EditAccountSuccessfully(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		Activity.stopSpinWheel();
		
		super.EditAccountSuccessfully(jsonObject);
		Activity.onBackPressed(); 
		Toast.makeText(Activity, R.string.profile_edited_successfully, Toast.LENGTH_SHORT).show();
		Log.e("jsonObject", "" + jsonObject.toString());
		/*Fragment myaFragment = new AccountInfoInnerpageFragment();
		Activity.popFragments(myaFragment);*/
		
		PrefUtil.setOccupation(Activity, occupationString);
		PrefUtil.setGender(Activity, genderString);
		
		PrefUtil.setNewsletter(Activity, newsletterstatus.toString());
		PrefUtil.setsms(Activity, SMSstatus.toString());

	}

	@Override
	public void EditAccountfail(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.EditAccountfail(jsonObject);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
