package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.MainActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.CountryAreaSpinnerAdapter;
import com.mawaqaa.u3an.adapters.CuisineSpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Country;
import com.mawaqaa.u3an.data.Cuisine;
import com.mawaqaa.u3an.data.SingleAreaCountry;
import com.mawaqaa.u3an.dialogfragment.AdvertisementBanner;
import com.mawaqaa.u3an.interfaces.DataHandlingUtilities;
import com.mawaqaa.u3an.interfaces.onLanguageChangeListener;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class HomeFragment extends BaseFragment implements OnClickListener {

	private Button find_restaurents_Button;
	private Spinner cusineSpinner, countrySpinner;
	private TextView m_order_count;
	ArrayList<Cuisine> arraylist_cusines_data;
	ArrayList<SingleAreaCountry> arraylist_country_data;
	static String FILENAMECUISINEDATA = "filecuisinesdata.txt";
	static String FILENAMEAREADATA = "fileareadata.txt";
	String FILENAMENOTIFICATION = "notificationLog.txt";
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	onLanguageChangeListener langSwitch;
	Button loginButton, registerButton, loginAsGuestButton;
	LinearLayout homePageLayout;
	AdvertisementBanner adBanner;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.homepage, container, false);
		setupUI(rootView, false);
		initView(rootView);
		if (Utilities.isNetworkAvailable(Activity)) {
			AccumulateJSONObject();
		} else {
			Toast.makeText(Activity, R.string.no_network, Toast.LENGTH_SHORT)
					.show();
		}
		WriteDataToFile();
		return rootView;
	}

	private void WriteDataToFile() {
		String data = "No Data";
		FileOutputStream fosdummyy;
		try {
			fosdummyy = Activity.openFileOutput(FILENAMENOTIFICATION,
					Context.MODE_PRIVATE);
			fosdummyy.write(data.getBytes());
			fosdummyy.close();
		} catch (Exception e) {

		}
	}

	private void AccumulateJSONObject() {
		Log.e("Entered here......", "Available");
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();	
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
				Log.e("Jijooooooooooo", ""+jsonObject);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadCuisineData(jsonObject);
	}

	public void loadCuisineData(JSONObject jsonObject) {
		showProgressbar();
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_cusines_url,
					jsonObject);
		}
	}

	@Override
	public void onCusinesDataLoadedSuccessfully(JSONObject jsonObject) {
		super.onCusinesDataLoadedSuccessfully(jsonObject);
		hideProgressbar();
		WriteCusineDataToFile(jsonObject.toString());
		ParseCuisineData(jsonObject);
	}

	private void WriteCusineDataToFile(String data) {
		FileOutputStream fosdummyy;
		try {
			fosdummyy = Activity.openFileOutput(FILENAMECUISINEDATA,
					Context.MODE_PRIVATE);
			fosdummyy.write(data.getBytes());
			fosdummyy.close();
		} catch (Exception e) {
		}
	}

	@Override
	public void onCusinesLoadingFailed(JSONObject jsonObject) {
		super.onCusinesLoadingFailed(jsonObject);
		hideProgressbar();
	}

	private void ParseCuisineData(JSONObject jsonObject) {
		// Create an array
		arraylist_cusines_data = new ArrayList<Cuisine>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.get_cusines_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.get_cusines_json_obj);
				for (int i = 0; i < jsonarray.length(); i++) {
					if (!jsonarray.isNull(i)) {
						Cuisine cuisine = new Cuisine(
								jsonarray.optJSONObject(i));
				//Jijo		
						if(cuisine.strCuisineId.equals("")||cuisine.strCuisineName.equals("")){
							Log.e("jijo", "nullllllll");
						}
						else
							arraylist_cusines_data.add(cuisine);
							Log.e("ParseCuisine::::", ""+arraylist_cusines_data);
					}
				}
			}
			PopulateCusinesSpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("The error", ""+e.getMessage());
		}
	}

	private void PopulateCusinesSpinner() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject_area = new JSONObject();
		try {
			jsonObject_area = new JSONObject();
			jsonObject_area.accumulate("countryId", "1");
			if(sDefSystemLanguage.equals("en")){
				jsonObject_area.accumulate("locale", "en-US");
			}else{
				jsonObject_area.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Collections.sort(arraylist_cusines_data, new Comparator<Cuisine>() {
			@Override
			public int compare(Cuisine c1, Cuisine c2) {
				return c1.strCuisineName.compareTo(c2.strCuisineName);
			}
		});
		cusineSpinner.setAdapter(new CuisineSpinnerAdapter(Activity,
				arraylist_cusines_data));
		loadCategotyDataCountry(jsonObject_area);
	}

	private void loadCategotyDataCountry(JSONObject jsonObject) {
		showProgressbar();
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.areas_url, jsonObject);
		}
	}

	@Override
	public void onCountryLoadedSuccessfully(JSONObject jsonObject) {
		super.onCountryLoadedSuccessfully(jsonObject);
		hideProgressbar();
		ParseCountryData(jsonObject);
		WriteAreaData(jsonObject.toString());
	}

	private void WriteAreaData(String data) {
		FileOutputStream fosdummyy;
		try {
			fosdummyy = Activity.openFileOutput(FILENAMEAREADATA,
					Context.MODE_PRIVATE);
			fosdummyy.write(data.getBytes());
			fosdummyy.close();
		} catch (Exception e) {
		}
	}

	@Override
	public void onCountryLoadingFailed(JSONObject jsonObject) {
		super.onCountryLoadingFailed(jsonObject);
		hideProgressbar();
	}

	private void ParseCountryData(JSONObject jsonObject) {
		// Create an array
		arraylist_country_data = new ArrayList<SingleAreaCountry>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.areas_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.areas_json_obj);

				for (int i = 0; i < jsonarray.length(); i++) {
					if (!jsonarray.isNull(i)) {
						jsonObject = jsonarray.getJSONObject(i);
						Country country = new Country(jsonObject);
						addAreas2SpinnerArrayList(country);
					}
				}
			}
			PopulateCountrySpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error", ""+e.getMessage());
		}
	}

	private void addAreas2SpinnerArrayList(Country country) {
		SingleAreaCountry areaCountry = new SingleAreaCountry();
		// for showing city Name in Spinner
		areaCountry.areaName = "-- " + country.cityName + " --";
		areaCountry.isCityName = true;
		arraylist_country_data.add(areaCountry);
		// adding all areas in the city
		arraylist_country_data.addAll(country.areas);
	
	}

	private void PopulateCountrySpinner() {
		countrySpinner.setAdapter(new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data));
		new doGetOrderCount().execute();
	}

	private void initView(View view) {
		find_restaurents_Button = (Button) view
				.findViewById(R.id.button_find_restrntz);
		find_restaurents_Button.setOnClickListener(this);
		cusineSpinner = (Spinner) view.findViewById(R.id.spinner1_cusine);
		countrySpinner = (Spinner) view.findViewById(R.id.spinner2_country);
		m_order_count = (TextView) view.findViewById(R.id.order_count);
		loginButton = (Button) view.findViewById(R.id.loginbutton);
		registerButton = (Button) view.findViewById(R.id.registerbutton);
		loginAsGuestButton = (Button) view.findViewById(R.id.loginasguest);
		loginButton.setOnClickListener(this);
		registerButton.setOnClickListener(this);
		loginAsGuestButton.setOnClickListener(this);
		homePageLayout = (LinearLayout) view.findViewById(R.id.login_control);
	}

	@Override
	public void onResume() {
		super.onResume();

		DataHandlingUtilities listenerHideView = (DataHandlingUtilities) Activity;
		listenerHideView.hideToolBar();
		listenerHideView.defaultBottomBar();
		if (PrefUtil.isUserSignedIn(Activity)) {
			homePageLayout.setVisibility(View.GONE);
		} else {
			homePageLayout.setVisibility(View.VISIBLE);
		}

		String data = null;
		try {
			data = Activity.readFile();
		} catch (Exception e) {

		}
		if (data != null) {
			if (data.equals("Guest"))
				loginAsGuestButton.setVisibility(View.GONE);
			else
				loginAsGuestButton.setVisibility(View.VISIBLE);
		}

	}
//*******************************************************************************************************	
//**************************************************button_find_restrntz*********************************
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button_find_restrntz:
			PrefUtil.setFragHeading(Activity, getResources().getString(R.string.detailed_search_heading));
			callDetailedSearchFragment();
			break;
		case R.id.loginbutton:
			Fragment fr = new LoginFragment();
			Activity.pushFragments(fr, false, true);
			break;
		case R.id.registerbutton:
			Fragment fr1 = new RegisterFirstStepFragment();
			Activity.pushFragments(fr1, false, true);
			break;
		case R.id.loginasguest:
			loginAsGuestButton.setVisibility(View.GONE);
			Activity.WriteDataToFile("Guest");
			break;
		default:
			break;
		}
	}

	private void callDetailedSearchFragment() {
		try {
			Bundle bundle = new Bundle();
			int pos = countrySpinner.getSelectedItemPosition();
			SingleAreaCountry country = arraylist_country_data.get(pos);
			String strAreaId = country.areaId;
			if (strAreaId == null || strAreaId.equals(null)
					|| strAreaId.trim().equals("") || country.isCityName) {
				Toast.makeText(Activity, R.string.select_area,
						Toast.LENGTH_SHORT).show();
				return;
			}
			bundle.putString(AppConstants.area_id, strAreaId);
			bundle.putInt("AREA_SPINNER_POS", pos);
			pos = cusineSpinner.getSelectedItemPosition();
			Cuisine cuisine = arraylist_cusines_data.get(pos);
			String strCuisineId = cuisine.strCuisineId;
			Log.e("strCuisineId....",""+strCuisineId);
			bundle.putString(AppConstants.cuisine_id, strCuisineId);
			bundle.putInt("CUSINE_SPINNER_POS", pos);
			Log.e("CUSINE_SPINNER_POS....",""+pos);
			
//***********************************************************************************************************
//***********************************************************************************************************
			
			DetailedSearchFragment detailedSearchFragment = new DetailedSearchFragment();
			detailedSearchFragment.setArguments(bundle);
			Activity.pushFragments(detailedSearchFragment, false, true);
			
			
			
//***********************************************************************************************************			
//***********************************************************************************************************			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void hideProgressbar() {
		Activity.stopSpinWheel();
	}

	private void showProgressbar() {
		Activity.startSpinwheel(false, true);
	}

	private void getOrderCount() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		sendCmnd4OrderCount(jsonObject);
	}

	private void sendCmnd4OrderCount(JSONObject jsonObject) {
		showProgressbar();
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_order_count_url,
					jsonObject);
		}
	}

	@Override
	public void onOrderCountGot(JSONObject jsonObject) {
	}

	@Override
	public void onOrderCountGettingFail(JSONObject jsonObject) {
	}

	// Asynch task for posting the value to the server.....
	public class doGetOrderCount extends AsyncTask<Void, String, Void> {
		String result;
		//JSONObject jObj = null;
		InputStream istream;

		public doGetOrderCount() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressbar();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();

			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost httppost = new HttpPost(
						AppConstants.get_order_count_url);
				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";

					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();
					Log.i("Login Result", ""+result);
					//jObj = new JSONObject(result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {

			if (this.result != null) {
				m_order_count.setText(this.result.substring(1,
						this.result.length() - 1)
						+ getResources().getString(R.string.order_count));
			} else {
				Toast.makeText(getActivity(), R.string.server_error,
						Toast.LENGTH_SHORT).show();
			}
			hideProgressbar();
			if (PrefUtil.getAds(Activity.getApplicationContext()) == true) {

				MyDialogFragmentAds adBanner = MyDialogFragmentAds
						.newInstance();
				Bundle args = new Bundle();
				args.putParcelable("BundleIcon", MainActivity.image);
				//args.putString("sdd", MainActivity.adsbannerImage);
				adBanner.setArguments(args);
				adBanner.show(Activity.getFragmentManager(), "AddBanner");
				PrefUtil.setAds(Activity.getApplicationContext(), false);
			}
		}
	}

	private void AccumulateJSONOBjectForAdd() {
		showProgressbar();
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();
			jsonObject.accumulate("DeviceType", "1");
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadAdvertisementData(jsonObject);
	}

	public void loadAdvertisementData(JSONObject jsonObject) {
		showProgressbar();
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.AdvertisementBannerUrl,
					jsonObject);
		}
	}

	@Override
	public void onAdvertisementLoadedSuccessfully(JSONObject jsonObject) {
		super.onAdvertisementLoadedSuccessfully(jsonObject);
		hideProgressbar();
	}

	@Override
	public void onAdvertisementLoadingFailed(JSONObject jsonObject) {
		super.onAdvertisementLoadingFailed(jsonObject);
		hideProgressbar();
	}

	@Override
	public void pushFragments4LanSwitch() {
		
		if (PrefUtil.isUserSignedIn(Activity)) {
			homePageLayout.setVisibility(View.GONE);
		} else {
			homePageLayout.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		langSwitch = (onLanguageChangeListener) getActivity();
	}

	@Override
	public void LanSwitchTrigered() {
		if (Activity.getSupportFragmentManager().getBackStackEntryCount() == 0)
			Activity.pushFragments4LanSwitch(new HomeFragment(), false);
		else if (langSwitch != null) {
			langSwitch.OnLanguageChange(HomeFragment.this);
		}
	}
}
