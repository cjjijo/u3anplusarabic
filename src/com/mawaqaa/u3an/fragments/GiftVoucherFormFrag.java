package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class GiftVoucherFormFrag extends HeaderViewControlFragment implements
		OnClickListener, OnCheckedChangeListener {
	EditText coupenName, email;
	ImageButton submit;
	CheckBox privacy;
	String Coupen_Name, Coupen_Email, url;
	EditText errorEditTxt;
	boolean privacyStatus;
	BaseActivity activity;
	String Ammount, giftvoucherId;
	public String FILENAMEAUTHKEY = "authentication_key.txt";

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.giftvoucherform, gp, false);
		setupUI(v, false);
		initView(v);
		PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.gift_voucher_heading));
		Bundle data = getArguments();
		Ammount = data.getString(AppConstants.amount);
		giftvoucherId = data.getString(AppConstants.GiftVoucherId);
		Log.e("Ammount", "" + Ammount);
		Log.e("giftvoucherId", "" + giftvoucherId);

		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		return v;
	}

	private void initView(View v) {
		coupenName = (EditText) v.findViewById(R.id.coupen_name);
		email = (EditText) v.findViewById(R.id.coupen_Email);
		submit = (ImageButton) v.findViewById(R.id.submit_Btn);
		privacy = (CheckBox) v.findViewById(R.id.privacy);
		setTitleForPage(getResources().getString(R.string.gift_voucher_heading));
		submit.setOnClickListener(this);
		alignCheckBox(privacy);
		privacy.setOnCheckedChangeListener(this);
	}

	private void alignCheckBox(CheckBox checkBox) {
		final float scale = this.getResources().getDisplayMetrics().density;
		checkBox.setPadding(checkBox.getPaddingLeft()
				+ (int) (10.0f * scale + 0.5f), checkBox.getPaddingTop(),
				checkBox.getPaddingRight(), checkBox.getPaddingBottom());
	}

	@Override
	public void onClick(View v) {
		validate();
	}

	private void validate() {
		boolean isvalid = DoNamevalidation();
		if (!isvalid)
			return;
		
		
		isvalid = DoEmailValidation();
		if (!isvalid)
			return;
		
		isvalid = privacy.isChecked();
		if (!isvalid){
			Toast.makeText(Activity, R.string.agree_term_condition, Toast.LENGTH_SHORT).show();
			return;
		}
		else
			DoSubmitAction();
	}

	private boolean DoEmailValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		Coupen_Email = email.getText().toString().trim();
		Log.e("Coupen_Email", "" + Coupen_Email);
		if (Coupen_Email == null || Coupen_Email.length() < 1) {
			email.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_email)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				email.setError(Html
						.fromHtml("<font color='white'>Enter email id to proceed</font>"));
			}else{
				email.setError(Html
						.fromHtml("<font color='white'>أدخل البريد الإلكتروني</font>"));
			}*/
			errorEditTxt = email;
			email.addTextChangedListener(registerInputTextWatcher);
			// emailEditText.requestFocus();
			return false;
		}
		if (!Utilities.doemailValidation(Coupen_Email)) {
			
			email.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_email)+"</font>"));
			
			errorEditTxt = email;
			email.addTextChangedListener(registerInputTextWatcher);
			// emailEditText.requestFocus();
			
			return false;
		} else {
			return true;
		}
	}

	private boolean DoNamevalidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		Coupen_Name = coupenName.getText().toString();
		Log.e("Coupen_Email", "" + Coupen_Name);
		if (Coupen_Name.length() == 0) {
			coupenName
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.first_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				coupenName
						.setError(Html
								.fromHtml("<font color='white'>Enter a first name to continue</font>"));
			}else{
				coupenName
					.setError(Html
						.fromHtml("<font color='white'>أدخل الاسم الأول للاستمرار</font>"));
			}*/
			errorEditTxt = coupenName;
			coupenName.addTextChangedListener(registerInputTextWatcher);
			// firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private void accumulateData() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("CouponName", Coupen_Name);
			jsonObject.accumulate("Email", Coupen_Email);
			jsonObject.accumulate("giftVoucherId", giftvoucherId);
			jsonObject.accumulate("amount", Ammount);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("Request  ", "" + jsonObject.toString());
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.get_pagedetails_url);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.buy_GiftVoucher,
					jsonObject);
		}
	}

	@Override
	public void LoadBuyGiftVoucherSuccesfully(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.LoadBuyGiftVoucherSuccesfully(jsonObject);
		ParseJsonobject(jsonObject);
		Log.e("jsonObject", "" + jsonObject.toString());
	}

	private void ParseJsonobject(JSONObject jsonObject) {
		try {
			url = jsonObject.getString("Url");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		PoppulteData();
		Log.e("url in giftvoucher", "url" + url);
	}

	private void DoSubmitAction() {
		privacyStatus = privacy.isChecked();
		accumulateData();
	}

	private void PoppulteData() {
		Fragment kentfragment = new KnetForGiftvoucher();
		Bundle data = new Bundle();
		data.putString("Url", url);
		kentfragment.setArguments(data);
		Activity.pushFragments(kentfragment, false, true);
	}

	@Override
	public void LoadBuyGiftVoucherFail(JSONObject jsonObject) {
		super.LoadBuyGiftVoucherFail(jsonObject);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		if (isChecked) {
			PrivacyDailogueFragment Dialoguesfragment = new PrivacyDailogueFragment(
					activity);

			// ((PrivacyDailogueFragment)
			// Dialoguesfragment).show(Activity.getSupportFragmentManager(),
			// "PrivacyDailogueFragment");
			// Dialoguesfragment.show(getFragmentManager(),
			// "PrivacyDailogueFragment");

		}

	}
}
