package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class GetCustemerDetails extends HeaderViewControlFragment {
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.deleveryinformation, gp, false);
		setupUI(v, false);
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		accumulate();
		return v;
	}
	
	private void accumulate() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL", ""+AppConstants.getdeleveryinformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.getdeleveryinformation,
					jsonObject);
		}
	}
	
	@Override
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
		super.getCustomerAddressSuccess(jsonObject);
		parse(jsonObject);
	}

	private void parse(JSONObject jsonObject) {
		try {
			String uancreadit = jsonObject.getString("U3ANCredit");
			PrefUtil.setU3ancredit(getActivity(), uancreadit);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void getCustomerAddressFail(JSONObject jsonObject) {
		super.getCustomerAddressFail(jsonObject);
	}
}
