package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.CountryAreaSpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Country;
import com.mawaqaa.u3an.data.SingleAreaCountry;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class EditAddressFrag extends HeaderViewControlFragment implements
		OnClickListener {
	RadioGroup selectionRadioGroup;
	RadioButton selectedRadioButton;
	String street,profile,building,block,judda,directions,apartment,floor;
	EditText profileNameEditText, blockEditText, juddaEditText, streetEditText,
			buildingNoEditText, floorEditText, apartmentEditText,
			directionsEditText;
	ImageButton villaButton, buildingButton, officeButton, nextButton;
	TextView villaTextView, buildingTextView, officeTextView;
	Spinner areaSpinner;
	LinearLayout apartmentLayout, floorLayout;
	String areaString, profileNameString, blockString, juddaString,
			streetString, houseNoString, floorString, apartmentString,
			directionsString, areaId, AddresID, gender, firstNameString,
			lastNameString, mobileno, housePhno, addresstyp, isprimary;
	String areaID, AuthKey;
	EditText errorEditText;
	ArrayList<SingleAreaCountry> arraylist_country_data;
	Boolean villaFlag = true, buildingFlag = false, officeFlag = false;
	String address_Type = "0";
	JSONObject areaJsonObject;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	int maxLength;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.editadress, gp, false);
		setupUI(v, false);
		View converterView = v
				.findViewById(R.id.linlay_secondstage);
		setTitleForPage(getResources().getString(R.string.add_address_heading));
		setupUI(converterView, false);

		init(v);
		fixUI();
		
		AuthKey = PrefUtil.getAuthkey(getActivity());
		FileOutputStream fosdummyy;

		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		areaJsonObject = readDataFromFile(HomeFragment.FILENAMEAREADATA);
		ParseCountryData(areaJsonObject);
		return v;

	}

	private JSONObject readDataFromFile(String fileName) {
		JSONObject jsonObject = null;
		try {
			FileInputStream fis = Activity.openFileInput(fileName);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			String data = br.readLine();
			br.close();
			in.close();
			fis.close();
			if (data != null)
				jsonObject = new JSONObject(data);

		} catch (Exception e) {
		}
		return jsonObject;
	}

	private void init(View v) {

		profileNameEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_profile_Name);

		profileNameEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						profileNameEditText));

		profileNameEditText
				.setOnFocusChangeListener(new FocusChangedListener());
		profileNameEditText.setFocusableInTouchMode(false);
		/*
		 * profileNameEditText.setFilters(new InputFilter[] { new InputFilter()
		 * { public CharSequence filter(CharSequence src, int start, int end,
		 * Spanned dst, int dstart, int dend) { if(src.equals("")){ // for
		 * backspace return src; } if(src.toString().matches("[a-zA-Z ]+")){
		 * return src; } return ""; }
		 * 
		 * 
		 * } });
		 */
		/*
		 * profileNameEditText.setFilters(new InputFilter[]{new
		 * InputFilter.LengthFilter(maxLength)});
		 */
		blockEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Block);
	

		juddaEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Judda);
	

		streetEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Street);
	
		streetEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				streetEditText));
		streetEditText.setOnFocusChangeListener(new FocusChangedListener());
		buildingNoEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_House_No);
		
		buildingTextView = (TextView) v.findViewById(R.id.buildingTextview);
		/*buildingTextView.setOnClickListener(this);*/

		buildingNoEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				buildingNoEditText));
		buildingNoEditText.setOnFocusChangeListener(new FocusChangedListener());
		floorEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Floor);
		floorEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				floorEditText));
		floorEditText.setOnFocusChangeListener(new FocusChangedListener());
		
		apartmentEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Apartment);
		apartmentEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				apartmentEditText));
		apartmentEditText.setOnFocusChangeListener(new FocusChangedListener());
		
		directionsEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Directions);

		nextButton = (ImageButton) v.findViewById(R.id.nextBtnSecondStage);
		nextButton.setOnClickListener(this);

		areaSpinner = (Spinner) v.findViewById(R.id.Edittext_register2_Area);
		// areaSpinner.setSelection(Integer.getInteger(areaId));

		villaButton = (ImageButton) v.findViewById(R.id.villaRadioBtn);
		villaButton.setOnClickListener(this);

		officeButton = (ImageButton) v.findViewById(R.id.officeRadioBtn);
		officeButton.setOnClickListener(this);

		buildingButton = (ImageButton) v.findViewById(R.id.buildingRadioBtn);
		buildingButton.setOnClickListener(this);

		apartmentLayout = (LinearLayout) v.findViewById(R.id.linlayaperment);
		apartmentLayout.setOnClickListener(this);

		floorLayout = (LinearLayout) v.findViewById(R.id.linlayfloor);
		floorLayout.setOnClickListener(this);

		villaTextView = (TextView) v.findViewById(R.id.villaTextview);
		villaTextView.setOnClickListener(this);

		officeTextView = (TextView) v.findViewById(R.id.officeTextview);
		officeTextView.setOnClickListener(this);

		Bundle bundle = getArguments();
		if (bundle != null) {
			areaId = bundle.getString(AppConstants.AreaId);
			areaString = bundle.getString(AppConstants.areaName);
			Log.e("areaString in add address", "" + areaString);

			blockString = bundle.getString(AppConstants.Block);
			Log.e("blockStringggggggg.................", "" + blockString);
			juddaString = bundle.getString(AppConstants.judda);
			Log.e("juddaStringggggg.................", "" + juddaString);
			streetString = bundle.getString(AppConstants.street);
			Log.e("streetString in edit.................", "" + streetString);
			directionsString = bundle.getString(AppConstants.Extradirections);
			houseNoString = bundle.getString(AppConstants.Buildingno);
			address_Type = bundle.getString(AppConstants.type);
			Log.e("addresstyppppp.................", "" + address_Type);
			floorString = bundle.getString(AppConstants.floor);
				 
			
			Log.e("floorStringggg.................", "" + floorString);
			profileNameString = bundle.getString(AppConstants.profileName);
			Log.e("prifilename111..............", "" + profileNameString);
			AddresID = bundle.getString(AppConstants.id);
			apartmentString = bundle.getString(AppConstants.suite);
			isprimary = bundle.getString(AppConstants.isprimary);
			Log.e("isprimary..............", "" + isprimary);

			profileNameEditText.setText(profileNameString);

			
			
			
			blockEditText.setText(blockString);

			juddaEditText.setText(juddaString);

			streetEditText.setText(streetString);

			buildingNoEditText.setText(houseNoString);

			directionsEditText.setText(directionsString);

			floorEditText.setText(floorString);

			apartmentEditText.setText(apartmentString);

			/*
			 * ArrayList<String> dummy = new ArrayList<String>();
			 * dummy.add(areaString); Log.e("dummy", "" + dummy);
			 */// areaSpinner.setAdapter(new SpinnerAdapter(Activity, dummy));

			if ((address_Type.startsWith("V"))
					|| (address_Type.startsWith("v"))) {
				ClickActionForVilla();
			} else if ((address_Type.startsWith("B"))
					|| (address_Type.startsWith("b"))) {
				clickActionForBuilding();
			} else {
				clickActionForOffice();
			}

		}
		gender = PrefUtil.getgender(getActivity());
		firstNameString = PrefUtil.getFirstname(getActivity());
		lastNameString = PrefUtil.getLastname(getActivity());
		mobileno = PrefUtil.getphonenum(getActivity());
		housePhno = PrefUtil.getreside_phonenum(getActivity());

	}

	private void fixUI() {
		if (villaFlag == true) {
			villaButton.setBackgroundResource(R.drawable.villahouse2);
		} else {
			villaButton.setBackgroundResource(R.drawable.villahouse1);
		}

		if (officeFlag == true) {
			officeButton.setBackgroundResource(R.drawable.office2);
		} else {
			officeButton.setBackgroundResource(R.drawable.office1);
		}
	
		if (buildingFlag == true) {
			buildingButton.setBackgroundResource(R.drawable.building2);
		} else {
			buildingButton.setBackgroundResource(R.drawable.building1);
		}
	}

	private void addAreas2SpinnerArrayList(Country country) {
		SingleAreaCountry areaCountry = new SingleAreaCountry();
		// for showing city Name in Spinner
		areaCountry.areaName = "-- " + country.cityName + " --";
		areaCountry.isCityName = true;
		arraylist_country_data.add(areaCountry);
		// adding all areas in the city
		arraylist_country_data.addAll(country.areas);

	}

	private void ParseCountryData(JSONObject jsonObject) {
		// Create an array
		arraylist_country_data = new ArrayList<SingleAreaCountry>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.areas_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.areas_json_obj);

				for (int i = 0; i < jsonarray.length(); i++) {
					if (!jsonarray.isNull(i)) {
						jsonObject = jsonarray.getJSONObject(i);
						Country country = new Country(jsonObject);
						addAreas2SpinnerArrayList(country);
					}
				}
			}
			PopulateCountrySpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error", ""+e.getMessage());
		}
	}

	private void PopulateCountrySpinner() {
		areaSpinner.setAdapter(new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data));

		int i = -1;

		for (SingleAreaCountry sa : arraylist_country_data) {
			++i;
			// Log.e("Area Id in List",sa.areaId);
			Log.e("Area Nme in List",""+ sa.areaName); // if(sa.areaId.equals(areaId))
			if (sa.areaName.equals(areaString))
				break;
		}
		areaSpinner.setSelection(i);

		int position = areaSpinner.getSelectedItemPosition();
		areaString = arraylist_country_data.get(position).areaName;
		// areaSelectedTextView.setText(areaString);

		areaSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				areaString = arraylist_country_data.get(position).areaName;

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub

			}
		});

	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private class AddRestaurantTextwatcher implements TextWatcher {

		private EditText editText;

		AddRestaurantTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {
			/*
			 * switch (editText.getId()) { case R.id.firstname:
			 * DoFirstNameValidation();
			 * 
			 * break; case R.id.lastname: DoLastNameValidation(); break; case
			 * R.id.restaurant_name: DoRestaurantNameValidation(); break; case
			 * R.id.restaurant_address: DoRestaurantAddressValidation(); break;
			 * case R.id.telephone: DoPhoneNumberValidation(); break; case
			 * R.id.email: doEmailIdValidation(); break;
			 * 
			 * default: break; }
			 */
		}

	}

	private class FocusChangedListener implements OnFocusChangeListener {

		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {

				case R.id.Edittext_register2_profile_Name:
					doProfileNameValidation();
					break;
				case R.id.Edittext_register2_Street:
					doStreetValidation();
					break;
				case R.id.Edittext_register2_House_No:
					doHouseNumberValidation();
					break;
				case R.id.Edittext_register2_Floor:
					doFloorValidation();
					break;
				case R.id.Edittext_register2_Building:
					doHouseNumberValidation();
					break;

				default:
					break;
				}
			}/*
			 * else if (v != null && v instanceof EditText) { ((EditText)
			 * v).setError(null); }
			 */
		}

	}

	private Boolean doApartmentValidation() {
		String apartString = apartmentEditText.getText().toString();
		if (apartString.length() > 0) {
			return true;
		} else {
			apartmentEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getString(R.string.enter_apartment_data)+"</font>"));
			errorEditText = apartmentEditText;
			apartmentEditText.addTextChangedListener(registerInputTextWatcher);
			// apartmentEditText.requestFocus();
			return false;
		}
	}

	private Boolean doFloorValidation() {
		String flString = floorEditText.getText().toString();
		Log.e("floorString", "" + flString);
		if (flString.length() > 0) {
			return true;
		} else {
			/*Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();*/
			floorEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_floor)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				floorEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter the floor number to continue</font>"));
			}else{
				floorEditText
					.setError(Html
						.fromHtml("<font color='white'>أدخل رقم الطابق للاستمرار</font>"));
			}*/
			errorEditText = floorEditText;
			floorEditText.addTextChangedListener(registerInputTextWatcher);
			// floorEditText.requestFocus();
			return false;
		}
	}

	private Boolean doHouseNumberValidation() {
		String hNoString = buildingNoEditText.getText().toString();
		if (hNoString.length() > 0) {
			return true;
		} else {
			/*Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();*/
			buildingNoEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_house_num)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				buildingNoEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter the house number to continue</font>"));
			}else{
				buildingNoEditText
				.setError(Html
						.fromHtml("<font color='white'>أدخل رقم المنزل للاستمرار</font>"));
			}*/
			errorEditText = buildingNoEditText;
			buildingNoEditText.addTextChangedListener(registerInputTextWatcher);
			// houseNoEditText.requestFocus();
			return false;
		}
	}

	private Boolean doStreetValidation() {
		String stString = streetEditText.getText().toString();
		if (stString.length() > 0) {
			return true;
		} else {
			streetEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_street)+"</font>"));
			/*Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			if(sDefSystemLanguage.equals("en")){
				streetEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a Street name to continue</font>"));
			}else{
				streetEditText
					.setError(Html
						.fromHtml("<font color='white'>أدخل اسم الشارع للاستمرار</font>"));
			}*/
			errorEditText = streetEditText;
			streetEditText.addTextChangedListener(registerInputTextWatcher);
			// streetEditText.requestFocus();
			return false;
		}
	}

	private Boolean doAreaValidation() {

		int pos = areaSpinner.getSelectedItemPosition();
		SingleAreaCountry country = arraylist_country_data.get(pos);
		String strAreaId = country.areaId;
		areaID = strAreaId;
		if (strAreaId == null || strAreaId.equals(null)
				|| strAreaId.trim().equals("") || country.isCityName) {
			Toast.makeText(Activity, getResources().getString(R.string.select_area),
					Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}

	}

	private Boolean doProfileNameValidation() {
		/*Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();*/
		String profNameString = profileNameEditText.getText().toString();
		if (profNameString.length() > 0) {
			return true;
		} else {
			profileNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getString(R.string.enter_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				profileNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a name to continue</font>"));
			}else{
				profileNameEditText
					.setError(Html
						.fromHtml("<font color='white'>أدخل الاسم للاستمرار</font>"));
			}*/
			errorEditText = profileNameEditText;
			profileNameEditText
					.addTextChangedListener(registerInputTextWatcher);
			// profileNameEditText.requestFocus();
			return false;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.nextBtnSecondStage:
			validateData();
			break;

		case R.id.villaRadioBtn:
			ClickActionForVilla();
			break;

		case R.id.buildingRadioBtn:
			clickActionForBuilding();
			break;

		case R.id.officeRadioBtn:
			clickActionForOffice();
			break;

		case R.id.villaTextview:
			ClickActionForVilla();
			break;

		case R.id.buildingTextview:
			clickActionForBuilding();
			break;

		case R.id.officeTextview:
			clickActionForOffice();
			break;

		default:
			break;
		}
	}

	private void validateData() {
		boolean isvalid = doAreaValidation();
		if (!isvalid)
			return;

		isvalid = doProfileNameValidation();
		if (!isvalid)
			return;

		isvalid = doStreetValidation();
		if (!isvalid)
			return;

		isvalid = doHouseNumberValidation();
		if (!isvalid)
			return;

		else {

			if ((buildingFlag == true) || (officeFlag == true)) {
				doOptionalDataValidation();
			} else {
				floorString = getResources().getString(R.string.nodata);//"No Data";
				apartmentString = getResources().getString(R.string.nodata);

				doSubmitAction();
			}
		}
	}

	private void doOptionalDataValidation() {
		Boolean isvalid = doFloorValidation();
		if (!isvalid)
			return;
		isvalid = doApartmentValidation();
		if (!isvalid)
			return;
		else {
			doSubmitAction();
		}
	}

	private void clickActionForOffice() {
		villaFlag = false;
		buildingFlag = false;
		officeFlag = true;
		address_Type = "2";
		fixUI();
		visibilityControl("office");
	}

	private void clickActionForBuilding() {
		villaFlag = false;
		buildingFlag = true;
		officeFlag = false;
		address_Type = "1";
		fixUI();
		visibilityControl("building");
	}

	private void ClickActionForVilla() {
		villaFlag = true;
		buildingFlag = false;
		officeFlag = false;
		address_Type = "0";
		fixUI();
		visibilityControl("villa");
	}

	private void visibilityControl(String string) {
		switch (string) {
		case "villa":
			apartmentLayout.setVisibility(View.GONE);
			floorLayout.setVisibility(View.GONE);
			break;

		case "building":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;

		case "office":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}
	}

	private void doSubmitAction() {
		block = blockEditText.getText().toString();
		judda = juddaEditText.getText().toString();
		directions = directionsEditText.getText().toString();
		profile = profileNameEditText.getText().toString();
		Log.e("profile", ""+profile);
		apartment=apartmentEditText.getEditableText().toString();
		floor=floorEditText.getText().toString();
		street=streetEditText.getText().toString();
		building=buildingNoEditText.getText().toString();
		AccumulateData();
		Bundle b=new Bundle();
		b.putString("street", street);
		
		

	}

	private void AccumulateData() {
		JSONObject jsonobject = new JSONObject();

		try {
Log.e("IsPrimary","" +isprimary);
			jsonobject.accumulate("AddressId", AddresID);

			jsonobject.accumulate("AreaId", areaID);
			jsonobject.accumulate("AreaName", areaString);
			jsonobject.accumulate("Block", block);
			jsonobject.accumulate("BuildingNo", building);
			jsonobject.accumulate("FirstName", firstNameString);
			jsonobject.accumulate("Floor", floor);
			jsonobject.accumulate("Gender", gender);
			jsonobject.accumulate("HousePhone", housePhno);
			jsonobject.accumulate("Id", AddresID);
			if (isprimary.equalsIgnoreCase("True")) {
				jsonobject.accumulate("IsPrimary", "true");
			}
			else {
				jsonobject.accumulate("IsPrimary", "false");
			}
			
	
			
			jsonobject.accumulate("Judda", judda);	
			jsonobject.accumulate("LastName", lastNameString);
			jsonobject.accumulate("Mobile", mobileno);
			jsonobject.accumulate("ProfileName", profile);
			jsonobject.accumulate("Street", street);

			jsonobject.accumulate("ExtraDirections", directions);

			jsonobject.accumulate("Suite", apartment);
			jsonobject.accumulate("Type", address_Type);
			jsonobject.accumulate("UserId", FILENAMEAUTHKEY);
			jsonobject.accumulate("WorkPhone", "");

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("jsonobject in edit address", "" + jsonobject.toString());
		LoadData(jsonobject);
	}

	private void LoadData(JSONObject jsonobject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {

			CommandFactory commandFactory = new CommandFactory();
			commandFactory
					.sendPostCommand(AppConstants.EditAddress, jsonobject);
		}

	}

	@Override
	public void editAddressSuccess(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		Activity.stopSpinWheel();
		Log.e("jsonObject in editaddress...................", "" + jsonObject.toString());
		super.editAddressSuccess(jsonObject);

		Activity.onBackPressed();
	}

	@Override
	public void editAddressFail(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		super.editAddressFail(jsonObject);

	}
}
