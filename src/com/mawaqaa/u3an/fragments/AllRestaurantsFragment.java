package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.AllRestaurantsBadgeAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.DataHandlingUtilities;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class AllRestaurantsFragment extends HeaderViewControlFragment {
	JSONObject jsonObject;
	JSONArray jsonarray;
	GridView gridView;
	ArrayList<HashMap<String, String>> arraylist_allrestaurants_data;
	TextView errorTextView;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_allrestaurants, gp,
				false);
		setupUI(rootView, false);
		initView(rootView);
		PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.all_restaurent));
		setTitleForPage(getResources().getString(R.string.all_restaurent));
		AccumulateJsonObject();
		
		/*Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		Log.e("jijooooo", "All restaurent Fragment::"+sDefSystemLanguage);*/
		
// Jijo 29-12-15		
		gridView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				HashMap<String, String> map = new HashMap<String, String>();
				map = arraylist_allrestaurants_data.get(position);
				String staString = map
						.get(AppConstants.all_restaurants_resturant_status);
				if(PrefUtil.getAppLanguage(Activity).equals("en")){
					switch (staString) {
					case AppConstants.OPEN:
						Fragment allRestaurantsDetailsFragment = new MostSellingRestaurantsDetails();
						Bundle params1 = new Bundle();
						Log.e("All restaurant tag",""+ AppConstants.ALL_RESTAURANT_TAG);
						params1.putString("Header", AppConstants.ALL_RESTAURANT_TAG);
						params1.putString(AppConstants.all_restaurants_id,
								map.get(AppConstants.all_restaurants_id));
						params1.putString(
								AppConstants.all_restaurants_resturant_name,
								map.get(AppConstants.all_restaurants_resturant_name));
						params1.putString(
								AppConstants.all_restaurants_resturant_logo,
								map.get(AppConstants.all_restaurants_resturant_logo));
						allRestaurantsDetailsFragment.setArguments(params1);
						Activity.pushFragments(allRestaurantsDetailsFragment,
								false, true);
						break;
					default:
						break;
					}
				}else{
					if(staString.equalsIgnoreCase(getResources().getString(R.string.open))){
						Fragment allRestaurantsDetailsFragment = new MostSellingRestaurantsDetails();
						Bundle params1 = new Bundle();
						Log.e("All restaurant tag",""+ AppConstants.ALL_RESTAURANT_TAG);
						params1.putString("Header", AppConstants.ALL_RESTAURANT_TAG);
						params1.putString(AppConstants.all_restaurants_id,
								map.get(AppConstants.all_restaurants_id));
						params1.putString(
								AppConstants.all_restaurants_resturant_name,
								map.get(AppConstants.all_restaurants_resturant_name));
						params1.putString(
								AppConstants.all_restaurants_resturant_logo,
								map.get(AppConstants.all_restaurants_resturant_logo));
						allRestaurantsDetailsFragment.setArguments(params1);
						Activity.pushFragments(allRestaurantsDetailsFragment,
								false, true);
						
					}
				}
				
			}			
		});
		return rootView;
	}

	private void initView(View view) {
		gridView = (GridView) view.findViewById(R.id.gridView_allresturants);
		errorTextView = (TextView) view.findViewById(R.id.empty);
	}

	private void AccumulateJsonObject() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		jsonObject = new JSONObject();
		try {
			// country id is always 1 for kuwait..
			jsonObject = new JSONObject();
			jsonObject.accumulate("countryId", "1");
			jsonObject.accumulate("cuisineId", " ");
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("restName", " ");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryData(jsonObject);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.all_restaurants_url,
					jsonObject);
		}
	}

	@Override
	public void onAllRestaurantsDataLoadedSuccessfully(JSONObject jsonObject) {
		super.onAllRestaurantsDataLoadedSuccessfully(jsonObject);
		Log.w("jsonObject======",""+jsonObject);
		parseData(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onAllresturantDataLoadingFailed(JSONObject jsonObject) {
		super.onAllresturantDataLoadingFailed(jsonObject);
		Activity.stopSpinWheel();
	}

	private void parseData(JSONObject jObj) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObj;
		// Create an array
		arraylist_allrestaurants_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.all_restaurants_json_obj);
			
			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);

				map.put(AppConstants.all_restaurants_rating, jsonObject
						.getString(AppConstants.all_restaurants_rating));
				map.put(AppConstants.all_restaurants_id,
						jsonObject.getString(AppConstants.all_restaurants_id));
				map.put(AppConstants.all_restaurants_resturant_logo, jsonObject
						.getString(AppConstants.all_restaurants_resturant_logo));
				map.put(AppConstants.all_restaurants_resturant_name, jsonObject
						.getString(AppConstants.all_restaurants_resturant_name));
				map.put(AppConstants.all_restaurants_resturant_status,
						jsonObject
								.getString(AppConstants.all_restaurants_resturant_status));
				map.put(AppConstants.all_restaurants_resturant_sort_order,
						jsonObject
								.getString(AppConstants.all_restaurants_resturant_sort_order));
				map.put(AppConstants.all_restaurants_resturant_price,
						jsonObject
								.getString(AppConstants.all_restaurants_resturant_price));
			
				String status = jsonObject
						.getString(AppConstants.all_restaurants_resturant_status);

				if (status.equals("HIDDEN")) {

				} else {
					arraylist_allrestaurants_data.add(map);
				}
			}
			populateGridview();
			Log.e("arraylist_allrestaurants_data", ""
					+ arraylist_allrestaurants_data);
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error", ""+e.getMessage());
		}
	}

	private void populateGridview() {
		Activity.stopSpinWheel();
		if (arraylist_allrestaurants_data.size() > 0) {
			AllRestaurantsBadgeAdapter adapter = new AllRestaurantsBadgeAdapter(
					Activity, arraylist_allrestaurants_data);
			gridView.setAdapter(adapter);
		} else {
			errorTextView.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		DataHandlingUtilities listenerHideView = (DataHandlingUtilities) Activity;
		listenerHideView.hideToolBar();
		listenerHideView.allResturantsBottomBar();
	}
}
