package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.ReviewAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class ReviewFragment extends HeaderViewControlFragment {
	GridView gridView;
	ArrayList<HashMap<String, String>> arrayList;
	HashMap<String, String> map;
	ListView listView;
	ImageView Res_logo;
	TextView res_name;
	TextView emptyView;
	private String restaurantID, restaurant_logo, restaurant_Name,
			restaurent_rating;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_review, gp, false);
		setupUI(v, false);
		Bundle data = getArguments();
		if (data != null) {
			restaurantID = data.getString("restId");
			restaurant_Name = data.getString("restName");
			restaurant_logo = data.getString("resLog");
			restaurent_rating = data.getString("resrating");
		}
	    //setHeader();
		initView(v);
		accumulatData();
		Log.e("restaurant_Name", "" + restaurant_Name);
		return v;
	}

	private void initView(View v) {
		gridView = (GridView) v.findViewById(R.id.gridview_review);
		Res_logo = (ImageView) v.findViewById(R.id.imageView1_rest_logo);
		res_name = (TextView) v.findViewById(R.id.rest_name);
		emptyView = (TextView) v.findViewById(android.R.id.empty);
		//setTitleForPage("Restaurant Reviews");
		setTitleForPage(PrefUtil.getFragHeading(getActivity()));
		if (URLUtil.isValidUrl(restaurant_logo) && restaurant_logo.length() > 5) {
			Picasso.with(Activity).load(restaurant_logo)
					.error(R.drawable.imagenotavailable).into(Res_logo);
		} else {
			Res_logo.setBackgroundResource(R.drawable.imagenotavailable);
		}
		if (restaurant_Name != null)
			res_name.setText(restaurant_Name);
		else

			res_name.setText("restaurant_Name");
		RatingBar m_rest_rating = (RatingBar) v
				.findViewById(R.id.resturant_rating);
		float rating = 0;
		try {
			rating = Float.valueOf(restaurent_rating);
		} catch (Exception exception) {
			Log.e(TAB_TAG,
					"Exception in rating converting : "
							+ exception.getMessage());
		}

		m_rest_rating.setRating(rating);

	}

	private void accumulatData() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		JSONObject jsonObject = new JSONObject();
		Activity.startSpinwheel(false, true);
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("restId", restaurantID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		emptyView.setVisibility(View.GONE);
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_review_url,
					jsonObject);
		}

	}

	@Override
	public void onNewReviewSuccessfully(JSONObject jsonObject) {
		super.onNewReviewSuccessfully(jsonObject);
		Log.e("jsonobject", "" + jsonObject.toString());
		ParseData(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onNewReviewFail(JSONObject jsonObject) {
		super.onNewReviewFail(jsonObject);
		Activity.stopSpinWheel();
	}

	private void ParseData(JSONObject jObj2) {
		JSONArray jsonArray = new JSONArray();
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObj2;

		arrayList = new ArrayList<HashMap<String, String>>();
		try {

			jsonArray = jsonObject.getJSONArray("review");
			for (int i = 0; i < jsonArray.length(); i++) {
				map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);
				map.put(AppConstants.createOn,
						jsonObject.getString(AppConstants.createOn));
				map.put(AppConstants.Review,
						jsonObject.getString(AppConstants.Review));
				map.put(AppConstants.Username,
						jsonObject.getString(AppConstants.Username));

				arrayList.add(map);
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
		populateGridview();
	}

	private void populateGridview() {

		ReviewAdapter adapter = new ReviewAdapter(Activity, arrayList);
		gridView.setAdapter(adapter);

		gridView.setEmptyView(emptyView);
	}

}
