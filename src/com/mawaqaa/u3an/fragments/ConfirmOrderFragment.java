package com.mawaqaa.u3an.fragments;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class ConfirmOrderFragment extends HeaderViewControlFragment implements
		OnClickListener {

	RadioButton cash, knet, visa, eclipse, fullPoint, myPoint, noPoint;
	WebView textDescription;
	ArrayList<HashMap<String, String>> arraylist;
	String block, streetString, juddaString, buildinString, floorString,
			apartmentString, directionString, firstName, email, addressType,
			areaId, areaName, buildingNumber, housePhone, lastName,
			mobileNumber, workphone, userName, company;
	Button confirm_orderButton;
	String areaID;
	String subTotal, deliveryCharges, GrandTotal, payByCashAll, discountAll;
	JSONArray productArray = new JSONArray();

	TextView subTotalTextView, deliveryChargesTextView, grandTotalTextView,
			payByCAshTextView, totalPointKD, totalPointUsed, paybyKnetCaption, u3anChargeText, u3anchargeText1, pointUsedText1;

	String paymentMethod = "1";

	Boolean checkBoolean;
	Boolean paymentOptionCash = true;
	Boolean paymentOptionknet = false;
	Boolean paymentOptionVisa = false;
	Boolean paymentOptionorengeEclipse = false;

	String payByCash, payByCredit, payByKNET, payByU3anCredit;

	Boolean knetTag, visaTag, cashOndeliverFlag;
	LinearLayout creditLayout, knetLayout, visaLayout, cashOndeliveryLinlay, pointUsedLinLay,  
				 pointInBottomLinLay, pointSystemSelectionLinLay, u3anChargeLay, u3anPointHeadLay;
	//HorizontalScrollView pointSystemSelectionLinLay;
	View u3anPointDivider1,u3anPointDivider2;
	Boolean defaultdeliveryFlag = false;
	Boolean deliveryFailed = false;
	
	String maximumPoint, minimunPoint, pointInKD, totalPoint, u3anCharge;
	String grandTotalKnet, grandTotalPayByCash, grandTotal;
	
	EditText pointUsedText;
	int pointEntered, pointEnteredValue = 0;
	double floatGrandTotal, floatPayByCashAll, tempFloatPayByCashAll;
	int totalPt1;
	double u3anChargeValue =0.0d;
		
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	DecimalFormat df = (DecimalFormat)nf;
	public ConfirmOrderFragment(ArrayList<HashMap<String, String>> arrayList) {
		this.arraylist = arrayList;
		createJSONArray(arraylist);
	}

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.confirmyourorder, gp, false);
		setupUI(v, false);

		userName = getArguments().getString("UserName");
		firstName = getArguments().getString("firstName");
		email = getArguments().getString("email");
		block = getArguments().getString("block");
		streetString = getArguments().getString("streetString");
		juddaString = getArguments().getString("juddaString");
		buildinString = getArguments().getString("buildinString");
		floorString = getArguments().getString("floorString");
		apartmentString = getArguments().getString("apartmentString");
		directionString = getArguments().getString("directionString");
		addressType = getArguments().getString("address_Type");

		subTotal = getArguments().getString("subTotal");
		deliveryCharges = getArguments().getString("deliveryCharges");
		GrandTotal = getArguments().getString("GrandTotal");
		payByCashAll = getArguments().getString("payByCashAll");
		discountAll = getArguments().getString("discountAll");
		
		minimunPoint = getArguments().getString("minPoint");
		maximumPoint = getArguments().getString("maxPoint");
		pointInKD = getArguments().getString("pointKD");
		totalPoint = getArguments().getString("totalPoint");
		u3anCharge = getArguments().getString("u3anCharges");
		
		
		lastName = getArguments().getString("lastName");
		mobileNumber = getArguments().getString("mobile");

		housePhone = getArguments().getString("housephone");
		streetString = getArguments().getString("Workphone");
		company = getArguments().getString("company");
		streetString = getArguments().getString("street");
		areaId = getArguments().getString("areID");

		knetTag = getArguments().getBoolean("KNET");
		visaTag = getArguments().getBoolean("VISA");
		grandTotal = GrandTotal;
		setTitleForPage(getResources().getString(R.string.confirm_order_heading));
		Initview(v);
		return v;
	}

	private void createJSONArray(ArrayList<HashMap<String, String>> arrayList) {
		JSONArray jArray = new JSONArray();
		for (int i = 0; i < arrayList.size(); i++) {
			HashMap<String, String> map = arrayList.get(i);
			JSONObject styleJSON = new JSONObject();
			try {
				String itemchoiceids = map.get("ItemChoiceIds");
				if (itemchoiceids.equals("null")) {
					itemchoiceids = " ";
				}
				areaID = map.get("AreaId");
				styleJSON.put("AreaId", map.get("AreaId"));
				styleJSON.put("Deliverytime", map.get("Delivery time"));
				styleJSON.put("GeneralRequest", map.get("General Request"));
				styleJSON.put("ItemChoiceIds", itemchoiceids);
				styleJSON.put("ItemChoiceQty", map.get("Quantity"));
				styleJSON.put("ItemId", map.get("ItemId"));
				styleJSON.put("PaymentMethod", map.get("1"));
				styleJSON.put("Price", map.get("ItemPrice"));
				styleJSON.put("Quantity", map.get("Quantity"));
				styleJSON.put("RestaurantId", map.get("RestaurantId"));
				styleJSON.put("SpecialRequest", map.get("Special Request"));
				
				productArray.put(styleJSON);
			} catch (Exception e) {
				Log.e("Caught an exception",""+ e.getMessage());
			}
		}
		Log.e("The data received", ""+productArray.toString());
	}

	private void Initview(View v) {
		
		df.applyPattern("###,##0.000");
		cashOndeliveryLinlay = (LinearLayout) v
				.findViewById(R.id.linlaycashondelivery);
		knetLayout = (LinearLayout) v.findViewById(R.id.linlayknet);
		visaLayout = (LinearLayout) v.findViewById(R.id.linlaycc);
		creditLayout = (LinearLayout) v.findViewById(R.id.linlaycredit);
		pointUsedLinLay = (LinearLayout) v.findViewById(R.id.containerPointUsed);
		pointInBottomLinLay = (LinearLayout) v.findViewById(R.id.pointingLinLay);
		pointSystemSelectionLinLay = (LinearLayout) v.findViewById(R.id.horizontalScrollView1);
		u3anPointHeadLay = (LinearLayout)v.findViewById(R.id.u3an_point_head11);
		u3anPointDivider1 =v.findViewById(R.id.u3an_point_divider1);
		u3anPointDivider2 =v.findViewById(R.id.u3an_point_divider2);
		u3anChargeLay = (LinearLayout)v.findViewById(R.id.u3an_charge_lay);
		paybyKnetCaption = (TextView) v.findViewById(R.id.subtottal4);
		textDescription = (WebView) v.findViewById(R.id.webViewPointDescription);
		/*if(PrefUtil.getAppLanguage(Activity).equals("en")){
			
		}else{
			Log.d("Horizontal Scroll view", "entered");
			pointSystemSelectionLinLay.postDelayed(new Runnable() {

	            @Override
	            public void run() {
	            	pointSystemSelectionLinLay.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
	            	
	           }
	        }, 10);
			//pointSystemSelectionLinLay.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
			
		}*/
		/*Log.d("hiii", "Total Point:"+Integer.parseInt(totalPoint));
		Log.d("hiii", "Minimum Point:"+Integer.parseInt(minimunPoint));*/
		if(!(Integer.parseInt(totalPoint)>=Integer.parseInt(minimunPoint))){
			pointSystemSelectionLinLay.setVisibility(View.GONE);
			pointUsedLinLay.setVisibility(View.GONE);
			pointInBottomLinLay.setVisibility(View.GONE);
			u3anPointHeadLay.setVisibility(View.GONE);
			u3anPointDivider1.setVisibility(View.GONE);
			u3anPointDivider2.setVisibility(View.GONE);
		}
		
		//pointSystemSelectionLinLay.scrollBy(20, 20);
		TextView discountTextView = (TextView) v.findViewById(R.id.discount21);
		totalPointKD = (TextView) v.findViewById(R.id.totalPointInKd);
		pointUsedText = (EditText) v.findViewById(R.id.etPointUsed);
		pointUsedText.setText(totalPoint);
// Add a method for it		
		int totalPt = Integer.parseInt(totalPoint);
		double ptInKD = Double.parseDouble(pointInKD);
		//pointEntered = Integer.parseInt(pointUsedText.getText().toString().trim());
		
		pointUsedText.setEnabled(false);
		pointUsedLinLay.setVisibility(View.GONE);
		pointInBottomLinLay.setVisibility(View.GONE);
		
				
// Anju
		//cashOndeliveryLinlay.setVisibility(View.GONE);
		//cashOndeliverFlag = false;
		if (Utilities.doemailValidation(userName)) {
			cashOndeliveryLinlay.setVisibility(View.VISIBLE);
			cashOndeliverFlag = true;
		} else {
			cashOndeliveryLinlay.setVisibility(View.VISIBLE);
			cashOndeliverFlag = true;
		}
		Log.e("The Discount all",""+ discountAll);

		float data = Float.parseFloat(discountAll);

		Log.e("The dataaaa", ""+String.format("%.2f", data));

		discountTextView.setText(getResources().getString(R.string.KD) +" "+ df.format(data));

		if (knetTag) {
		} else {
			knetLayout.setVisibility(View.GONE);
		}

		if (visaTag) {
		} else {
			visaLayout.setVisibility(View.GONE);
		}

		if (Utilities.doemailValidation(userName)) {
			String credit = PrefUtil.getU3Ancredit(Activity);
			if (credit.length() > 0) {
				creditLayout.setVisibility(View.VISIBLE);
			} else {
				creditLayout.setVisibility(View.GONE);
			}
		} else {
			creditLayout.setVisibility(View.GONE);
		}

		cash = (RadioButton) v.findViewById(R.id.confordRadioCash);
		knet = (RadioButton) v.findViewById(R.id.confordRadioKNET);
		visa = (RadioButton) v.findViewById(R.id.confordRadioCC);
		eclipse = (RadioButton) v.findViewById(R.id.confordRadioorenge);
		
		fullPoint = (RadioButton) v.findViewById(R.id.radioFullPont);
		myPoint = (RadioButton) v.findViewById(R.id.radioMyPont);
		noPoint = (RadioButton) v.findViewById(R.id.radioNoPoint);
		
		cash.setOnClickListener(this);
		knet.setOnClickListener(this);
		visa.setOnClickListener(this);
		eclipse.setOnClickListener(this);
// to do as guest		
		if (Utilities.doemailValidation(userName)){
			fullPoint.setOnClickListener(this);
			myPoint.setOnClickListener(this);
			noPoint.setOnClickListener(this);
			WebSettings webSettings = textDescription.getSettings();
			webSettings.setDefaultFontSize(13);
			String htmlText = "<html><body style=\"text-align:justify\"> %s </body></Html>";
			String htmlText1 = "<html dir=\"rtl\"><body style=\"text-align:justify\">"+ getResources().getString(R.string.u3an_point_description) +"</body></Html>";
			String myData = getResources().getString(R.string.u3an_point_description);
			if(PrefUtil.getAppLanguage(Activity).equals(AppConstants.STR_EN))
				textDescription.loadData(String.format(htmlText, getResources().getString(R.string.u3an_point_description)), "text/html", "utf-8");
			else
				textDescription.loadData(htmlText1, "text/html; charset=utf-8" ,"utf-8");
		}else{
			pointSystemSelectionLinLay.setVisibility(View.GONE);
			pointUsedLinLay.setVisibility(View.GONE);
			pointInBottomLinLay.setVisibility(View.GONE);
			u3anPointHeadLay.setVisibility(View.GONE);
			u3anPointDivider1.setVisibility(View.GONE);
			u3anPointDivider2.setVisibility(View.GONE);
			textDescription.setVisibility(View.GONE);
		}
		
// Jijo		
		confirm_orderButton = (Button) v.findViewById(R.id.confirmorderButton);
		confirm_orderButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Log.d("Confirm Order", "Point Entered:"+pointEntered);
				Log.d("Confirm Order", "UserName"+userName);
				Log.d("Radio button Auto:", ""+fullPoint.isChecked());
				Log.d("Radio button Not:", ""+noPoint.isChecked());
				if(!(Utilities.doemailValidation(userName))){
					Log.d("Guest", "entered");
					pointEntered = 0;
					goToConfirmOrder();
				}
				else if(!fullPoint.isChecked()&&!noPoint.isChecked()&&pointEntered==0){
					//Toast.makeText(Activity, getResources().getString(R.string.notify_enter_point), Toast.LENGTH_LONG).show();
					Utilities.show(getActivity(),getResources().getString(R.string.notify_enter_point),getResources().getString(R.string.pls_note),okClicked);
					totalPointKD.setText(getResources().getString(R.string.KD)+" "
							+ df.format(0.00));
					totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
					payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(tempFloatPayByCashAll));
					grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(tempFloatPayByCashAll));
				}
				else if(fullPoint.isChecked()||myPoint.isChecked()){
					if(fullPoint.isChecked()&& pointEntered!=0){
						if(!(Integer.parseInt(totalPoint)<Integer.parseInt(minimunPoint)) && !(pointEntered>Integer.parseInt(maximumPoint))){
							goToConfirmOrder();
						}else{
							pointEntered = 0;
							Utilities.show(getActivity(),getResources().getString(R.string.notify_point_status),getResources().getString(R.string.pls_note),okClicked);
							//Toast.makeText(Activity, getResources().getString(R.string.notify_point_status), Toast.LENGTH_LONG).show();
					// use a method	
						//	backtrackToPreviousStage();
							totalPointKD.setText(getResources().getString(R.string.KD)+" "
									+ df.format(0.00));
							totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
							payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
									+ df.format(tempFloatPayByCashAll));
							grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
									+ df.format(tempFloatPayByCashAll));
							/*myPoint.setChecked(false);
							noPoint.setChecked(true);
							fullPoint.setChecked(false);
							
							pointUsedLinLay.setVisibility(View.GONE);
							pointInBottomLinLay.setVisibility(View.GONE);*/
							
						}
						
					}else if(myPoint.isChecked() && pointEntered!=0){
						Log.d("Confirm Order", "Point Entered111:"+pointEntered);
							/*if(pointEntered<Integer.parseInt(minimunPoint)){
								Toast.makeText(Activity, getResources().getString(R.string.notify_min_point)+" "+minimunPoint+" "+getResources().getString(R.string.to_continue), Toast.LENGTH_LONG).show();
								pointUsedText.setText("");
								totalPointKD.setText(getResources().getString(R.string.KD)+" "
										+ df.format(0.00));
								totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								
							}*/if(pointEntered>Integer.parseInt(maximumPoint)){
								Utilities.show(getActivity(),getResources().getString(R.string.notify_max_point)+" "+maximumPoint,getResources().getString(R.string.pls_note),okClicked);
								//Toast.makeText(Activity, getResources().getString(R.string.notify_max_point)+" "+maximumPoint, Toast.LENGTH_LONG).show();
								pointUsedText.setText("");
								totalPointKD.setText(getResources().getString(R.string.KD)+ " "
										+ df.format(0.00));
								totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
							}else if(pointEntered>Integer.parseInt(totalPoint)){
								Utilities.show(getActivity(),getResources().getString(R.string.notify_avail_point),getResources().getString(R.string.pls_note),okClicked);
								//Toast.makeText(Activity, getResources().getString(R.string.notify_avail_point), Toast.LENGTH_LONG).show();
								pointUsedText.setText("");
								totalPointKD.setText(getResources().getString(R.string.KD)+" "
										+ df.format(0.00));
								totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
							}
							else if(pointEntered==0){
								Toast.makeText(Activity, getResources().getString(R.string.notify_enter_point), Toast.LENGTH_LONG).show();
								totalPointKD.setText(getResources().getString(R.string.KD)+" "
										+ df.format(0.00));
								totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								
							}
							else{
								if(!(pointUsedText.getText().toString().equals(""))){
									goToConfirmOrder();
									Log.d("jijoooooo....if", ""+pointEntered);
								}
								else{
									pointEntered = 0;
									Toast.makeText(Activity, getResources().getString(R.string.notify_enter_point), Toast.LENGTH_LONG).show();
									totalPointKD.setText(getResources().getString(R.string.KD)+ " "
											+ df.format(0.00));
									totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
									payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
									grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
									Log.d("jijoooooo....else", ""+pointEntered);
								}
							}
					}else if(pointEntered<Integer.parseInt(maximumPoint)&& !(pointEntered>Integer.parseInt(totalPoint))){
						if(!(pointUsedText.getText().toString().equals("")))
							goToConfirmOrder();
						else{
							pointEntered = 0;
							Utilities.show(getActivity(),getResources().getString(R.string.notify_enter_point),getResources().getString(R.string.pls_note),okClicked);
							//Toast.makeText(Activity, getResources().getString(R.string.notify_enter_point), Toast.LENGTH_LONG).show();
							totalPointKD.setText(getResources().getString(R.string.KD)+" "
									+ df.format(0.00));
							totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
							payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
									+ df.format(tempFloatPayByCashAll));
							grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
									+ df.format(tempFloatPayByCashAll));
							Log.d("jijoooooo....else", ""+pointEntered);
						}
							
					}
					
				}/*else if((pointUsedText.getText().toString().equals(""))){
					pointEntered = 0;
					Toast.makeText(Activity, getResources().getString(R.string.notify_enter_point), Toast.LENGTH_LONG).show();
					totalPointKD.setText(getResources().getString(R.string.KD)
							+ df.format(0.00));
					totalPointUsed.setText(getResources().getString(R.string.KD)+df.format(0.00));
					payByCAshTextView.setText(getResources().getString(R.string.KD)
							+ df.format(tempFloatPayByCashAll));
					grandTotalTextView.setText(getResources().getString(R.string.KD)
							+ df.format(tempFloatPayByCashAll));
				}*/
				else if(!(Utilities.doemailValidation(userName))){
					Log.d("Guest", "entered");
					pointEntered = 0;
					goToConfirmOrder();
				}
			
				else{
					Log.d("jijooooooo.....", "Not required enter::"+GrandTotal);
					pointEntered = 0;
					if(noPoint.isChecked()){
						if(u3anChargeValue>0){
							double grand = Double.parseDouble(grandTotal);
							double grandValue = grand+u3anChargeValue;
							double grandtotal1 =  new BigDecimal(grandValue).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
							GrandTotal = String.valueOf(grandtotal1);
							
						}else{
							GrandTotal = grandTotal;
							
						}
					}
					
					goToConfirmOrder();
				}
				
			}
			
		});

		subTotalTextView = (TextView) v.findViewById(R.id.price1);
		deliveryChargesTextView = (TextView) v.findViewById(R.id.price2);
		grandTotalTextView = (TextView) v.findViewById(R.id.price3);
		payByCAshTextView = (TextView) v.findViewById(R.id.price4);
		totalPointUsed = (TextView) v.findViewById(R.id.myPointUsedValue);
		u3anChargeText = (TextView) v.findViewById(R.id.u3ancharge1);
		u3anchargeText1 = (TextView)v.findViewById(R.id.u3ancharge);
		pointUsedText1 = (TextView)v.findViewById(R.id.myPointUsed);
		u3anchargeText1.setSelected(true);
		pointUsedText1.setSelected(true);
		
		double floatSubTotal = Double.parseDouble(subTotal);
		double floatDeliveryCharges = Double.parseDouble(deliveryCharges);
		u3anChargeValue = Double.parseDouble(u3anCharge);
		floatGrandTotal = Double.parseDouble(GrandTotal);
		floatPayByCashAll = Double.parseDouble(payByCashAll);
		if(u3anChargeValue>0){
			tempFloatPayByCashAll = floatPayByCashAll+u3anChargeValue;
		}else{
			tempFloatPayByCashAll = floatPayByCashAll;
		}
		
		
		
		if(u3anChargeValue>0){
			u3anChargeLay.setVisibility(View.VISIBLE);
			u3anChargeText.setText(getResources().getString(R.string.KD)+" "
				+ df.format(u3anChargeValue));
			
		}
		/*double pointUsedInKD = new BigDecimal(totalPt * ptInKD).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		totalPointKD.setText("KD "+String.valueOf(pointUsedInKD));
		floatPayByCashAll = (float) (floatGrandTotal - pointUsedInKD);
		payByCAshTextView.setText(getResources().getString(R.string.KD)
				+ df.format(floatPayByCashAll));
		grandTotalTextView.setText(getResources().getString(R.string.KD)
				+ df.format(floatPayByCashAll));*/
		pointEntered = totalPt;
		double pointUsedInKD = new BigDecimal(totalPt * ptInKD).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
		totalPointKD.setText(getResources().getString(R.string.KD)+ " "
				+ df.format(pointUsedInKD));
		totalPointUsed.setText(getResources().getString(R.string.KD)+df.format(pointUsedInKD));
		floatPayByCashAll = (float) (floatGrandTotal - pointUsedInKD)+u3anChargeValue;
		payByCAshTextView.setText(getResources().getString(R.string.KD)+ " "
				+ df.format(tempFloatPayByCashAll));
		grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
				+ df.format(tempFloatPayByCashAll));
	

		subTotalTextView.setText(getResources().getString(R.string.KD)+" "
				+ df.format(floatSubTotal));
		deliveryChargesTextView.setText(getResources().getString(R.string.KD)+" "
				+ df.format(floatDeliveryCharges));
		
		
		//grandTotalTextView.setText(getResources().getString(R.string.KD)
				//+ df.format(floatGrandTotal));
		//payByCAshTextView.setText(getResources().getString(R.string.KD)
				//+ df.format(floatPayByCashAll));

		if (!cashOndeliverFlag) {
			if (!knetTag) {
				if (!visaTag) {
					deliveryFailed = true;					
				}
			}
		}
	}
	
	private void goToConfirmOrder(){
		if (deliveryFailed) {
			Toast.makeText(
					Activity,
					R.string.no_delivery_option,
					Toast.LENGTH_SHORT).show();
		}
		else{
		if (paymentMethod.equals("1"))
			payByCash = GrandTotal;
		if (paymentMethod.equals("2"))
			payByCredit = GrandTotal;
		if (paymentMethod.equals("3"))
			payByKNET = GrandTotal;
		if (paymentMethod.equals("4"))
			payByU3anCredit = GrandTotal;
		Log.d("Json Put.....", "PointUsed:"+pointEntered);
		AccumulateJSONOnject();
		}
	}

	@Override
	public void onClick(View v) {
		boolean checked = ((RadioButton) v).isChecked();
		switch (v.getId()) {
		case R.id.confordRadioCash:
			paymentOptionCash = true;
			paymentOptionknet = false;
			paymentOptionVisa = false;
			paymentOptionorengeEclipse = false;
			cash.setChecked(true);
			knet.setChecked(false);
			visa.setChecked(false);
			eclipse.setChecked(false);
			paymentMethod = "1";
			paybyKnetCaption.setText(getResources().getString(R.string.pay_by_cash_order));
			if (checked)
				Log.e("checked", "checked1");
			else {
			}
			break;
		case R.id.confordRadioKNET:
			paymentOptionCash = false;
			paymentOptionknet = true;
			paymentOptionVisa = false;
			paymentOptionorengeEclipse = false;
			paymentMethod = "3";
			knet.setChecked(true);
			cash.setChecked(false);
			visa.setChecked(false);
			eclipse.setChecked(false);
			paybyKnetCaption.setText(getResources().getString(R.string.pay_by_knet_order));
			if (checked)
				Log.e("checked", "checked2");
			break;
		case R.id.confordRadioCC:
			paymentOptionCash = false;
			paymentOptionknet = false;
			paymentOptionVisa = true;
			paymentOptionorengeEclipse = false;
			paymentMethod = "2";
			visa.setChecked(true);
			knet.setChecked(false);
			cash.setChecked(false);
			eclipse.setChecked(false);
			if (checked)
				Log.e("checked", "checked3");
			break;
		case R.id.confordRadioorenge:
			paymentOptionCash = false;
			paymentOptionknet = false;
			paymentOptionVisa = false;
			paymentOptionorengeEclipse = true;
			paymentMethod = "4";
			visa.setChecked(false);
			knet.setChecked(false);
			cash.setChecked(false);
			eclipse.setChecked(true);
			if (checked)
				Log.e("checked", "checked4");
			break;
		case R.id.radioFullPont:
			myPoint.setChecked(false);
			noPoint.setChecked(false);
			fullPoint.setChecked(true);
			pointUsedLinLay.setVisibility(View.VISIBLE);
			pointInBottomLinLay.setVisibility(View.VISIBLE);
			pointUsedText.setText(totalPoint);
			pointUsedText.setEnabled(false);
//Add a method for calculating point
			int totalPt = Integer.parseInt(totalPoint);
			double ptInKD = Double.parseDouble(pointInKD);
			//pointEntered = Integer.parseInt(pointUsedText.getText().toString().trim());
			if(totalPt<Integer.parseInt(minimunPoint)){
				Toast.makeText(Activity, getResources().getString(R.string.notify_point_status)+","+getResources().getString(R.string.you_have)+" "+totalPt+" "+getResources().getString(R.string.your_wallet), Toast.LENGTH_LONG).show();
				pointEntered = 0;
				pointInBottomLinLay.setVisibility(View.GONE);
				pointUsedLinLay.setVisibility(View.GONE);
				noPoint.setChecked(true);
				myPoint.setChecked(false);
				fullPoint.setChecked(false);
				
			}else{
				pointEntered = totalPt;
				double pointUsedInKD = new BigDecimal(totalPt * ptInKD).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
				int pointNeed = 0;
		// Point is greater than pay by cash
				if(pointUsedInKD>tempFloatPayByCashAll){
					int max = Integer.parseInt(maximumPoint);
					for(int i =1; i<=max; i++){
						//double totalPointVal = i * ptInKD;
						if(i * ptInKD == tempFloatPayByCashAll ){
							pointNeed = i;
							Log.d("Pay by cash:::", ""+tempFloatPayByCashAll);
							Log.d("Point Round Cash:::", ""+i * ptInKD);
							Log.d("Point Need:::", ""+pointNeed);
							break;
						}
					}
					
					pointUsedText.setText(String.valueOf(pointNeed));
					double pointUsedInKD1 = new BigDecimal(pointNeed * ptInKD).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
					totalPointKD.setText(getResources().getString(R.string.KD)+" "
							+ df.format(pointUsedInKD1));
					totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format((pointUsedInKD1)));
					floatPayByCashAll = (double) (floatGrandTotal - pointUsedInKD1)+u3anChargeValue;
					payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(0.00));
					grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(0.00));
					GrandTotal = String.valueOf(0.00);
					
					
				}else{
					totalPointKD.setText(getResources().getString(R.string.KD)+" "
							+ df.format(pointUsedInKD));
					totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format((pointUsedInKD)));
					floatPayByCashAll = (double) (floatGrandTotal - pointUsedInKD)+u3anChargeValue;
					payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(floatPayByCashAll));
					grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(floatPayByCashAll));
					GrandTotal = String.valueOf(floatPayByCashAll);
				}
				
			}
			
			break;
		case R.id.radioMyPont:
			noPoint.setChecked(false);
			fullPoint.setChecked(false);
			myPoint.setChecked(true);
			pointUsedLinLay.setVisibility(View.VISIBLE);
			pointInBottomLinLay.setVisibility(View.VISIBLE);
			pointUsedText.setFocusable(true);
			pointUsedText.requestFocus();
			pointUsedText.setText("");
			totalPointKD.setText("");
			Log.d("Maximum Points......", ""+maximumPoint);
			totalPt1 = Integer.parseInt(totalPoint);
			try {
				if(!(Integer.parseInt(totalPoint)<Integer.parseInt(minimunPoint))){
					
					double pointUsedInKD = new BigDecimal(totalPt1 *  Double.parseDouble(pointInKD)).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
					int pointNeed = 0;
			// Point is greater than pay by cash
					if(pointUsedInKD>tempFloatPayByCashAll){
						int max = Integer.parseInt(maximumPoint);
						for(int i =1; i<=max; i++){
							//double totalPointVal = i * ptInKD;
							if(i *  Double.parseDouble(pointInKD) == tempFloatPayByCashAll ){
								pointNeed = i;
								Log.d("Pay by cash:::", ""+tempFloatPayByCashAll);
								Log.d("Point Round Cash:::", ""+i *  Double.parseDouble(pointInKD));
								Log.d("Point Need:::", ""+pointNeed);
								break;
							}
						}
						pointUsedText.setEnabled(true);
						pointUsedText.setFocusable(true);
						pointUsedText.requestFocus();
						pointUsedText.setText("");
						pointUsedText.append(String.valueOf(pointNeed));
						//pointUsedText.setText(String.valueOf(pointNeed));
						double pointUsedInKD1 = new BigDecimal(pointNeed *  Double.parseDouble(pointInKD)).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
						totalPointKD.setText(getResources().getString(R.string.KD)+" "
								+ df.format(pointUsedInKD1));
						totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format((pointUsedInKD1)));
						floatPayByCashAll = (double) (floatGrandTotal - pointUsedInKD1)+u3anChargeValue;
						Log.d("Grand total as pay by cash", ""+floatPayByCashAll);
						payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
								+ df.format(0.00));
						grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
								+ df.format(0.00));
						GrandTotal = String.valueOf(0.00);
						
						
					}else{
						pointUsedText.setEnabled(true);
						pointUsedText.setFocusable(true);
						//pointUsedText.setSelection();
						pointUsedText.requestFocus();
						//pointUsedText.setText(totalPoint);
						pointUsedText.setText("");
						pointUsedText.append(totalPoint);
						double pointUsedInKD1 = new BigDecimal(totalPt1 *  Double.parseDouble(pointInKD)).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
						totalPointKD.setText(getResources().getString(R.string.KD)+" "
								+ df.format(pointUsedInKD1));
						totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format((pointUsedInKD1)));
						floatPayByCashAll = (double) (floatGrandTotal - pointUsedInKD1)+u3anChargeValue;
						Log.d("Grand total as pay by cash", ""+floatPayByCashAll);
						payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
								+ df.format(floatPayByCashAll));
						grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
								+ df.format(floatPayByCashAll));
						GrandTotal = String.valueOf(floatPayByCashAll);
						
					}
					
					
					/*pointUsedText.setEnabled(true);
					totalPointKD.setText(getResources().getString(R.string.KD)+ " "
							+ df.format(0.00));
					totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
									
					payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(tempFloatPayByCashAll));
					grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(tempFloatPayByCashAll));*/
					pointUsedText.addTextChangedListener(watch);
					Log.d("TextWatcherrrrr Changing Listener", "hiii");
					//pointUsedText.clearFocus();
					
					pointUsedText.setOnFocusChangeListener(new OnFocusChangeListener() {
						
						@Override
						public void onFocusChange(View v, boolean hasFocus) {
							// TODO Auto-generated method stub

						     /* if (!hasFocus) {
						    	  Log.d("Has Focus", "hiii:-"+hasFocus);
						    	  if(pointEntered==0){
										Toast.makeText(Activity, getResources().getString(R.string.notify_enter_point), Toast.LENGTH_LONG).show();
										totalPointKD.setText(getResources().getString(R.string.KD)+" "
												+ df.format(0.00));
										totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
										payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
												+ df.format(tempFloatPayByCashAll));
										grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
												+ df.format(tempFloatPayByCashAll));
									}
						      }*/
							
						}
					});
					
				}else{
					
						Toast.makeText(Activity, getResources().getString(R.string.notify_point_status), Toast.LENGTH_LONG).show();
						pointEntered = 0;
						pointInBottomLinLay.setVisibility(View.GONE);
						pointUsedLinLay.setVisibility(View.GONE);
						noPoint.setChecked(true);
						myPoint.setChecked(false);
						fullPoint.setChecked(false);
					
					
			// use a method		
					totalPointKD.setText(getResources().getString(R.string.KD)+ " "
							+ df.format(0.00));
					totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
					payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(tempFloatPayByCashAll));
					grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
							+ df.format(tempFloatPayByCashAll));
					GrandTotal = String.valueOf(tempFloatPayByCashAll);
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
			
			
			
			
			/*if(!(totalPt1<Integer.parseInt(minimunPoint))){
				totalPointKD.setText(getResources().getString(R.string.KD)
						+ df.format(0.00));
				totalPointUsed.setText("KD "+String.valueOf(0.00));
				payByCAshTextView.setText(getResources().getString(R.string.KD)
						+ df.format(tempFloatPayByCashAll));
				grandTotalTextView.setText(getResources().getString(R.string.KD)
						+ df.format(tempFloatPayByCashAll));
				
				double pointUsedInKDValue;
				
				pointUsedText.setOnKeyListener(new OnKeyListener() {
					public boolean onKey(View v, int keyCode, KeyEvent event) {
						// TODO Auto-generated method stub
						if(event.getAction()!=KeyEvent.ACTION_UP){
							if(!(pointUsedText.getText().toString().trim().equals(""))){
								
							}else{
								totalPointKD.setText(getResources().getString(R.string.KD)
										+ df.format(0.00));
								totalPointUsed.setText("KD "+String.valueOf(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)
										+ df.format(tempFloatPayByCashAll));
							}
							return false;
						}
							
						try {
							if(!(pointUsedText.getText().toString().trim().equals(""))){
								int totalPt = Integer.parseInt(totalPoint);
								double ptInKD = Double.parseDouble(pointInKD);
								pointEntered = Integer.parseInt(pointUsedText.getText().toString().trim());
								double pointUsedInKD = new BigDecimal(pointEntered * ptInKD).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();;
								totalPointKD.setText(getResources().getString(R.string.KD)
										+ df.format(pointUsedInKD));
								totalPointUsed.setText("KD "+String.valueOf(pointUsedInKD));
								floatPayByCashAll = (float) (floatGrandTotal - pointUsedInKD);
								payByCAshTextView.setText(getResources().getString(R.string.KD)
										+ df.format(floatPayByCashAll));
								grandTotalTextView.setText(getResources().getString(R.string.KD)
										+ df.format(floatPayByCashAll));
								Log.d("jijo:.....Listener in point", ""+pointUsedInKD);
							}else{
								totalPointKD.setText(getResources().getString(R.string.KD)
										+ df.format(0.00));
								totalPointUsed.setText("KD "+String.valueOf(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)
										+ df.format(tempFloatPayByCashAll));
								return false;
							}
							
							
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						pointEnteredValue = pointEntered;
						return true;
					}
					
				});
			}else{
				Toast.makeText(Activity, "You havn't enough point to redeem", Toast.LENGTH_LONG).show();
				pointUsedText.setEnabled(false);
			}*/
			
			break;
		case R.id.radioNoPoint:
			myPoint.setChecked(false);
			fullPoint.setChecked(false);
			noPoint.setChecked(true);
			pointUsedLinLay.setVisibility(View.GONE);
			pointInBottomLinLay.setVisibility(View.GONE);
			if(u3anChargeValue>0){
				double grand = Double.parseDouble(grandTotal);
				double grandValue = grand+u3anChargeValue;
				double grandtotal1 =  new BigDecimal(grandValue).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
				payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
						+ df.format(grandValue));
				grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
						+ df.format(grandValue));
				GrandTotal = String.valueOf(grandtotal1);
				Log.d("ghdhgdhgdhgdh", "Grand Value using No point:"+grandtotal1);
			}else{
				payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
						+ df.format(Double.parseDouble(grandTotal)));
				grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
						+ df.format(Double.parseDouble(grandTotal)));
				
			}
			
			break;
		default:
			break;
		}
	}
	
	TextWatcher watch = new TextWatcher(){
		 
	    @Override
	    public void afterTextChanged(Editable arg0) {
	        // TODO Auto-generated method stub
	    	    	
	    }
	 
	    @Override
	    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
	            int arg3) {
	        // TODO Auto-generated method stub
	    	/*totalPointKD.setText(getResources().getString(R.string.KD)+" "
					+ df.format(0.00));
			totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
			payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
					+ df.format(tempFloatPayByCashAll));
			grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
					+ df.format(tempFloatPayByCashAll));*/
	 
	    }
	 
	    @Override
	    public void onTextChanged(CharSequence s, int a, int b, int c) {
	        // TODO Auto-generated method stub
	 
	       // output.setText(s);
	    	if(!(totalPt1<Integer.parseInt(minimunPoint))){
				totalPointKD.setText(getResources().getString(R.string.KD)+" "
						+ df.format(0.00));
				totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
				payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
						+ df.format(tempFloatPayByCashAll));
				grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
						+ df.format(tempFloatPayByCashAll));
				
				double pointUsedInKDValue;
											
						try {
							if(!(pointUsedText.getText().toString().trim().equals(""))){
								if(Integer.parseInt(pointUsedText.getText().toString().trim())>totalPt1){
									//Toast.makeText(Activity, getResources().getString(R.string.notify_avail_point)+" "+ getResources().getString(R.string.notify_avail_point_value)+" "+totalPt1, Toast.LENGTH_LONG).show();
									Utilities.show(getActivity(),getResources().getString(R.string.notify_avail_point)+" "+ getResources().getString(R.string.notify_avail_point_value)+" "+totalPt1,"Please note",okClicked);	
									pointUsedText.setText("");
									totalPointKD.setText(getResources().getString(R.string.KD)+" "
											+ df.format(0.00));
									totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
									payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
									grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
									GrandTotal = String.valueOf(tempFloatPayByCashAll);
								}
								else if(Integer.parseInt(pointUsedText.getText().toString().trim())>1000){
									
									//Toast.makeText(Activity, getResources().getString(R.string.notify_max_point)+" "+maximumPoint, Toast.LENGTH_LONG).show();
									Utilities.show(getActivity(),getResources().getString(R.string.notify_max_point)+" "+maximumPoint,getResources().getString(R.string.pls_note),okClicked);	
									pointUsedText.setText("");
									totalPointKD.setText(getResources().getString(R.string.KD)+" "
											+ df.format(0.00));
									totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
									payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
									GrandTotal = String.valueOf(tempFloatPayByCashAll);
								
								
								}/*else if(Integer.parseInt(pointUsedText.getText().toString().trim())<Integer.parseInt(minimunPoint)){
									Toast.makeText(Activity, getResources().getString(R.string.notify_min_point)+" "+minimunPoint+" "+getResources().getString(R.string.to_continue), Toast.LENGTH_LONG).show();
									pointUsedText.setText("");
									totalPointKD.setText(getResources().getString(R.string.KD)+" "
											+ df.format(0.00));
									totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
									payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
									grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(tempFloatPayByCashAll));
								}*/
								else{
									int totalPt = Integer.parseInt(totalPoint);
									double ptInKD = Double.parseDouble(pointInKD);
									pointEntered = Integer.parseInt(pointUsedText.getText().toString().trim());
									double pointUsedInKD = new BigDecimal(pointEntered * ptInKD).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();;
									totalPointKD.setText(getResources().getString(R.string.KD)+" "
											+ df.format(pointUsedInKD));
									totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(pointUsedInKD));
									floatPayByCashAll = (double) (floatGrandTotal - pointUsedInKD)+u3anChargeValue;
									floatPayByCashAll =  new BigDecimal(floatPayByCashAll).setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
									//Log.i("U3an Pay by cash::::", ""+floatPayByCashAll);
									payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(floatPayByCashAll));
									grandTotalTextView.setText(getResources().getString(R.string.KD)+" "
											+ df.format(floatPayByCashAll));
									GrandTotal = String.valueOf(floatPayByCashAll);
									Log.d("jijo:.....point In KD", ""+pointUsedInKD);
									
								}
							}
							else{
								pointEntered = 0;
								totalPointKD.setText(getResources().getString(R.string.KD)+" "
										+ df.format(0.00));
								totalPointUsed.setText(getResources().getString(R.string.KD)+" "+df.format(0.00));
								payByCAshTextView.setText(getResources().getString(R.string.KD)+" "
										+ df.format(tempFloatPayByCashAll));
								GrandTotal = String.valueOf(tempFloatPayByCashAll);
								//return false;
							}
							
							
						} catch (Exception e) {
							// TODO: handle exception
							e.printStackTrace();
						}
						pointEnteredValue = pointEntered;
						//return true;
					
			}
	    	else{
	    		Log.d("dhgdhgdhgdhghdghdghdsh", "gdjghdsghd");
				Toast.makeText(Activity, getResources().getString(R.string.notify_point_status), Toast.LENGTH_LONG).show();
				pointUsedText.setEnabled(false);
			}
	    }};
	 
	
	
// Jijo 07-12-15
	private void AccumulateJSONOnject() {
		Log.d("Json Put.....", "PointUsed:"+pointEntered);
		Log.d("Json Put.....", "Pay by cash:"+payByCash);
		Log.d("Json Put.....", "Pay by knet:"+payByKNET);
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();
			jsonObject.accumulate("PaymentMode", paymentMethod);
			jsonObject.accumulate("UsedPoints",pointEntered);
			jsonObject.accumulate("addressType", addressType);
			jsonObject.accumulate("apartment", apartmentString);
			jsonObject.accumulate("areaId", areaId);
			jsonObject.accumulate("authKey", PrefUtil.getAuthkey(Activity));
			jsonObject.accumulate("block", block);
			jsonObject.accumulate("buildingNo", buildinString);
			jsonObject.accumulate("company", company);
			jsonObject.accumulate("discount", "");
			jsonObject.accumulate("email", email);
			jsonObject.accumulate("extraDirections", directionString);

			jsonObject.accumulate("firstname", firstName);
			jsonObject.accumulate("floor", floorString);
			jsonObject.accumulate("housephone", housePhone);
			jsonObject.accumulate("judda", juddaString);
			jsonObject.accumulate("lastname", lastName);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("mobile", mobileNumber);
			jsonObject.accumulate("orderSource", "Mobile application");
			jsonObject.put("orderXML", productArray);
			jsonObject.accumulate("payByCC", payByCredit);
			jsonObject.accumulate("payByCash", payByCash);
			jsonObject.accumulate("payByKNET", payByKNET);
			jsonObject.accumulate("payByU3an", payByU3anCredit);
			jsonObject.accumulate("street", streetString);
			jsonObject.accumulate("username", userName);
			jsonObject.accumulate("workphone", workphone);

		} catch (JSONException e) {
			e.printStackTrace();
		}


		Log.e("The data request",""+ jsonObject.toString());


		loadCuisineData(jsonObject);
	}

	private void loadCuisineData(JSONObject jsonObject) {
		Log.w("gdgdgdgdg Jijou",""+ jsonObject.toString());
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			if (Utilities.doemailValidation(userName)) {
				commandFactory.sendPostCommand(AppConstants.placeOrder_URL,
						jsonObject);
			} else {
				commandFactory.sendPostCommand(
						AppConstants.placeQuickOrder_URL, jsonObject);
			}
		}
	}

	@Override
	public void PlaceQuickSuccessfully(JSONObject jsonObject) {
		super.PlaceQuickSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("The data in PlaceQuickSuccessfully ", ""+jsonObject.toString());
		ParseData(jsonObject);
	}

	@Override
	public void PlaceQuickFailed(JSONObject jsonObject) {
		super.PlaceQuickFailed(jsonObject);
		Log.e("The data in QuickPlaceFailed ", ""+jsonObject.toString());
		Activity.stopSpinWheel();
	}

	@Override
	public void placeOrderSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.placeOrderSuccess(jsonObject);
		Log.e("The data placeOrderSuccess", ""+jsonObject.toString());
		ParseData(jsonObject);
	}

	@Override
	public void placeOrderFail(JSONObject jsonObject) {
		super.placeOrderFail(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity,
				getResources().getString(R.string.cannot_order),
				Toast.LENGTH_SHORT).show();
	}

	private void ParseData(JSONObject jsonObject) {
		String successStr = null;
		String value = null;
		try {
			successStr = jsonObject.getString("Status");
			value = jsonObject.getString("Value");
			Log.e("The data is",""+ successStr + value);
			if ((successStr.contains("Fail")) || (successStr.contains("fail"))) {
				Toast.makeText(Activity,
						getResources().getString(R.string.cannot_order),
						Toast.LENGTH_SHORT).show();
			} else {
				Fragment kentfragment = new PaymentKNETPage(arraylist, Activity);
				Bundle data = new Bundle();
				data.putString("Url", value);
				kentfragment.setArguments(data);
				Activity.pushFragments(kentfragment, false, true);
			}

		} catch (Exception e) {
			Log.e("The data is", ""+e.getMessage());
		}
	}
	private DialogInterface.OnClickListener okClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			//popFragments();
			//setLanguageSwitching();
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		if (Activity.readFile() == null || Activity.readFile().equals("")) {
			Fragment frag1 = new U3AnFoodCartFragment();
			Activity.popFragments(frag1);
			Activity.popFragments(new ConfirmOrderDeliveryDetailsFragment());
			Activity.popFragments(new ConfirmOrderSecondStage());
			Activity.popFragments(new ConfirmOrderFragment(arraylist));
		}
	}
}
