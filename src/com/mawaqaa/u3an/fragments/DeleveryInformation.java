package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.CountryAreaSpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Country;
import com.mawaqaa.u3an.data.SingleAreaCountry;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class DeleveryInformation extends HeaderViewControlFragment implements
		OnClickListener {
	RadioButton maleRadioButton, femaleRadioButton;
	private RadioGroup radioGroup;
	RadioGroup selectionRadioGroup;
	RadioButton selectedRadioButton;
	CountryAreaSpinnerAdapter spinneradapter;
	int maxLength=25;
	EditText profileNameEditText, blockEditText, juddaEditText, streetEditText,
			buildingNoEditText, floorEditText, apartmentEditText,
			directionsEditText;
	ImageButton villaButton, buildingButton, officeButton, nextButton;
	TextView villaTextView, buildingTextView, officeTextView;
	Spinner areaSpinner;
	LinearLayout apartmentLayout, floorLayout;
	String areaString, profileNameString, blockString, juddaString,
			streetString, houseNoString, floorString, apartmentString,
			directionsString, areaId;
	String areaID, AuthKey;
	EditText errorEditText;
	ArrayList<SingleAreaCountry> arraylist_country_data;
	Boolean villaFlag = true, buildingFlag = false, officeFlag = false;
	String address_Type = "0";
	String genderString;
	JSONObject areaJsonObject;
	public String FILENAMEAUTHKEY = "authentication_key.txt"  ;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.deleveryinformation, gp, false);
		View converterView = v
				.findViewById(R.id.linlay_secondstage);
		setTitleForPage(getResources().getString(R.string.add_address_heading));
		setupUI(converterView, false);

		init(v);
		fixUI();
		AuthKey = PrefUtil.getAuthkey(getActivity());
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		areaJsonObject = readDataFromFile(HomeFragment.FILENAMEAREADATA);
		ParseCountryData(areaJsonObject);
		return v;
	}

	private JSONObject readDataFromFile(String fileName) {
		JSONObject jsonObject = null;
		try {
			FileInputStream fis = Activity.openFileInput(fileName);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			String data = br.readLine();
			br.close();
			in.close();
			fis.close();
			if (data != null)
				jsonObject = new JSONObject(data);
		} catch (Exception e) {
		}
		return jsonObject;
	}

	private void init(View v) {
		profileNameEditText = (EditText) v
				.findViewById(R.id.register2_profile_name);
		profileNameEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						profileNameEditText));
		profileNameEditText
				.setOnFocusChangeListener(new FocusChangedListener());
		
		/*profileNameEditText.setFilters(new InputFilter[] {
			    new InputFilter.LengthFilter(25){
			        public CharSequence filter(CharSequence src, int start,
			                int end, Spanned dst, int dstart, int dend) {
			            if(src.equals("")){ // for backspace
			                return src;
			            }
			            if(src.toString().matches("[a-zA-Z ]+")){			                
			            	return src;
			            }
			            	return "";		            
			           
			        }
			    }
			});*/
		profileNameEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		blockEditText = (EditText) v
				.findViewById(R.id.register2_block);
		blockEditText
		.addTextChangedListener(new AddRestaurantTextwatcher(
				blockEditText));
		blockEditText
		.setOnFocusChangeListener(new FocusChangedListener());
		blockEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		juddaEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Judda);
		streetEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Street);
		streetEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				streetEditText));
		streetEditText.setOnFocusChangeListener(new FocusChangedListener());
		streetEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		buildingNoEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_House_No);
		buildingTextView = (TextView) v.findViewById(R.id.buildingTextview);
		buildingTextView.setOnClickListener(this);
		buildingNoEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		buildingNoEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				buildingNoEditText));
		buildingNoEditText.setOnFocusChangeListener(new FocusChangedListener());
		floorEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Floor);
		floorEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				floorEditText));
		floorEditText.setOnFocusChangeListener(new FocusChangedListener());
		floorEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		apartmentEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Apartment);
		apartmentEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				apartmentEditText));
		apartmentEditText.setOnFocusChangeListener(new FocusChangedListener());
		apartmentEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		directionsEditText = (EditText) v
				.findViewById(R.id.Edittext_register2_Directions);

		nextButton = (ImageButton) v.findViewById(R.id.nextBtnSecondStage);
		nextButton.setOnClickListener(this);

		areaSpinner = (Spinner) v.findViewById(R.id.Edittext_register2_Area);

		villaButton = (ImageButton) v.findViewById(R.id.villaRadioBtn);
		villaButton.setOnClickListener(this);

		officeButton = (ImageButton) v.findViewById(R.id.officeRadioBtn);
		officeButton.setOnClickListener(this);

		buildingButton = (ImageButton) v.findViewById(R.id.buildingRadioBtn);
		buildingButton.setOnClickListener(this);

		apartmentLayout = (LinearLayout) v.findViewById(R.id.linlayaperment);
		apartmentLayout.setOnClickListener(this);

		floorLayout = (LinearLayout) v.findViewById(R.id.linlayfloor);
		floorLayout.setOnClickListener(this);

		villaTextView = (TextView) v.findViewById(R.id.villaTextview);
		villaTextView.setOnClickListener(this);

		officeTextView = (TextView) v.findViewById(R.id.officeTextview);
		officeTextView.setOnClickListener(this);
	
	}

	private void fixUI() {
		if (villaFlag == true) {
			villaButton.setBackgroundResource(R.drawable.villahouse2);
		} else {
			villaButton.setBackgroundResource(R.drawable.villahouse1);
		}

		if (officeFlag == true) {
			officeButton.setBackgroundResource(R.drawable.office2);
		} else {
			officeButton.setBackgroundResource(R.drawable.office1);
		}

		if (buildingFlag == true) {
			buildingButton.setBackgroundResource(R.drawable.building2);
		} else {
			buildingButton.setBackgroundResource(R.drawable.building1);
		}
	}

	private void addAreas2SpinnerArrayList(Country country) {
		SingleAreaCountry areaCountry = new SingleAreaCountry();
		// for showing city Name in Spinner
		areaCountry.areaName = "-- " + country.cityName + " --";
		areaCountry.isCityName = true;
		arraylist_country_data.add(areaCountry);
		// adding all areas in the city
		arraylist_country_data.addAll(country.areas);
	}

	private void ParseCountryData(JSONObject jsonObject) {
		// Create an array
		arraylist_country_data = new ArrayList<SingleAreaCountry>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.areas_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.areas_json_obj);

				for (int i = 0; i < jsonarray.length(); i++) {
					if (!jsonarray.isNull(i)) {
						jsonObject = jsonarray.getJSONObject(i);
						Country country = new Country(jsonObject);
						addAreas2SpinnerArrayList(country);
					}
				}
			}
			PopulateCountrySpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		}
	}

	private void PopulateCountrySpinner() {
		
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		areaSpinner.setAdapter(spinneradapter);
		/*
		 * int i = -1; Log.e("Area Id",areaId); Log.e("Area Name",areaString);
		 * for(SingleAreaCountry sa : arraylist_country_data) { ++i;
		 * //Log.e("Area Id in List",sa.areaId);
		 * Log.e("Area Nme in List",sa.areaName); //if(sa.areaId.equals(areaId))
		 * if(sa.areaName.equals(areaString)) break; }
		 * 
		 * areaSpinner.setSelection(i);
		 */
		int position = areaSpinner.getSelectedItemPosition();
		areaString = arraylist_country_data.get(position).areaName;
		// areaSelectedTextView.setText(areaString);

		areaSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				areaString = arraylist_country_data.get(position).areaName;

			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				
			}
		});

	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private class AddRestaurantTextwatcher implements TextWatcher {

		private EditText editText;

		AddRestaurantTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {
			/*
			 * switch (editText.getId()) { case R.id.firstname:
			 * DoFirstNameValidation();
			 * 
			 * break; case R.id.lastname: DoLastNameValidation(); break; case
			 * R.id.restaurant_name: DoRestaurantNameValidation(); break; case
			 * R.id.restaurant_address: DoRestaurantAddressValidation(); break;
			 * case R.id.telephone: DoPhoneNumberValidation(); break; case
			 * R.id.email: doEmailIdValidation(); break;
			 * 
			 * default: break; }
			 */
		}
	}

	private class FocusChangedListener implements OnFocusChangeListener {

		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {

				case R.id.register2_profile_name:
					doProfileNameValidation();
					break;
				case R.id.Edittext_register2_Street:
					doStreetValidation();
					break;
				case R.id.register2_block:
					doBlockValidation();
				
					break;
				case R.id.Edittext_register2_Floor:
					doFloorValidation();
					break;
				case R.id.Edittext_register2_Building:
					doHouseNumberValidation();
					break;

				default:
					break;
				}
			}/*
			 * else if (v != null && v instanceof EditText) { ((EditText)
			 * v).setError(null); }
			 */
		}
	}

	private Boolean doApartmentValidation() {
		String apartString = apartmentEditText.getText().toString();
		if (apartString.length() > 0) {
			return true;
		} else {
			apartmentEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_apartment_data)+"</font>"));
			errorEditText = apartmentEditText;
			apartmentEditText.addTextChangedListener(registerInputTextWatcher);
			// apartmentEditText.requestFocus();
			return false;
		}
	}

	public boolean doBlockValidation() {		
		String blckString = blockEditText.getText().toString();
		if (blckString.length() > 0) {
			return true;
		} else {
			blockEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_block)+"</font>"));
			errorEditText = blockEditText;
			blockEditText.addTextChangedListener(registerInputTextWatcher);
			// apartmentEditText.requestFocus();
			return false;
		}
	}

	private Boolean doFloorValidation() {
		String fString = floorEditText.getText().toString();
		if (fString.length() > 0) {
			return true;
		} else {
			floorEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_floor)+"</font>"));
			errorEditText = floorEditText;
			floorEditText.addTextChangedListener(registerInputTextWatcher);
			// floorEditText.requestFocus();
			return false;
		}
	}

	private Boolean doHouseNumberValidation() {
		houseNoString = buildingNoEditText.getText().toString();
		if (houseNoString.length() > 0) {
			return true;
		} else {
			buildingNoEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_building_num)+"e</font>"));
			errorEditText = buildingNoEditText;
			buildingNoEditText.addTextChangedListener(registerInputTextWatcher);
			// houseNoEditText.requestFocus();
			return false;
		}
	}

	private Boolean doStreetValidation() {
		String sString = streetEditText.getText().toString();
		if (sString.length() > 0) {
			return true;
		} else {
			streetEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_street)+"</font>"));
			errorEditText = streetEditText;
			streetEditText.addTextChangedListener(registerInputTextWatcher);
			// streetEditText.requestFocus();
			return false;
		}
	}

	private Boolean doAreaValidation() {

		int pos = areaSpinner.getSelectedItemPosition();
		SingleAreaCountry country = arraylist_country_data.get(pos);
		String strAreaId = country.areaId;
		areaID = strAreaId;
		if (strAreaId == null || strAreaId.equals(null)
				|| strAreaId.trim().equals("") || country.isCityName) {
			Toast.makeText(Activity, R.string.select_area,
					Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}
	}

	private Boolean doProfileNameValidation() {
		String profNameString = profileNameEditText.getText().toString();
		if (profNameString.length() > 0) {
			return true;
		} else {
			profileNameEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_name)+"</font>"));
			errorEditText = profileNameEditText;
			profileNameEditText
					.addTextChangedListener(registerInputTextWatcher);
			// profileNameEditText.requestFocus();
			return false;
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.nextBtnSecondStage:
			validateData();
			break;
		
		case R.id.villaRadioBtn:
			ClickActionForVilla();
			break;

		case R.id.buildingRadioBtn:
			clickActionForBuilding();
			break;

		case R.id.officeRadioBtn:
			clickActionForOffice();
			break;

		case R.id.villaTextview:
			ClickActionForVilla();
			break;

		case R.id.buildingTextview:
			clickActionForBuilding();
			break;

		case R.id.officeTextview:
			clickActionForOffice();
			break;

		default:
			break;
		}
	}

	private void validateData() {
		Log.e("next", ""+"next");
		boolean isvalid = doAreaValidation();
		Log.e("doAreaValidation...", ""+isvalid);
		if (!isvalid)
			return;
		
		isvalid=doBlockValidation();
		Log.e("doBlockValidation...", ""+isvalid);
		if (!isvalid) 
			return;
		
		isvalid = doProfileNameValidation();
		Log.e("doProfileNameValidation...", ""+isvalid);
		if (!isvalid)
			return;
		
		isvalid = doStreetValidation();
		Log.e("doStreetValidation...", ""+isvalid);
		if (!isvalid)
			return;

		isvalid = doBuildingValidation();
		if (!isvalid)
			return;

		else {
			if ((buildingFlag == true) || (officeFlag == true)) {
				doOptionalDataValidation();
			} else {
				floorString = "";
				apartmentString = "";
				doSubmitAction();
			}
		}
	}
private Boolean doBuildingValidation(){
	String profNameString = buildingNoEditText.getText().toString();
	if (profNameString.length() > 0) {
		return true;
	} else {
		buildingNoEditText
				.setError(Html
						.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_building_num)+"</font>"));
		errorEditText = buildingNoEditText;
		buildingNoEditText
				.addTextChangedListener(registerInputTextWatcher);
		// profileNameEditText.requestFocus();
		return false;
	}
}

	private void doOptionalDataValidation() {
		Boolean isvalid = doFloorValidation();
		Log.e("doFloorValidation...", ""+isvalid);
		if (!isvalid)
			return;
		isvalid = doApartmentValidation();
		Log.e("doApartmentValidation...", ""+isvalid);
		if (!isvalid)
			return;
		else {
			doSubmitAction();
		}
	}

	private void clickActionForOffice() {
		villaFlag = false;
		buildingFlag = false;
		officeFlag = true;
		address_Type = "2";
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		spinneradapter.notifyDataSetChanged();
		areaSpinner.setAdapter(spinneradapter);
		clearField();
		fixUI();
		visibilityControl("office");
	}

	private void clickActionForBuilding() {
		villaFlag = false;
		buildingFlag = true;
		officeFlag = false;
		address_Type = "1";
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		spinneradapter.notifyDataSetChanged();
		areaSpinner.setAdapter(spinneradapter);
		clearField();
		fixUI();
		visibilityControl("building");
	}

	private void ClickActionForVilla() {
		villaFlag = true;
		buildingFlag = false;
		officeFlag = false;
		address_Type = "0";
		clearField();
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		spinneradapter.notifyDataSetChanged();
		areaSpinner.setAdapter(spinneradapter);
		fixUI();
		visibilityControl("villa");
	}
	private void clearField() {		
		blockEditText.setText("");
		streetEditText.setText("");
		juddaEditText.setText("");
		buildingNoEditText.setText("");
		floorEditText.setText("");
		apartmentEditText.setText("");
		directionsEditText.setText("");
		profileNameEditText.setText("");
	}
	private void visibilityControl(String string) {
		switch (string) {
		case "villa":
			apartmentLayout.setVisibility(View.GONE);
			floorLayout.setVisibility(View.GONE);
			break;

		case "building":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;

		case "office":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}
	}

	private void doSubmitAction() {
		blockString = blockEditText.getText().toString();
		juddaString = juddaEditText.getText().toString();
		directionsString = directionsEditText.getText().toString();
		profileNameString=profileNameEditText.getText().toString();
		houseNoString=buildingNoEditText.getText().toString();
		apartmentString=apartmentEditText.getText().toString();
		floorString=floorEditText.getText().toString();
		streetString=streetEditText.getText().toString();
		AccumulateData();	
	}

	private void AccumulateData() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonobject = new JSONObject();
		try {
			jsonobject.accumulate("areaId", areaID);
			jsonobject.accumulate("authKey", AuthKey);
			jsonobject.accumulate("block", blockString);
			jsonobject.accumulate("buildingNo", houseNoString);
			jsonobject.accumulate("extraDirections", directionsString);
			jsonobject.accumulate("isPrimary", null);
			jsonobject.accumulate("judda", juddaString);
			jsonobject.accumulate("floor", floorString);
			if(sDefSystemLanguage.equals("en")){
				jsonobject.accumulate("locale", "en-US");
			}else{
				jsonobject.accumulate("locale", "ar-KW");
			}
			jsonobject.accumulate("profileName", profileNameString);
			jsonobject.accumulate("street", streetString);
			jsonobject.accumulate("suite",apartmentString );
			jsonobject.accumulate("type", address_Type);
			jsonobject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("jsonobject", "" + jsonobject.toString());
		LoadData(jsonobject);
	}

	private void LoadData(JSONObject jsonobject) {		
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.get_pagedetails_url);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.deleveryinformation,
					jsonobject);
		}
	}

	@Override
	public void LoaddeleverySuccessfully(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.LoaddeleverySuccessfully(jsonObject);
		Activity.popFragments(this);
		Log.e("jsonresponse", "" + jsonObject.toString());
		Toast.makeText(Activity, R.string.address_added_successfully, Toast.LENGTH_LONG)
				.show();
	}

	@Override
	public void Loaddeleveryfail(JSONObject jsonObject) {
		super.Loaddeleveryfail(jsonObject);
	}
}
