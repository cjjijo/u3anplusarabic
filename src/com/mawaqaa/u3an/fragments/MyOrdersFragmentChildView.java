package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MyOrdersSecondPageAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.interfaces.OnListItemClicked;
import com.mawaqaa.u3an.interfaces.OnRatingbarChanged;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MyOrdersFragmentChildView extends HeaderViewControlFragment implements OnItemClickListener {
	int pos;
	private ListView orderListview;
	private TextView date, total, area;
	private ArrayList<HashMap<String, String>> arraylist;
	private MyOrdersSecondPageAdapter adapter;
	private ArrayList<JSONObject> listItem;
	OnListItemClicked clickedItem;
	OnRatingbarChanged rateBarChecked;
	LogoutListener listener;
	public MyOrdersFragmentChildView(int position) {
		this.pos = position;
	}

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.my_oreders_secondpage, gp, false);
		setupUI(v, false);
		setTitleForPage(getString(R.string.my_accnt));
		initView(v);
		fetData();
		clickedItem=new OnListItemClicked() {
			
			@Override
			public void onItemClicked(int i) {
				try {
					
					JSONArray array = listItem.get(i).getJSONArray("Orders");
					Log.e("Orders array", "......" + array.length());
					if (array.length() > 0) {
						MyOrdersFragmentChildView2 frag = new MyOrdersFragmentChildView2(pos,i);
						Activity.pushFragments(frag, false, true);

					} else {
						Log.e("Orders array", "......" + i);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}

				
			}
		};
		rateBarChecked=new OnRatingbarChanged() {
			
			@Override
			public void ItemRatingComple(String type, String id, String count) {
				doRateBarChangedUpdate(type, id, count);
				
			}
		};
		return v;
	}

	private void fetData() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		try {
			JSONObject jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", Activity.readFile());
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.myOrdersUrl, jsonObject);
			}
			Log.e("My order params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	private void initView(View v) {
		orderListview = (ListView) v.findViewById(R.id.orderListView);
		date = (TextView) v.findViewById(R.id.date);
		total = (TextView) v.findViewById(R.id.total);
		area = (TextView) v.findViewById(R.id.area);
		orderListview.setOnItemClickListener(this);

	}

	@Override
	public void onMyOrdersLoadedSuccessfully(JSONObject jsonObject) {
		super.onMyOrdersLoadedSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("My order response", "" + jsonObject.toString());
		fetchmyOrders(jsonObject);
	}

	@Override
	public void onMyOrdersLoadFail(JSONObject jsonObject) {
		super.onMyOrdersLoadFail(jsonObject);
		Activity.stopSpinWheel();
	}

	private void fetchmyOrders(JSONObject data) {
		try {
			arraylist = new ArrayList<HashMap<String, String>>();
			listItem = new ArrayList<JSONObject>();
			if (data.getString("Status").equals("Success")) {
				JSONArray array = data.getJSONArray("TransactionList");
				if(array.length()>pos){
				JSONObject objectData=array.getJSONObject(pos);
				if (objectData != null) {

					if (!objectData.isNull(AppConstants.pDate)) {
						date.setText(objectData.getString(AppConstants.pDate));
					} else {

					}
					if (!objectData.isNull(AppConstants.DeliveryArea)) {
						area.setText(objectData.getString(AppConstants.DeliveryArea) + ","
								+ (objectData.getString(AppConstants.DeliveryBlock)));
					} else {

					}
					if (!objectData.isNull(AppConstants.Total)) {
						total.setText(getResources().getString(R.string.KD) + objectData.getString(AppConstants.Total));
					} else {

					}
					JSONArray oderArray = objectData.getJSONArray("Orders");
					for (int i = 0; i < oderArray.length(); i++) {
						HashMap<String, String> map = new HashMap<String, String>();
						JSONObject obj = oderArray.getJSONObject(i);
						map.put(AppConstants.pDate, objectData.getString(AppConstants.pDate));
						map.put(AppConstants.Restaurant, obj.getString(AppConstants.Restaurant));
						map.put(AppConstants.all_restaurants_id, obj.getString(AppConstants.all_restaurants_id));
						map.put(AppConstants.all_restaurants_rating, obj.getString(AppConstants.all_restaurants_rating));
						map.put(AppConstants.delivery_time_json_obj, obj.getString(AppConstants.delivery_time_json_obj));
						arraylist.add(map);
						listItem.add(obj);
					}
				}
				}
				setAdapter(arraylist);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void doRateBarChangedUpdate(String type, String id, String count) {

		Activity.startSpinwheel(false, true);
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.accumulate(AppConstants.ItemOrRestaurantId, id);
			jsonObject.accumulate("UserId", Activity.readFile());
			jsonObject.accumulate(AppConstants.Rating, count);
			jsonObject.accumulate(AppConstants.flag, type);
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.postRating, jsonObject);
			}
			Log.e("rating params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	@Override
	public void onItemorRestaurantratingLoadFail(JSONObject jsonObject) {
		super.onItemorRestaurantratingLoadFail(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity, R.string.failed, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onItemorRestaurantratingSuccessfully(JSONObject jsonObject) {
		super.onItemorRestaurantratingSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("rating response", "" + jsonObject.toString());
		try {
			if (jsonObject.getString("Status").equals("Success")) {
				fetData();
			} else {
				Toast.makeText(Activity, R.string.failed, Toast.LENGTH_SHORT).show();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setAdapter(ArrayList<HashMap<String, String>> list) {
		adapter = new MyOrdersSecondPageAdapter(Activity, list,clickedItem,rateBarChecked);
		orderListview.setAdapter(adapter);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		try {
			JSONArray array = listItem.get(position).getJSONArray("Orders");
			Log.e("Orders array", "......" + array.length());
			if (array.length() > 0) {
				MyOrdersFragmentChildView2 frag = new MyOrdersFragmentChildView2(pos,position);
				Activity.pushFragments(frag, false, true);

			} else {
				Log.e("Orders array", "......" + position);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
}
