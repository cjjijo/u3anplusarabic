package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MyFavouriteAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.interfaces.OnItemRemovedListener;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MyFavouriteFragment extends HeaderViewControlFragment {
	GridView favouriteGridview;	
	LogoutListener listener;
	private ArrayList<HashMap<String, String>> arraylist;
	MyFavouriteAdapter adapter;
	OnItemRemovedListener removeListener;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.my_favourite, gp, false);
		setupUI(v, false);
		setTitleForPage(getString(R.string.my_accnt));
		favouriteGridview = (GridView) v.findViewById(R.id.gridView1);
		fetchmyFavourites();
		removeListener = new OnItemRemovedListener() {
			@Override
			public void ItemCheckComple(String id) {
				removeItem(id);
			}
		};
		return v;
	}

	protected void removeItem(String id) {
		Activity.startSpinwheel(false, true);
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.accumulate("ItemId", id);
			jsonObject.accumulate("UserId", Activity.readFile());
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.removeFromMyFavUrl, jsonObject);
			}
			Log.e("remove from my favourites params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void fetchmyFavourites() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		try {
			JSONObject jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", Activity.readFile());
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.getMyFavUrl, jsonObject);
			}
			Log.e("get my favourites params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void MyFavouriteLoadedSuccessfully(JSONObject jsonObject) {
		super.MyFavouriteLoadedSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("get my favourites result", "" + jsonObject.toString());
		parseMyfavouriteData(jsonObject);
	}

	@Override
	public void MyFavouriteLoadingFail(JSONObject jsonObject) {
		super.MyFavouriteLoadingFail(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void removedFromFavouriteFail(JSONObject jsonObject) {
		super.removedFromFavouriteFail(jsonObject);
		Activity.stopSpinWheel();
		Toast.makeText(Activity, R.string.failed, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void removedFromFavouriteSuccessfully(JSONObject jsonObject) {
		super.removeCartItemSuccess(jsonObject);
		Activity.stopSpinWheel();
		if (jsonObject.toString().contains("Success")) {
			Toast.makeText(Activity, R.string.removed_successfuly, Toast.LENGTH_SHORT).show();
			fetchmyFavourites();
		} else {
			Toast.makeText(Activity, R.string.failed, Toast.LENGTH_SHORT).show();
		}
	}

	private void parseMyfavouriteData(JSONObject jsonObject) {
		arraylist = new ArrayList<HashMap<String, String>>();
		try {
			JSONArray jarray = jsonObject.getJSONArray("FavouritesList");
			for (int i = 0; i < jarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				JSONObject obj = jarray.getJSONObject(i);
				map.put(AppConstants.get_cart_info_ItemId, obj.getString(AppConstants.get_cart_info_ItemId));
				map.put(AppConstants.Name, obj.getString(AppConstants.Name));
				map.put(AppConstants.Price, obj.getString(AppConstants.Price));
				map.put(AppConstants.Thumbnail, obj.getString(AppConstants.Thumbnail));
				map.put(AppConstants.most_selling_dishes_description,
						obj.getString(AppConstants.most_selling_dishes_description));
				arraylist.add(map);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		showMyFavourites(arraylist);
	}

	private void showMyFavourites(ArrayList<HashMap<String, String>> arraylist2) {
		adapter = new MyFavouriteAdapter(Activity, arraylist2, removeListener);
		favouriteGridview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		if (arraylist2.size() > 0) {
			return;
		} else {
			Toast.makeText(Activity, R.string.nodata, Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
