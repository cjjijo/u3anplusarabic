package com.mawaqaa.u3an.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.utilities.Utilities;

public class MyDialogFragmentu3ancredit extends DialogFragment {

	// Context cnt;

	FrameLayout trmscndtnFrame;
	WebView trmscdtn;
	String htmlData;

	public static MyDialogFragmentu3ancredit newInstance() {
		String title = "My Fragment";
		MyDialogFragmentu3ancredit f = new MyDialogFragmentu3ancredit();
		Bundle args = new Bundle();
		args.putString("title", title);
		f.setArguments(args);
		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		final Dialog myDialog = new Dialog(getActivity());
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.dialog_termsandconditns);
		// cnt = getActivity().getApplicationContext();
		// setupUI(rootView, false);
		trmscndtnFrame = (FrameLayout) myDialog
				.findViewById(R.id.trmscndtn_webview_container);
		// setTitleForPage("Consultation");
		Button clk = (Button) myDialog.findViewById(R.id.buttonclse);
		clk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				myDialog.dismiss();
			}
		});

		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;
		myDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		myDialog.getWindow().setLayout(
				getActivity().getResources().getDisplayMetrics().widthPixels,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		myDialog.show();
		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;

		return myDialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// new getAdsBannerFromTheservice(true).execute();
		setDialogPosition();
		htmlData = UanCreditFragment.crhtmlData;

		setWebView();
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private void setDialogPosition() {
		SharedPreferences sp = getActivity().getSharedPreferences("lan",
				Context.MODE_PRIVATE);
		String lan = sp.getString("lan", "en");

		Window window = this.getDialog().getWindow();

		if (lan.equals("en"))
			window.setGravity(Gravity.BOTTOM | Gravity.LEFT);
		else
			window.setGravity(Gravity.BOTTOM | Gravity.RIGHT);

		WindowManager.LayoutParams params = window.getAttributes();
		params.y = dpToPx(0);
		window.setAttributes(params);
	}

	private int dpToPx(int dp) {
		DisplayMetrics metrics = getActivity().getResources()
				.getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				metrics);
	}

	private void setWebView() {
		// htmlData =
		// "<html><p class=\"MsoNormal\" style=\"line-height:150%\"><span style=\"font-size:14.0pt;line-height:150%;font-family:times,serif;color:#505862\">About us:</span></p>\r\n<p class=\"MsoNormal\" style=\"text-indent:.5in;line-height:150%\"><span style=\"font-size:14.0pt;line-height:150%;font-family:times,serif;color:#505862\">Welcometo U3an.com, your number one source for ordering food online. We�re dedicatedto giving you the very best of home delivery, with a focus on dependability,customer service and uniqueness. Founded in 2014 by <span style=\"background:yellow;mso-highlight:yellow\">[founder's name],</span> U3an.com has come a longway from its beginnings. We now serve customers all over Kuwait, and arethrilled to give you the best home delivery service. U3an is the place whereyou fulfill your five senses. We provide you with the whole menu of eachrestaurant and images if possible. Forget the phone orders complications andgetting wrong orders to your door. Our site is one of the best online foodservice. With a click of a button you get what you desire and more. We hope youenjoy our services and the promotions we are offering as much as we enjoyoffering them to you. If you have any questions or comments, please don�thesitate to contact us.<o:P /></span></p></html>";

		trmscdtn = new WebView(getActivity());
		trmscdtn.setVerticalScrollBarEnabled(false);
		trmscdtn.getSettings().setJavaScriptEnabled(true);
		trmscdtn.setBackgroundColor(Color.parseColor("#FFFFFF"));
		if (Build.VERSION.SDK_INT >= 11)
			trmscdtn.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		this.trmscdtn.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				trmscdtn.setBackgroundColor(Color.parseColor("#FFFFFF"));
				if (Build.VERSION.SDK_INT >= 11)
					trmscdtn.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				// parenrLyt.setBackgroundDrawable(getResources().getDrawable(R.drawable.webview_bg));
			}
		});
		int pad = Utilities.convertDip2Pixels(getActivity(), 45);
		trmscdtn.setPadding(pad, pad, pad, pad);
		trmscndtnFrame.addView(trmscdtn);
		Log.e("htmlData", ""+htmlData);
		Log.e("html fromHtml", "" + Html.fromHtml(htmlData));
		
		trmscdtn.loadDataWithBaseURL(null,
				Utilities.getHtmlDatawithFont(getActivity(), htmlData),
				"text/html", "utf-8", "about:blank");
		
	}

	/*
	 * // AsyncTask for loading data.... public class getAdsBannerFromTheservice
	 * extends AsyncTask<Void, String, Void> { String type, result; boolean
	 * postStatus = false; JSONObject jObj = null; Boolean
	 * progressVisibiltyflag; InputStream istream;
	 * 
	 * public getAdsBannerFromTheservice(Boolean progressVisibilty) {
	 * this.progressVisibiltyflag = progressVisibilty; }
	 * 
	 * @Override protected void onPreExecute() { showProgressbar();
	 * super.onPreExecute(); }
	 * 
	 * @Override protected Void doInBackground(Void... params) {
	 * 
	 * try { JSONObject objJson = new JSONObject();
	 * objJson.accumulate("PageName", "terms_credit");
	 * objJson.accumulate("locale", "en-US");
	 * 
	 * HttpClient httpClient = new DefaultHttpClient(); HttpPost httppost = new
	 * HttpPost(AppConstants.get_pagedetails_url); StringEntity entity = new
	 * StringEntity(objJson.toString()); httppost.setEntity(entity);
	 * httppost.setHeader("Accept", "application/json");
	 * httppost.setHeader("Content-type", "application/json"); HttpResponse
	 * response = httpClient.execute(httppost); istream =
	 * response.getEntity().getContent();
	 * 
	 * if (istream != null) { BufferedReader bufferedReader = new
	 * BufferedReader( new InputStreamReader(istream)); String line = ""; result
	 * = ""; while ((line = bufferedReader.readLine()) != null) result += line;
	 * 
	 * istream.close();
	 * 
	 * jObj = new JSONObject(result);
	 * 
	 * } else { result = null; }
	 * 
	 * } catch (JSONException e) { Log.e("Data", "JSONException Error:" +
	 * e.getMessage()); e.printStackTrace(); } catch
	 * (UnsupportedEncodingException e) { Log.e("Data",
	 * "UnsupportedEncodingException Error:" + e.getMessage());
	 * e.printStackTrace(); } catch (ClientProtocolException e) { Log.e("Data",
	 * "ClientProtocolException Error:" + e.getMessage()); e.printStackTrace();
	 * } catch (IOException e) { Log.e("Data", "IOException Error:" +
	 * e.getMessage()); e.printStackTrace(); } return null; }
	 * 
	 * @Override protected void onPostExecute(Void result) { hideProgressbar();
	 * 
	 * try { if (jObj.getString("Status").equalsIgnoreCase("Success")) {
	 * if(jObj.getString("Data").equals("")) { htmlData =
	 * "<center>"+getResources().getString(R.string.nodatahtml)+"</center>"; }
	 * else { htmlData = jObj.getString("Data"); } setWebView(); } else {
	 * htmlData = "<h3>Sorry... No Data Available</h3>"; setWebView(); }
	 * 
	 * } catch (JSONException e) { e.printStackTrace(); }
	 * 
	 * } }
	 * 
	 * 
	 * private void showProgressbar() { ((BaseActivity)
	 * getActivity()).startSpinwheel(false, false); }
	 * 
	 * private void hideProgressbar() { ((BaseActivity)
	 * getActivity()).stopSpinWheel(); }
	 */

}
