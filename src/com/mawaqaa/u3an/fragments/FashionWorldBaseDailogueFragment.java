package com.mawaqaa.u3an.fragments;

import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


public class FashionWorldBaseDailogueFragment extends DialogFragment {
	private String TAG = "u3an::U3anBaseDailogueFragment";
	public void setupUI(View view) {
		Log.d(TAG, "setupUI");
	    //Set up touch listener for non-text box views to hide keyboard.
	    if(!(view instanceof EditText)) {
	    		
	        view.setOnTouchListener(new OnTouchListener() {

	            @Override
				public boolean onTouch(View v, MotionEvent event) {
	            	//Utilities.hideSoftKeyboard(getActivity(), v);
	                return false;
	            }

	        });
	    }
	    
	    if((view instanceof TextView)) {
	   //	 ((TextView) view).setTypeface(getRegularTypeFace());
	    }

	    //If a layout container, iterate over children and seed recursion.
	    if (view instanceof ViewGroup) {
	    	for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {

	            View innerView = ((ViewGroup) view).getChildAt(i);

	            setupUI(innerView);
	        }
	    }
	}
	
	
	
}
