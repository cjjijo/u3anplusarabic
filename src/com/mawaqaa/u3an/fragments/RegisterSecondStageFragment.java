package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.CountryAreaSpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Country;
import com.mawaqaa.u3an.data.SingleAreaCountry;
import com.mawaqaa.u3an.db.OpenHelper;

public class RegisterSecondStageFragment extends HeaderViewControlFragment
		implements OnClickListener {
	RadioGroup selectionRadioGroup;
	RadioButton selectedRadioButton;
	ImageButton nextButton;
	int maxLength=25;

	ImageButton villaButton, buildingButton, officeButton;

	OpenHelper db_Obj;
	CountryAreaSpinnerAdapter spinneradapter;

	String areaString, profileNameString, blockString, juddaString,
			streetString, houseNoString, floorString, apartmentString,
			directionsString;

	EditText profileNameEditText, blockEditText, juddaEditText, streetEditText,
			houseNoEditText, floorEditText, apartmentEditText,
			directionsEditText;
	EditText errorEditText;
	Spinner areaSpinner;

	String address_Type = "0";

	String areaID;
	ArrayList<SingleAreaCountry> arraylist_country_data;

	LinearLayout apartmentLayout, floorLayout;

	JSONObject areaJsonObject;

	TextView villaTextView, buildingTextView, officeTextView;

	Boolean villaFlag = true, buildingFlag = false, officeFlag = false;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		final View rootView = info.inflate(
				R.layout.fragment_register_secondstage, gp, false);
		init(rootView);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.sign_up_heading));
		fixUI();
		areaJsonObject = readDataFromFile(HomeFragment.FILENAMEAREADATA);
		ParseCountryData(areaJsonObject);
		return rootView;
	}

	private JSONObject readDataFromFile(String fileName) {
		JSONObject jsonObject = null;
		try {
			FileInputStream fis = Activity.openFileInput(fileName);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			String data = br.readLine();
			br.close();
			in.close();
			fis.close();
			if (data != null)
				jsonObject = new JSONObject(data);
		} catch (Exception e) {
		}
		return jsonObject;
	}

	private void fixUI() {
		if (villaFlag == true) {
			villaButton.setBackgroundResource(R.drawable.villahouse2);
		} else {
			villaButton.setBackgroundResource(R.drawable.villahouse1);
		}

		if (officeFlag == true) {
			officeButton.setBackgroundResource(R.drawable.office2);
		} else {
			officeButton.setBackgroundResource(R.drawable.office1);
		}

		if (buildingFlag == true) {
			buildingButton.setBackgroundResource(R.drawable.building2);
		} else {
			buildingButton.setBackgroundResource(R.drawable.building1);
		}
	}

	private void ParseCountryData(JSONObject jsonObject) {
		// Create an array
		arraylist_country_data = new ArrayList<SingleAreaCountry>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.areas_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.areas_json_obj);

				for (int i = 0; i < jsonarray.length(); i++) {
					if (!jsonarray.isNull(i)) {
						jsonObject = jsonarray.getJSONObject(i);
						Country country = new Country(jsonObject);
						addAreas2SpinnerArrayList(country);
					}
				}
			}
			PopulateCountrySpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		}
	}

	private void init(View view) {
		profileNameEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_profile_Name);
		profileNameEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						profileNameEditText));
		profileNameEditText
				.setOnFocusChangeListener(new FocusChangedListener());
		/*profileNameEditText.setFilters(new InputFilter[] {
			    new InputFilter() {
			        public CharSequence filter(CharSequence src, int start,
			                int end, Spanned dst, int dstart, int dend) {
			            if(src.equals("")){ // for backspace
			                return src;
			            }
			            if(src.toString().matches("[a-zA-Z ]+")){
			                return src;
			            }
			            return "";
			        }

					
			    }
			});*/
	/*	profileNameEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});*/
		profileNameEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		blockEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Block);
		blockEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		juddaEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Judda);
		juddaEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		streetEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Street);
		streetEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				streetEditText));
		streetEditText.setOnFocusChangeListener(new FocusChangedListener());
		streetEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		houseNoEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_House_No);
		houseNoEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				houseNoEditText));
		houseNoEditText.setOnFocusChangeListener(new FocusChangedListener());
		houseNoEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		floorEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Floor);
		floorEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				floorEditText));
		floorEditText.setOnFocusChangeListener(new FocusChangedListener());
		floorEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		apartmentEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Apartment);
		apartmentEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				apartmentEditText));
		apartmentEditText.setOnFocusChangeListener(new FocusChangedListener());
		apartmentEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});
		directionsEditText = (EditText) view
				.findViewById(R.id.Edittext_register2_Directions);
		directionsEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLength)});

		nextButton = (ImageButton) view.findViewById(R.id.nextBtnSecondStage);
		nextButton.setOnClickListener(this);

		areaSpinner = (Spinner) view.findViewById(R.id.Edittext_register2_Area);

		villaButton = (ImageButton) view.findViewById(R.id.villaRadioBtn);
		villaButton.setOnClickListener(this);

		officeButton = (ImageButton) view.findViewById(R.id.officeRadioBtn);
		officeButton.setOnClickListener(this);

		buildingButton = (ImageButton) view.findViewById(R.id.buildingRadioBtn);
		buildingButton.setOnClickListener(this);

		apartmentLayout = (LinearLayout) view.findViewById(R.id.linlayaperment);
		apartmentLayout.setOnClickListener(this);

		floorLayout = (LinearLayout) view.findViewById(R.id.linlayfloor);
		floorLayout.setOnClickListener(this);

		villaTextView = (TextView) view.findViewById(R.id.villaTextview);
		villaTextView.setOnClickListener(this);
		buildingTextView = (TextView) view.findViewById(R.id.buildingTextview);
		buildingTextView.setOnClickListener(this);
		officeTextView = (TextView) view.findViewById(R.id.officeTextview);
		officeTextView.setOnClickListener(this);
	}

	private void addAreas2SpinnerArrayList(Country country) {
		SingleAreaCountry areaCountry = new SingleAreaCountry();
		// for showing city Name in Spinner
		areaCountry.areaName = "-- " + country.cityName + " --";
		areaCountry.isCityName = true;
		arraylist_country_data.add(areaCountry);
		// adding all areas in the city
		arraylist_country_data.addAll(country.areas);

	}

	private void PopulateCountrySpinner() {
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		areaSpinner.setAdapter(spinneradapter);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.nextBtnSecondStage:
			validateData();
			break;

		case R.id.villaRadioBtn:
			ClickActionForVilla();
			break;

		case R.id.buildingRadioBtn:
			clickActionForBuilding();
			break;

		case R.id.officeRadioBtn:
			clickActionForOffice();
			break;

		case R.id.villaTextview:
			ClickActionForVilla();
			break;

		case R.id.buildingTextview:
			clickActionForBuilding();
			break;

		case R.id.officeTextview:
			clickActionForOffice();
			break;

		default:
			break;
		}
	}

	// Address type 0,1,2 for villa ,building and office
	private void clickActionForOffice() {
		villaFlag = false;
		buildingFlag = false;
		officeFlag = true;
		address_Type = "2";
		fixUI();
		visibilityControl("office");
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		spinneradapter.notifyDataSetChanged();
		areaSpinner.setAdapter(spinneradapter);
		Clearfields();
	}

	private void clickActionForBuilding() {
		villaFlag = false;
		buildingFlag = true;
		officeFlag = false;
		address_Type = "1";
		fixUI();
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		spinneradapter.notifyDataSetChanged();
		areaSpinner.setAdapter(spinneradapter);
		visibilityControl("building");
		Clearfields();
	}

	private void ClickActionForVilla() {
		villaFlag = true;
		buildingFlag = false;
		officeFlag = false;
		address_Type = "0";
		fixUI();
		spinneradapter=new CountryAreaSpinnerAdapter(Activity,
				arraylist_country_data) ;
		spinneradapter.notifyDataSetChanged();
		areaSpinner.setAdapter(spinneradapter);
		visibilityControl("villa");
		Clearfields();

	}

	private void Clearfields() {
		profileNameEditText.setText("");
		blockEditText.setText("");
		juddaEditText.setText("");
		streetEditText.setText("");
		houseNoEditText.setText("");
		floorEditText.setText("");
		apartmentEditText.setText("");
		directionsEditText.setText("");
	}

	private void visibilityControl(String string) {
		switch (string) {
		case "villa":
			apartmentLayout.setVisibility(View.GONE);
			floorLayout.setVisibility(View.GONE);
			break;

		case "building":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;

		case "office":
			apartmentLayout.setVisibility(View.VISIBLE);
			floorLayout.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}
	}

	private void validateData() {
		boolean isvalid = doAreaValidation();
		if (!isvalid)
			return;
		
		isvalid = doBlockValidation();
		if (!isvalid) 
			return;

		isvalid = doProfileNameValidation();
		if (!isvalid)
			return;

		isvalid = doStreetValidation();
		if (!isvalid)
			return;

		isvalid = doHouseNumberValidation();
		if (!isvalid)
			return;

		else {

			if ((buildingFlag == true) || (officeFlag == true)) {
				doOptionalDataValidation();
			} else {
				floorString = getResources().getString(R.string.nodata);
				apartmentString = getResources().getString(R.string.nodata);
				doSubmitAction();
			}
		}
	}

	private void doSubmitAction() {
		blockString = blockEditText.getText().toString();
		juddaString = juddaEditText.getText().toString();
		directionsString = directionsEditText.getText().toString();

		db_Obj = new OpenHelper(Activity);
		db_Obj.DataInsertionStageTwo(areaID, profileNameString, address_Type,
				blockString, juddaString, streetString, houseNoString,
				floorString, apartmentString, directionsString);
		Fragment registerThirdStage = new RegisterThirdStageFragment();
		Activity.pushFragments(registerThirdStage, false, true);
	}

	private void doOptionalDataValidation() {
		Boolean isvalid = doFloorValidation();
		if (!isvalid)
			return;
		isvalid = doApartmentValidation();
		if (!isvalid)
			return;
		else {
			doSubmitAction();
		}
	}

	private Boolean doApartmentValidation() {
		apartmentString = apartmentEditText.getText().toString();
		if (apartmentString.length() > 0) {
			return true;
		} else {
			apartmentEditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.apartment_data)+"</font>"));
			errorEditText = apartmentEditText;
			apartmentEditText.addTextChangedListener(registerInputTextWatcher);
			// apartmentEditText.requestFocus();
			return false;
		}
	}
	private Boolean doBlockValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		blockString = blockEditText.getText().toString();
		if (blockString.length() > 0) {
			return true;
		} else {
			blockEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_block)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				blockEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter block to continue</font>"));
			}else{
				blockEditText
				.setError(Html
						.fromHtml("<font color='white'>أدخل رقم القطعة للاستمرار</font>"));
			}*/
			errorEditText = blockEditText;
			blockEditText.addTextChangedListener(registerInputTextWatcher);
			// apartmentEditText.requestFocus();
			return false;
		}
	}

	private Boolean doFloorValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		floorString = floorEditText.getText().toString();
		if (floorString.length() > 0) {
			return true;
		} else {
			floorEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_floor)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				floorEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter the floor number to continue</font>"));
			}else{
				floorEditText
				.setError(Html
						.fromHtml("<font color='white'>أدخل رقم الطابق للاستمرار</font>"));
			}*/
			errorEditText = floorEditText;
			floorEditText.addTextChangedListener(registerInputTextWatcher);
			// floorEditText.requestFocus();
			return false;
		}
	}

	private Boolean doHouseNumberValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		houseNoString = houseNoEditText.getText().toString();
		if (houseNoString.length() > 0) {
			return true;
		} else {
			houseNoEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_house_num)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				houseNoEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter the house number to continue</font>"));
			}else{
				houseNoEditText
				.setError(Html
						.fromHtml("<font color='white'>أدخل رقم المنزل للاستمرار</font>"));
			}*/
			errorEditText = houseNoEditText;
			houseNoEditText.addTextChangedListener(registerInputTextWatcher);
			// houseNoEditText.requestFocus();
			return false;
		}
	}

	private Boolean doStreetValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		streetString = streetEditText.getText().toString();
		if (streetString.length() > 0) {
			return true;
		} else {
			streetEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_street)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				streetEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a Street name to continue</font>"));
			}else{
				streetEditText
					.setError(Html
						.fromHtml("<font color='white'>أدخل اسم الشارع للاستمرار</font>"));
			}*/
			errorEditText = streetEditText;
			streetEditText.addTextChangedListener(registerInputTextWatcher);
			// streetEditText.requestFocus();
			return false;
		}
	}

	private Boolean doAreaValidation() {

		int pos = areaSpinner.getSelectedItemPosition();
		SingleAreaCountry country = arraylist_country_data.get(pos);
		String strAreaId = country.areaId;
		areaID = strAreaId;
		if (strAreaId == null || strAreaId.equals(null)
				|| strAreaId.trim().equals("") || country.isCityName) {
			Toast.makeText(Activity, getResources().getString(R.string.select_area),
					Toast.LENGTH_SHORT).show();
			return false;
		} else {
			return true;
		}

	}

	private Boolean doProfileNameValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		profileNameString = profileNameEditText.getText().toString();
		if (profileNameString.length() > 0) {
			return true;
		} else {
			profileNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				profileNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a name to continue</font>"));
			}else{
				profileNameEditText
					.setError(Html
						.fromHtml("<font color='white'>أدخل الاسم للاستمرار</font>"));
			}*/
			errorEditText = profileNameEditText;
			profileNameEditText
					.addTextChangedListener(registerInputTextWatcher);
			// profileNameEditText.requestFocus();
			return false;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private class AddRestaurantTextwatcher implements TextWatcher {

		private EditText editText;

		AddRestaurantTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {
		
		}

	}

	private class FocusChangedListener implements OnFocusChangeListener {

		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {

				case R.id.Edittext_register2_profile_Name:
					doProfileNameValidation();
					break;
				case R.id.Edittext_register2_Street:
					doStreetValidation();
					break;
				case R.id.Edittext_register2_House_No:
					doHouseNumberValidation();
					break;
				case R.id.Edittext_register2_Floor:
					doFloorValidation();
					break;
				case R.id.Edittext_register2_Apartment:
					doApartmentValidation();
					break;

				default:
					break;
				}
			}/*
			 * else if (v != null && v instanceof EditText) { ((EditText)
			 * v).setError(null); }
			 */
		}

	}

}
