package com.mawaqaa.u3an.fragments;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class FaqFragment extends HeaderViewControlFragment {

	private FrameLayout faqlay;
	private WebView Faqwebview;
	String html, data;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		// TODO Auto-generated method stub
		View v = info.inflate(R.layout.fragment_faq, gp, false);
		setupUI(v, false);
		initView(v);
		return v;
	}

	private void initView(View v) {
		faqlay = (FrameLayout) v.findViewById(R.id.faq_web_container);
		setTitleForPage(getResources().getString(R.string.frequenly_asked));
		accumulateData();
	}

	private void accumulateData() {
		// TODO Auto-generated method stub
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonobject = new JSONObject();
		try {
			jsonobject.accumulate("PageName", "faq");
			if(sDefSystemLanguage.equals("en")){
				jsonobject.accumulate("locale", "en-US");
			}else{
				jsonobject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("jsonobject.........", "" + jsonobject.toString());
		LoadData(jsonobject);
	}

	private void LoadData(JSONObject jsobject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL", ""+AppConstants.get_pagedetails_url);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_pagedetails_url,
					jsobject);
		}
	}

	@Override
	public void pageLoadSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.pageLoadSuccess(jsonObject);
		Log.e("jsonobject", "" + jsonObject.toString());
		parseData(jsonObject);
	}

	private void parseData(JSONObject jsonObject) {
		try {
			if (jsonObject.getString("Status").equalsIgnoreCase("Success")) {
				data = jsonObject.getString("Data");
			} else {
				data = getResources().getString(R.string.nodata);
			}
		} catch (JSONException e) {		
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		showdata();
	}

	@Override
	public void pageLoadFail(JSONObject jsonObject) {
		super.pageLoadFail(jsonObject);
		Log.e("jsonobject", "" + jsonObject.toString());
	}

	private void showdata() {
		// TODO Auto-generated method stub

		Faqwebview = new WebView(Activity);
		Faqwebview.setVerticalScrollBarEnabled(false);
		Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
		Faqwebview.getSettings().setJavaScriptEnabled(true);
		if (Build.VERSION.SDK_INT >= 11)
			Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		this.Faqwebview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
				if (Build.VERSION.SDK_INT >= 11)
					Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				// parenrLyt.setBackgroundDrawable(getResources().getDrawable(R.drawable.webview_bg));
			}
		});
		int pad = com.mawaqaa.u3an.utilities.Utilities.convertDip2Pixels(
				getActivity(), 30);
		Faqwebview.setPadding(pad, pad, pad, pad);
		faqlay.addView(Faqwebview);
		Log.i("WebPage Content:", ""+data);
		Faqwebview.loadDataWithBaseURL("file:///android_asset/fonts/BREESERIF-REGULAR.OTF",
				com.mawaqaa.u3an.utilities.Utilities.getHtmlDatawithFont(getActivity(),
						data), "text/html", "utf-8", "about:blank");
	}

}
