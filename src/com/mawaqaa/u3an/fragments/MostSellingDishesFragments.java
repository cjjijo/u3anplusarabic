package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.GridView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.AllResturantsDataAdapter;
import com.mawaqaa.u3an.adapters.BrandSpinnerAdpater;
import com.mawaqaa.u3an.adapters.MostSellingCuisinesAdapter;
import com.mawaqaa.u3an.adapters.MostSellingDishesAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.DataHandlingUtilities;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MostSellingDishesFragments extends HeaderViewControlFragment
		implements OnClickListener {

	JSONObject jsonObject;
	JSONArray jsonarray;

	ArrayList<HashMap<String, String>> arraylist;
	ArrayList<HashMap<String, String>> arraylist_mostselling_dishes_data;
	ArrayList<HashMap<String, String>> arraylist_mostselling_restaurants_data;
	ArrayList<HashMap<String, String>> arraylist_cusines_data;

	GridView most_Selling_dishes;
	TextView emptyView;
	ArrayList<String> list;

	int position_selection_flag;
	ProgressDialog mProgressDialog;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.layout_mostsellingdishes, gp, false);
		setupUI(v, false);
		initView(v);
		
		PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.most_selling_dishes));
		addDataToArrayList();

		category.setAdapter(new BrandSpinnerAdpater(Activity, list));
		category.setSelection(0);
		// category.setPopupBackgroundResource(R.drawable.spinner);
		category.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				position_selection_flag = position;

				if (position == 1) { // by restaurants
					new doUploadNewAdressTaskRestaurants().execute();
				} else if (position == 0) {
					AccumulateJsonObjectMostSellingDishes();
				} else if (position == 2) { // by cuisine
					AccumulateJsonObject();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		setTitleForPage(getResources().getString(R.string.most_selling_dishes));
		showCategorySpinner(true);
// Jijo 29-12-15
		most_Selling_dishes.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				switch (position_selection_flag) {
				case 0: // Most Selling
					HashMap<String, String> map = new HashMap<String, String>();
					map = arraylist_mostselling_dishes_data.get(position);
					
					Log.e("The Most selling dishes data is", ""+map.toString());
					String staString = map
							.get(AppConstants.most_selling_resturant_status);
					if(PrefUtil.getAppLanguage(Activity).equals("en")){
						switch (staString) {
						case AppConstants.OPEN:
							callMostSellingDishesDetails(position);
							break;

						default:
							break;
						}
					}else{
						if(staString.equalsIgnoreCase(getResources().getString(R.string.open))){
							callMostSellingDishesDetails(position);
						}
					}
					
					break;
				case 1: // by restaurants
					HashMap<String, String> map1 = new HashMap<String, String>();
					map1 = arraylist_mostselling_restaurants_data.get(position);
					String staString1 = map1
							.get(AppConstants.all_restaurants_resturant_status);
					if(PrefUtil.getAppLanguage(Activity).equals("en")){
						switch (staString1) {
						case AppConstants.OPEN:
							CallDishesDataByRestaurants(position);
							break;

						default:
							break;
						}
					}else{
						if(staString1.equalsIgnoreCase(getResources().getString(R.string.open))){
							CallDishesDataByRestaurants(position);
						}
					}
					
					break;
				case 2:// by cuisine
					callDishesDataByCuisine(position);
					break;
				default:
					break;
				}
			}
		});
		return v;
	}

	private void callDishesDataByCuisine(int position) { // get_cusines_country_id
		HashMap<String, String> map = new HashMap<String, String>();
		map = arraylist_cusines_data.get(position);
		Fragment cuisinesDishlistFragment = new MostSellingByCuisineDishlistFragment();
		Bundle params1 = new Bundle();
		params1.putString(AppConstants.get_cusines_country_id,
				map.get(AppConstants.get_cusines_country_id));
		cuisinesDishlistFragment.setArguments(params1);
		Activity.pushFragments(cuisinesDishlistFragment, false, true);
	}

	private void CallDishesDataByRestaurants(int position) {
		HashMap<String, String> map = new HashMap<String, String>();
		map = arraylist_mostselling_restaurants_data.get(position);
		Fragment dishesDetailsFragment = new MostSellingByRestaurantDishlistFragment();
		Bundle params1 = new Bundle();
		params1.putString(AppConstants.all_restaurants_id,
				map.get(AppConstants.all_restaurants_id));
		params1.putString(AppConstants.all_restaurants_resturant_name,
				map.get(AppConstants.all_restaurants_resturant_name));
		params1.putString(AppConstants.all_restaurants_resturant_logo,
				map.get(AppConstants.all_restaurants_resturant_logo));
		dishesDetailsFragment.setArguments(params1);
		Activity.pushFragments(dishesDetailsFragment, false, true);
	}

	private void callMostSellingDishesDetails(int position) {
		HashMap<String, String> map = new HashMap<String, String>();
		map = arraylist_mostselling_dishes_data.get(position);
		Fragment mostSellingDishesDetailsFragment = new MostSellingRestaurantsDetails();
		Bundle params1 = new Bundle();
		params1.putString("Header", AppConstants.MOST_SELLING_TAG);
		params1.putString(AppConstants.all_restaurants_id,
				map.get(AppConstants.most_selling_id));
		params1.putString(AppConstants.all_restaurants_resturant_name,
				map.get(AppConstants.most_selling_resturant_name));
		params1.putString(AppConstants.all_restaurants_resturant_logo,
				map.get(AppConstants.most_selling_thumbnail));
		
		mostSellingDishesDetailsFragment.setArguments(params1);
		Activity.pushFragments(mostSellingDishesDetailsFragment, false, true);
	}

	private void AccumulateJsonObject() {
		Activity.startSpinwheel(false, true);
		emptyView.setVisibility(View.GONE);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectCuisines = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectCuisines = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObjectCuisines.accumulate("locale", "en-US");
			}else{
				jsonObjectCuisines.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryDataCuisines(jsonObjectCuisines);
	}

	private void AccumulateJsonObjectMostSellingDishes() {
		Activity.startSpinwheel(false, true);
		emptyView.setVisibility(View.GONE);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectCuisines = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectCuisines = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObjectCuisines.accumulate("locale", "en-US");
			}else{
				jsonObjectCuisines.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryDataMostSelling(jsonObjectCuisines);
	}

	private void loadSubCategoryDataMostSelling(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.most_selling_url,
					jsonObject);
		}
	}

	private void loadSubCategoryDataCuisines(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_cusines_url,
					jsonObject);
		}
	}

	@Override
	public void onCusinesDataLoadedSuccessfully(JSONObject jsonObject) {
		super.onCusinesDataLoadedSuccessfully(jsonObject);
		Log.w("jsonObject===","======"+jsonObject);
		ParseDataCuisine(jsonObject);
	}

	private void ParseDataCuisine(JSONObject jsonObject1) {

		JSONObject jsonObject = new JSONObject();
		jsonObject = jsonObject1;
		// Create an array
		arraylist_cusines_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.get_cusines_json_obj);

			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);

				// Retrieve JSON Objects
				map.put(AppConstants.get_cusines_country_id, jsonObject
						.getString(AppConstants.get_cusines_country_id));
				map.put(AppConstants.get_cusines_country_name, jsonObject
						.getString(AppConstants.get_cusines_country_name));

				// Set the JSON Objects into the array
				arraylist_cusines_data.add(map);
			}

			PopulateCusinesGrid();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		}
	}

	private void PopulateCusinesGrid() {
		Activity.stopSpinWheel();
		MostSellingCuisinesAdapter adapter = new MostSellingCuisinesAdapter(
				Activity, arraylist_cusines_data);
		most_Selling_dishes.setAdapter(adapter);
		most_Selling_dishes.setEmptyView(emptyView);
		// emptyView.setVisibility(View.VISIBLE);
	}

	@Override
	public void onMostSellingDishesLoadedSuccessfully(JSONObject jsonObject) {
		super.onMostSellingDishesLoadedSuccessfully(jsonObject);
		parseData(jsonObject);

	}

	private void parseData(JSONObject jObject) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObject;

		// Create an array
		arraylist_mostselling_dishes_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.most_selling_json_obj);

			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);

				// Retrieve JSON Objects
				map.put(AppConstants.most_selling_thumbnail, jsonObject
						.getString(AppConstants.most_selling_thumbnail));
				map.put(AppConstants.most_selling_dishname, jsonObject
						.getString(AppConstants.most_selling_dishname));

				map.put(AppConstants.most_selling_resturant_name, jsonObject
						.getString(AppConstants.most_selling_resturant_name));
				map.put(AppConstants.most_selling_resturant_status, jsonObject
						.getString(AppConstants.most_selling_resturant_status));

				map.put(AppConstants.most_selling_id,
						jsonObject.getString(AppConstants.most_selling_id));
				// map.put(AppConstants.restaurant_summary,
				// jsonObject.getString(AppConstants.restaurant_summary));
				
				if(jsonObject.getString(AppConstants.most_selling_resturant_status).equals("HIDDEN")){
					
				}else{
				// Set the JSON Objects into the array
				arraylist_mostselling_dishes_data.add(map);
				}
			}
			Log.w("Most Selling Dishes", ""+jsonarray);
			PopulateGridView();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onMostSellingDishesByRestaurantsLoadedSuccessfully(
			JSONObject jsonObject) {
		super.onMostSellingDishesByRestaurantsLoadedSuccessfully(jsonObject);
		parseDataRestaurants(jsonObject);
	}

	private void parseDataRestaurants(JSONObject jObj) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObj;
		// Create an array
		arraylist_mostselling_restaurants_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL
		// address.....all_restaurants_id
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.most_selling_restaurants_jsonobj);
			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);
				// Retrieve JSON Objects
				map.put(AppConstants.all_restaurants_resturant_name, jsonObject
						.getString(AppConstants.most_selling_resturant_name));
				map.put(AppConstants.all_restaurants_resturant_status,
						jsonObject
								.getString(AppConstants.most_selling_restaurants_status));
				map.put(AppConstants.all_restaurants_resturant_logo,
						jsonObject
								.getString(AppConstants.most_selling_restaurants_thumbnails));
				map.put(AppConstants.all_restaurants_id,
						jsonObject.getString(AppConstants.all_restaurants_id));
				
				if (jsonObject.getString(AppConstants.most_selling_restaurants_status).equals("HIDDEN")) {
					
				}else{
				// Set the JSON Objects into the array
				arraylist_mostselling_restaurants_data.add(map);
				}
			}
			populateResturantGridData();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("Got exception",""+ e.getMessage());
		}
	}

	private void populateResturantGridData() {
		Activity.stopSpinWheel();
		AllResturantsDataAdapter adapter = new AllResturantsDataAdapter(
				Activity, arraylist_mostselling_restaurants_data);
		most_Selling_dishes.setAdapter(adapter);
		Log.w("emptyView.......", ""+emptyView);
		most_Selling_dishes.setEmptyView(emptyView);
	}

	private void addDataToArrayList() {
		list = new ArrayList<String>();
		list.add(getResources().getString(R.string.most_selling));
		list.add(getResources().getString(R.string.by_restaurants));
		list.add(getResources().getString(R.string.by_cuisine));
	}

	private void initView(View v) {
		most_Selling_dishes = (GridView) v
				.findViewById(R.id.gridView_most_selling_dishes);
		emptyView = (TextView) v.findViewById(android.R.id.empty);
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public void onResume() {
		super.onResume();
		DataHandlingUtilities listenerHideView = (DataHandlingUtilities) Activity;
		listenerHideView.hideToolBar();
		listenerHideView.mostSellingItemBottomBar();
	}

	private void PopulateGridView() {
		MostSellingDishesAdapter adapter = new MostSellingDishesAdapter(
				Activity, arraylist_mostselling_dishes_data);
		most_Selling_dishes.setAdapter(adapter);
		most_Selling_dishes.setEmptyView(emptyView);
		most_Selling_dishes.setVisibility(View.VISIBLE);
		Activity.stopSpinWheel();
	}

	// AsyncTask for loading data....
	public class doUploadNewAdressTask extends AsyncTask<Void, String, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;

		InputStream istream;

		public doUploadNewAdressTask(Boolean progressVisibilty) {
			this.progressVisibiltyflag = progressVisibilty;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Activity.startSpinwheel(false, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(AppConstants.most_selling_url);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);
					jsonObject = jObj;
				} else {
					result = null;
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			parseData(jsonObject);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// AsyncTask for loading data....
	public class doUploadNewAdressTaskRestaurants extends
			AsyncTask<Void, String, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;

		InputStream istream;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Activity.startSpinwheel(false, true);
			emptyView.setVisibility(View.GONE);
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						AppConstants.most_selling_restaurants_url);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);
					jsonObject = jObj;
				} else {
					result = null;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			try{
			parseDataRestaurants(jsonObject);
			}catch(Exception e){
				
			}
		}
	}
}
