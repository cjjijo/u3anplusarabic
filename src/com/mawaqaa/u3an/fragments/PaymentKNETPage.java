package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.dialogfragment.DialogReadyU3AnVideo;

public class PaymentKNETPage extends BaseFragment {
	private FrameLayout faqlay;
	private LinearLayout linearVideoShow;
	private VideoView videoViewReadyUan, video;
	private WebView Faqwebview;
	String url;
	
	private String uriPath;
	
	ArrayList<HashMap<String, String>> data;
	ImageButton backButton, homeButton, liveChatButton;
	TextView backTextView;

	BaseActivity activity;
	MediaPlayer mp;
		
	public PaymentKNETPage(ArrayList<HashMap<String, String>> arraylist, BaseActivity baseActivity) {
		data = arraylist;
		this.activity = baseActivity;
		//mp = MediaPlayer.create(baseActivity, R.raw.ready_audio);
	}
	
	
	/*@Override
	View InitializeContainer(LayoutInflater inflater, ViewGroup gp) {
		
		View rootView = inflater.inflate(R.layout.fragment_knet, gp, false);
		Bundle data = getArguments();
		url = data.getString("Url");
		Log.e("url in KnetForGiftvoucher", "" + url);
		video = (VideoView) rootView.findViewById(R.id.videoViewReadyUan);
		initView(rootView);
		//setTitleForPage("Payment");
		//showHomeButton(true);
		return rootView;
	}
*/
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_knet, container, false);
		Bundle data = getArguments();
		url = data.getString("Url");
		Log.e("url in KnetForGiftvoucher", "" + url);
		video = (VideoView) rootView.findViewById(R.id.videoViewReadyUan);
		//showHomeButton(true);
		initView(rootView);
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void PopAllCartFragment() {
		Fragment frag1 = new U3AnFoodCartFragment();
		activity.popFragments(frag1);
		activity.popFragments(new ConfirmOrderDeliveryDetailsFragment());
		activity.popFragments(new ConfirmOrderSecondStage());
		activity.popFragments(new ConfirmOrderFragment(data));
	}

	private void initView(View v) {
		// setTitleForPage(PrefUtil.getFragHeading(getActivity()));
		faqlay = (FrameLayout) v.findViewById(R.id.faq_web_container);
		
		linearVideoShow = (LinearLayout) v.findViewById(R.id.linearVideoDisplay);
		
		videoViewReadyUan = (VideoView) v.findViewById(R.id.videoViewReadyUan);
		
		backButton = (ImageButton) v.findViewById(R.id.backButton);
		backTextView = (TextView) v.findViewById(R.id.title);
		
		homeButton = (ImageButton) v.findViewById(R.id.imgHomeButton);
		
		liveChatButton = (ImageButton) v.findViewById(R.id.imgLiveChat);
		//showHomeButton(true);
		showdata();
		try {
			if (activity.readFile().equals("Guest")) {
				activity.WriteDataToFile("");
			}
		} catch (Exception e) {
			Log.e("Data", "msg");
		}
		backButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mp.stop();
				videoViewReadyUan.stopPlayback();
				PopAllCartFragment();
			}
		});
		backTextView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				PopAllCartFragment();
			}
		});
		homeButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mp.stop();
				videoViewReadyUan.stopPlayback();
				HomeFragment homeFragment = new HomeFragment();
				Activity.pushFragments(homeFragment, false, true);
			}
		});
		liveChatButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mp.stop();
				videoViewReadyUan.stopPlayback();
				LiveChatFragment fragment = new LiveChatFragment();
				Activity.pushFragments(fragment, false, true);
			}
		});
	}

	private void showdata() {
		Faqwebview = new WebView(activity);
		Faqwebview.setVerticalScrollBarEnabled(false);
		
		Faqwebview.getSettings().setJavaScriptEnabled(true);
		Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));

		if (Build.VERSION.SDK_INT >= 11)
			Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		 mp = MediaPlayer.create(getActivity(), R.raw.ready_audio);
		
		/*this.Faqwebview.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				activity.startSpinwheel(false, false);
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				activity.stopSpinWheel();
				Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
				if (Build.VERSION.SDK_INT >= 11)
					Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				// parenrLyt.setBackgroundDrawable(getResources().getDrawable(R.drawable.webview_bg));

				if (url.contains("http://www.u3an.com/Thankyou.aspx")) {
					linearVideoShow.setVisibility(View.VISIBLE);
					
					//showVideoDialog();
					
					
					uriPath = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.sounded_ready_order_gray_bg;

					final MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.ready_audio);
					videoViewReadyUan.setVideoPath(uriPath);
					mp.start();
					videoViewReadyUan.start();
					
					
				    videoViewReadyUan.setOnPreparedListener (new OnPreparedListener() {                    
				        @Override
				        public void onPrepared(MediaPlayer mp1) {
				        	
				        	mp.setLooping(true);
				        	
				        	mp1.setLooping(true);
				        	
				           
				        }
				    });
				   //mp.start();
				    
					
				}
			}
		});
*/
		int pad = com.mawaqaa.u3an.utilities.Utilities.convertDip2Pixels(getActivity(), 20);
		Faqwebview.setPadding(pad, pad, pad, pad);
		faqlay.addView(Faqwebview);
	// Jijo		
		Faqwebview.setWebViewClient(new MyBrowser());
		Faqwebview.loadUrl(url);
			
		
	}

	private void showVideoDialog() {
		try {
			DialogReadyU3AnVideo qtyDialog = new DialogReadyU3AnVideo();
			qtyDialog.show(getActivity().getSupportFragmentManager(), "DialogReadyU3AnVideo");
		} catch (NullPointerException ne) {
			// Log.e(TAG, "Null Pointer Exception in Sharing");
			ne.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		videoViewReadyUan.stopPlayback();
		mp.stop();
		PopAllCartFragment();
	}
	
	
	private class MyBrowser extends WebViewClient {
		 
		@Override
		public void onPageStarted(WebView view, String url, Bitmap favicon) {
			activity.startSpinwheel(false, false);
			super.onPageStarted(view, url, favicon);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			activity.stopSpinWheel();
			Faqwebview.setBackgroundColor(Color.parseColor("#FFFFFF"));
			if (Build.VERSION.SDK_INT >= 11)
				Faqwebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			// parenrLyt.setBackgroundDrawable(getResources().getDrawable(R.drawable.webview_bg));

			if (url.contains("http://www.u3an.com/Thankyou.aspx")) {
				linearVideoShow.setVisibility(View.VISIBLE);
				
				//showVideoDialog();
				try{
					
					if(getActivity().getPackageName()!=null){
						
						uriPath = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.sounded_ready_order_gray_bg;

						videoViewReadyUan.setVideoPath(uriPath);
						mp.start();
						videoViewReadyUan.start();
						
						
					    videoViewReadyUan.setOnPreparedListener (new OnPreparedListener() {                    
					        @Override
					        public void onPrepared(MediaPlayer mp1) {
					        	
					        	mp.setLooping(true);
					        	
					        	mp1.setLooping(true);
					        	
					           
					        }
					    });
					   //mp.start();
				    
						}
				}catch(NullPointerException ex){
					ex.printStackTrace();
				}
				
			}
		}

	    @Override
	    public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        if (url.startsWith("http://www.u3an.com/Receipt.aspx")) {
	           Log.d("WebView", "Click on Button"+"URL:"+url);
	           videoViewReadyUan.stopPlayback();
	           mp.stop();
			   linearVideoShow.setVisibility(View.GONE);
			   view.loadUrl(url);
	           return true;
	        }
	        return false;
	    }       
	   }


	@Override
	public void pushFragments4LanSwitch() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void LanSwitchTrigered() {
		// TODO Auto-generated method stub
		
	}

}
