package com.mawaqaa.u3an.fragments;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Locale;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;

public class HeaderViewControlFragment extends BaseFragment {

	private String TAG = "BrandView parent fragment";
	ImageButton backButton, homeButton;
	TextView title;
	RelativeLayout containerView;
	// LayoutInflater layInflat;
	// ViewGroup contain;
	FragmentManager fragmentManager;
	Spinner category;
	View v;
	ListView listName;
	String header;
	protected TextView m_txtFilter;
	protected RelativeLayout rellayBase;

	TextView cartTextView;
	TextView countView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.layout_header_view_control,
				container, false);
		intitializeView(rootView);
		setupUI(rootView, true);
		AddContentView(InitializeContainer(inflater, container));
		fragmentManager = getFragmentManager();
		return rootView;
	}

	private void intitializeView(View rootView) {
		title = (TextView) rootView.findViewById(R.id.title);
		backButton = (ImageButton) rootView.findViewById(R.id.backButton);
		
		homeButton = (ImageButton) rootView.findViewById(R.id.imgHomeButton);
		
		containerView = (RelativeLayout) rootView
				.findViewById(R.id.view_container);
		category = (Spinner) rootView.findViewById(R.id.category_spinner);
		m_txtFilter = (TextView) rootView.findViewById(R.id.filter_txt);

		listName = (ListView) rootView.findViewById(R.id.drawer_list);
		title.setSelected(true);

		cartTextView = (TextView) rootView.findViewById(R.id.cart_txt_cart);
		countView = (TextView) rootView.findViewById(R.id.textViewcartcount);
		rellayBase = (RelativeLayout) rootView.findViewById(R.id.relaybase);
		backButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				backButtonClicked();
			}
		});

		title.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				backButtonClicked();
			}
		});
	}

	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		return v;
	}

	public void AddContentView(View v) {
		if (containerView == null) {
		} else {
			containerView.removeAllViews();
			if (v == null) {
			} else {
				containerView.addView(v);
			}
		}
	}
//Jijo
	protected void setHeader() {
		Bundle data = getArguments();
		if (data != null)
			header = data.getString("Header");
		if (header != null)
			Log.e("the data", ""+header);
		switch (header) {
		case AppConstants.ALL_RESTAURANT_TAG:
			setTitleForPage1(R.string.all_restaurent);
			break;
		case AppConstants.MOST_SELLING_TAG:
			setTitleForPage1(R.string.most_selling_dishes);
			break;
		case AppConstants.NEW_RESTAURANTS_TAG:
			setTitleForPage1(R.string.new_restaurent);
			break;
		case AppConstants.DETAILED_SEARCH_TAG:
			setTitleForPage1(R.string.detailed_search_heading);
			break;
		default:
			break;
		}
	}

	public void showBackButton(boolean show) {
		if (show) {
			backButton.setVisibility(View.VISIBLE);
		} else {
			backButton.setVisibility(View.GONE);
		}
	}
	
	public void showHomeButton(boolean show) {
		if(show){
			homeButton.setVisibility(View.VISIBLE);
		}else{
			homeButton.setVisibility(View.GONE);
		}
	}

	public void showCartButton(boolean show) {
		if (show) {
			rellayBase.setVisibility(View.VISIBLE);
		} else {
			rellayBase.setVisibility(View.GONE);
		}
	}
//Jijo
	public void showCategorySpinner(boolean show) {
		if(getLanguage().equals("en"))
			category.setBackgroundResource(R.drawable.top_btn);
		else
			category.setBackgroundResource(R.drawable.top_btn_ar);
		if (show) {
			category.setVisibility(View.VISIBLE);
		} else {
			category.setVisibility(View.GONE);
		}
	}

	public void showFilterText(boolean show) {
		if (show) {
			m_txtFilter.setVisibility(View.VISIBLE);
		} else {
			m_txtFilter.setVisibility(View.GONE);
		}
	}

	public void setTitleForPage(String text) {
		title.setText(text);
	}
	
	public void setTitleForPage1(int resId) {
		title.setText(getResources().getResourceEntryName(resId));
	}
	

	@Override
	public void onResume() {
		super.onResume();
	}

	protected void backButtonClicked() {
		// Activity.popFragments();
		getActivity().onBackPressed();
	}

	@Override
	public void pushFragments4LanSwitch() {
		// TODO Auto-generated method stub

	}

	@Override
	public void LanSwitchTrigered() {
		Log.e("Language ", "changed");
		
	}
	
	public String getLanguage(){
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		return sDefSystemLanguage;
	}

}
