package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;

public class LoginOptionFragment extends DialogFragment implements
		OnClickListener {
	private String TAG = "loginOption::Fragment";
	Button loginButton, guestButton;
	Activity act;

	String areaID, itemChoiceID, itemChoiceQuantity, itemID, quantity,
			restaurantID, userName;
	
	String restaurant_Name, rest_Logo, min_amt, sectID, sectName, ratng;
	int idSele;
	
	public LoginOptionFragment(BaseActivity baseActivity, String areaiD,
			String iteString, String qty, String itmId, String rest_ID,
			String userN,String restName,String rest_Logo,String min_Amt,String sect_ID,String sect_NAme,String rating,int idForselection) {
		this.act = baseActivity;
		areaID = areaiD;
		itemChoiceID = iteString;
		itemChoiceQuantity = qty;
		itemID = itmId;
		quantity = qty;
		restaurantID = rest_ID;
		userName = userN;
		restaurant_Name = restName;
		this.rest_Logo = rest_Logo;
		min_amt = min_Amt;
		sectID = sect_ID;
		sectName = sect_NAme;
		ratng = rating;
		idSele =idForselection;
	}

	@Override
	public void onStart() {
		super.onStart();
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		int width = display.getWidth() * 4 / 4;
		params.height = width;
		params.dimAmount = 0.8f; // dim only a little bit
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog mydiDialog = new Dialog(getActivity());
		mydiDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mydiDialog.setContentView(R.layout.dialog_login_option);
		if (getActivity() instanceof BaseActivity)
			((BaseActivity) getActivity()).setupUI(
					mydiDialog.findViewById(R.id.dialogue_lyt), false);
		loginButton = (Button) mydiDialog.findViewById(R.id.login_option_login);
		guestButton = (Button) mydiDialog.findViewById(R.id.login_option_guest);
		loginButton.setOnClickListener(this);
		guestButton.setOnClickListener(this);
		mydiDialog.setCanceledOnTouchOutside(true);
		return mydiDialog;
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;
		getDialog().dismiss();
		switch (v.getId()) {
		case R.id.login_option_login:
			fragment = new LoginFragment();
			((BaseActivity) act).pushFragments(fragment, false, true);
			break;
		case R.id.login_option_guest:
			((BaseActivity) act).WriteDataToFile("Guest");
			getDialog().cancel();
			if (idSele == 1) {
				Toast.makeText(act, R.string.select_choice, Toast.LENGTH_SHORT).show();
			}else{
			new doAddToTempCart().execute();
			}
			break;
		default:
			break;
		}
	}

	public class doAddToTempCart extends AsyncTask<Void, String, Void> {
		String result;
		JSONObject jObj = null;
		InputStream istream;

		public doAddToTempCart() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			((BaseActivity) act).startSpinwheel(false, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				JSONObject objJson = new JSONObject();
				objJson.accumulate("AreaId", areaID);
				objJson.accumulate("ItemChoiceIds", itemChoiceID);
				objJson.accumulate("ItemChoiceQty", itemChoiceQuantity);
				objJson.accumulate("ItemId", itemID);
				objJson.accumulate("Quantity",quantity);
				objJson.accumulate("RestaurantId", restaurantID);
				objJson.accumulate("Username", userName);
				Log.e("quantity", ""+quantity);

				Log.e("The JSON Objec...............t", ""+objJson.toString());
				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost httppost = new HttpPost(AppConstants.addToTempCartURL);
				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				Log.i("Login Request", ""+objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);
					Log.i("Login Result", ""+result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			((BaseActivity) act).stopSpinWheel();
			if (this.result.contains("Success")) {
				Fragment fragment = new RestaurantMenuInnerSectionFragment();
				Bundle bundle = new Bundle();
				bundle.putString("Area_ID", areaID);
				bundle.putString("restaurant_id", restaurantID);
				bundle.putString("restaurant_name", restaurant_Name);
				bundle.putString("restaurant_rating", ratng);
				bundle.putString("Header", "df");
				bundle.putString("restaurant_logo", rest_Logo);
				bundle.putString("menuItemId", "" + sectID);
				bundle.putString("menuItemName", sectName);
				bundle.putString("minimum_Amount", min_amt);
				fragment.setArguments(bundle);
				((BaseActivity) act).pushFragments(fragment, false, true);
			} else {
				Toast.makeText(act, getResources().getString(R.string.cannot_order),
						Toast.LENGTH_SHORT).show();
			}
		}
	}
}
