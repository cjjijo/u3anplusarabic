package com.mawaqaa.u3an.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;

public class PrivacyDailogueFragment extends DialogFragment {
	Activity act;
	public PrivacyDailogueFragment(BaseActivity baseActivity) {
		this.act=baseActivity;
	}
	@Override
	public void onStart() {
		super.onStart();
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		int width = display.getWidth() * 4 / 4;
		params.height = width;
		params.dimAmount = 0.8f; // dim only a little bit
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final Dialog mydiDialog = new Dialog(getActivity());
		mydiDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mydiDialog.setContentView(R.layout.privacydialogue);
		if (getActivity() instanceof BaseActivity)
			((BaseActivity) getActivity()).setupUI(
					mydiDialog.findViewById(R.id.dialogue_lyt), false);
		return mydiDialog;
		
	}
}
