package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class AccountInfoInnerpageFragment extends HeaderViewControlFragment
		implements OnClickListener {
	EditText firstNameEditText, lastNameEditText, phoneEditText,
			resi_Phone_EditText;
	String userName;
	ArrayList<HashMap<String, String>> AddressList;
	Button saveButton;
	String firstNameString, lastNameString, phoneString, resi_phoneString,
			firstname, lastname, mobilePhone, resi_PhNo, gender, sms,
			newsletter, occupation, uancreadit;
	EditText errorEditTxt;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	LogoutListener listener;
	int maxLength = 25;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_account_infoinner, gp,
				false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.my_account_heading));
		init(rootView);
		Log.e("firstNameString", "" + firstNameString);
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;

		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		// AccumulateDataforGetAddress();
		return rootView;
	}

	private void init(View view) {
		firstNameEditText = (EditText) view
				.findViewById(R.id.Edittext_my_Account_firstname);
		firstNameString = PrefUtil.getFirstname(getActivity());
		Log.e("firstNameString", "" + firstNameString);
		firstNameEditText.setText(firstNameString);
		firstNameEditText.addTextChangedListener(new SaveAddressTextwatcher(
				firstNameEditText));
		firstNameEditText.setOnFocusChangeListener(new FocusChangedListener());
		firstNameEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		lastNameEditText = (EditText) view
				.findViewById(R.id.Edittext_myaccount_lastname);
		lastNameString = PrefUtil.getLastname(getActivity());
		lastNameEditText.setText(lastNameString);
		lastNameEditText.addTextChangedListener(new SaveAddressTextwatcher(
				lastNameEditText));
		lastNameEditText.setOnFocusChangeListener(new FocusChangedListener());

		phoneEditText = (EditText) view
				.findViewById(R.id.Edittext_myaccount_email);
		phoneString = PrefUtil.getphonenum(getActivity());
		phoneEditText.setText(phoneString);
		phoneEditText.addTextChangedListener(new SaveAddressTextwatcher(
				phoneEditText));
		phoneEditText.setOnFocusChangeListener(new FocusChangedListener());

		resi_Phone_EditText = (EditText) view
				.findViewById(R.id.Edittext_myaccount_phoneNumber);
		resi_phoneString = PrefUtil.getreside_phonenum(getActivity());
		resi_Phone_EditText.setText(resi_phoneString);
		resi_Phone_EditText.addTextChangedListener(new SaveAddressTextwatcher(
				resi_Phone_EditText));
		resi_Phone_EditText
				.setOnFocusChangeListener(new FocusChangedListener());
		saveButton = (Button) view.findViewById(R.id.saveButtonAccountInfo);
		saveButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.saveButtonAccountInfo:
			DataValidation();
			break;
		default:
			break;
		}
	}

	private void DataValidation() {
		boolean isvalid = doFirstNameValidation();
		if (!isvalid)
			return;
		isvalid = doLastNameValidation();
		if (!isvalid)
			return;
		isvalid = doPhoneValidation();
		if (!isvalid)
			return;
		isvalid = doResi_PhoneValidation();
		if (!isvalid)
			return;
		if (isvalid) {
			UpLoadData();
		} else {
			return;
		}
	}

	private Boolean doFirstNameValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		firstNameString = firstNameEditText.getText().toString();
		if (firstNameString.length() == 0) {
			firstNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.first_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				firstNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter first name to continue</font>"));
			}else{
				firstNameEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£ÙˆÙ„ Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
			errorEditTxt = firstNameEditText;
			firstNameEditText.addTextChangedListener(registerInputTextWatcher);
			// firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean doLastNameValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		lastNameString = lastNameEditText.getText().toString();
		if (lastNameString.length() == 0) {
			lastNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.last_name)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				lastNameEditText
						.setError(Html
								.fromHtml("<font color='white'>Enter last name to continue</font>"));
			}else{
				lastNameEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£Ø®ÙŠØ± Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
			errorEditTxt = lastNameEditText;
			lastNameEditText.addTextChangedListener(registerInputTextWatcher);
			// firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean doPhoneValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		phoneString = phoneEditText.getText().toString();
		Boolean isValidPhone = Utilities.isValidPhoneNumber(phoneString);
		if (phoneString.length() == 0) {
			phoneEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				phoneEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				phoneEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			errorEditTxt = phoneEditText;
			phoneEditText.addTextChangedListener(registerInputTextWatcher);
			// mobileEditText.requestFocus();
			return false;
		}
		if (!Utilities.isValidPhoneNumber(phoneString)) {
			phoneEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				phoneEditText
						.setError(Html
								.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				phoneEditText
					.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			errorEditTxt = phoneEditText;
			phoneEditText.addTextChangedListener(registerInputTextWatcher);
			// mobileEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private Boolean doResi_PhoneValidation() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		resi_phoneString = resi_Phone_EditText.getText().toString();
		if (resi_phoneString.length() == 0) {
			resi_Phone_EditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_resi_phNum)+"</font>"));
			/*if(sDefSystemLanguage.equals("en")){
				resi_Phone_EditText
						.setError(Html
								.fromHtml("<font color='white'>Enter a phone number to continue</font>"));
			}else{
				resi_Phone_EditText
					.setError(Html
						.fromHtml("<font color='white'> Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� Ø§Ù„Ù…Ù†Ø²Ù„</font>"));
			}*/
			errorEditTxt = resi_Phone_EditText;
			resi_Phone_EditText
					.addTextChangedListener(registerInputTextWatcher);
			// mobileEditText.requestFocus();
			return false;
		}
		if (!Utilities.isValidPhoneNumber(resi_phoneString)) {
			resi_Phone_EditText
					.setError(Html
							.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_resi_phNum)+"</font>"));
			errorEditTxt = resi_Phone_EditText;
			resi_Phone_EditText
					.addTextChangedListener(registerInputTextWatcher);
			// mobileEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};

	private class SaveAddressTextwatcher implements TextWatcher {
		private EditText editText;

		SaveAddressTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	}

	// Data Validation on Focus change in Edittext
	private class FocusChangedListener implements OnFocusChangeListener {

		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {
				case R.id.Edittext_my_Account_firstname:
					doFirstNameValidation();
					break;
				case R.id.Edittext_myaccount_lastname:
					doLastNameValidation();
					break;
				case R.id.Edittext_myaccount_email:
					doPhoneValidation();
					break;
				case R.id.Edittext_myaccount_phoneNumber:
					doResi_PhoneValidation();
					break;
				default:
					break;
				}
			}
		}
	}

	// Getting data from the form and uploading to server.
	private void UpLoadData() {
		firstNameString = firstNameEditText.getText().toString();
		Log.e("firstNameString", "" + firstNameString);
		PrefUtil.setFirstname(getActivity(), firstNameString);
		lastNameString = lastNameEditText.getText().toString();
		Log.e("lastNameString", "" + lastNameString);
		PrefUtil.setLastname(getActivity(), lastNameString);
		phoneString = phoneEditText.getText().toString();
		PrefUtil.setPhonenum(getActivity(), phoneString);
		Log.e("phoneString", "" + phoneString);
		resi_phoneString = resi_Phone_EditText.getText().toString();
		Log.e("resi_phoneString", "" + resi_phoneString);
		PrefUtil.setredsi_Phonenum(getActivity(), resi_phoneString);
		Fragment personalFragment = new PersonalInformationFragment();
		Bundle data = new Bundle();
		data.putString("firstName", firstNameString);
		data.putString("lastName", lastNameString);
		data.putString("mobileNo", phoneString);
		data.putString("homePhNo", resi_phoneString);
		AccumulateData();
	}

	private void AccumulateData() {
		String authkey = PrefUtil.getAuthkey(getActivity());
		String gender = PrefUtil.getgender(Activity);
		String newsletter = PrefUtil.getnewsletter(getActivity());
		String occupation = PrefUtil.getOccupation(getActivity());
		String sms = PrefUtil.getsms(getActivity());
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("authKey", authkey);
			jsonObject.accumulate("birthday", "");
			jsonObject.accumulate("company", "");
			jsonObject.accumulate("firstName", firstNameString);
			jsonObject.accumulate("gender", gender);
			jsonObject.accumulate("housePhone", resi_phoneString);
			jsonObject.accumulate("howToContact", "");
			jsonObject.accumulate("lastName", lastNameString);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("mobile", phoneString);
			jsonObject.accumulate("subscribedToNewsletter", newsletter);
			jsonObject.accumulate("subscribedToSMS", sms);
			jsonObject.accumulate("occupation", occupation);
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
			jsonObject.accumulate("workPhone", "");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL", ""+AppConstants.AccountInformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.AccountInformation,
					jsonObject);
		}
	}

	@Override
	public void EditAccountSuccessfully(JSONObject jsonObject) {
		Toast.makeText(Activity, R.string.account_edit,
				Toast.LENGTH_SHORT).show();
		Activity.stopSpinWheel();
		super.EditAccountSuccessfully(jsonObject);
		Activity.onBackPressed();
		Log.e("jsonObject", "" + jsonObject.toString());
	}

	@Override
	public void EditAccountfail(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.EditAccountfail(jsonObject);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
