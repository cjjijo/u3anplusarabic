package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class ConfirmOrderSecondStage extends HeaderViewControlFragment
		implements OnClickListener {

	String currentDay;
	String currentTime;
	String userName;
	JSONObject jsonobject;
	JSONArray jsonarray;

	ArrayList<HashMap<String, String>> arraylist_cart_data;
	ArrayList<HashMap<String, String>> arraylist_item_data = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> arraylist_final_data = new ArrayList<HashMap<String, String>>();
	ArrayList<HashMap<String, String>> arraylist_complete_data = new ArrayList<HashMap<String, String>>();

	ArrayList<View> arraylist_GeneralView = new ArrayList<View>();
	ArrayList<View> arraylist_SpecialView = new ArrayList<View>();

	Boolean acceptKNETTag = true, acceptVisaTag = true;

	String imageString, minimumAmountString, restaurantName;
	LinearLayout containerLinearLayout;

	String subTotal, deliveryCharges, GrandTotal, payByCashAll, discountAll,maximumPoint,minimumPoint,pointInKD, totalPoint, u3anCharges;

	LinearLayout fullLayout;

	boolean acceptCC, acceptKnet, AcceptCash;
	Button nextButton;
	ArrayList<String> tyming_data = new ArrayList<String>();

	String firstName, email, block, streetString, juddaString, buildinString,
			floorString, apartmentString, directionString, address_Type,
			areaId, housePhone, lastName, mobileNumber, workphone, company;

	String radioGroupTag = "111";
	String spinnerTag = "222";
	String generalEditTextTag = "333";
	String specialEditTextTag = "444";
	String linearlayoytTag = "555";

	String subTotalString, discount, delivery_Charges;
	float dummyRestaurantAmount;
	String restaurant_Amount;
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	DecimalFormat df = (DecimalFormat)nf;
	
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.confirm_order_second_stage, gp,
				false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.confirm_order_heading));
		address_Type = getArguments().getString("address_Type");
		areaId = getArguments().getString("areaID");
		block = getArguments().getString("block");
		buildinString = getArguments().getString("buildinString");
		directionString = getArguments().getString("directionString");
		firstName = getArguments().getString("firstName");
		floorString = getArguments().getString("floorString");
		housePhone = getArguments().getString("housePhone");
		juddaString = getArguments().getString("juddaString");
		lastName = getArguments().getString("lastName");
		mobileNumber = getArguments().getString("mobile");
		streetString = getArguments().getString("streetString");
		workphone = getArguments().getString("workPhone");
		company = getArguments().getString("Company");
		userName = getArguments().getString("UserName");
		email = getArguments().getString("email");
		apartmentString = getArguments().getString("apartmentString");

		Activity.stopSpinWheel();
		init(rootView);
		new getCartInformation().execute();
		return rootView;
	}

	private void init(View view) {
		containerLinearLayout = (LinearLayout) view
				.findViewById(R.id.linlay_container);
		containerLinearLayout.setVisibility(View.INVISIBLE);

		fullLayout = (LinearLayout) view.findViewById(R.id.fulllayout);
		fullLayout.setVisibility(View.INVISIBLE);

		nextButton = (Button) view.findViewById(R.id.nextButton);
		nextButton.setOnClickListener(this);
	}

	public class getCartInformation extends AsyncTask<Void, Void, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		String fName;

		InputStream istream;
		private ProgressDialog mProgressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
			mProgressDialog.setCancelable(false);
			mProgressDialog
					.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				objJson.accumulate("userId", userName);
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost;
				if (Utilities.doemailValidation(userName)) {
					httppost = new HttpPost(AppConstants.GET_CART_INFO_URL);
				} else {
					httppost = new HttpPost(AppConstants.GET_TEMP_CART_INFO_URL);
				}

				StringEntity entity = new StringEntity(objJson.toString());
				Log.i("Login Request",""+ objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);

				} else {
					result = null;
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.w("=========",""+jObj);
			mProgressDialog.dismiss();
			if(jObj==null){
				Toast.makeText(getActivity(), getResources().getString(R.string.no_service), Toast.LENGTH_SHORT).show();
				Activity.onBackPressed();
			}else{
				parseAndGroupData(jObj);
			}
		}
	}

	private void parseAndGroupData(JSONObject jsonObject1) {
		containerLinearLayout.setVisibility(View.VISIBLE);
		fullLayout.setVisibility(View.VISIBLE);
		Log.d("ConfirmOrderSecondStage....jijooooooo", "Parse and group data");
		String restaurant_Name = "noname";
		jsonobject = new JSONObject();
		jsonobject = jsonObject1;
		try {
			// Create an array
			arraylist_cart_data = new ArrayList<HashMap<String, String>>();

			deliveryCharges = jsonObject1.getString("DeliveryChargesAll");
			subTotal = jsonObject1.getString("SubtotalAll");
			GrandTotal = jsonObject1.getString("GrandTotalAll");
			payByCashAll = jsonObject1.getString("PayByCashAll");
			discountAll = jsonObject1.getString("DiscountAll");
			minimumPoint = jsonObject1.getString("MinimumPoint");
			maximumPoint = jsonObject1.getString("MaximumPoint");
			pointInKD = jsonObject1.getString("PointinKD");
			totalPoint = jsonObject1.getString("TotalPoint");
			u3anCharges = jsonObject1.getString("U3anCharges");

			try {
				// Locate the array name in JSON
				jsonarray = jsonobject
						.getJSONArray(AppConstants.get_cart_info_jsonobj);

				if (jsonarray.length() > 0) {
					for (int i = 0; i < jsonarray.length(); i++) {
						jsonobject = jsonarray.getJSONObject(i);
						JSONArray jsonArray = jsonobject
								.getJSONArray(AppConstants.get_cart_info_sub_jsonobj);
						for (int j = 0; j < jsonArray.length(); j++) {
							HashMap<String, String> map = new HashMap<String, String>();
							JSONObject dummyJsonObject = jsonArray
									.getJSONObject(j);
							// Retrieve JSON Objects
							map.put(AppConstants.get_cart_info_area_ID,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_area_ID));
							Log.e("Area ID",
									dummyJsonObject
											.getString(AppConstants.get_cart_info_area_ID));
							map.put(AppConstants.get_cart_info_ItemChoiceIds,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ItemChoiceIds));
							map.put(AppConstants.get_cart_info_ItemId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ItemId));
							map.put(AppConstants.get_cart_info_ItemName,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ItemName));
							map.put(AppConstants.get_cart_info_OrderDate,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_OrderDate));
							map.put(AppConstants.get_cart_info_Quantity,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_Quantity));
							map.put(AppConstants.get_cart_info_RestaurantId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_RestaurantId));
							map.put(AppConstants.get_cart_info_ShoppingCartId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_ShoppingCartId));
							map.put(AppConstants.get_cart_info_price,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_price));

							// Set the JSON Objects into the array
							arraylist_item_data.add(map);

							map.put(AppConstants.get_cart_info_RestaurantId,
									dummyJsonObject
											.getString(AppConstants.get_cart_info_RestaurantId));
							arraylist_final_data.add(map);

						}
						imageString = jsonobject
								.getString(AppConstants.get_cart_info_Thumbnail);

						subTotalString = jsonobject.getString("Subtotal");
						delivery_Charges = jsonobject
								.getString("DeliveryCharges");
						discount = jsonobject.getString("Discount");
//Jijo problem found
						/* Extracting timing data */
						JSONArray jsonarraytymin = jsonobject
								.getJSONArray(AppConstants.get_cart_info_timing_obj);
						
						//Log.d("Time................", ""+jsonarraytymin
						//			.getJSONObject(jsonarraytymin.length()-1).getString("DelTime"));
						
						for (int j = 0; j < jsonarraytymin.length(); j++) {
							JSONObject dummytymngJsonObject = jsonarraytymin
									.getJSONObject(j);
							tyming_data
									.add(dummytymngJsonObject
											.getString(AppConstants.get_cart_info_timing));
						}
						// Minimum amount is not available right now, So replace
						// it
						// with, when available(Top priority)
						minimumAmountString = jsonobject
								.getString(AppConstants.get_cart_info_MinimumAmount);
						restaurantName = jsonobject
								.getString(AppConstants.get_cart_info_RestaurantName);

						acceptCC = jsonobject
								.getBoolean(AppConstants.get_cart_info_AcceptsCC);
						AcceptCash = jsonobject
								.getBoolean(AppConstants.get_cart_info_AcceptsCash);
						acceptKnet = jsonobject
								.getBoolean(AppConstants.get_cart_info_AcceptsKNET);
						
						restaurant_Amount = null;
						restaurant_Amount = jsonobject.getString("RestaurantTotal");
						Log.e("Restaurant Total from service", ""+restaurant_Amount);
						dummyRestaurantAmount = Float.parseFloat(restaurant_Amount);
						//restaurant_Amount = String.format("%.2f", dummy);

						AddView();
					}
				} else {

					Activity.popFragments(new ConfirmOrderDeliveryDetailsFragment());
					Activity.popFragments(new ConfirmOrderSecondStage());
				}

			} catch (JSONException e) {
				Log.e("Error",""+ e.getMessage());
				e.printStackTrace();
				Activity.popFragments(new ConfirmOrderDeliveryDetailsFragment());
				Activity.popFragments(new ConfirmOrderSecondStage());
			}
		} catch (Exception e) {
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		arraylist_cart_data = new ArrayList<HashMap<String, String>>();
		arraylist_item_data = new ArrayList<HashMap<String, String>>();
		arraylist_final_data = new ArrayList<HashMap<String, String>>();
		arraylist_complete_data = new ArrayList<HashMap<String, String>>();

		arraylist_GeneralView = new ArrayList<View>();
		arraylist_SpecialView = new ArrayList<View>();
	}

	
	//Dynamically adding restaurant Specific View
	private void AddView() {
		df.applyPattern("###,##0.000");
		String restaurant_ID = null;
		float restaurant_Total = 0;
		LayoutInflater layoutInflater = (LayoutInflater) Activity
				.getBaseContext().getSystemService(
						Context.LAYOUT_INFLATER_SERVICE);
		final View addView = layoutInflater
				.inflate(R.layout.ordersummery, null);

		ImageView imageView = (ImageView) addView
				.findViewById(R.id.imageView1_rest_logo);
		TextView restaurant_Name = (TextView) addView
				.findViewById(R.id.rest_name);
		RadioGroup radioGroup = (RadioGroup) addView
				.findViewById(R.id.radiodate);
		final Spinner timingSpinner = (Spinner) addView
				.findViewById(R.id.deliverinfo);
		timingSpinner.setVisibility(View.GONE);
		EditText generalViewEditText = (EditText) addView
				.findViewById(R.id.editTextnamelogin);

		radioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				Log.d("chk", "id" + checkedId);

				if (checkedId == R.id.now) {
				timingSpinner.setVisibility(View.GONE);
				} else if (checkedId == R.id.delivery) {
					timingSpinner.setVisibility(View.VISIBLE);
				}
			}
		});

		Spinner timinSpinner = (Spinner) addView.findViewById(R.id.deliverinfo);
		timinSpinner.setAdapter(new SpinnerAdapter(Activity, tyming_data));
		tyming_data = new ArrayList<String>();

		ImageView visaImageView = (ImageView) addView.findViewById(R.id.visa);
		ImageView knetImageView = (ImageView) addView.findViewById(R.id.knet);
		ImageView cashImageView = (ImageView) addView.findViewById(R.id.cash);
		TextView restaurant_Totals = (TextView) addView
				.findViewById(R.id.restaurant_Total);
		LinearLayout subcontainerLayout = (LinearLayout) addView
				.findViewById(R.id.subcontainer);
		for (int i = 0; i < arraylist_item_data.size(); i++) {
			LayoutInflater layoutInflatersubview = (LayoutInflater) Activity
					.getBaseContext().getSystemService(
							Context.LAYOUT_INFLATER_SERVICE);
			
			//Item Specific View inside the restaurant
			final View addViewsubview = layoutInflatersubview.inflate(
					R.layout.menusingleitem, null);
			TextView itemName = (TextView) addViewsubview
					.findViewById(R.id.itemname);
			TextView quantity = (TextView) addViewsubview
					.findViewById(R.id.qty);
			final Button closeButton = (Button) addViewsubview
					.findViewById(R.id.close_btn);

			EditText specialViewEditText = (EditText) addViewsubview
					.findViewById(R.id.editTextnamelogin);
			TextView price = (TextView) addViewsubview.findViewById(R.id.price);
			TextView total = (TextView) addViewsubview.findViewById(R.id.total);
			try {
				HashMap<String, String> map = arraylist_item_data.get(i);
				itemName.setText(map.get(AppConstants.get_cart_info_ItemName));
				itemName.setSelected(true);
				quantity.setText(map.get(AppConstants.get_cart_info_Quantity));
				//anju
				float priz = Float.parseFloat(map
						.get(AppConstants.get_cart_info_price));
				price.setText(df.format(priz));
				
				int quanty = Integer.parseInt(map
						.get(AppConstants.get_cart_info_Quantity));
				restaurant_ID = map
						.get(AppConstants.get_cart_info_RestaurantId);
				restaurant_Total = restaurant_Total + (priz * quanty);
				
				total.setText( df.format(priz * quanty));

				closeButton.setId(Integer.parseInt(map
						.get(AppConstants.get_cart_info_ShoppingCartId)));
				specialViewEditText.setId(Integer.parseInt(specialEditTextTag
						+ map.get(AppConstants.get_cart_info_ShoppingCartId)));

			} catch (Exception e) {
				Log.e("Something went wrong",""+ e.getMessage());
			}
			closeButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					accumulateJSONObject(closeButton.getId());
				}
			});
			subcontainerLayout.addView(addViewsubview);
			arraylist_SpecialView.add(addViewsubview);
		}

		if (acceptCC) {
			visaImageView.setVisibility(View.VISIBLE);
		} else {
			visaImageView.setVisibility(View.GONE);
			acceptVisaTag = false;
		}

		if (acceptKnet) {
			knetImageView.setVisibility(View.VISIBLE);
		} else {
			knetImageView.setVisibility(View.GONE);
			acceptKNETTag = false;
		}
		cashImageView.setVisibility(View.VISIBLE);
        //Anju
		boolean chkLang = PrefUtil.isAppLanEnglish(getActivity());
		//if(chkLang){
			restaurant_Totals.setText(getString(R.string.restaurant_Total)+getString(R.string.KD)+" "+ df.format(dummyRestaurantAmount));
		/*}else{
			restaurant_Totals.setText(df.format(dummyRestaurantAmount)+getString(R.string.KD)+getString(R.string.restaurant_Total));
		}*/
		Picasso.with(Activity).load(imageString).into(imageView);
		restaurant_Name.setText(restaurantName);
		TextView subTotalTextView = (TextView) addView
				.findViewById(R.id.sub_Total);
		TextView discount = (TextView) addView.findViewById(R.id.discount);
		TextView deliveryCharges = (TextView) addView
				.findViewById(R.id.delivery_charges);

		float dummy = Float.parseFloat(subTotalString);	
		//anju
		subTotalTextView.setText(getString(R.string.KD)+" " + df.format(dummy));
		
		float dummyDiscount = Float.parseFloat(this.discount);
		//anju		
		discount.setText(getString(R.string.KD) +" "+df.format(dummyDiscount) );
		
		float floatdeliveryCharges = Float.parseFloat(delivery_Charges);
		//anju
		deliveryCharges.setText(getString(R.string.KD) +" "+df.format(floatdeliveryCharges) );

		timingSpinner.setId(Integer.parseInt(spinnerTag + restaurant_ID));
		generalViewEditText.setId(Integer.parseInt(generalEditTextTag
				+ restaurant_ID));

		containerLinearLayout.addView(addView);

		for (int j = 0; j < arraylist_item_data.size(); j++) {
			arraylist_GeneralView.add(addView);
		}
		arraylist_item_data = new ArrayList<HashMap<String, String>>();
		restaurant_Total = 0;
	}

	private void accumulateJSONObject(int id) {
		Activity.startSpinwheel(false, true);
		JSONObject jsonObjectRemoveCartItems = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectRemoveCartItems = new JSONObject();
			jsonObjectRemoveCartItems.accumulate("ShoppingCartId", id);
			jsonObjectRemoveCartItems.accumulate("Username", userName);
			Log.e("The data....... s.......",
					""+jsonObjectRemoveCartItems.toString());

		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryForgetPassword(jsonObjectRemoveCartItems);
	}

	private void loadSubCategoryForgetPassword(JSONObject jsonObject) {

		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			if (Utilities.doemailValidation(userName)) {
				commandFactory.sendPostCommand(
						AppConstants.REMOVE_CART_ITEM_URL, jsonObject);
			} else {
				commandFactory.sendPostCommand(
						AppConstants.REMOVE_TEMP_CART_ITEM_URL, jsonObject);
			}
		}
	}

	@Override
	public void removetempCartItemSuccess(JSONObject jsonObject) {
		super.removetempCartItemSuccess(jsonObject);
		Activity.stopSpinWheel();
		Log.e("The data", ""+jsonObject.toString());

		if (jsonObject.toString().contains("Success")) {
			Toast.makeText(Activity, R.string.cart_item_remove,
					Toast.LENGTH_SHORT).show();
			Log.e("Entered into removed ", "Token");
			Activity.onBackPressed();
			Fragment cartFragment = new ConfirmOrderSecondStage();
			Bundle data = new Bundle();
			data.putString("address_Type", address_Type);
			data.putString("areaID", areaId);
			data.putString("block", block);
			data.putString("buildinString", buildinString);
			data.putString("directionString", directionString);
			data.putString("firstName", firstName);
			data.putString("floorString", floorString);
			data.putString("housePhone", housePhone);
			data.putString("juddaString", juddaString);
			data.putString("lastName", lastName);
			data.putString("mobile", mobileNumber);
			data.putString("streetString", streetString);
			data.putString("workPhone", workphone);
			data.putString("UserName", userName);
			data.putString("email", email);
			data.putString("apartmentString", apartmentString);
			data.putString("Company", company);
			cartFragment.setArguments(data);
			Activity.pushFragments(cartFragment, false, true);
		} else {
			Toast.makeText(Activity, R.string.failed_to_remove,
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void removetempCartItemFail(JSONObject jsonObject) {
		super.removetempCartItemFail(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void removeCartItemSuccess(JSONObject jsonObject) {
		super.removeCartItemSuccess(jsonObject);
		Activity.stopSpinWheel();

		if (jsonObject.toString().contains("Success")) {
			Toast.makeText(Activity, R.string.cart_item_remove,
					Toast.LENGTH_SHORT).show();
			Log.e("Entered into removed ", "Token");
			Activity.onBackPressed();
			Fragment cartFragment = new ConfirmOrderSecondStage();
			Bundle data = new Bundle();
			data.putString("address_Type", address_Type);
			data.putString("areaID", areaId);
			data.putString("block", block);
			data.putString("buildinString", buildinString);
			data.putString("directionString", directionString);
			data.putString("firstName", firstName);
			data.putString("floorString", floorString);
			data.putString("housePhone", housePhone);
			data.putString("juddaString", juddaString);
			data.putString("lastName", lastName);
			data.putString("mobile", mobileNumber);
			data.putString("streetString", streetString);
			data.putString("workPhone", workphone);
			data.putString("UserName", userName);
			data.putString("email", email);
			data.putString("apartmentString", apartmentString);
			data.putString("Company", company);
			cartFragment.setArguments(data);
			Activity.pushFragments(cartFragment, false, true);
		} else {
			Toast.makeText(Activity, R.string.failed_to_remove,
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.nextButton:
			categorizeData();
			break;

		default:
			break;
		}
	}

	private void categorizeData() {
		for (int i = 0; i < arraylist_final_data.size(); i++) {
			String tym = null;
			HashMap<String, String> map = arraylist_final_data.get(i);

			String id = map.get(AppConstants.get_cart_info_RestaurantId);
			View sampleView = arraylist_GeneralView.get(i);
			EditText ed = (EditText) sampleView.findViewById(Integer
					.parseInt(generalEditTextTag + id));
			String x = ed.getText().toString();

			Spinner tymngSpinner = (Spinner) sampleView.findViewById(Integer
					.parseInt(spinnerTag + id));
//Jijo02-12-15
			try {
				tym = tymngSpinner.getSelectedItem().toString();
				//tym = getCurrentTime();
				Log.e("Get Current Time", ""+tym);
			} catch (Exception e) {

				tym = "";
				//tym = /* getCurrentDay() + " " + */getCurrentTime();
				Log.e("Get Current Time in Catch",""+ tym);
			}
			String shoppingCartId = map
					.get(AppConstants.get_cart_info_ShoppingCartId);

			View speciView = arraylist_SpecialView.get(i);
			EditText specialViewEditText = (EditText) speciView
					.findViewById(Integer.parseInt(specialEditTextTag
							+ shoppingCartId));

			String specialViewData = specialViewEditText.getText().toString();

			Log.e("The Special Data", ""+specialViewData);

			map.put("General Request", x);
			map.put("Special Request", specialViewData);
			map.put("Delivery time", tym);

			arraylist_complete_data.add(map);
		}
		Log.e("The data to Control visibility", acceptKNETTag + " "
				+ acceptVisaTag + " ");

		Fragment fragment = new ConfirmOrderFragment(arraylist_final_data);
		Bundle data = new Bundle();
		data.putString("UserName", userName);
		data.putString("areID", areaId);
		data.putString("firstName", firstName);
		data.putString("email", email);
		data.putString("block", block);
		data.putString("streetString", streetString);
		data.putString("juddaString", juddaString);
		data.putString("buildinString", buildinString);
		data.putString("floorString", floorString);
		data.putString("apartmentString", apartmentString);
		data.putString("directionString", directionString);
		data.putString("address_Type", address_Type);
		data.putString("deliveryCharges", deliveryCharges);
		data.putString("subTotal", subTotal);
		data.putString("GrandTotal", GrandTotal);
		data.putString("payByCashAll", payByCashAll);
		data.putString("discountAll", discountAll);
		data.putString("lastName", lastName);
		data.putString("mobile", mobileNumber);
		
		data.putString("housephone", housePhone);
		data.putString("Workphone", workphone);
		data.putString("company", company);
		data.putString("street", streetString);

		data.putBoolean("KNET", acceptKNETTag);
		data.putBoolean("VISA", acceptVisaTag);
		
		data.putString("minPoint", minimumPoint);
		data.putString("maxPoint", maximumPoint);
		data.putString("pointKD", pointInKD);
		data.putString("totalPoint", totalPoint);
		data.putString("u3anCharges", u3anCharges);

		fragment.setArguments(data);
		Activity.pushFragments(fragment, false, true);

	}

	private String getCurrentDay() {
		String weekDay;
		SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);
		Calendar calendar = Calendar.getInstance();
		weekDay = dayFormat.format(calendar.getTime());
		return weekDay;
	}

	private String getCurrentTime() {
		Date now = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("K:mm a",Locale.US);
		String formattedTime = sdf.format(now);
		return formattedTime;
	}

}
