package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.RestaurantMenuItemAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.MenuItemDetail;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.interfaces.UpdateCartCount;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.view.GridView;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class RestaurantMenuInnerSectionFragment extends
		HeaderViewControlFragment {
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	private GridView restMenuItemsGrid;
	String restaurant_Logo, menuSectionId, menuSectionName, restaurant_ID,
			restaurant_Name, restaurant_rating, area_ID, minimum_Amount;
	LogoutListener listener;

	String userName;
	UpdateCartCount update;
	
	int cartCount;
	
	public static RestaurantMenuInnerSectionFragment newInstance() {
		RestaurantMenuInnerSectionFragment f = new RestaurantMenuInnerSectionFragment();
		
        return f;
	}

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		TAB_TAG = getClass().getName();

		showCartButton(true);
		cartTextView.setOnClickListener(cartSelected);
		View v = info.inflate(R.layout.fragment_restaurantmenu_section, gp,
				false);

		getActivity().getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		setupUI(v, false);
		Typeface type = Typeface.createFromAsset(Activity.getAssets(),
				"fonts/TAHOMA.ttf");
		countView.setTypeface(type);
		//Anju
		setTitleForPage(PrefUtil.getFragHeading(getActivity()));
		setTitleForPage(getResources().getString(R.string.detailed_search_heading));
		userName = Activity.readFile();
		if (userName == null) {
			userName = Secure.getString(Activity.getContentResolver(),
					Secure.ANDROID_ID);
			Log.i("if.........getCartCount()", "............");
			getCartCount();
		} else if (Utilities.doemailValidation(userName)) {
			Log.i("else if.........getCartCount()", "............");
			getCartCount();
		} else {
			userName = Secure.getString(Activity.getContentResolver(),
					Secure.ANDROID_ID);
			getCartCount();
			Log.i("else.........getCartCount()", "............");
		}

		update = new UpdateCartCount() {
			@Override
			public void updateCount() {
				// if (userName == null) {
				// userName = Secure.getString(
				// Activity.getContentResolver(),
				// Secure.ANDROID_ID);
				// getCartCount();
				// }
				// else if (Utilities.doemailValidation(userName)) {
				// getCartCount();
				// }else{
				// userName = Secure.getString(
				// Activity.getContentResolver(),
				// Secure.ANDROID_ID);
				// getCartCount();
				// }
				//
				// AccumulateJSONObject(menuSectionId);
				if (listener != null) {
					listener.reCreateFrag(RestaurantMenuInnerSectionFragment.this);
				}
			}
		};
		initView(v);
		return v;
	}

	private void getCartCount() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			// country id is always 1 for kuwait...
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", userName);
			Log.e("Cart Count", ""+jsonObject.toString());
			Log.w("Cart Count",""+ jsonObject.toString());
			Log.d("Cart Count", ""+jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadCountData(jsonObject);
	}

	private void loadCountData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();

			if (Utilities.doemailValidation(userName)) {
				commandFactory.sendPostCommand(AppConstants.cart_count_URL,
						jsonObject);
			} else {
				commandFactory.sendPostCommand(
						AppConstants.temp_cart_count_URL, jsonObject);
			}
		}
	}
//Anju
	@Override
	public void cartCountSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.cartCountSuccess(jsonObject);
		Log.e("The data received", ""+jsonObject.toString());
		if(jsonObject.toString().length()!=0){
			parseCountData(jsonObject);
		}else{
			Toast.makeText(Activity, getResources().getString(R.string.nodata), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void cartCountFail(JSONObject jsonObject) {
		// TODO Auto-generated method stub
		Activity.stopSpinWheel();
		super.cartCountFail(jsonObject);
		Log.e("The data received failedddddddddddddd", ""+jsonObject.toString());
		if(jsonObject.toString().length()!=0){
			parseCountData(jsonObject);
		}else{
			Toast.makeText(Activity, getResources().getString(R.string.nodata), Toast.LENGTH_SHORT).show();
		}
	}
	@Override
	public void tempCartCountSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.tempCartCountSuccess(jsonObject);
		if(jsonObject.toString().length()!=0){
			parseCountData(jsonObject);
		}else{
			Toast.makeText(Activity, getResources().getString(R.string.nodata), Toast.LENGTH_SHORT).show();
		}
	}

	private void parseCountData(JSONObject jsonObject) {
		String value = null;
		try {
			value = jsonObject.getString("Value");
			Log.e("The data", ""+value);
			Log.w("parseCountData;;;;;;","==="+ value);
			Log.d("parseCountData;;;;;;","==="+ value);
			cartCount = Integer.parseInt(value);			
		} catch (Exception e) {
			Log.e("The Expedition", e.getMessage());
		}
		countView.setText(value);
	}
//*************************************************************************************************************
	private OnClickListener cartSelected = new OnClickListener() {
		@Override
		public void onClick(View v) {

			userName = Activity.readFile();

			if (userName == null || userName.equals("")) {
				if (cartCount>0) {		
					
				Activity.WriteDataToFile("Guest");
				Fragment cartFragment = new U3AnFoodCartFragment();
				Bundle data = new Bundle();
				data.putString("UserName", Secure.getString(
						Activity.getContentResolver(), Secure.ANDROID_ID));
				cartFragment.setArguments(data);
				Activity.pushFragments(cartFragment, false, true);
				}
				else{
					//Toast.makeText(Activity, "Select an item to continue", Toast.LENGTH_SHORT).show();
					Toast.makeText(Activity, getResources().getString(R.string.no_item_in_cart), Toast.LENGTH_SHORT).show();
				}
			} else if (Utilities.doemailValidation(userName)) {
				if (cartCount>0) {	
					Fragment cartFragment = new U3AnFoodCartFragment();
					Bundle data = new Bundle();
					data.putString("UserName", userName);
					cartFragment.setArguments(data);
					Activity.pushFragments(cartFragment, false, true);
				}
				else{
					//Toast.makeText(Activity, "Select an item to continue", Toast.LENGTH_SHORT).show();
					Toast.makeText(Activity, getResources().getString(R.string.no_item_in_cart), Toast.LENGTH_SHORT).show();
				}
			} else {
				if (cartCount>0) {	
					Fragment cartFragment = new U3AnFoodCartFragment();
					Bundle data = new Bundle();
					data.putString("UserName", Secure.getString(
						Activity.getContentResolver(), Secure.ANDROID_ID));
					cartFragment.setArguments(data);
					Activity.pushFragments(cartFragment, false, true);
				}
				else{
					//Toast.makeText(Activity, "Select an item to continue", Toast.LENGTH_SHORT).show();
					Toast.makeText(Activity, getResources().getString(R.string.no_item_in_cart), Toast.LENGTH_SHORT).show();
				}
			}
		}
	};
// Jijo
	private void initView(View view) {
		restMenuItemsGrid = (GridView) view
				.findViewById(R.id.rest_menu_details);
		restMenuItemsGrid.setNumColumns(2);
		View header = View.inflate(getActivity(),
				R.layout.restaurantmenu_sectionheader, null);
		
		restMenuItemsGrid.setFixedHeader(true);
		restMenuItemsGrid.addHeaderView(header);
		

		ImageView m_restLogo = (ImageView) header.findViewById(R.id.rest_logo);
		TextView m_restName = (TextView) header.findViewById(R.id.rest_name);
		m_restName.setSelected(true);
		RatingBar m_restRating = (RatingBar) header
				.findViewById(R.id.resturant_rating);
		TextView m_restSectionTitle = (TextView) header
				.findViewById(R.id.restaurant_section_name);

		Bundle bundle = getArguments();
		if (bundle != null) {
			area_ID = bundle.getString("Area_ID");
			restaurant_Logo = bundle.getString("restaurant_logo");
			menuSectionId = bundle.getString("menuItemId");
			menuSectionName = bundle.getString("menuItemName");
			restaurant_ID = getArguments().getString("restaurant_id");
			restaurant_Name = getArguments().getString("restaurant_name");
			restaurant_rating = getArguments().getString("restaurant_rating");
			minimum_Amount = getArguments().getString("minimum_Amount");
			if (restaurant_Name != null)
				m_restName.setText(restaurant_Name);
			

			if (menuSectionName != null)
				m_restSectionTitle.setText(menuSectionName);

			float rating = 0;
			try {
				rating = Float.valueOf(restaurant_rating);
			} catch (Exception exception) {
				Log.e(TAB_TAG,
						"Exception in rating converting : "
								+ exception.getMessage());
			}

			m_restRating.setRating(rating);
			header.findViewById(R.id.rest_review_lyt).setOnClickListener(
					new OnClickListener() {
						@Override
						public void onClick(View v) {
							Fragment revFragment = new ReviewFragment();
							Bundle data = new Bundle();
							data.putString("restId", restaurant_ID);
							data.putString("restName", restaurant_Name);
							data.putString(
									"Header",
									AppConstants.DETAILED_SEARCH_TAG);
							data.putString("resLog", restaurant_Logo);
							data.putString("resrating", restaurant_rating);
							revFragment.setArguments(data);
							Activity.pushFragments(revFragment, false, true);
						}
					});

			if (URLUtil.isValidUrl(restaurant_Logo)
					&& restaurant_Logo.length() > 5) {
				Picasso.with(Activity).load(restaurant_Logo)
						.error(R.drawable.imagenotavailable).into(m_restLogo);
			} else {
				m_restLogo.setBackgroundResource(R.drawable.imagenotavailable);
			}
			AccumulateJSONObject(menuSectionId);
		}
	}

	private void AccumulateJSONObject(String menuItemId) {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("menuSectionId", menuItemId);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("jsonobject", "" + jsonObject.toString());
		loadSubCategoryData(jsonObject);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.get_restaurant_menu_sections_url, jsonObject);
		}
		Log.e("params....", "" + jsonObject.toString());
	}

	@Override
	public void onRestaurantMenuSectionsLoadedSuccessfully(JSONObject jsonObject) {
		super.onRestaurantMenuLoadedSuccessfully(jsonObject);
		Log.e("jsonobject", "" + jsonObject.toString());
		parseData(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onRestaurantMenuSectionsLoadingFailed(JSONObject jsonObject) {
		super.onRestaurantMenuLoadingFailed(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	private void parseData(JSONObject jsonObject) {
		ArrayList<MenuItemDetail> menuItemDetail = null;
		if (jsonObject != null && !jsonObject.isNull(MenuItemDetail.MENU_LIST)) {
			try {
				JSONArray jsonArray = jsonObject
						.getJSONArray(MenuItemDetail.MENU_LIST);
				menuItemDetail = new ArrayList<MenuItemDetail>();
				for (int i = 0; i < jsonArray.length(); i++) {
					if (!jsonArray.isNull(i))
						menuItemDetail.add(new MenuItemDetail(jsonArray
								.optJSONObject(i)));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (menuItemDetail != null)
				setAdapter(menuItemDetail);
		}
	}

	private void setAdapter(ArrayList<MenuItemDetail> menuItemDetail) {
		RestaurantMenuItemAdapter restaurantMenuItemAdapter = new RestaurantMenuItemAdapter(
				Activity, menuItemDetail, header, restaurant_ID, area_ID,
				restaurant_Name, restaurant_Logo, minimum_Amount,
				restaurant_rating, menuSectionId, menuSectionName, update);
		restMenuItemsGrid.setAdapter(restaurantMenuItemAdapter);
	}

	@Override
	public void onResume() {
		super.onResume();
		Log.e("on resume", "entered on resume...");
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			listener.reCreateFrag(this);
		}
	}
}
