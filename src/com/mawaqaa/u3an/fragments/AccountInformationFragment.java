package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.interfaces.LogoutListener;

public class AccountInformationFragment extends HeaderViewControlFragment
		implements OnClickListener {

	TextView account_Info_TextView, change_password_TextView,
			personal_info_TextView, add_address_TextView,
			address_list_TextView;
	String firstNameString, lastNameString, phoneString, resi_phoneString,
	firstname, lastname, mobilePhone, resi_PhNo, gender, sms,
	newsletter, occupation, uancreadit;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	ArrayList<HashMap<String, String>> AddressList;

	ImageButton account_info_Button, change_passwordButton,
			personal_info_Button, add_address_Button, address_list_Button;
	LogoutListener listener;
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info.inflate(R.layout.fragment_account_info, gp, false);
		setupUI(rootView, false);
		setTitleForPage(getResources().getString(R.string.my_account_heading));
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		init(rootView);
		//AccumulateDataforGetAddress();
		return rootView;
	}
	/*private void AccumulateDataforGetAddress() {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.accumulate("locale", "en-US");
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		Log.e("jsongetaddrees", ""+jsonObject.toString());
		LoadDataforGetAddress(jsonObject);
	}

	private void LoadDataforGetAddress(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL", AppConstants.getdeleveryinformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.getdeleveryinformation,
					jsonObject);
		}
	}
	
	@Override
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.getCustomerAddressSuccess(jsonObject);
		parse(jsonObject);
	}

	private void parse(JSONObject jsonObject) {

		AddressList = new ArrayList<HashMap<String, String>>();
		try {
			JSONArray jsonArray = jsonObject.getJSONArray("CustomerDetails");
			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonArray.getJSONObject(i);

				map.put(AppConstants.AreaId,
						jsonObject.getString(AppConstants.AreaId));
				map.put(AppConstants.Areaname,
						jsonObject.getString(AppConstants.Areaname));
				map.put(AppConstants.Block,
						jsonObject.getString(AppConstants.Block));
				map.put(AppConstants.Buildingno,
						jsonObject.getString(AppConstants.Buildingno));
				map.put(AppConstants.Extradirections,
						jsonObject.getString(AppConstants.Extradirections));
				map.put(AppConstants.floor,
						jsonObject.getString(AppConstants.floor));
				map.put(AppConstants.id, jsonObject.getString(AppConstants.id));
				map.put(AppConstants.LastName,
						jsonObject.getString(AppConstants.LastName));
				map.put(AppConstants.FirstName,
						jsonObject.getString(AppConstants.FirstName));
				map.put(AppConstants.getCustomermobile,
						jsonObject.getString(AppConstants.getCustomermobile));
				map.put(AppConstants.Res_Ph,
						jsonObject.getString(AppConstants.Res_Ph));
				map.put(AppConstants.Gender,
						jsonObject.getString(AppConstants.Gender));
				map.put(AppConstants.isprimary,
						jsonObject.getString(AppConstants.isprimary));
				map.put(AppConstants.judda,
						jsonObject.getString(AppConstants.judda));
				map.put(AppConstants.profileName,
						jsonObject.getString(AppConstants.profileName));
				map.put(AppConstants.street,
						jsonObject.getString(AppConstants.street));
				map.put(AppConstants.suite,
						jsonObject.getString(AppConstants.suite));
				map.put(AppConstants.subscribed_to_newsletter, jsonObject
						.getString(AppConstants.subscribed_to_newsletter));
				map.put(AppConstants.subscribed_to_sms,
						jsonObject.getString(AppConstants.subscribed_to_sms));
				map.put(AppConstants.occupation,
						jsonObject.getString(AppConstants.occupation));
				AddressList.add(map);
				Log.e("firstname", "" + map.get(AppConstants.FirstName));

				firstname = map.get(AppConstants.FirstName);
				lastname = map.get(AppConstants.LastName);
				mobilePhone = map.get(AppConstants.getCustomermobile);
				resi_PhNo = map.get(AppConstants.Res_Ph);
				gender = map.get(AppConstants.Gender);
				sms = map.get(AppConstants.subscribed_to_sms);
				newsletter = map.get(AppConstants.subscribed_to_newsletter);
				occupation = map.get(AppConstants.occupation);
			}
			Log.e("occupation..........................", "" + occupation);
			PrefUtil.setFirstname(getActivity(), firstname);
			PrefUtil.setLastname(Activity, lastname);
			PrefUtil.setPhonenum(getActivity(), mobilePhone);
			PrefUtil.setredsi_Phonenum(getActivity(), resi_PhNo);
			PrefUtil.setGender(getActivity(), gender);
			PrefUtil.setNewsletter(getActivity(), newsletter);
			PrefUtil.setsms(getActivity(), sms);
			PrefUtil.setOccupation(getActivity(), occupation);
			uancreadit = jsonObject.getString("U3ANCredit");
			PrefUtil.setU3ancredit(getActivity(), uancreadit);

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		Log.e("uancreadit", "" + AddressList);
	}
*/
	
	private void init(View view) {
		account_Info_TextView = (TextView) view
				.findViewById(R.id.account_infoTextview);
		change_password_TextView = (TextView) view
				.findViewById(R.id.change_password_Textview);
		personal_info_TextView = (TextView) view
				.findViewById(R.id.personal_infoTextview);
		add_address_TextView = (TextView) view
				.findViewById(R.id.add_addressTextview);
		address_list_TextView = (TextView) view
				.findViewById(R.id.address_listTextview);

		account_Info_TextView.setOnClickListener(this);
		change_password_TextView.setOnClickListener(this);
		personal_info_TextView.setOnClickListener(this);
		add_address_TextView.setOnClickListener(this);
		address_list_TextView.setOnClickListener(this);

		account_info_Button = (ImageButton) view
				.findViewById(R.id.btn_acount_info);
		change_passwordButton = (ImageButton) view
				.findViewById(R.id.btn_change_password);
		personal_info_Button = (ImageButton) view
				.findViewById(R.id.btn_personal_info);
		add_address_Button = (ImageButton) view
				.findViewById(R.id.btn_add_address);
		address_list_Button = (ImageButton) view
				.findViewById(R.id.btn_address_list);

		account_info_Button.setOnClickListener(this);
		change_passwordButton.setOnClickListener(this);
		personal_info_Button.setOnClickListener(this);
		add_address_Button.setOnClickListener(this);
		address_list_Button.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.account_infoTextview:
			accountInfo();
			break;
		case R.id.btn_acount_info:
			accountInfo();
			break;

		case R.id.change_password_Textview:
			changePassword();
			break;
		case R.id.btn_change_password:
			changePassword();
			break;

		case R.id.personal_infoTextview:
			onPersonalInfoClicked();
			break;
		case R.id.btn_personal_info:
			onPersonalInfoClicked();
			break;

		case R.id.add_addressTextview:
			onAddAddressClicked();
			break;
		case R.id.btn_add_address:
			onAddAddressClicked();
			break;

		case R.id.address_listTextview:
			onAddressListClicked();
			break;
		case R.id.btn_address_list:
			onAddressListClicked();
			break;

		default:
			break;
		}
	}

	private void accountInfo() {
		Fragment fragment = new AccountInfoInnerpageFragment();
		Activity.pushFragments(fragment, false, true);
	}

	private void changePassword() {
		Fragment fragment = new ChangePasswordFragment();
		Activity.pushFragments(fragment, false, true);
	}

	private void onPersonalInfoClicked() {
		Fragment fragment = new PersonalInformationFragment();
		Activity.pushFragments(fragment, false, true);
	}

	private void onAddAddressClicked() {
		Fragment fragment = new DeleveryInformation();
		Activity.pushFragments(fragment, false, true);
	}

	private void onAddressListClicked() {
		Fragment fragment = new AddressListFrag();
		Activity.pushFragments(fragment, false, true);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
	
}
