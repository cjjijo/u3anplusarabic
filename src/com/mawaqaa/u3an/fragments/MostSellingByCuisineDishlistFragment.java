package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MostSellingByCuisineDishlistAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;

public class MostSellingByCuisineDishlistFragment extends
		HeaderViewControlFragment {

	GridView dishGridView;
	String cuisine_ID;
	JSONArray jsonarray;
	TextView errorTextView;
	ArrayList<HashMap<String, String>> arraylist_mostselling_cuisines_dishes_data;
	ProgressDialog mProgressDialog;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_byrestaurant_dishlist, gp,
				false);
		setupUI(v, false);
		initView(v);
		setTitleForPage(getResources().getString(R.string.most_selling_dishes));
		cuisine_ID = getArguments().getString(
				AppConstants.get_cusines_country_id);
		Log.d("Cuisie id", ""+cuisine_ID);

		if (Utilities.isNetworkAvailable(Activity)) {
			new doUploadNewAdressTask(true).execute();
		} else {
			Toast.makeText(Activity,getResources().getString(R.string.no_network), Toast.LENGTH_SHORT)
					.show();
		}
		dishGridView.setOnItemClickListener(itemSelectedListener);
		return v;
	}

	private void initView(View view) {
		dishGridView = (GridView) view.findViewById(R.id.gridView_dish_list);
		errorTextView = (TextView) view.findViewById(R.id.errorTextview);
	}

	// AsyncTask for loading data....
	public class doUploadNewAdressTask extends AsyncTask<Void, String, Void> {

		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;

		InputStream istream;

		public doUploadNewAdressTask(Boolean progressVisibilty) {
			this.progressVisibiltyflag = progressVisibilty;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			Activity.startSpinwheel(false, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();


			try {
				JSONObject objJson = new JSONObject();
				objJson.accumulate("CuisineId", cuisine_ID);
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}

				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						AppConstants.most_selling_by_cuisines_url);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);
				} else {
					result = null;
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Activity.stopSpinWheel();
			try {
				parseData(jObj);
			} catch (Exception e) {

			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			Activity.stopSpinWheel();
		}
	}

	private void parseData(JSONObject jOBj) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jOBj;
		// Create an array
		arraylist_mostselling_cuisines_dishes_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			jsonarray = jsonObject
					.getJSONArray(AppConstants.most_selling_by_cuisines_json_obj);

			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);

				// Retrieve JSON Objects
				map.put(AppConstants.most_selling_by_cuisines_dishname,
						jsonObject
								.getString(AppConstants.most_selling_by_cuisines_dishname));
				map.put(AppConstants.most_selling_by_cuisines_restaurant_name,
						jsonObject
								.getString(AppConstants.most_selling_by_cuisines_restaurant_name));

				map.put(AppConstants.most_selling_by_cuisines_restaurant_id,
						jsonObject
								.getString(AppConstants.most_selling_by_cuisines_restaurant_id));
				map.put(AppConstants.most_selling_by_cuisines_restaurant_status,
						jsonObject
								.getString(AppConstants.most_selling_by_cuisines_restaurant_status));

				map.put(AppConstants.most_selling_by_cuisines_restaurant_thumbnail,
						jsonObject
								.getString(AppConstants.most_selling_by_cuisines_restaurant_thumbnail));
				map.put(AppConstants.most_selling_by_cuisines_dish_thumbnail,
						jsonObject
								.getString(AppConstants.most_selling_by_cuisines_dish_thumbnail));

				if (jsonObject
						.getString(
								AppConstants.most_selling_by_cuisines_restaurant_status)
						.equals("HIDDEN")) {

				} else {
					// Set the JSON Objects into the array
					arraylist_mostselling_cuisines_dishes_data.add(map);
				}
			}

			PopulateGridView();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void PopulateGridView() {
		if (arraylist_mostselling_cuisines_dishes_data.size() > 0) {
			MostSellingByCuisineDishlistAdapter adapter = new MostSellingByCuisineDishlistAdapter(
					Activity, arraylist_mostselling_cuisines_dishes_data);
			dishGridView.setAdapter(adapter);
		} else {
			errorTextView.setVisibility(View.VISIBLE);
		}
	}

	OnItemClickListener itemSelectedListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			HashMap<String, String> map = new HashMap<String, String>();
			map = arraylist_mostselling_cuisines_dishes_data.get(position);

			Log.e("The Most selling dishes data is", ""+map.toString());
			String staString = map
					.get(AppConstants.most_selling_by_cuisines_restaurant_status);
			if(PrefUtil.getAppLanguage(Activity).equals("en")){
				switch (staString) {
				case AppConstants.OPEN:
					Fragment allRestaurantsDetailsFragment = new MostSellingRestaurantsDetails();
					Bundle params1 = new Bundle();
					params1.putString("Header", AppConstants.ALL_RESTAURANT_TAG);
					params1.putString(
							AppConstants.all_restaurants_id,
							arraylist_mostselling_cuisines_dishes_data
									.get(position)
									.get(AppConstants.most_selling_by_cuisines_restaurant_id));
					params1.putString(
							AppConstants.all_restaurants_resturant_logo,
							arraylist_mostselling_cuisines_dishes_data
									.get(position)
									.get(AppConstants.most_selling_by_cuisines_restaurant_thumbnail));
					params1.putString(
							AppConstants.all_restaurants_resturant_name,
							arraylist_mostselling_cuisines_dishes_data
									.get(position)
									.get(AppConstants.most_selling_by_cuisines_restaurant_name));
					allRestaurantsDetailsFragment.setArguments(params1);
					Activity.pushFragments(allRestaurantsDetailsFragment, false,
							true);
					break;

				default:
					break;
				}
			}else{
				if(staString.equalsIgnoreCase(getResources().getString(R.string.open))){
					Fragment allRestaurantsDetailsFragment = new MostSellingRestaurantsDetails();
					Bundle params1 = new Bundle();
					params1.putString("Header", AppConstants.ALL_RESTAURANT_TAG);
					params1.putString(
							AppConstants.all_restaurants_id,
							arraylist_mostselling_cuisines_dishes_data
									.get(position)
									.get(AppConstants.most_selling_by_cuisines_restaurant_id));
					params1.putString(
							AppConstants.all_restaurants_resturant_logo,
							arraylist_mostselling_cuisines_dishes_data
									.get(position)
									.get(AppConstants.most_selling_by_cuisines_restaurant_thumbnail));
					params1.putString(
							AppConstants.all_restaurants_resturant_name,
							arraylist_mostselling_cuisines_dishes_data
									.get(position)
									.get(AppConstants.most_selling_by_cuisines_restaurant_name));
					allRestaurantsDetailsFragment.setArguments(params1);
					Activity.pushFragments(allRestaurantsDetailsFragment, false,
							true);
				}
			}
			

		}
	};
}
