package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TableRow;
import android.widget.Toast;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;

public class SettingsFragment extends DialogFragment implements OnClickListener {
	private String TAG = "U3ANSettings Fragment";
	Button u3anCredit, Sign_up, SignIn, Logout;
	TableRow pushNotification;
	Activity act;

	String regID;

	private Switch pushNotificationSwitch;
	public String FILENAMEAUTHKEY = null;
	public String FILENAMEREGKEY = "registrationID.txt";

	public SettingsFragment(BaseActivity baseActivity) {
		this.act = baseActivity;
	}

	@Override
	public void onStart() {
		super.onStart();
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		Display display = getActivity().getWindowManager().getDefaultDisplay();
		int width = display.getWidth() * 4 / 5;
		params.height = width;
		params.dimAmount = 0.8f; // dim only a little bit
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		final Dialog mydiDialog = new Dialog(getActivity());
		mydiDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mydiDialog.setContentView(R.layout.settings_popup);
		if (getActivity() instanceof BaseActivity)
			((BaseActivity) getActivity()).setupUI(
					mydiDialog.findViewById(R.id.dialogue_lyt), false);
		Sign_up = (Button) mydiDialog.findViewById(R.id.sign_up);
		SignIn = (Button) mydiDialog.findViewById(R.id.sign_in);
		// u3anCredit = (Button) mydiDialog.findViewById(R.id.u3ancredit);
		Logout = (Button) mydiDialog.findViewById(R.id.log_out);
		pushNotification = (TableRow) mydiDialog.findViewById(R.id.my_acccnt);

		pushNotificationSwitch = (Switch) mydiDialog
				.findViewById(R.id.notofication_switch);
		if (PrefUtil.ispushNotificationEnabled(act)) {
			pushNotificationSwitch.setChecked(true);
		} else {
			pushNotificationSwitch.setChecked(false);
		}

		// attach a listener to check for changes in state
		pushNotificationSwitch
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						if (isChecked) {
							regID = readFromFile();
							PrefUtil.setpushNotificationEnabled(act);
							new AddDeviceToken().execute();
						} else {
							PrefUtil.setpushNotificationDisabled(act);
							new RemoveDeviceToken().execute();
						}
					}
				});

		Sign_up.setOnClickListener(this);
		Logout.setOnClickListener(this);
		// u3anCredit.setOnClickListener(this);
		pushNotification.setOnClickListener(this);
		SignIn.setOnClickListener(this);
		mydiDialog.setCanceledOnTouchOutside(true);
		return mydiDialog;
	}

	@Override
	public void onResume() {
		super.onResume();
		setUI();
	}

	private void setUI() {
		if (PrefUtil.isUserSignedIn(getActivity())) {
			Sign_up.setVisibility(View.GONE);
			SignIn.setVisibility(View.GONE);
			Logout.setVisibility(View.VISIBLE);
			pushNotification.setVisibility(View.VISIBLE);
		} else {
			Sign_up.setVisibility(View.VISIBLE);
			SignIn.setVisibility(View.VISIBLE);
			Logout.setVisibility(View.GONE);
			pushNotification.setVisibility(View.VISIBLE);
		}
	}

	private String readFromFile() {
		String ret = "";
		try {
			InputStream inputStream = act.openFileInput(FILENAMEREGKEY);

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("login activity", "Can not read file: " + e.toString());
		}

		return ret;
	}

	@Override
	public void onClick(View v) {
		Fragment fragment = null;
		getDialog().dismiss();
		switch (v.getId()) {
		case R.id.sign_in:
			fragment = new LoginFragment();
			break;
		case R.id.sign_up:
			fragment = new RegisterFirstStepFragment();
			break;
		case R.id.log_out:
			clearuserid();
			PrefUtil.setUserSignedOut(act);
			PrefUtil.setFirstname(getActivity(), "");
			clearuserid();
			Toast.makeText(act, R.string.log_out_sucess, Toast.LENGTH_SHORT)
					.show();
			PrefUtil.setAuthkey(getActivity(), "");
			((BaseActivity) act).logout();
			((BaseActivity) act).WriteDataToFile("");
			fragment = new HomeFragment();
			break;
		case R.id.my_acccnt:

			break;

		default:
			break;
		}
		if (fragment != null)
			((BaseActivity) act).pushFragments(fragment, false, true);
	}

	private void clearuserid() {
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			fisdummy = act.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
		} catch (Exception e) {
		}
	}

	// Async task for posting the value to the server.....
	public class AddDeviceToken extends AsyncTask<Void, String, Void> {
		String result;
		JSONObject jObj = null;
		InputStream istream;

		public AddDeviceToken() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressbar();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				JSONObject objJson = new JSONObject();
				objJson.accumulate("pushplatform", 0);
				objJson.accumulate("pushtoken", regID);
				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);

				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost httppost = new HttpPost(AppConstants.Add_Device_Token);

				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				Log.i("Login Request", ""+objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";

					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();
					jObj = new JSONObject(result);
					Log.i("Login Result", ""+result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			hideProgressbar();
		}
	}

	// Async task for posting the value to the server.....
	public class RemoveDeviceToken extends AsyncTask<Void, String, Void> {
		String result;
		JSONObject jObj = null;
		InputStream istream;

		public RemoveDeviceToken() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			showProgressbar();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {

				JSONObject objJson = new JSONObject();
				objJson.accumulate("pushplatform", 0);
				objJson.accumulate("pushtoken", regID);
				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);

				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost httppost = new HttpPost(
						AppConstants.Remove_Device_Token);

				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				Log.i("Login Request", ""+objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";

					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();
					jObj = new JSONObject(result);
					Log.i("Login Result", ""+result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			hideProgressbar();
			Log.e("Login Result", ""+jObj.toString());
		}
	}

	private void hideProgressbar() {
		((BaseActivity) act).stopSpinWheel();
	}

	private void showProgressbar() {
		((BaseActivity) act).startSpinwheel(false, true);
	}
}
