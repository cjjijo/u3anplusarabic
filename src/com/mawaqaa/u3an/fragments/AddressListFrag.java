package com.mawaqaa.u3an.fragments;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.AddressListingAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.interfaces.OnItemRemovedListener;
import com.mawaqaa.u3an.interfaces.OnMakeAsPrimary;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class AddressListFrag extends HeaderViewControlFragment {
	ListView Addresslist;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	ArrayList<HashMap<String, String>> arrayList;
	String uancreadit, username, firstname, lastname, mobilePhone, resi_PhNo,
			gender, sms, newsletter, occupation, type;
	OnMakeAsPrimary makePrimary;
	AddressListingAdapter addressListingAdapter;
	OnItemRemovedListener removeListener;
	TextView errortext;
	LogoutListener listener;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.addreslist, gp, false);
		setupUI(v, false);
		Activity.stopSpinWheel();
		/*
		 * Bundle data=getArguments();
		 * username = data.getString("UserName");
		 */
		setTitleForPage(getResources().getString(R.string.address_list));
		AccumulateData();
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = Activity.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());

		} catch (Exception e) {
		}

		init(v);
		makePrimary = new OnMakeAsPrimary() {
			@Override
			public void makeAsPrimaryComplete(String pos) {
				Log.e("ID", "" + pos);
				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.accumulate("AddressId", pos);
					jsonObject.accumulate("UserId", FILENAMEAUTHKEY);
				} catch (JSONException e) {
					e.printStackTrace();
				}
				makeAsPrimaryComplete(jsonObject);
			}

			private void makeAsPrimaryComplete(JSONObject jsonObject) {
				if (VolleyUtils.volleyEnabled) {
					Log.e("URL",""+ AppConstants.getdeleveryinformation);
					CommandFactory commandFactory = new CommandFactory();
					commandFactory.sendPostCommand(AppConstants.MakeAsPrimary,
							jsonObject);
				} }
		};
		
		removeListener = new OnItemRemovedListener() {
			@Override
			public void ItemCheckComple(String id) {
				JSONObject jsonObject = new JSONObject();
				try {
					jsonObject.accumulate("AddressId", id);
					jsonObject.accumulate("UserId", FILENAMEAUTHKEY);
					if (VolleyUtils.volleyEnabled) {
						CommandFactory commandFactory = new CommandFactory();
						commandFactory.sendPostCommand(
								AppConstants.REMOVE_Address, jsonObject);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		};
		return v;
	}

	@Override
	public void onAddressRemovalFail(JSONObject jsonObject) {
		super.onAddressRemovalFail(jsonObject);
	}

	@Override
	public void onAddressRemovalSuccess(JSONObject jsonObject) {
		super.onAddressRemovalSuccess(jsonObject);
		if (jsonObject.toString().contains("Success")) {
			AccumulateData();
		} else {
			Toast.makeText(Activity, R.string.cannot_order,
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void MakeAsPrimarySuccess(JSONObject jsonObject) {
		super.MakeAsPrimarySuccess(jsonObject);
		AccumulateData();
	}

	@Override
	public void MakeAsPrimaryFail(JSONObject jsonObject) {

		super.MakeAsPrimaryFail(jsonObject);
		Toast.makeText(Activity, R.string.no_data_availabale, Toast.LENGTH_LONG)
				.show();
	}

	private void init(View v) {
		Addresslist = (ListView) v.findViewById(R.id.addresslist);
		errortext = (TextView) v.findViewById(android.R.id.empty);
		AccumulateData();
	}

	private void AccumulateData() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		JSONObject jsonObject = new JSONObject();
		try {
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", FILENAMEAUTHKEY);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		/* errortext.setVisibility(View.GONE); */
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.getdeleveryinformation);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.getdeleveryinformation,
					jsonObject);
		}
	}

	@Override
	public void getCustomerAddressSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.getCustomerAddressSuccess(jsonObject);
		parse(jsonObject);
	}

	private void parse(JSONObject jsonObject) {
		arrayList = new ArrayList<HashMap<String, String>>();
		try {
			JSONArray jsonArray = jsonObject.getJSONArray("CustomerDetails");
			Log.e("array length................", "" + jsonArray.length());
			for (int i = 0; i < jsonArray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				JSONObject Object = jsonArray.getJSONObject(i);
				map.put(AppConstants.AreaId,
						Object.getString(AppConstants.AreaId));
				map.put(AppConstants.Areaname,
						Object.getString(AppConstants.Areaname));
				map.put(AppConstants.Block,
						Object.getString(AppConstants.Block));
				map.put(AppConstants.Buildingno,
						Object.getString(AppConstants.Buildingno));
				map.put(AppConstants.Extradirections,
						Object.getString(AppConstants.Extradirections));
				map.put(AppConstants.floor,
						Object.getString(AppConstants.floor));
				map.put(AppConstants.id, Object.getString(AppConstants.id));
				map.put(AppConstants.LastName,
						Object.getString(AppConstants.LastName));
				map.put(AppConstants.FirstName,
						Object.getString(AppConstants.FirstName));
				map.put(AppConstants.getCustomermobile,
						Object.getString(AppConstants.getCustomermobile));
				map.put(AppConstants.Res_Ph,
						Object.getString(AppConstants.Res_Ph));
				map.put(AppConstants.Gender,
						Object.getString(AppConstants.Gender));
				map.put(AppConstants.isprimary,
						Object.getString(AppConstants.isprimary));
				map.put(AppConstants.judda,
						Object.getString(AppConstants.judda));
				map.put(AppConstants.profileName,
						Object.getString(AppConstants.profileName));
				map.put(AppConstants.street,
						Object.getString(AppConstants.street));
				map.put(AppConstants.suite,
						Object.getString(AppConstants.suite));
				map.put(AppConstants.type, Object.getString(AppConstants.type));
				map.put(AppConstants.subscribed_to_newsletter,
						Object.getString(AppConstants.subscribed_to_newsletter));
				map.put(AppConstants.subscribed_to_sms,
						Object.getString(AppConstants.subscribed_to_sms));
				map.put(AppConstants.occupation,
						Object.getString(AppConstants.occupation));
				arrayList.add(map);
			}
			uancreadit = jsonObject.getString("U3ANCredit");
			// PrefUtil.setU3ancredit(getActivity(), uancreadit);

		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		Log.e("uancreadit", "" + arrayList);
		poppulateData();
	}

	private void poppulateData() {
		addressListingAdapter = new AddressListingAdapter(Activity, arrayList,
				makePrimary, removeListener);
		Addresslist.setAdapter(addressListingAdapter);
		Addresslist.setEmptyView(errortext);
	}

	@Override
	public void getCustomerAddressFail(JSONObject jsonObject) {
		super.getCustomerAddressFail(jsonObject);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
}
