package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpecialOfferAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class SpecialOffersFragment extends HeaderViewControlFragment {
	String restaurant_ID, restaurant_name, restaurant_logo;
	ArrayList<HashMap<String, String>> arraylist_specialoffer_data;
	GridView special_offer_GridView;
	TextView restaurant_NameTextView;
	ImageView logoImageView;
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View rootView = info
				.inflate(R.layout.fragment_specialoffers, gp, false);
		setupUI(rootView, false);
		restaurant_ID = getArguments().getString("restaurant_id");
		restaurant_name = getArguments().getString("restaurant_name");
		restaurant_logo = getArguments().getString("restaurant_logo");

		initView(rootView);
		
		restaurant_NameTextView.setText(restaurant_name);
		if (restaurant_logo.startsWith("http")) {
			Picasso.with(Activity).load(restaurant_logo).into(logoImageView);
		} else {
			logoImageView.setBackgroundResource(R.drawable.imagenotavailable);

		}
		setTitleForPage(getResources().getString(R.string.special_offers_restaurant));
		AccumulateJSONObject();
		return rootView;
	}

	private void initView(View view) {
		special_offer_GridView = (GridView) view
				.findViewById(R.id.gridView_special_offer);
		
		restaurant_NameTextView  =  (TextView) view.findViewById(R.id.restaurant_name_Textview);
		logoImageView   =   (ImageView) view.findViewById(R.id.restaurant_logo);
	}

	private void AccumulateJSONObject() {
		Activity.startSpinwheel(true, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("restId", restaurant_ID);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryData(jsonObject);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.special_offers_url,
					jsonObject);
		}
	}

	@Override
	public void onSpecialOffersLoadedSuccessfull(JSONObject jsonObject) {
		super.onSpecialOffersLoadedSuccessfull(jsonObject);
		parseData(jsonObject);
		Activity.stopSpinWheel();
	}

	private void parseData(JSONObject jObj) {
		JSONObject jsonObject = new JSONObject();
		jsonObject = jObj;
		// Create an array
		arraylist_specialoffer_data = new ArrayList<HashMap<String, String>>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			JSONArray jsonarray = jsonObject
					.getJSONArray(AppConstants.special_offers_json_obj);

			for (int i = 0; i < jsonarray.length(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				jsonObject = jsonarray.getJSONObject(i);

				// Retrieve JSON Objects
				map.put(AppConstants.special_offers_description, jsonObject
						.getString(AppConstants.special_offers_description));
				map.put(AppConstants.special_offers_name,
						jsonObject.getString(AppConstants.special_offers_name));

				map.put(AppConstants.special_offers_price,
						jsonObject.getString(AppConstants.special_offers_price));
				map.put(AppConstants.special_offers_rating, jsonObject
						.getString(AppConstants.special_offers_rating));
				map.put(AppConstants.special_offers_thumbnail, jsonObject
						.getString(AppConstants.special_offers_thumbnail));
				arraylist_specialoffer_data.add(map);
			}
			populateGridview();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		}
	}

	private void populateGridview() {
		SpecialOfferAdapter adapter = new SpecialOfferAdapter(Activity,
				arraylist_specialoffer_data);
		special_offer_GridView.setAdapter(adapter);
	}
}
