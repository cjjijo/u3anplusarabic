package com.mawaqaa.u3an.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;

public class MyDialogFragmentAds extends DialogFragment {
	ImageView ads;
	Context cnt;
	String gett;
	public static MyDialogFragmentAds newInstance() {
	//	String title = "My Fragment";
		MyDialogFragmentAds f = new MyDialogFragmentAds();
		/*Bundle args = new Bundle();
		args.putString("title", title);
		f.setArguments(args);*/
		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		final Dialog myDialog = new Dialog(getActivity());
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.showadds);

		cnt = getActivity().getApplicationContext();		
		Button clk = (Button) myDialog
				.findViewById(R.id.buttoncls);
		clk.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				myDialog.dismiss();
			}
		});
		
			 ads = (ImageView)myDialog.findViewById(R.id.shwadd);
			//ads.setImageResource(R.drawable.welcomepagebg);
			
			/*gett = getArguments().getString("sdd");
		Picasso.with(cnt).load(gett)
		.error(R.drawable.imagenotavailable)
		.into(ads);
		*/
		
			
			 try{
				    
				    Log.e("isitisitisit", "nowww");
				   // Bundle wget = new Bundle();
				    Bitmap icon = getArguments().getParcelable("BundleIcon");
				   // Bitmap icon = HomeFragment.image;
				    Log.e("bbbbbbbbb", "nowww");
				  // BitmapDrawable ob = new BitmapDrawable(getResources(), icon);
				  

				  // ads.setBackgroundDrawable(ob);
				   if(icon != null){
				    ads.setImageBitmap(icon);
				    Log.e("MyDialogFragmentSocialMedia","Not null Icon");
				   }
				   else{
				    Log.e("MyDialogFragmentSocialMedia","Null Icon");
				    Toast.makeText(getActivity(), R.string.null_image_bitmap, Toast.LENGTH_SHORT).show();
				   }
				   //ads.setImageBitmap(icon);
				   myDialog.show();
				   }catch(Exception e){
				    e.printStackTrace();
				   }				 

				   Handler handler = new Handler();
				         handler.postDelayed(new Runnable() {
				             @Override
							public void run() {				                
				                 // final Dialog myDialog = new Dialog(getActivity());
				                  myDialog.dismiss();        
				             }  
				         }, 5000);
					
		myDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		myDialog.getWindow().setLayout(
				getActivity().getResources().getDisplayMetrics().widthPixels,
				ViewGroup.LayoutParams.WRAP_CONTENT);
 		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;

		/* Handler handlers = new Handler();
         handlers.postDelayed(new Runnable() {
             public void run() {

            		myDialog.show();                
             }
         }, 1500);
		
		 Handler handler = new Handler();
         handler.postDelayed(new Runnable() {
             public void run() {
                	 myDialog.dismiss();                   
             }
         }, 5000);*/
		return myDialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setDialogPosition();
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private void setDialogPosition() {
		SharedPreferences sp = getActivity().getSharedPreferences("lan",
				Context.MODE_PRIVATE);
		
		String lan = sp.getString("lan", "en");
		Window window = this.getDialog().getWindow();		
		if (lan.equals("en"))
			window.setGravity(Gravity.CENTER | Gravity.LEFT);
		else
			window.setGravity(Gravity.CENTER | Gravity.RIGHT);
		
		WindowManager.LayoutParams params = window.getAttributes();
		params.y = dpToPx(0);
		window.setAttributes(params);
	}

	private int dpToPx(int dp) {
		DisplayMetrics metrics = getActivity().getResources()
				.getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				metrics);
	}
}
