package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.DialogInterface;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.CuisineSpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Cuisine;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class AddRestaurantFragment extends HeaderViewControlFragment {

	private EditText m_firstNameEditText;
	private EditText m_lastNameEditText;
	private EditText m_restaurantNameEditText;
	private EditText m_restaurantAddressEditText;
	private EditText m_telephoneEditText;
	private EditText m_emailEditText;
	private CheckBox m_havedeliveryChbx;
	private Spinner m_cuisinesSpinner;
	private Button m_submitBtn;
	private ArrayList<Cuisine> arraylist_cusines_data;
	int maxLength = 25;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_add_restaurant, gp, false);
		setupUI(v, false);
		initView(v);
		setTitleForPage(getResources().getString(R.string.add_restaurant_heading));
		return v;
	}

	private void initView(View v) {

		m_firstNameEditText = (EditText) v.findViewById(R.id.firstname);
		m_firstNameEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						m_firstNameEditText));
		m_firstNameEditText
				.setOnFocusChangeListener(new FocusChangedListener());
		m_firstNameEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		m_lastNameEditText = (EditText) v.findViewById(R.id.lastname);
		m_lastNameEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				m_lastNameEditText));
		m_lastNameEditText.setOnFocusChangeListener(new FocusChangedListener());
		m_lastNameEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });
		m_restaurantNameEditText = (EditText) v
				.findViewById(R.id.restaurant_name);
		m_firstNameEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						maxLength) });

		m_restaurantNameEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						m_restaurantNameEditText));
		m_restaurantNameEditText
				.setOnFocusChangeListener(new FocusChangedListener());

		m_restaurantAddressEditText = (EditText) v
				.findViewById(R.id.restaurant_address);
		m_restaurantAddressEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						m_restaurantAddressEditText));

		m_restaurantAddressEditText
				.setOnFocusChangeListener(new FocusChangedListener());
		m_restaurantAddressEditText
				.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
						100) });

		m_telephoneEditText = (EditText) v.findViewById(R.id.telephone);
		m_telephoneEditText
				.addTextChangedListener(new AddRestaurantTextwatcher(
						m_telephoneEditText));
		m_telephoneEditText
				.setOnFocusChangeListener(new FocusChangedListener());

		m_emailEditText = (EditText) v.findViewById(R.id.email);
		m_emailEditText.addTextChangedListener(new AddRestaurantTextwatcher(
				m_emailEditText));
		m_emailEditText.setOnFocusChangeListener(new FocusChangedListener());

		m_havedeliveryChbx = (CheckBox) v.findViewById(R.id.delivery_chkbox);
		m_cuisinesSpinner = (Spinner) v.findViewById(R.id.cuisines_spinner);
		m_submitBtn = (Button) v.findViewById(R.id.submit_rest);
		m_submitBtn.setOnClickListener(submitBtnClicked);
		JSONObject cuisineJsonPbj = readDataFromFile(HomeFragment.FILENAMECUISINEDATA);
		ParseCuisineData(cuisineJsonPbj);
	}

	private class AddRestaurantTextwatcher implements TextWatcher {

		private EditText editText;

		AddRestaurantTextwatcher(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			if (editText != null)
				editText.setError(null);
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	}

	private class FocusChangedListener implements OnFocusChangeListener {

		private EditText editText;

		private FocusChangedListener() {
		}

		private FocusChangedListener(EditText editText) {
			this.editText = editText;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (!hasFocus) {
				switch (v.getId()) {

				case R.id.firstname:
					DoFirstNameValidation();
					break;
				case R.id.lastname:
					DoLastNameValidation();
					break;
				case R.id.restaurant_name:
					DoRestaurantNameValidation();
					break;
				case R.id.restaurant_address:
					DoRestaurantAddressValidation();
					break;
				case R.id.telephone:
					DoPhoneNumberValidation();
					break;
				case R.id.email:
					doEmailIdValidation();
					break;
				default:
					break;
				}
			}
		}
	}
// Jijo
	private Boolean DoPhoneNumberValidation() {
		String phoneString = m_telephoneEditText.getText().toString();
		if (phoneString.length() == 0) {
			m_telephoneEditText
				.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_telephoneEditText
				.setError(Html
						.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				m_telephoneEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			
			// m_telephoneEditText.requestFocus();
			return false;
		}
		if (!Utilities.isValidPhoneNumber(phoneString)) {
			m_telephoneEditText
				.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_phNumber)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_telephoneEditText
				.setError(Html
						.fromHtml("<font color='white'>Please enter a valid phone number with 6 to 15 digits</font>"));
			}else{
				m_telephoneEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø§Ù„Ø±Ø¬Ø§Ø¡ Ø¥Ø¯Ø®Ø§Ù„ Ø±Ù‚Ù… Ù‡Ø§ØªÙ� ØµØ­ÙŠØ­ Ù…Ù† 6-15 Ø±Ù‚Ù…</font>"));
			}*/
			
			// m_telephoneEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}
// Jijo
	private Boolean DoFirstNameValidation() {
		String firstNameString = m_firstNameEditText.getText().toString();
		if (firstNameString.length() == 0) {
			m_firstNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.first_name)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_firstNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Enter first name to continue</font>"));
			}else{
				m_firstNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£ÙˆÙ„ Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
			
			// m_firstNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}
// Jijo
	private Boolean DoLastNameValidation() {
		String lastNameString = m_lastNameEditText.getText().toString();
		if (lastNameString.length() == 0) {
			m_lastNameEditText
				.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.last_name)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_lastNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Enter last name to continue</font>"));
			}else{
				m_lastNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ù„Ø§Ø³Ù… Ø§Ù„Ø£Ø®ÙŠØ± Ù„Ù„Ø§Ø³ØªÙ…Ø±Ø§Ø±</font>"));
			}*/
			
			// m_lastNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}
// Jijo
	private Boolean DoRestaurantNameValidation() {
		String restaurantNameString = m_restaurantNameEditText.getText()
				.toString();
		if (restaurantNameString.length() == 0) {
			m_restaurantNameEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_name)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_restaurantNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Enter a valid Name to continue</font>"));
			}else{
				m_restaurantNameEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø§Ø³Ù… ØµØ­ÙŠØ­</font>"));
			}*/
			
			// m_restaurantNameEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}
// Jijo
	private Boolean DoRestaurantAddressValidation() {
		String restaurantAddressString = m_restaurantAddressEditText.getText()
				.toString();
		if (restaurantAddressString.length() <= 4) {
			m_restaurantAddressEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.restaurant_addr_chara)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_restaurantAddressEditText
				.setError(Html
						.fromHtml("<font color='white'>Restaurant address should contain atleast 5 characters</font>"));
			}else{
				m_restaurantAddressEditText
				.setError(Html
						.fromHtml("<font color='white'>Restaurant address should contain atleast 5 characters</font>"));
			}*/
			// m_restaurantAddressEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}
// Jijo
	private boolean doEmailIdValidation() {
		String emailString = m_emailEditText.getText().toString()
				.trim();
		if (emailString == null || emailString.length() < 1) {
			m_emailEditText
			.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_email)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_emailEditText
				.setError(Html
						.fromHtml("<font color='white'>Enter email id to proceed</font>"));
			}else{
				m_emailEditText
				.setError(Html
						.fromHtml("<font color='white'>Enter email id to proceed</font>"));
			}*/
			
			// m_emailEditText.requestFocus();
			return false;
		}
		if (!Utilities.doemailValidation(emailString)) {
			m_emailEditText
				.setError(Html
					.fromHtml("<font color='white'>"+getResources().getString(R.string.enter_valid_email)+"</font>"));
			/*if(PrefUtil.getAppLanguage(getActivity()).equals(AppConstants.STR_EN)){
				m_emailEditText
				.setError(Html
						.fromHtml("<font color='white'>Enter a Valid Email id</font>"));
			}else{
				m_emailEditText
				.setError(Html
						.fromHtml("<font color='white'>Ø£Ø¯Ø®Ù„ Ø¨Ø±ÙŠØ¯ Ø¥Ù„ÙƒØªØ±ÙˆÙ†ÙŠ ØµØ­ÙŠØ­</font>"));
			}*/
			
			// m_emailEditText.requestFocus();
			return false;
		} else {
			return true;
		}
	}

	private OnClickListener submitBtnClicked = new OnClickListener() {
		@Override
		public void onClick(View v) {
			doSubmitAction();
		}
	};

	private DialogInterface.OnClickListener okClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			Activity.popFragments();
		}
	};

	private DialogInterface.OnClickListener cancelClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
		}
	};

	private void doSubmitAction() {
		if (!DoFirstNameValidation())
			return;
		if (!DoLastNameValidation())
			return;
		if (!DoRestaurantNameValidation())
			return;
		if (!DoRestaurantAddressValidation())
			return;
		if (!DoPhoneNumberValidation())
			return;
		if (!doEmailIdValidation())
			return;
		addRestaurant();
	}

	private JSONObject readDataFromFile(String fileName) {
		JSONObject jsonObject = null;
		try {
			FileInputStream fis = Activity.openFileInput(fileName);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			String data = br.readLine();
			br.close();
			in.close();
			fis.close();
			if (data != null)
				jsonObject = new JSONObject(data);
		} catch (Exception e) {
		}
		return jsonObject;
	}

	private void ParseCuisineData(JSONObject jsonObject1) {

		JSONObject jsonObject = new JSONObject();
		jsonObject = jsonObject1;
		// Create an array
		arraylist_cusines_data = new ArrayList<Cuisine>();
		// Retrieve JSON Objects from the given URL address.....
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.get_cusines_json_obj)) {
				JSONArray jsonarray = jsonObject
						.getJSONArray(AppConstants.get_cusines_json_obj);
				for (int i = 0; i < jsonarray.length(); i++) {

					if (!jsonarray.isNull(i)) {
						Cuisine cuisine = new Cuisine(
								jsonarray.optJSONObject(i));

						arraylist_cusines_data.add(cuisine);
					}
				}
			}
			PopulateCusinesSpinner();
		} catch (JSONException e) {
			e.printStackTrace();
			Log.e("The error", ""+e.getMessage());
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("The error",""+ e.getMessage());
		}
	}

	private void PopulateCusinesSpinner() {

		if (arraylist_cusines_data != null) {
			Collections.sort(arraylist_cusines_data, new Comparator<Cuisine>() {
				@Override
				public int compare(Cuisine c1, Cuisine c2) {
					return c1.strCuisineName.compareTo(c2.strCuisineName);
				}
			});
		} else
			return;

		m_cuisinesSpinner.setAdapter(new CuisineSpinnerAdapter(Activity,
				arraylist_cusines_data));
	}

	private void addRestaurant() {
		Log.e("adding restaurant", "msg");
		JSONObject jsonObject = new JSONObject();
		String deliver_Status = "0";
		if (m_havedeliveryChbx.isChecked()) {
			deliver_Status = "1";
		}
		Log.e("Delivery status",""+ deliver_Status);
		try {
			jsonObject.put("Cuisine", getSelectedCuisine());

			jsonObject.put("Delivery", deliver_Status);
			jsonObject.put("Email", m_emailEditText.getText().toString());
			jsonObject.put("FirstName", m_firstNameEditText.getText()
					.toString());
			String firstname = m_firstNameEditText.getText().toString();
			PrefUtil.setFirstname(getActivity(), firstname);
			jsonObject.put("LastName", m_lastNameEditText.getText().toString());
			String lastname = m_lastNameEditText.getText().toString();
			PrefUtil.setLastname(Activity, lastname);
			jsonObject.put("RestaurantName", m_restaurantNameEditText.getText()
					.toString());
			jsonObject.put("Restaurant_Address", m_restaurantAddressEditText
					.getText().toString());
			jsonObject.put("Telephone", m_telephoneEditText.getText()
					.toString());
			callAddRestaurantService(jsonObject);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private String getSelectedCuisine() {
		String strCuisine = "";
		if (m_cuisinesSpinner != null && arraylist_cusines_data != null) {
			Cuisine cuisine = arraylist_cusines_data.get(m_cuisinesSpinner
					.getSelectedItemPosition());
			strCuisine = cuisine.strCuisineName;
		}
		return strCuisine;
	}

	private void callAddRestaurantService(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.add_restaurant_url,
					jsonObject);
		}
	}

	@Override
	public void restaurantAddedSuccessfully(JSONObject jsonObject) {
		super.restaurantAddedSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		String status = null;
		//Log.e("Entered here", jsonObject.toString());
		try {
			if (jsonObject != null && !jsonObject.isNull("Status"))
				status = jsonObject.getString("Status");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (status != null && status.equalsIgnoreCase("success")) {
			Utilities.show(Activity,getResources().getString(R.string.restaurant_added_success), 
					getResources().getString(R.string.restaurant_success),okClicked);
			clearAllEdittext();
		} else {
			Utilities.show(Activity, getResources().getString(R.string.restaurant_added_fail), 
					getResources().getString(R.string.restaurant_fail),cancelClicked);
		}
	}

	private void clearAllEdittext() {
		m_firstNameEditText.setText("");
		m_lastNameEditText.setText("");
		m_restaurantNameEditText.setText("");
		m_restaurantAddressEditText.setText("");
		m_telephoneEditText.setText("");
		m_emailEditText.setText("");
	}

	@Override
	public void addingRestaurantFail(JSONObject jsonObject) {
		super.addingRestaurantFail(jsonObject);
		Utilities.show(Activity, getResources().getString(R.string.restaurant_added_fail), getResources().getString(R.string.failed),
				cancelClicked);
		Activity.stopSpinWheel();
	}
}
