package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpinnerAreaAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Restaurant;
import com.mawaqaa.u3an.data.SingleAreaCountry;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class MostSellingRestaurantsDetails extends HeaderViewControlFragment
		implements OnClickListener {
	private ImageView restaurantLogoImageView, m_img_knet, m_img_cc,
			m_img_cash;

	private TextView restaurantNameTextView;
	private Spinner restaurantLocation;
	private TextView status, minimumAmount, workingHours, deliverTime,
			deliveryCharges, Cuisines;

	private String restaurantID, restaurantName, restaurantLogo,
			restaurantAbout;

	private ArrayList<SingleAreaCountry> areaList;
	private String restaurant_rating;
	private String area_ID;

	private Button show_menu_Button;
	private TextView about_restaurantTextView, most_selling_dishes_TextView,
			specialTextView;
	String minimum_Amount;

	String FILENAMEAREA = "areaName.txt";

	ScrollView scroll;
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	DecimalFormat df = (DecimalFormat)nf;
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		TAB_TAG = getClass().getName();
		View v = info.inflate(R.layout.mostsellingdishesdetails, gp, false);
		setupUI(v, false);
		initView(v);
		df.applyPattern("###,##0.000");
		Bundle data = getArguments();
		restaurantLogoImageView.requestFocus();
		restaurantID = data.getString(AppConstants.all_restaurants_id);
		restaurantName = data
				.getString(AppConstants.all_restaurants_resturant_name);
		restaurantLogo = data
				.getString(AppConstants.all_restaurants_resturant_logo);

		restaurantNameTextView.setText(restaurantName);
		Log.d(TAB_TAG, "restaurantLogo :" + restaurantLogo);
		Picasso.with(Activity).load(restaurantLogo)
				.into(restaurantLogoImageView);
		
		setTitleForPage(PrefUtil.getFragHeading(getActivity()));

		if (Utilities.isNetworkAvailable(Activity)) {
			new doUploadNewAdressTask(true).execute();

		} else {
			Toast.makeText(Activity, R.string.no_network, Toast.LENGTH_SHORT)
					.show();
		}		
		System.gc();
		return v;
	}

	private void AccumulateJSONObject(int opr,
			SingleAreaCountry singleAreaCountry) {
		showProgressbar();
		Log.d("resturant id", ""+restaurantID);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		if (opr == 1) {
			try {
				jsonObject = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					jsonObject.accumulate("locale", "en-US");
				}else{
					jsonObject.accumulate("locale", "ar-KW");
				}
				jsonObject.accumulate("restId", restaurantID);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			loadSubCategoryData(jsonObject,
					AppConstants.area_by_restaurants_url);
		} else if (opr == 2) {
			try {
				jsonObject = new JSONObject();
				jsonObject.accumulate("areaId", singleAreaCountry.areaId);
				if(sDefSystemLanguage.equals("en")){
					jsonObject.accumulate("locale", "en-US");
				}else{
					jsonObject.accumulate("locale", "ar-KW");
				}
				jsonObject.accumulate("restId", restaurantID);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			loadSubCategoryData(jsonObject, AppConstants.restaurant_details_url);
		}
	}

	private void loadSubCategoryData(JSONObject jsonObject, String serviceUrl) {
		showProgressbar();
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(serviceUrl, jsonObject);
		}
	}

	@Override
	public void onGetAreaByRestaurantsLoadedSuccessfully(JSONObject jsonObject) {
		super.onGetAreaByRestaurantsLoadedSuccessfully(jsonObject);
		parseData(jsonObject);
	}

	@Override
	public void onGetAreaByRestaurantsLoadingFailed(JSONObject jsonObject) {
		super.onGetAreaByRestaurantsLoadingFailed(jsonObject);
		if (jsonObject != null)
			Log.e("Error", ""+jsonObject.toString());
		else
			Log.e("Error", "Null JSON Object");
	}

	@Override
	public void onRestaurantDetailsLoadedSuccessfully(JSONObject jsonObject) {
		super.onRestaurantDetailsLoadedSuccessfully(jsonObject);
		hideProgressbar();
		parseRestDetailsData(jsonObject);
	}

	@Override
	public void onRestaurantDetailsLoadingFailed(JSONObject jsonObject) {
		super.onRestaurantDetailsLoadingFailed(jsonObject);
		hideProgressbar();
		Toast.makeText(Activity, R.string.restaurant_details_failed,
				Toast.LENGTH_SHORT).show();
		if (jsonObject != null)
			Log.e("Error in GetRestaurant Details", ""+jsonObject.toString());
		else
			Log.e("Error in GetRestaurant Details", "Null JSON Object");
	}

	private void parseData(JSONObject jsonObject) {
		try {
			if (jsonObject != null
					&& !jsonObject.isNull(AppConstants.area_by_json_obj)) {
				JSONArray jsonArray = jsonObject
						.getJSONArray(AppConstants.area_by_json_obj);
				if (jsonArray != null && jsonArray.length() > 0) {
					areaList = new ArrayList<SingleAreaCountry>();
					for (int i = 0; i < jsonArray.length(); i++) {

						if (!jsonArray.isNull(i)) {
							JSONObject jobj = jsonArray.getJSONObject(i);
							if (jobj != null) {
								SingleAreaCountry singleAreaCountry = new SingleAreaCountry(
										jobj);
								if (singleAreaCountry.areaName != null)
									areaList.add(singleAreaCountry);
							}
						}
					}
				} else {
					Toast.makeText(
							Activity,
							R.string.message_no_data,
							Toast.LENGTH_SHORT).show();
					Activity.popFragments();
				}
			} else {				
			}
		} catch (Exception e) {
			
			e.printStackTrace();
			Log.e("Data Control Here", "Caught Exception");
		}
		populateSpinner();
	}

	private void populateSpinner() {
		if (areaList != null) {
			Collections.sort(areaList, new Comparator<SingleAreaCountry>() {

				@Override
				public int compare(SingleAreaCountry lhs, SingleAreaCountry rhs) {
					return lhs.areaName.compareTo(rhs.areaName);
				} 
			});
			//Anju
			SpinnerAreaAdapter spinAdapter = new SpinnerAreaAdapter(Activity,
					areaList);
			restaurantLocation.setAdapter(spinAdapter);
		} else
			return;
		/*SpinnerAreaAdapter spinAdapter = new SpinnerAreaAdapter(Activity,
				areaList);
		restaurantLocation.setAdapter(spinAdapter);*/
	}

	private void parseRestDetailsData(JSONObject jsonObject) {
		try {
			try {
				if (jsonObject != null
						&& !jsonObject.isNull(Restaurant.RESTAURANT_DETAILS)) {
					Restaurant restaurant = null;
					JSONArray jsonArray = jsonObject
							.getJSONArray(Restaurant.RESTAURANT_DETAILS);
					if (jsonArray != null && jsonArray.length() > 0) {
						for (int i = 0; i < jsonArray.length(); i++) {
							restaurant = new Restaurant(
									jsonArray.getJSONObject(i));
						}
						status.setText(restaurant.restaurantStatus);
						double range = 0.000;
						try {
							range = Double.valueOf(restaurant.minimumAmount);
							minimum_Amount = restaurant.minimumAmount;
						} catch (Exception exception) {
							Log.e(TAB_TAG, "Exception in  parsing : "
									+ exception.getMessage());
						}
						minimumAmount.setText(getString(R.string.to_kd)+" "+df.format(range));
						workingHours.setText(restaurant.workingHour);
						deliverTime.setText(getString(R.string.to_minites,
								restaurant.deliveryTime));
						range = 0.000;
						try {
							range = Double.valueOf(restaurant.u3anCharge);
						} catch (Exception exception) {
							Log.e(TAB_TAG, "Exception in  parsing : "
									+ exception.getMessage());
						}
						deliveryCharges
								.setText(getString(R.string.to_kd)+" "+df.format(range));

						m_img_knet
								.setVisibility(restaurant.acceptsKNet ? View.VISIBLE
										: View.GONE);
						m_img_cc.setVisibility(restaurant.acceptsCC ? View.VISIBLE
								: View.GONE);
						m_img_cash
								.setVisibility(restaurant.acceptsCash ? View.VISIBLE
										: View.VISIBLE);
						Cuisines.setText(restaurant.cuisine);
					}
				}
			} catch (JSONException e) {
				Log.e(TAB_TAG,
						"Exception In Restaurant paring :" + e.getMessage());
				e.printStackTrace();
			}

			JSONArray jArray = jsonObject
					.getJSONArray(AppConstants.rest_det_json_array);
			JSONObject jObj = jArray.getJSONObject(0);

			restaurantNameTextView.setText(jObj
					.getString(AppConstants.restaurant_name_json_obj));
			restaurant_rating = jObj.getString(AppConstants.rating_json_obj);
			restaurantLogo = jObj
					.getString(AppConstants.restaurant_logo_json_obj);
			Picasso.with(getActivity()).load(restaurantLogo)
					.into(restaurantLogoImageView);
			restaurantAbout = jObj.getString(AppConstants.restaurant_summary);

		} catch (Exception e) {
		}
	}

	private void initView(View view) {

		scroll = (ScrollView) view.findViewById(R.id.scrollView1);
		status = (TextView) view.findViewById(R.id.status_all_rest);
		status.setSelected(true);
		minimumAmount = (TextView) view.findViewById(R.id.miniamnt__all_rest);
		minimumAmount.setSelected(true);

		minimumAmount.setText(getString(R.string.to_kd, df.format(0.000)));
		workingHours = (TextView) view.findViewById(R.id.wrking_hrs_all_resta);
		workingHours.setSelected(true);
		deliverTime = (TextView) view.findViewById(R.id.delvry_tym_all_resta);
		deliverTime.setSelected(true);
		deliveryCharges = (TextView) view
				.findViewById(R.id.delvry_chrgs_all_resta);
		deliveryCharges.setText(getString(R.string.to_kd, df.format(0.000)));
		deliveryCharges.setSelected(true);
		/*
		 * postDatedDeliveryServices = (TextView) view
		 * .findViewById(R.id.post_dated_del_all_resta); LoyaltyProgramme =
		 * (TextView) view .findViewById(R.id.lylty_pgms_all_resta);
		 */
		// Payment = (TextView) view.findViewById(R.id.payments_all_resta);
		Cuisines = (TextView) view.findViewById(R.id.cuisine_all_resta);
		Cuisines.setSelected(true);
		restaurantLogoImageView = (ImageView) view
				.findViewById(R.id.imageView1_rest_logo);
		restaurantNameTextView = (TextView) view
				.findViewById(R.id.resturant_name_details);
		restaurantNameTextView.setSelected(true);
		
		Typeface custom_font = Typeface.createFromAsset(Activity.getAssets(), "fonts/BREESERIF-REGULAR.OTF");
		restaurantNameTextView.setTypeface(custom_font);
		
		restaurantLocation = (Spinner) view
				.findViewById(R.id.spinner_regioselecton);
		show_menu_Button = (Button) view.findViewById(R.id.show_menu);
		show_menu_Button.setOnClickListener(this);

		about_restaurantTextView = (TextView) view
				.findViewById(R.id.about_restaurants);
		about_restaurantTextView.setOnClickListener(this);
		about_restaurantTextView.setSelected(true);
		specialTextView = (TextView) view.findViewById(R.id.txt_special_offer);
		specialTextView.setOnClickListener(this);
		specialTextView.setSelected(true);
		most_selling_dishes_TextView = (TextView) view
				.findViewById(R.id.txt_most_selling_dishes);
		most_selling_dishes_TextView.setOnClickListener(this);
		most_selling_dishes_TextView.setSelected(true);
		m_img_knet = (ImageView) view.findViewById(R.id.imageView_paymentKNET);
		m_img_cc = (ImageView) view.findViewById(R.id.imageView_paymentVisa);
		m_img_cash = (ImageView) view.findViewById(R.id.imageView_paymentCash);
	}

	// AsyncTask for loading data....
	public class doUploadNewAdressTask extends AsyncTask<Void, String, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;
		InputStream istream;

		public doUploadNewAdressTask(Boolean progressVisibilty) {
			this.progressVisibiltyflag = progressVisibilty;
		}

		@Override
		protected void onPreExecute() {
			showProgressbar();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
	
		
			Log.e("Entered into the AsyncTask", ""+restaurantID);
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				objJson.accumulate("restId", restaurantID);

				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(
						AppConstants.GET_AREA_BY_RESTAURANT_URL);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();

					jObj = new JSONObject(result);

				} else {
					result = null;
				}

			} catch (JSONException e) {
				Log.e(TAB_TAG, "JSONException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				Log.e(TAB_TAG,
						"UnsupportedEncodingException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				Log.e(TAB_TAG,
						"ClientProtocolException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				Log.e(TAB_TAG, "IOException Error:" + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			hideProgressbar();
			try {
				parseData(jObj);
			} catch (Exception e) {
				Log.e(TAB_TAG, "Error:" + e.getMessage());
				Toast.makeText(Activity,
						R.string.check_network,
						Toast.LENGTH_SHORT).show();
			}

			// calling restaurant details
			restaurantLocation
					.setOnItemSelectedListener(new OnItemSelectedListener() {
						@Override
						public void onItemSelected(AdapterView<?> parent,
								View view, int position, long id) {
							area_ID = areaList.get(position).areaId;
							writeDataIntoFile(areaList.get(position).areaName);
							Log.e("The data received",
									""+areaList.get(position).areaId);
							AccumulateJSONObject(2, areaList.get(position));
						}

						@Override
						public void onNothingSelected(AdapterView<?> parent) {
						}
					});
			scroll.fullScroll(View.FOCUS_UP);
		}
	}

	private void writeDataIntoFile(String string) {
		FileOutputStream fosdummyy;
		try {
			fosdummyy = Activity.openFileOutput(FILENAMEAREA,
					Context.MODE_PRIVATE);
			fosdummyy.write(string.getBytes());
			fosdummyy.close();
		} catch (Exception e) {

		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		case R.id.show_menu:
			Fragment restaurantMenuFragment = new RestaurantMenuFragment();
			Bundle params1 = new Bundle();
			params1.putString("Area_ID", area_ID);
			params1.putString("restaurant_id", restaurantID);
			params1.putString("restaurant_logo", restaurantLogo);
			params1.putString("restaurant_name", restaurantName);
			params1.putString("restaurant_rating", restaurant_rating);
			params1.putString("Header", header);
			params1.putString("minimum_Amount", minimum_Amount);
			restaurantMenuFragment.setArguments(params1);
			Activity.pushFragments(restaurantMenuFragment, false, true);
			break;

		case R.id.about_restaurants:
			Fragment about_restaurantsFragment = new AboutRestaurantFragment();
			Bundle params11 = new Bundle();
			params11.putString("restaurant_id", restaurantID);
			params11.putString("restaurant_logo", restaurantLogo);
			params11.putString("restaurant_name", restaurantName);
			params11.putString(AppConstants.restaurant_summary, restaurantAbout);
			about_restaurantsFragment.setArguments(params11);
			Activity.pushFragments(about_restaurantsFragment, false, true);
			break;

		case R.id.txt_special_offer:
			Fragment specialOfferFragment = new SpecialOffersFragment();
			Bundle params = new Bundle();
			params.putString("restaurant_id", restaurantID);
			params.putString("restaurant_logo", restaurantLogo);
			params.putString("restaurant_name", restaurantName);
			specialOfferFragment.setArguments(params);
			Activity.pushFragments(specialOfferFragment, false, true);
			break;

		case R.id.txt_most_selling_dishes:
			Fragment fragment = new MostSellingDishesByRestaurantsFragment();
			Bundle params111 = new Bundle();
			params111.putString("Area_ID", area_ID);
			params111.putString("restaurant_id", restaurantID);
			params111.putString("restaurant_logo", restaurantLogo);
			params111.putString("restaurant_name", restaurantName);
			params111.putString("restaurant_rating", restaurant_rating);
			params111.putString("Header", header);
			params111.putString("minimum_Amount", minimum_Amount);
			fragment.setArguments(params111);
			Activity.pushFragments(fragment, false, true);
			break;

		default:
			break;
		}
	}

	private void showProgressbar() {
		Activity.startSpinwheel(false, true);
	}

	private void hideProgressbar() {
		Activity.stopSpinWheel();
	}

}
