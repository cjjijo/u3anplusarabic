package com.mawaqaa.u3an.fragments;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class TermsAndConditionFrag extends HeaderViewControlFragment {
	FrameLayout framelay;
	WebView termstext;
	String data;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		// TODO Auto-generated method stub
		final View v = info.inflate(R.layout.fragment_faq, gp, false);
		setupUI(v, false);
		initView(v);
		return v;
	}

	private void initView(View v) {
// Jijo
		framelay = (FrameLayout) v.findViewById(R.id.faq_web_container);
		setTitleForPage(getResources().getString(R.string.terms_and_condition));
		AccumulateData();
	}

	private void AccumulateData() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		JSONObject jsonObject = new JSONObject();

		try {
			jsonObject.accumulate("PageName", "terms");
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LoadData(jsonObject);
	}

	private void LoadData(JSONObject jsobject) {
		Activity.startSpinwheel(false, true);
		if (VolleyUtils.volleyEnabled) {
			Log.e("URL",""+ AppConstants.get_pagedetails_url);
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(AppConstants.get_pagedetails_url,
					jsobject);

		}

	}

	

	@Override
	public void pageLoadSuccess(JSONObject jsonObject) {
		super.SignupSuccess(jsonObject);
		Activity.stopSpinWheel();

		Log.e("Consultation Frag The response Success", ""+jsonObject.toString());
		if (jsonObject != null)
			parseData(jsonObject);
		else
			Log.e("Error", "Null JsonObject Response");

	}

	@Override
	public void pageLoadFail(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		Toast.makeText(getActivity(),
				getResources().getString(R.string.sorry_page_load),Toast.LENGTH_LONG).show();
		// Log.e("Consultation Frag The response Fail", jsonObject.toString());
	}

	private void parseData(JSONObject jsonData) {
		try {
			if (jsonData.getString("Status").equalsIgnoreCase("Success")) {
				data = jsonData.getString("Data");
				setWebView();
			} else {
				data = "<html><h3>"+getResources().getString(R.string.nodata)+"</h3></html>";
				setWebView();
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void setWebView() {
		// htmlData =
		// "<html><p class=\"MsoNormal\" style=\"line-height:150%\"><span style=\"font-size:14.0pt;line-height:150%;font-family:times,serif;color:#505862\">About us:</span></p>\r\n<p class=\"MsoNormal\" style=\"text-indent:.5in;line-height:150%\"><span style=\"font-size:14.0pt;line-height:150%;font-family:times,serif;color:#505862\">Welcometo U3an.com, your number one source for ordering food online. We�re dedicatedto giving you the very best of home delivery, with a focus on dependability,customer service and uniqueness. Founded in 2014 by <span style=\"background:yellow;mso-highlight:yellow\">[founder's name],</span> U3an.com has come a longway from its beginnings. We now serve customers all over Kuwait, and arethrilled to give you the best home delivery service. U3an is the place whereyou fulfill your five senses. We provide you with the whole menu of eachrestaurant and images if possible. Forget the phone orders complications andgetting wrong orders to your door. Our site is one of the best online foodservice. With a click of a button you get what you desire and more. We hope youenjoy our services and the promotions we are offering as much as we enjoyoffering them to you. If you have any questions or comments, please don�thesitate to contact us.<o:P /></span></p></html>";

		termstext = new WebView(Activity);
		termstext.setVerticalScrollBarEnabled(false);
		termstext.getSettings().setJavaScriptEnabled(true);
		termstext.setBackgroundColor(Color.parseColor("#FFFFFF"));
		if (Build.VERSION.SDK_INT >= 11)
			termstext.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		this.termstext.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				termstext.setBackgroundColor(Color.parseColor("#FFFFFF"));

				if (Build.VERSION.SDK_INT >= 11)
					termstext.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				// parenrLyt.setBackgroundDrawable(getResources().getDrawable(R.drawable.webview_bg));
			}
		});
		int pad = Utilities.convertDip2Pixels(getActivity(), 45);
		termstext.setPadding(pad, pad, pad, pad);
		framelay.addView(termstext);
		Log.e("htmlData", ""+data);
		Log.e("html fromHtml", "" + Html.fromHtml(data));
		termstext.loadDataWithBaseURL(null,
				Utilities.getHtmlData(getActivity(), data), "text/html",
				"utf-8", "about:blank");
		// aboutUsData.loadDataWithBaseURL(null, htmlData, "text/html", "utf-8",
		// null);

	}
}
