package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.dialogfragment.DialogReadyU3AnVideo;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class RestaurantMenuFragment extends HeaderViewControlFragment {
	public String restaurant_ID, restaurant_Name, restaurant_Logo,
			restaurant_rating, area_ID;
	private ListView m_res_menu_list;
	private ArrayList<Menu> menus;
	LinearLayout Review;
	Context context;
	public String minimum_Amount;
	String userName;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		TAB_TAG = getClass().getName();
		View v = info.inflate(R.layout.fragment_restaurantmenu, gp, false);
		setupUI(v, false);
		// showCartButton(true);
		cartTextView.setOnClickListener(cartSelected);
		area_ID = getArguments().getString("Area_ID");
		restaurant_ID = getArguments().getString("restaurant_id");
		restaurant_Name = getArguments().getString("restaurant_name");
		restaurant_Logo = getArguments().getString("restaurant_logo");
		restaurant_rating = getArguments().getString("restaurant_rating");
		minimum_Amount = getArguments().getString("minimum_Amount");

		//Anju
		/* setHeader(); */
		//PrefUtil.setFragHeading(getActivity(), getResources().getString(R.string.detailed_search_heading));
		setTitleForPage(getResources().getString(R.string.detailed_search_heading));
		initView(v);
		
		//showVideoDialog();
		
		AccumulateJSONObject();

		userName = Activity.readFile();
		if (userName == null) {
			userName = Secure.getString(Activity.getContentResolver(),
					Secure.ANDROID_ID);
			getCartCount();
		} else if (Utilities.doemailValidation(userName)) {
			getCartCount();
		} else {
			userName = Secure.getString(Activity.getContentResolver(),
					Secure.ANDROID_ID);
			getCartCount();
		}
		return v;
	}

	private void getCartCount() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			// country id is always 1 for kuwait...
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", userName);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadCountData(jsonObject);
	}

	private void loadCountData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();

			if (Utilities.doemailValidation(userName)) {
				commandFactory.sendPostCommand(AppConstants.cart_count_URL,
						jsonObject);
			} else {
				commandFactory.sendPostCommand(
						AppConstants.temp_cart_count_URL, jsonObject);
			}
		}
	}

	@Override
	public void cartCountSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.cartCountSuccess(jsonObject);
		Log.e("The data receivedd", ""+jsonObject.toString());
		parseCountData(jsonObject);
	}

	@Override
	public void tempCartCountSuccess(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.tempCartCountSuccess(jsonObject);
		parseCountData(jsonObject);
	}

	private void parseCountData(JSONObject jsonObject) {
		String value = null;
		try {
			value = jsonObject.getString("Value");
			Log.e("The data", ""+value);
		} catch (Exception e) {
			Log.e("The Expedition",""+ e.getMessage());
		}
		countView.setText(value);
	}

	private void AccumulateJSONObject() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();


		JSONObject jsonObject = new JSONObject();
		try {
			// country id is always 1 for kuwait...
			jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("restId", restaurant_ID);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryData(jsonObject);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.most_selling_get_restaurant_menu_url,
					jsonObject);
		}
	}

	@Override
	public void onRestaurantMenuLoadedSuccessfully(JSONObject jsonObject) {
		super.onRestaurantMenuLoadedSuccessfully(jsonObject);
		parseData(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onRestaurantMenuLoadingFailed(JSONObject jsonObject) {
		super.onRestaurantMenuLoadingFailed(jsonObject);
		Activity.stopSpinWheel();
	}
	
	
	private void showVideoDialog() {
		try {
			DialogReadyU3AnVideo qtyDialog = new DialogReadyU3AnVideo();
			qtyDialog.show(getActivity().getSupportFragmentManager(), "DialogReadyU3AnVideo");
		} catch (NullPointerException ne) {
			// Log.e(TAG, "Null Pointer Exception in Sharing");
			ne.printStackTrace();
		}
	}
	
	

	private void parseData(JSONObject jsonObject) {
		if (jsonObject != null) {

			if (!jsonObject.isNull(AppConstants.restaurant_menu_list)) {
				try {
					JSONArray jsonArray = jsonObject
							.getJSONArray(AppConstants.restaurant_menu_list);
					menus = new ArrayList<Menu>();
					for (int i = 0; i < jsonArray.length(); i++) {
						if (!jsonArray.isNull(i))
							menus.add(new Menu(jsonArray.getJSONObject(i)));
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			setAdapter();
		}
	}

	private void setAdapter() {
		MenuAdapter menuAdapter = new MenuAdapter(Activity, menus);
		m_res_menu_list.setAdapter(menuAdapter);
	}

	private void initView(View view) {
		m_res_menu_list = (ListView) view.findViewById(R.id.rest_menu_list);
		m_res_menu_list.setOnItemClickListener(onMenuItemSelectedListener);
		TextView m_rest_name = (TextView) view.findViewById(R.id.rest_name);
		m_rest_name.setSelected(true);
		Typeface custom_font = Typeface.createFromAsset(Activity.getAssets(), "fonts/BREESERIF-REGULAR.OTF");		
		m_rest_name.setTypeface(custom_font);
		
		Review = (LinearLayout) view.findViewById(R.id.reviews);
		m_rest_name.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {				
			}
		});
		
		Review.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Fragment reFragment = new ReviewFragment();
				Bundle data = new Bundle();
				Log.e(TAB_TAG, "restaurant_ID:" + restaurant_ID);
				Log.e(TAB_TAG, "restaurant_Name:" + restaurant_Name);
				Log.e(TAB_TAG, "restaurant_Logo:" + restaurant_Logo);
				data.putString("restId", restaurant_ID);
				data.putString("restName", restaurant_Name);
				data.putString("Header", header);
				data.putString("resLog", restaurant_Logo);
				data.putString("resrating", restaurant_rating);
				reFragment.setArguments(data);
				Activity.pushFragments(reFragment, false, true);
			}
		});

		if (restaurant_Name != null)
			m_rest_name.setText(restaurant_Name);

		RatingBar m_rest_rating = (RatingBar) view
				.findViewById(R.id.resturant_rating);
		float rating = 0;
		try {
			rating = Float.valueOf(restaurant_rating);
		} catch (Exception exception) {
			Log.e(TAB_TAG,
					"Exception in rating converting : "
							+ exception.getMessage());
		}

		m_rest_rating.setRating(rating);

		ImageView m_logo_img = (ImageView) view.findViewById(R.id.rest_logo);
		if (URLUtil.isValidUrl(restaurant_Logo) && restaurant_Logo.length() > 5) {
			Picasso.with(Activity).load(restaurant_Logo)
					.error(R.drawable.imagenotavailable).into(m_logo_img);
		} else {
			m_logo_img.setBackgroundResource(R.drawable.imagenotavailable);
		}

	}
//**************************************************************************************************    
//************************************************************************************************************	
	
	
	
	private OnItemClickListener onMenuItemSelectedListener = new OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			try {
				Menu menu = menus.get(position);
				if (menu != null) {

					Fragment fragment = new RestaurantMenuInnerSectionFragment();
					Bundle bundle = new Bundle();
					bundle.putString("Area_ID", area_ID);
					bundle.putString("restaurant_id", restaurant_ID);
					bundle.putString("restaurant_name", restaurant_Name);
					bundle.putString("restaurant_rating", restaurant_rating);
					bundle.putString("Header", header);
					bundle.putString("restaurant_logo", restaurant_Logo);
					bundle.putString("menuItemId", "" + menu.menuId);
					bundle.putString("menuItemName", menu.menuName);
					bundle.putString("minimum_Amount", minimum_Amount);
					fragment.setArguments(bundle);
					Activity.pushFragments(fragment, false, true);
				}
			} catch (Exception e) {
				Log.e(TAB_TAG, "Exception :" + e.getMessage());
			}
		}
	};

	class Menu {

		public int menuId;
		public String menuName;

		public Menu(JSONObject jsonObject) {
			if (jsonObject != null) {
				if (!jsonObject.isNull(AppConstants.restaurant_menu_id))
					menuId = jsonObject.optInt(AppConstants.restaurant_menu_id);

				if (!jsonObject.isNull(AppConstants.restaurant_menu_name)) {
					menuName = jsonObject
							.optString(AppConstants.restaurant_menu_name);
					// Capitalize First letter
					menuName = menuName.substring(0, 1).toUpperCase()
							+ menuName.substring(1);
				}
			}
		}
	}

	class MenuAdapter extends BaseAdapter {
		ArrayList<Menu> menus;
		private LayoutInflater inflater;
		Context context;

		public MenuAdapter(Context _con, ArrayList<Menu> menus) {
			context = _con;
			this.menus = menus;
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public int getCount() {
			return menus.size();
		}

		@Override
		public Menu getItem(int position) {
			return menus.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View v = convertView;
			ViewHolder holder;
			if (convertView == null) {
				v = inflater.inflate(R.layout.restaurant_menu_item, null);
				holder = new ViewHolder();
				holder.name = (TextView) v.findViewById(R.id.item_category);
				holder.name.setSelected(true);

				v.setTag(holder);
			} else {
				holder = (ViewHolder) v.getTag();
			}
			//Anju
			if(menus.size()!=0){
				holder.name.setText(menus.get(position).menuName);
				holder.name.setTextColor(context.getResources().getColor(
						R.color.black_font_color));
			}else{
				holder.name.setText("");
			}
			return v;
		}
		public class ViewHolder {
			public TextView name;
		}
	}

	private OnClickListener cartSelected = new OnClickListener() {
		@Override
		public void onClick(View v) {
			userName = Activity.readFile();
			if (userName == null || userName.equals("")) {
				Toast.makeText(Activity, R.string.item_to_continue,
						Toast.LENGTH_SHORT).show();

			} else if (Utilities.doemailValidation(userName)) {
				Fragment cartFragment = new U3AnFoodCartFragment();
				Bundle data = new Bundle();
				data.putString("UserName", userName);
				cartFragment.setArguments(data);
				Activity.pushFragments(cartFragment, false, true);
			} else {
				Fragment cartFragment = new U3AnFoodCartFragment();
				Bundle data = new Bundle();
				data.putString("UserName", Secure.getString(
						Activity.getContentResolver(), Secure.ANDROID_ID));
				cartFragment.setArguments(data);
				Activity.pushFragments(cartFragment, false, true);
			}
		}
	};
}
