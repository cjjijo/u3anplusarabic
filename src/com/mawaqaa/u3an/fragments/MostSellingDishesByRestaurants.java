package com.mawaqaa.u3an.fragments;

import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MostSellingDishesByRestaurants extends HeaderViewControlFragment {
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.mostsellingdishesdetails, gp, false);
		setupUI(v, false);
		initView(v);
		
		if (Utilities.isNetworkAvailable(Activity)) {
			AccumulateJSONObject();
		}else{
			Toast.makeText(Activity, getResources().getString(R.string.no_network), Toast.LENGTH_SHORT).show();
		}
		
		return v;
	}

	private void initView(View v) {
	}

	private void AccumulateJSONObject() {
		Activity.startSpinwheel(true, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObjectCuisines = new JSONObject();
		try {
			// country id is always 1 for kuwait
			jsonObjectCuisines = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObjectCuisines.accumulate("locale", "en-US");
			}else{
				jsonObjectCuisines.accumulate("locale", "ar-KW");
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		loadSubCategoryData(jsonObjectCuisines);
	}

	private void loadSubCategoryData(JSONObject jsonObject) {
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory.sendPostCommand(
					AppConstants.most_selling_dishlist_url, jsonObject);
		}
	}
}
