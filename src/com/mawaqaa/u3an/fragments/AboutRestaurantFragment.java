package com.mawaqaa.u3an.fragments;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.squareup.picasso.Picasso;

public class AboutRestaurantFragment extends HeaderViewControlFragment{
	
	private String restaurant_name,restaurant_logo,Stringsummary;
	private TextView restaurantname_TextView,About;
	private ImageView restaurant_logo_ImageView;
	 	
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.fragment_aboutus, gp, false);
		setupUI(v, false);
		init(v);
		restaurant_name = getArguments().getString("restaurant_name");
		restaurant_logo = getArguments().getString("restaurant_logo");
		Stringsummary = getArguments().getString(
				AppConstants.restaurant_summary);
		if (restaurant_logo.startsWith("http")) {
			Picasso.with(Activity).load(restaurant_logo)
					.into(restaurant_logo_ImageView);
		} else {
			restaurant_logo_ImageView
					.setBackgroundResource(R.drawable.imagenotavailable);
		}
		restaurantname_TextView.setText(restaurant_name);
		setTitleForPage(getResources().getString(R.string.about_restaurants));
		About.setText(Stringsummary);
		return v;
	}

	private void init(View view) {
		restaurantname_TextView = (TextView) view
				.findViewById(R.id.restaurant_name_Textview);
		restaurant_logo_ImageView = (ImageView) view
				.findViewById(R.id.restaurant_logo);
		About = (TextView) view.findViewById(R.id.rest_about);
	}
}
