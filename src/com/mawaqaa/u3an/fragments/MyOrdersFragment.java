 package com.mawaqaa.u3an.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.MyOrdersAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class MyOrdersFragment extends HeaderViewControlFragment implements OnItemClickListener {
	ListView orderListview;
	private ArrayList<HashMap<String, String>> arraylist;
	private MyOrdersAdapter adapter;
	private ArrayList<JSONObject> listItem;
	LogoutListener listener;
	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		View v = info.inflate(R.layout.my_oreders, gp, false);
		setupUI(v, false);
		setTitleForPage(getString(R.string.my_accnt));
		orderListview = (ListView) v.findViewById(R.id.orderListView);
		fetchmyOrders();
		orderListview.setOnItemClickListener(this);
		return v;
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		listener = (LogoutListener) getActivity();
	}

	@Override
	public void pushFragments4LanSwitch() {
		super.pushFragments4LanSwitch();
		Log.e("entered here", "logout trigered");
		if (listener != null) {
			Activity.popFragments();
		}
	}
	private void fetchmyOrders() {
		Activity.startSpinwheel(false, true);
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();

		try {
			JSONObject jsonObject = new JSONObject();
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("userId", Activity.readFile());
			if (VolleyUtils.volleyEnabled) {
				CommandFactory commandFactory = new CommandFactory();
				commandFactory.sendPostCommand(AppConstants.myOrdersUrl, jsonObject);
			}
			Log.e("My order params", "" + jsonObject.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onMyOrdersLoadedSuccessfully(JSONObject jsonObject) {
		super.onMyOrdersLoadedSuccessfully(jsonObject);
		Activity.stopSpinWheel();
		Log.e("My order response", "" + jsonObject.toString());
		parseMyorders(jsonObject);
	}

	private void parseMyorders(JSONObject jsonObject) {
		try {
			arraylist = new ArrayList<HashMap<String, String>>();
			listItem = new ArrayList<JSONObject>();
			if (jsonObject.getString("Status").equals("Success")) {
				JSONArray array = jsonObject.getJSONArray("TransactionList");
				for (int i = 0; i < array.length(); i++) {
					HashMap<String, String> map = new HashMap<String, String>();
					JSONObject obj = array.getJSONObject(i);
					map.put(AppConstants.pDate, obj.getString(AppConstants.pDate));
					map.put(AppConstants.DeliveryArea, obj.getString(AppConstants.DeliveryArea));
					map.put(AppConstants.DeliveryBlock, obj.getString(AppConstants.DeliveryBlock));
					map.put(AppConstants.Total, obj.getString(AppConstants.Total));
					arraylist.add(map);
					listItem.add(obj);

				}
				showMyOrders(arraylist);
			}
		} catch (JSONException e) {
		}
	}

	private void showMyOrders(ArrayList<HashMap<String, String>> arraylist2) {
		adapter = new MyOrdersAdapter(Activity, arraylist2);
		orderListview.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		if (arraylist2.size() > 0) {
			return;
		} else {
			Toast.makeText(Activity, R.string.data_availabale, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onMyOrdersLoadFail(JSONObject jsonObject) {
		super.onMyOrdersLoadFail(jsonObject);
		Activity.stopSpinWheel();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		try{
		JSONArray array=listItem.get(position).getJSONArray("Orders");
		Log.e("Orders array", "......"+array.length());
		if (array.length()>0) {
			MyOrdersFragmentChildView frag = new MyOrdersFragmentChildView(position);
			Activity.pushFragments(frag, false, true);
			
		} else {
			Log.e("Orders array", "......"+position);
		}
		}catch(JSONException e){
			e.printStackTrace();
		}
	}
}
