package com.mawaqaa.u3an.fragments;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.adapters.SpinnerAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.db.OpenHelper;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.volley.CommandFactory;
import com.mawaqaa.u3an.volley.VolleyUtils;

public class RegisterThirdStageFragment extends HeaderViewControlFragment
		implements OnClickListener {

	
	 public static String trhtmlData;
	EditText referredEditText;
	String newsletter,sms;

	Spinner occupationEditText, abt_usEditText;

	ImageButton submitButton;
	String genderString, occupationString, about_usString, referenceString;

	EditText errorEditText;

	ArrayList<String> occupationArrayList = new ArrayList<String>();
	ArrayList<String> abt_ArrayList = new ArrayList<String>();

	CheckBox checkbox_newsletter, checkBox_SMS, checkBox_terms_and_condition;

	private ArrayList<HashMap<String, String>> STAGE1_DATA = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> STAGE2_DATA = new ArrayList<HashMap<String, String>>();

	OpenHelper db_obj;

	Boolean newsletterstatus, SMSstatus, privacy_policy_status;

	String email, password, firstName, lastName, mobile, homephone;
	String area, profilename, address_Type, block, judda, street, houseno,
			floor, apartment, directions;
	private RadioGroup radioGroup;
	private RadioButton radioButton;

	RadioButton maleRadioButton, femaleRadioButton;

	@Override
	View InitializeContainer(LayoutInflater info, ViewGroup gp) {
		final View rootView = info.inflate(
				R.layout.fragment_register_third_stage, gp, false);
		init(rootView);
		setTitleForPage(getResources().getString(R.string.sign_up_heading));
		populateArraylist();
		setupUI(rootView, false);
		db_obj = new OpenHelper(Activity);

		STAGE1_DATA = db_obj.temporary_Contact_Records();
		STAGE2_DATA = db_obj.sign_up_Data_SecondStage_Retrieval();

		Log.e("First stage data", ""+STAGE1_DATA.toString());
		Log.e("dlkfjldj rf", ""+STAGE2_DATA.toString());
		  final int listzize=occupationArrayList.size()-1;
		   ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(
			         getActivity(),R.layout.simple_spinner_item, occupationArrayList){
			        @Override
			        public int getCount() {
			         return(listzize);
			        }
			       };
			       
			       adapter_state.setDropDownViewResource(R.layout.simple_spinner_item);
			       occupationEditText.setAdapter(adapter_state);
			       occupationEditText.setSelection(listzize);

	/*	SpinnerAdapter spinnerAdapter = new SpinnerAdapter(Activity,
				occupationArrayList);
		occupationEditText.setAdapter(spinnerAdapter);
*/
		SpinnerAdapter spinnerAdapter2 = new SpinnerAdapter(Activity,
				abt_ArrayList);
		abt_usEditText.setAdapter(spinnerAdapter2);

		ExtractData();
		return rootView;
	}

	private void ExtractData() {
		if (STAGE1_DATA.size() > 0) {
			Log.e("ARRAY", "not null");

			for (int i = 0; i < STAGE1_DATA.size(); i++) {
				HashMap<String, String> map = STAGE1_DATA.get(i);

				email = map.get("email");
				password = map.get("password");
				firstName = map.get("first_name");
				lastName = map.get("last_name");
				mobile = map.get("phone");
				homephone = map.get("home_phone");

			}
		}

		if (STAGE2_DATA.size() > 0) {
			Log.e("ARRAY", "not null");

			for (int i = 0; i < STAGE2_DATA.size(); i++) {
				HashMap<String, String> map = STAGE2_DATA.get(i);

				area = map.get("Area");
				profilename = map.get("ProfileName");
				address_Type = map.get("address_Type");
				block = map.get("Block");
				judda = map.get("Judda");
				street = map.get("Street");
				houseno = map.get("HouseNo");
				floor = map.get("Floor");
				apartment = map.get("Apartment");
				directions = map.get("Directions");
			}
			Log.e("area", ""+area);
		}
	}

	private void populateArraylist() {		
		occupationArrayList.add(getResources().getString(R.string.professional_service));
		occupationArrayList.add(getResources().getString(R.string.self_employed));
		occupationArrayList.add(getResources().getString(R.string.others));
		occupationArrayList.add(getResources().getString(R.string.select));
		abt_ArrayList.add(getResources().getString(R.string.select));
		abt_ArrayList.add(getResources().getString(R.string.others));
	}

	private void init(View view) {

		radioGroup = (RadioGroup) view.findViewById(R.id.radiogroup);
		occupationEditText = (Spinner) view
				.findViewById(R.id.Edittext_register3_Occupation);
		abt_usEditText = (Spinner) view
				.findViewById(R.id.Edittext_register3_How);
		referredEditText = (EditText) view
				.findViewById(R.id.Edittext_register3_Reference);
		submitButton = (ImageButton) view.findViewById(R.id.submit_Btn);
		submitButton.setOnClickListener(this);

		checkbox_newsletter = (CheckBox) view.findViewById(R.id.promos);
		checkBox_SMS = (CheckBox) view.findViewById(R.id.keep_me_logged);

		checkBox_terms_and_condition = (CheckBox) view
				.findViewById(R.id.privacy_policy);
		
		checkBox_terms_and_condition.setOnClickListener(new OnClickListener() {
			   
			   @Override
			   public void onClick(View v) {
			    // TODO Auto-generated method stub
				   if(trhtmlData!=null){
			    if(checkBox_terms_and_condition.isChecked()==true)
			    {
			    // TODO Auto-generated method stub
			    MyDialogFragmentTermscondition crBanner = MyDialogFragmentTermscondition.newInstance();

			   
			    crBanner.show(Activity.getFragmentManager(), "crBanner");
			    }
				   }
			   }
			  });

		alignCheckBox(checkBox_SMS);
		alignCheckBox(checkBox_terms_and_condition);
		alignCheckBox(checkbox_newsletter);

		maleRadioButton = (RadioButton) view.findViewById(R.id.radioMale);
		femaleRadioButton = (RadioButton) view.findViewById(R.id.radioFemale);

		alignRadioButton(maleRadioButton);
		alignRadioButton(femaleRadioButton);
		
		new getTrTheservice(true).execute();
		
	}

	private void alignCheckBox(CheckBox checkBox) {
		final float scale = this.getResources().getDisplayMetrics().density;
		checkBox.setPadding(checkBox.getPaddingLeft()
				+ (int) (10.0f * scale + 0.5f), checkBox.getPaddingTop(),
				checkBox.getPaddingRight(), checkBox.getPaddingBottom());

	}

	private void alignRadioButton(RadioButton radioButton) {
		final float scale = this.getResources().getDisplayMetrics().density;
		radioButton.setPadding(radioButton.getPaddingLeft()
				+ (int) (10.0f * scale + 0.5f), radioButton.getPaddingTop(),
				radioButton.getPaddingRight(), radioButton.getPaddingBottom());

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.submit_Btn:
			validateData(v);
			break;

		default:

		}
	}

	private void validateData(View view) {
		Boolean isValid = isGenderValid(view);
		if (!isValid)
			return;

		isValid = isOccupationValid();
		if (!isValid)
			return;

		isValid = isDataValid();
		if (!isValid)
			return;
		else {
			doSubmissionData();
		}
	}

	private Boolean isDataValid() {
		//about_usString = abt_usEditText.getSelectedItem().toString();
		  //Anju
		  if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_AR)){
		   if (abt_usEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.others))) {
		    about_usString = "Others";
		   }
		   
		  }else{
		   about_usString = abt_usEditText.getSelectedItem().toString();
		  }
		if (about_usString == "Select") {
			Toast.makeText(Activity, R.string.choose_data, Toast.LENGTH_SHORT)
					.show();
			return false;
		} else {
				//popFragments(this, false, true);
		
			return true;
		}

	}
	public void popFragments(Fragment fragment, boolean shouldAnimate,
			boolean shouldAdd) {
		FragmentManager manager = Activity.getSupportFragmentManager();
		String backStateName = fragment.getClass().getName();
		FragmentTransaction trans = manager.beginTransaction();
		trans.remove(fragment);
		trans.commit();
		manager.popBackStack();
		Log.d( "backStateName :", "" + backStateName);
		
	}

	private void doSubmissionData() {
		newsletterstatus = checkbox_newsletter.isChecked();
		
		newsletter=newsletterstatus.toString();
		Log.e("newsletterstatus", ""+newsletter);
		SMSstatus = checkBox_SMS.isChecked();
		sms=SMSstatus.toString();
		privacy_policy_status = checkBox_terms_and_condition.isChecked();
		referenceString = referredEditText.getText().toString();
		if (privacy_policy_status) {
			AcculateJSONoBJECT();
		}
		else {
			Toast.makeText(Activity, R.string.agree_term_condition, Toast.LENGTH_LONG).show();
		}
		
	}
	

	private void AcculateJSONoBJECT() {
		Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = new JSONObject();

			Log.e("First name", ""+firstName);
			
			jsonObject.accumulate("AddressType", address_Type);
			jsonObject.accumulate("AreaId", area);
			jsonObject.accumulate("Block", block);
			jsonObject.accumulate("Direction", directions);
			jsonObject.accumulate("HouseNumber", houseno);
			jsonObject.accumulate("Judda", judda);
			jsonObject.accumulate("ProfileName", profilename);
			jsonObject.accumulate("Street", street);

			jsonObject.accumulate("countryId", "1");
			jsonObject.accumulate("email", email);
			jsonObject.accumulate("firstName", firstName);
			jsonObject.accumulate("floor", floor);
			jsonObject.accumulate("gender", genderString);
			jsonObject.accumulate("housePhone", homephone);
			jsonObject.accumulate("howDidYouHear", about_usString);
			jsonObject.accumulate("lastName", lastName);
			if(sDefSystemLanguage.equals("en")){
				jsonObject.accumulate("locale", "en-US");
			}else{
				jsonObject.accumulate("locale", "ar-KW");
			}
			jsonObject.accumulate("mobile", mobile);
			jsonObject.accumulate("occupation", occupationString);

			jsonObject.accumulate("office", apartment);
			jsonObject.accumulate("password", password);
			jsonObject.accumulate("referredBy", referenceString);
			jsonObject.accumulate("subscribedToNewsletter", newsletter);
			jsonObject.accumulate("subscribedToSMS", sms);
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
Log.e("jsonobject", ""+jsonObject.toString());
		loadAreaData(jsonObject);
	}

	private void loadAreaData(JSONObject jsonObject) {
		Activity.startSpinwheel(false, true);
		
		if (VolleyUtils.volleyEnabled) {
			CommandFactory commandFactory = new CommandFactory();
			commandFactory
					.sendPostCommand(AppConstants.sign_up_url, jsonObject);
		}
	}

	@Override
	public void SignupSuccess(JSONObject jsonObject) {
		
		super.SignupSuccess(jsonObject);
		Activity.stopSpinWheel();

		Log.e("The response", ""+jsonObject.toString());

		parseData(jsonObject);
	}

	private void parseData(JSONObject jsonObject) {
		String resultString  =  jsonObject.toString();
		if (resultString.contains("Success")) {
			Toast.makeText(Activity, R.string.sign_up_success, Toast.LENGTH_SHORT)
					.show();
			db_obj.Deletrow(email);
			db_obj.DeletrowSecondStage(profilename);
			Fragment frag1 = new RegisterFirstStepFragment();
			Activity.popFragments(frag1);
			Activity.popFragments(new RegisterSecondStageFragment());			
			Activity.pushFragments(new LoginFragment(), false, true);
		} else {
			try{
			resultString =  jsonObject.getString("Status");
			Log.e("resultString ====reg  ", ""+resultString);
			}catch(Exception e){
				Log.e("The Exception Caught", e.getMessage());
			}
			Toast.makeText(Activity, resultString,
					Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void SignupFail(JSONObject jsonObject) {
		Activity.stopSpinWheel();
		super.SignupFail(jsonObject);
		
	}

	private Boolean isOccupationValid() {
		//occupationString = occupationEditText.getSelectedItem().toString();
		 //Anju
		  if(PrefUtil.getAppLanValue(Activity).equals(AppConstants.STR_AR)){
		   if(occupationEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.professional_service))){
		    occupationString = "Professional Services";
		   }else if (occupationEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.self_employed))) {
		    occupationString = "Self employed/Owner";
		   }else if (occupationEditText.getSelectedItem().toString().equalsIgnoreCase(getResources().getString(R.string.others))) {
		    occupationString = "Others";
		   }
		   
		  }else{
		   occupationString = occupationEditText.getSelectedItem().toString();
		  }
		if (occupationString == "Select") {
			Toast.makeText(Activity,getResources().getString(R.string.select_occupation), Toast.LENGTH_SHORT)
					.show();
			return false;
		} else {
			return true;
		}
	}

	private Boolean isGenderValid(View view) {
		if (maleRadioButton.isSelected()) {
			genderString = "Male";
		} else {
			genderString = "Female";
		}
		if (genderString.length() > 0) {
			return true;
	
		} else {
			return false;
		}
	}

	TextWatcher registerInputTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}
	};
	
	
	
	// AsyncTask for loading data....
	 public class getTrTheservice extends AsyncTask<Void, String, Void> {
	  String type, result;
	  boolean postStatus = false;
	  JSONObject jObj = null;
	  Boolean progressVisibiltyflag;
	  InputStream istream;

	  public getTrTheservice(Boolean progressVisibilty) {
	   this.progressVisibiltyflag = progressVisibilty;
	  }

	  @Override
	  protected void onPreExecute() {
	   //showProgressbar();
	   super.onPreExecute();
	  }

	  @Override
	  protected Void doInBackground(Void... params) {
		  Locale current = getResources().getConfiguration().locale;
		  String sDefSystemLanguage = current.getLanguage();

	   try {
	    JSONObject objJson = new JSONObject();
	    objJson.accumulate("PageName", "terms");
	    if(sDefSystemLanguage.equals("en")){
	    	objJson.accumulate("locale", "en-US");
	    }else{
	    	objJson.accumulate("locale", "ar-KW");
	    }

	    HttpClient httpClient = new DefaultHttpClient();
	    HttpPost httppost = new HttpPost(AppConstants.get_pagedetails_url);
	    StringEntity entity = new StringEntity(objJson.toString());
	    httppost.setEntity(entity);
	    httppost.setHeader("Accept", "application/json");
	    httppost.setHeader("Content-type", "application/json");
	    HttpResponse response = httpClient.execute(httppost);
	    istream = response.getEntity().getContent();

	    if (istream != null) {
	     BufferedReader bufferedReader = new BufferedReader(
	       new InputStreamReader(istream));
	     String line = "";
	     result = "";
	     while ((line = bufferedReader.readLine()) != null)
	      result += line;

	     istream.close();

	     jObj = new JSONObject(result);

	    } else {
	     result = null;
	    }

	   } catch (JSONException e) {
	    Log.e("Data", "JSONException Error:" + e.getMessage());
	    e.printStackTrace();
	   } catch (UnsupportedEncodingException e) {
	    Log.e("Data",
	      "UnsupportedEncodingException Error:" + e.getMessage());
	    e.printStackTrace();
	   } catch (ClientProtocolException e) {
	    Log.e("Data", "ClientProtocolException Error:" + e.getMessage());
	    e.printStackTrace();
	   } catch (IOException e) {
	    Log.e("Data", "IOException Error:" + e.getMessage());
	    e.printStackTrace();
	   }
	   return null;
	  }

	  @Override
	  protected void onPostExecute(Void result) {
	   //hideProgressbar();

	   try {
	    if (jObj.getString("Status").equalsIgnoreCase("Success"))
	    {
	     if(jObj.getString("Data").equals(""))
	     {
	      trhtmlData = "<center>"+getResources().getString(R.string.nodatahtml)+"</center>";
	     }
	     else
	     {
	      trhtmlData = jObj.getString("Data");
	     }
	     //setWebView();
	    }
	    else {
	     trhtmlData = "<h3>"+getResources().getString(R.string.nodata)+"</h3>";
	     //setWebView();
	    }

	   } catch (JSONException e) {
	    e.printStackTrace();
	   }

	  }
	 }
	
	
}
