package com.mawaqaa.u3an.appconstants;

public class AppConstants {
	
	// Authentication key storing file
	public static final String base_url = "http://u3anwcf.u3an.com/MobileAPI.svc";
	//public static final String base_url = "http://u3anwcf.mawaqaademo.com/MobileAPI.svc";

	// Identification Items
	public static final String MOST_SELLING_TAG = "1";
	public static final String ALL_RESTAURANT_TAG = "2";
	public static final String NEW_RESTAURANTS_TAG = "3";
	public static final String DETAILED_SEARCH_TAG = "4";

	// Get cuisines
	public static final String get_cusines_url = base_url + "/GetCuisines";
	public static final String get_cusines_json_obj = "cuisineDetails";
	public static final String get_cusines_country_id = "CuisineId";
	public static final String get_cusines_country_name = "CuisineName";

	// Get Restaurant by area
	public static final String get_restuarant_by_area = base_url
			+ "/GetRestaurantsByArea";
	public static final String area_id = "areaId";
	public static final String cuisine_id = "cuisineId";
	public static final String locale = "locale";
	public static final String restuarent_name = "restName";

	public static String DEVICE_ID = null;
	public static String gcm_id = null;

	// SignupData
	public static final String sign_up_url = base_url + "/RegisterCustomer";

	// add Restaurant
	public static final String add_restaurant_url = base_url
			+ "/AddNewReataurants";

	public static final String get_order_count_url = base_url
			+ "/GetOrderCount";

	// getOrderdetails
	public static final String get_order_details = base_url
			+ "/GetOrderDetails";

	public static final String ORDERED_ITEM_ID = "ItemId";
	public static final String ORDERED_ITEM_NAME = "ItemName";
	public static final String ORDERED_ITEM_PRICE = "ItemPrice";
	public static final String ORDERED_ITEM_QUANTITY = "ItemQuantity";
	public static final String ORDERED_ITEM_SPECIAL_REQUEST = "SpecialRequest";
	public static final String ORDERED_ITEM_CHOICE_ID = "ItemChoiceId";
	public static final String ORDERED_ITEM_CHOICE_NAME = "ItemChoiceName";
	public static final String ORDERED_ITEM_CHOICE_PRICE = "ItemChoicePrice";
	public static final String ORDERED_ITEM_CHOICE_QUANTITY = "ItemChoiceQty";
	public static final String ORDERED_ITEM_CHOICES = "OrderItemChoices";

	// All restaurant
	public static final String all_restaurants_url = base_url
			+ "/GetRestaurantsByCountry";
	public static final String all_restaurants_json_obj = "RestaurantList";
	public static final String all_restaurants_rating = "Rating";
	public static final String all_restaurants_id = "RestaurantId";
	public static final String all_restaurants_resturant_logo = "RestaurantLogo";
	public static final String all_restaurants_resturant_name = "RestaurantName";
	public static final String all_restaurants_resturant_status = "RestaurantStatus";
	public static final String all_restaurants_resturant_summary = "Summary";
	public static final String all_restaurants_resturant_sort_order = "SortOrder";
	public static final String all_restaurants_resturant_price = "PositionPrice";

	// Most Selling dishes by restaurants
	public static final String most_selling_restaurants_url = base_url
			+ "/MostSellingRestaurant";
	public static final String most_selling_restaurants_jsonobj = "RestaurantDetails";
	public static final String most_selling_restaurants_name = "RestaurantName";
	public static final String most_selling_restaurants_status = "RestaurantStatus";
	public static final String most_selling_restaurants_thumbnails = "RestaurantLogo";

	// Get area by restaurants
	public static final String area_by_restaurants_url = base_url
			+ "/GetAreasByRestaurant";
	public static final String area_by_json_obj = "AreaDetails";
	public static final String area_by_area_id = "AreaId";
	public static final String area_by_area_name = "AreaName";
	public static final String area_by_city_id = "CityId";
	public static final String area_by_city_name = "CityName";

	// Get Restaurant Details
	public static final String restaurant_details_url = base_url
			+ "/GetRestaurantDetails";
	public static final String rest_det_json_array = "RestaurantDetails";
	public static final String coverage_area_time_json_array = "CoverageAreaTimingDetails";
	public static final String delivery_time_json_obj = "DeliveryTime";
	public static final String delivery_charge_json_obj = "DeliveryCharges";
	public static final String min_amount_json_obj = "MinAmount";
	public static final String minimum_amount_json_obj = "MinimumAmount";
	public static final String rating_json_obj = "Rating";
	public static final String restaurant_logo_json_obj = "RestaurantLogo";
	public static final String restaurant_name_json_obj = "RestaurantName";
	public static final String autocovstatus_json_obj = "RestaurantStatus";
	public static final String cusine_json_obj = "Cuisine";
	public static final String workinghour_json_obj = "Workinghour";
	public static final String acceptCC_json_obj = "AcceptsCC";
	public static final String acceptCash_json_obj = "AcceptsCash";
	public static final String acceptKnet_json_obj = "AcceptsKNET";
	public static final String restaurant_summary = "Summary";

	// Login Details
	public static final String login_url = base_url + "/LoginCustomer";

	// Forget Password
	public static final String forget_password_url = base_url
			+ "/ForgotPassword";

	// Most selling dishes data(By restaurant inner page)
	public static final String most_selling_dishes_data = base_url
			+ "/MostSellingDishesByRestaurant";
	public static final String most_selling_dishes_jobj = "DishList";
	public static final String most_selling_dishes_dishname = "DishName";
	public static final String most_selling_dishes_restaurantname = "RestaurantName";
	public static final String most_selling_dishes_rastaurant_status = "RestaurentStatus";
	public static final String most_selling_dishes_image = "Thumbnail";
	public static final String most_selling_dishes_id = "Id";
	public static final String most_selling_dishes_price = "Price";
	public static final String most_selling_dishes_description = "Description";

	// Most Selling by cuisines dishlist
	public static final String most_selling_by_cuisines_url = base_url
			+ "/MostSellingDishesByCuisine";
	public static final String most_selling_by_cuisines_json_obj = "DishList";
	public static final String most_selling_by_cuisines_dishname = "DishName";
	public static final String most_selling_by_cuisines_restaurant_name = "RestaurantName";
	public static final String most_selling_by_cuisines_restaurant_id = "RestaurantId";
	public static final String most_selling_by_cuisines_restaurant_status = "RestaurentStatus";
	public static final String most_selling_by_cuisines_restaurant_thumbnail = "RestaurantLogo";
	public static final String most_selling_by_cuisines_dish_thumbnail = "Thumbnail";

	// Get New Restaurants
	public static final String new_restaurant_url = base_url
			+ "/NewReataurants";
	// Change password
	public static final String chnge_password = base_url + "/ChangePassword";;
	// Most selling Dishes By restaurants
	public static final String most_selling_dishlist_url = base_url
			+ "/MostSellingDishesByRestaurant";

	// Special Offers
	public static final String special_offers_url = base_url
			+ "/GetRestaurantsByPromotions";
	public static final String special_offers_json_obj = "PromotionItemsDetails";
	public static final String special_offers_description = "Description";
	public static final String special_offers_name = "Name";
	public static final String special_offers_price = "Price";
	public static final String special_offers_rating = "Rating";
	public static final String special_offers_thumbnail = "Thumbnail";

	// Areas URL
	public static final String areas_url = base_url + "/GetAreas";
	public static final String areas_json_obj = "AllAreaDetails";
	public static final String areas_all_areas = "ListAllAreas";
	public static final String areas_area_id = "AreaId";
	public static final String areas_area_name = "AreaName";

	public static final String get_restaurant_advance_search_url = base_url
			+ "/GetRestaurantsAdvanceSearch";
	public static final String filter_by = "FilterBy";

	// Get Restaurant Menus
	public static final String most_selling_get_restaurant_menu_url = base_url
			+ "/GetMenuSections";
	public static final String restaurant_menu_list = "MenuSectionList";
	public static final String restaurant_menu_id = "MenuSectionId";
	public static final String restaurant_menu_name = "Name";

	// Get Restaurant MenuSection
	public static final String get_restaurant_menu_sections_url = base_url
			+ "/GetMenuItemDetails";
	public static final String restaurant_menu_details_list = "MenuDetails";
	public static final String restaurant_menu_section_id = "menuSectionId";

	public static final String OPEN = "OPEN";

	// getFaq
	public static final String get_pagedetails_url = base_url
			+ "/GetPageDetails";

	// getRestaurantReview
	public static final String get_review_url = base_url
			+ "/GetRestaurantReviews";
	public static final String Review = "review";
	public static final String Username = "username";
	public static final String createOn = "CreatedOn";

	// Most selling Dishes
	public static final String most_selling_url = base_url
			+ "/MostSellingDishes";
	public static final String most_selling_json_obj = "DishList";
	public static final String most_selling_dishname = "DishName";
	public static final String most_selling_id = "Id";
	public static final String most_selling_price = "Price";
	public static final String most_selling_resturant_name = "RestaurantName";
	public static final String most_selling_thumbnail = "Thumbnail";
	public static final String most_selling_resturant_status = "RestaurentStatus";

	/* Login data File String */
	public static String FILENAMEAUTHKEY = "authentication_key.txt";

	/* Add to cart */
	public static final String ADDTO_CART_URL = base_url + "/AddToCart";

	/* Area By Restaurant */
	public static final String GET_AREA_BY_RESTAURANT_URL = base_url
			+ "/GetAreasByRestaurant";

	public static final String GET_TEMP_CART_INFO_URL = base_url
			+ "/GetTempCartInfo";
	
	/* GetCartInfo */
	// remove from cart
	public static final String REMOVE_Address = base_url + "/RemoveAddress";
	public static final String GET_CART_INFO_URL = base_url + "/GetCartInfo";
	public static final String get_cart_info_jsonobj = "CartInfoList";
	public static final String get_cart_info_sub_jsonobj = "CartInfo";

	public static final String get_cart_info_area_ID = "AreaId";
	public static final String get_cart_info_ItemChoiceIds = "ItemChoiceIds";
	public static final String get_cart_info_ItemId = "ItemId";
	public static final String get_cart_info_ItemName = "ItemName";
	public static final String get_cart_info_OrderDate = "OrderDate";
	public static final String get_cart_info_Quantity = "Quantity";
	public static final String get_cart_info_RestaurantId = "RestaurantId";
	public static final String get_cart_info_RestaurantName = "RestaurantName";
	public static final String get_cart_info_ShoppingCartId = "ShoppingCartId";
	public static final String get_cart_info_Thumbnail = "Thumbnail";
	public static final String get_cart_info_MinimumAmount = "MinimumAmount";
	public static final String get_cart_info_price = "ItemPrice";

	public static final String get_cart_info_AcceptsCC = "AcceptsCC";
	public static final String get_cart_info_AcceptsCash = "AcceptsCash";
	public static final String get_cart_info_AcceptsKNET = "AcceptsKNET";

	public static final String get_cart_info_timing_obj = "DeliveryTiming";
	public static final String get_cart_info_timing = "DelTime";

	// Remove Item
	public static final String REMOVE_CART_ITEM_URL = base_url
			+ "/RemoveCartItem";
	public static final String REMOVE_TEMP_CART_ITEM_URL = base_url
			+ "/RemoveTempCartItem";

	// Feedback
	public static final String Feedback_frm = base_url + "/FeedbackForm";
	public static final String user_comments = "Comments";
	public static final String DesignFeedback = "DesignFeedback";
	public static final String OrderProcess = "OrderProcess";
	public static final String UsabilityFeedback = "UsabilityFeedback";
	public static final String user_name = "Name";
	public static final String user_Email = "Email";

	// Get Customer address for Checkout
	public static final String getCustomerAddressURL = base_url
			+ "/GetCustomerAddresses";

	public static final String getCustomerAddressJSONObj = "CustomerDetails";
	public static final String getCustomerAddressAreaId = "AreaId";
	public static final String getCustomerAddressType = "Type";
	public static final String getCustomerAddressBlock = "Block";
	public static final String getCustomerAddressBuildingNo = "BuildingNo";
	public static final String getCustomerAddressExtraDirections = "ExtraDirections";
	public static final String getCustomerfirstName = "FirstName";
	public static final String getCustomerAddressFloor = "Floor";
	public static final String getCustomerhousePhone = "HousePhone";
	public static final String getCustomerAddressJudda = "Judda";
	public static final String getCustomerlastName = "LastName";
	public static final String getCustomermobile = "Mobile";
	public static final String getCustomerAddressStreet = "Street";
	public static final String getCustomerAddressWorkphone = "WorkPhone";
	public static final String getCustomerAddressHousePhone = "HousePhone";
	public static final String getCustomerAddressCompany = "company";

	public static final String getCustomerAddressId = "Id";
	public static final String getCustomerAddressIsPrimary = "IsPrimary";
	public static final String getCustomerAddressProfileName = "ProfileName";
	public static final String getCustomerAddressSuite = "Suite";
	public static final String getCustomeremail = "Type";
	public static final String getCustomerprofilename = "ProfileName";
	public static final String areaName = "AreaName";
	public static final String FirstName = "FirstName";
	public static final String Res_Ph = "HousePhone";
	public static final String Gender = "Gender";

	// GetAllRestaurantsPromotions
	public static final String all_restuarent_promotions = base_url
			+ "/GetAllRestaurantsPromotions";
	public static final String RestaurantDetailsWithItem = "RestaurantDetailsWithItem";
	public static final String PromotionItemsDetails = "PromotionItemsDetails";
	public static final String Description = "Description";
	public static final String RestaurantDescription = "RestaurantDescription";
	public static final String Name = "Name";
	public static final String Price = "Price";
	public static final String Rating = "Rating";
	public static final String Thumbnail = "Thumbnail";
	public static final String RestaurantId = "RestaurantId";
	public static final String RestaurantLogo = "RestaurantLogo";
	public static final String RestaurantName = "RestaurantName";
	public static final String status = "RestaurantStatus";

	// get restaurants by description
	public static final String get_restuarent_promotions = base_url
			+ "/GetRestaurantsByPromotions";

	// BuyU3ANCredit
	public static final String uancredit = base_url + "/BuyU3ANCredit";
	// BuyCreaditintervals
	public static final String uancreditintervals = base_url
			+ "/GetU3ANCreditIntervals";
	public static final String Intervals = "Interval";
	// GetgiftVoucher
	public static final String get_GiftVoucher = base_url + "/GetGiftVouchers";

	public static final String GiftVoucherImage = "GiftVoucher_Image";
	public static final String amount = "GiftVoucherAmount";
	public static final String GiftVoucherId = "GiftVoucher_Id";
	// BuyGiftVoucher
	public static final String buy_GiftVoucher = base_url + "/BuyGiftVoucher";

	// Add to favourite
	public static final String addtoFavUrl = base_url + "/AddToFavourite";
	// My favourite page
	public static final String getMyFavUrl = base_url + "/GetFavourite";
	public static final String removeFromMyFavUrl = base_url
			+ "/RemoveFavourite";
	// Delivery informaion
	public static final String deleveryinformation = base_url
			+ "/AddCustomerAddress";
	// Edit deleveryaddress
	public static final String EditAddress = base_url + "/EditAddress";
	// get customerAddress
	public static final String getdeleveryinformation = base_url
			+ "/GetCustomerAddresses";
	// MakeAs Primary
	public static final String MakeAsPrimary = base_url + "/MakeAsPrimary";
	public static final String AreaId = "AreaId";
	public static final String Areaname = "AreaName";
	public static final String Block = "Block";
	public static final String Buildingno = "BuildingNo";
	public static final String Extradirections = "ExtraDirections";
	public static final String floor = "Floor";
	public static final String id = "Id";
	public static final String isprimary = "IsPrimary";
	public static final String judda = "Judda";
	public static final String profileName = "ProfileName";
	public static final String street = "Street";
	public static final String suite = "Suite";
	public static final String type = "Type";
	public static final String LastName = "LastName";

	public static final String HousePhone = "WorkPhone";
	public static final String subscribed_to_newsletter = "subscribed_to_newsletter";
	public static final String subscribed_to_sms = "subscribed_to_sms";
	public static final String occupation = "occupation";

	public static final String addToTempCartURL = base_url + "/AddToTempCart";
	// my orders
	public static final String myOrdersUrl = base_url + "/GetOrders";
	public static final String pDate = "Date";
	public static final String DeliveryArea = "DeliveryArea";
	public static final String DeliveryBlock = "DeliveryBlock";
	public static final String Total = "Total";
	public static final String Orders = "Orders";
	public static final String Restaurant = "Restaurant";
	public static final String SpecialRequest = "SpecialRequest";
	public static final String ItemQuantity = "ItemQuantity";
	public static final String SubTotal = "SubTotal";
	public static final String postRating = base_url + "/PostItemRating";
	public static final String ItemOrRestaurantId = "ItemOrRestaurantId";
	public static final String flag = "flag";
	public static final String Gen_reqst = "GeneralRequest";
	// account information
	public static final String AccountInformation = base_url + "/EditCustomer";

	public static final String placeOrder_URL = base_url + "/PlaceOrder";

	public static final String placeQuickOrder_URL = base_url
			+ "/PlaceQuickOrder";

	public static final String cart_count_URL = base_url + "/GetCartCount";
	public static final String temp_cart_count_URL = base_url
			+ "/GetTempCartCount";

	public static String Add_Device_Token = base_url + "/AddDeviceToken";
	public static String device_Token = "pushtoken";
	public static String platform_Token = "pushplatform";
	
	public static String Remove_Device_Token = base_url + "/DeleteDeviceTocken";
	//Adding banner to
	public static final String  banner_url = base_url+"/GetApplicationBanner";
	
	public static final String  adsbanner_url = base_url+"/GetAdvertismentBanner";
			
	public static String get_Area_url = base_url+"/GetAreas";
	public static final String STR_EN = "en";
	public static final String STR_AR = "ar";
	
	
	public static final String AdvertisementBannerUrl =base_url + "/GetAdvertismentBanner";
	
}
