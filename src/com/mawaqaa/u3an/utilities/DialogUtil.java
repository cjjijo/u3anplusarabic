package com.mawaqaa.u3an.utilities;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.mawaqaa.u3an.fragments.AdFragment;

public class DialogUtil {
	private static String TAG = "FashionWorld::DailogueUtil";
	private static AdFragment adFrag;

	public static void showAdFrag(FragmentManager fm, Bitmap image,String link, String adTime) {
		Log.d(TAG, "showAdFrag()");
		if (adFrag == null) {
			adFrag = new AdFragment();
		} else if (adFrag.isVisible()) {
			return;
		}
		if (!adFrag.isVisible()) {			
			adFrag.setAdObj(image,link, adTime);
			adFrag.setCancelable(true);
			adFrag.show(fm, AdFragment.class.getName());
		}
	}

	public static void dismissAdFrag() {
		Log.d(TAG, "dismissAdFrag()");
		if (adFrag != null && adFrag.isVisible())
			adFrag.dismiss();
	}

}
