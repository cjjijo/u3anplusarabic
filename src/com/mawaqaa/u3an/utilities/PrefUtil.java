package com.mawaqaa.u3an.utilities;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.appconstants.AppConstants;

public class PrefUtil {
	private static final String PREF_USER_SIGN_IN = "pref_user_signin";
	private static final String PREF_USER_SIGN_IN_EMAIL = "email";
	private static final String PREF_USER_SIGN_IN_PASSWORD = "password";
	private static final String PREF_USER_SIGN_IN_fIRSTNAME = "firstname";
	private static final String PREF_USER_SIGN_IN_LAST_NAME = "lastname";
	private static final String PREF_USER_SIGN_IN_PHONE = "phone";
	private static final String PREF_USER_SIGN_IN_HOME_PHONE = "home_phone";
	public static final String PREF_USER_SIGN_IN_AUTH_KEY = "authKey";
	private static final String PREF_FRAG_HEAD = "page_heading";
	private static final String PREF_CREDIT_UAN = "U3ANCredit";
	private static final String PREF_POINTS_UAN = "U3ANPoints";
	public static final String MyPREFERENCES = "checkoutdata";
	public static final String firstname = "firstnameKey";
	public static final String lastname = "lastnameKey";
	public static final String Email = "emailKey";
	public static final String mobile = "mobileKey";
	public static final String blockS = "blockSKey";
	public static final String Street = "StreetKey";
	public static final String judda = "juddaKey";
	public static final String building = "buildingKey";
	public static final String floor = "floorKey";
	public static final String apartment = "apartmentKey";
	public static final String directions = "directionsKey";
	public static final String areaID = "areaID";
	public static final String areaName = "areaName";
	public static final String id = "id";
	public static final String isPrimary = "isPrimary";
	public static final String profileName = "profileName";
	public static final String suite = "suite";
	public static final String login_status = "login_status";
	public static final String loginData = "loginDatakey";
	public static final String GENDER = "Gender";
	public static final String NEWSLETTER = "subscribed_to_newsletter";
	public static final String SMS = "subscribed_to_sms";
	public static final String OCCUPATION = "occupation";
	private static final String PREF_APP_LAN = "pref_lan";
	public static final String PREF_APP_LAN_EN = "en";
	public static final String PREF_APP_LAN_AR = "ar";
	public static final String PREF_ADDs = "ads";
	public static final String total = "total";
	
	private static final String IS_ENABLED_PUSHNOTIFICATION = "is_enabled_pushnotification";
	
	public static void saveDataForConfirmOrder(){
		
	}
	public static boolean ispushNotificationEnabled(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getBoolean(IS_ENABLED_PUSHNOTIFICATION, false);
	}

	public static void setpushNotificationEnabled(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putBoolean(IS_ENABLED_PUSHNOTIFICATION, true).commit();

	}

	public static void setpushNotificationDisabled(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putBoolean(IS_ENABLED_PUSHNOTIFICATION, false).commit();

	}

	public static void saveData(final Context context, String email,
			String password, String firstName, String lastName, String phone,
			String home_phone, String auth_key) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putBoolean(PREF_USER_SIGN_IN, true).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_EMAIL, email).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_PASSWORD, password).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_fIRSTNAME, firstName).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_LAST_NAME, lastName).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_PHONE, phone).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_HOME_PHONE, home_phone).commit();
		sp.edit().putString(PREF_USER_SIGN_IN_AUTH_KEY, auth_key).commit();
	}

	public static boolean isUserSignedIn(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getBoolean(PREF_USER_SIGN_IN, false);
	}

	public static void setUserSignedIn(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putBoolean(PREF_USER_SIGN_IN, true).commit();

	}

	public static void setUserSignedOut(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putBoolean(PREF_USER_SIGN_IN, false).commit();

	}

	public static String getAuthkey(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		Log.e("authkey", "" + sp.getString(PREF_USER_SIGN_IN_AUTH_KEY, ""));
		return sp.getString(PREF_USER_SIGN_IN_AUTH_KEY, null);
	}

	public static void setAuthkey(final Context context, final String authkey) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_USER_SIGN_IN_AUTH_KEY, authkey).commit();

	}

	public static void setFragHeading(final Context context,
			final String heading) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_FRAG_HEAD, heading).commit();

	}

	public static String getFragHeading(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_FRAG_HEAD, "");

	}
	public static void setU3ancredit(final Context context,final String creadit)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_CREDIT_UAN, creadit).commit();
	}
	public static String getU3Ancredit(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_CREDIT_UAN, "");

	}
	public static void setU3anpoints(final Context context,final String points)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_POINTS_UAN, points).commit();
	}
	public static String getU3Anpoints(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_POINTS_UAN, "");

	}
	public static void setFirstname(final Context context,final String firstname)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_USER_SIGN_IN_fIRSTNAME, firstname).commit();
	}
	public static String getFirstname(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_USER_SIGN_IN_fIRSTNAME, "");

	}
	public static void setLastname(final Context context,final String lastname)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_USER_SIGN_IN_LAST_NAME, lastname).commit();
	}
	public static String getLastname(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_USER_SIGN_IN_LAST_NAME, "");

	}
	public static void setPhonenum(final Context context,final String phone)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_USER_SIGN_IN_PHONE, phone).commit();
	}
	public static String getphonenum(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_USER_SIGN_IN_PHONE, "");

	}
	public static void setredsi_Phonenum(final Context context,final String homeph)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_USER_SIGN_IN_HOME_PHONE, homeph).commit();
	}
	public static String getreside_phonenum(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(PREF_USER_SIGN_IN_HOME_PHONE, "");

	}
	
	public static void setGender(final Context context,final String gender)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(GENDER, gender).commit();
	}
	public static String getgender(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(GENDER, "");

	}
	public static void setNewsletter(final Context context,final String newsletter)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(NEWSLETTER, newsletter).commit();
	}
	public static String getnewsletter(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(NEWSLETTER, "");

	}
	public static void setsms(final Context context,final String sms)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(SMS, sms).commit();
	}
	public static String getsms(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(SMS, "");

	}
	public static void setOccupation(final Context context,final String occupation)
	{
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		sp.edit().putString(OCCUPATION, occupation).commit();
	}
	public static String getOccupation(final Context context) {
		SharedPreferences sp = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sp.getString(OCCUPATION, "");

	}
	public static String getAppLanguage(final Context context) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		return sp.getString(PREF_APP_LAN, PREF_APP_LAN_EN);
	}

	public static void setAppLanguage(final Context context, final String lan) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		sp.edit().putString(PREF_APP_LAN, lan).commit();
	}

	public static boolean isAppLanEnglish(final Context context) {
		if (getAppLanguage(context).equals(PREF_APP_LAN_EN))
			return true;
		else
			return false;
	}
	public static String getAppLanValue(BaseActivity activity) {
		if (isAppLanEnglish(activity))
			return AppConstants.STR_EN; // 1 for English
		else
			return AppConstants.STR_AR; // 2 for Arabic
	}
	
	public static void setAds(final Context context,final boolean adsval)
	 {
	  SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
	  sp.edit().putBoolean(PREF_ADDs, adsval).apply();
	  
	 }
	 public static Boolean getAds(final Context context)
	 {
	  SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
	  return sp.getBoolean(PREF_ADDs, true);
	 }
	 public static void setTotal(Context context,final String value){
		 SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		 sp.edit().putString(total, value).commit();
	 }
	 public static String getTotal(final Context context){
		 SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		 return sp.getString(total, "0.0");
	 }
}
