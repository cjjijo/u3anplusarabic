package com.mawaqaa.u3an.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.fragments.ForgotPassFragment;

public class Utilities {

	private static final String TAG = "U3AN::Utilities";
	static ForgotPassFragment fwdFrag;

	public static Boolean doemailValidation(String emailString) {
		return android.util.Patterns.EMAIL_ADDRESS.matcher(emailString)
				.matches();
	}

	public static boolean isValidPhoneNumber(String phoneNumber) {
		boolean check = false;
		if (android.util.Patterns.PHONE.matcher(phoneNumber).matches()) {

			if (phoneNumber.length() < 6 || phoneNumber.length() > 15) {
				check = false;
			} else {
				check = true;
			}
		}
		return check;
	}

	public static void hide_keyboard(Activity activity) {
		InputMethodManager inputMethodManager = (InputMethodManager) activity
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		// Find the currently focused view, so we can grab the correct window
		// token from it.
		View view = activity.getCurrentFocus();
		// If no view currently has focus, create a new one, just so we can grab
		// a window token from it
		if (view == null) {
			view = new View(activity);
		}
		inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static void show(Activity activity, String message, String title,
			OnClickListener listener, OnClickListener cancelListener) {
		if (!isValidWindow(activity)) {
			Log.d(TAG, "No window to show msg :" + message);
			return;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(message);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setPositiveButton(activity.getString(android.R.string.ok),
				listener);
		builder.setNegativeButton(activity.getString(android.R.string.cancel),
				cancelListener);
		builder.create().show();
	}

	public static void show(Activity activity, String message, String title,
			OnClickListener listener) {
		if (!isValidWindow(activity)) {
			return;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setMessage(message);
		builder.setCancelable(true);
		builder.setTitle(title);
		builder.setPositiveButton(activity.getString(android.R.string.ok),
				listener);
		builder.setCancelable(false);
		builder.create().show();
	}
	
	
// Jijo	
	
	/*public static void showAlert(Activity activity, String message, String title,
			OnClickListener listener1,OnClickListener listener2, String lan){
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
	    // Get the layout inflater
	    LayoutInflater inflater = (activity).getLayoutInflater();
	    // Inflate and set the layout for the dialog
	    // Pass null as the parent view because its going in the
	    // dialog layout
	    builder.setTitle(title);
	    builder.setCancelable(false);
	   
	    builder.setView(inflater.inflate(R.layout.alert_dialogue_title, null))
	    // Add action buttons
	            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                @Override
	                public void onClick(DialogInterface dialog, int id) {

	                    }
	                
	            });
	    builder.create();
	    builder.show();
	
}*/
	
	public static void showAlert4LanguageSwitch(Activity activity, String message, String title,
			OnClickListener listener1,OnClickListener listener2, String lan) {
		if (!isValidWindow(activity)) {
			return;
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		//builder.setMessage(message);
		builder.setCancelable(true);
		LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.alert_dialogue_title, null);
		
		builder.setView(view);	
		
			
		/*TextView alertHead = (TextView)view.findViewById(R.id.alert_dialogue_head);
		alertHead.setText(title);
		alertHead.setTextSize(20.0f);
		Log.d("AlertDialogue...", ""+alertHead.getTextSize());
		alertHead.setPadding(10, 10, 10, 10);
		alertHead.setGravity(Gravity.CENTER);*/
		//builder.setCustomTitle(alertHead);
		
					
			TextView divider = (TextView)view.findViewById(R.id.alert_dialogue_Divider);
			divider.setPadding(10, 10, 10, 10);
			
			
			TextView alertMeassage = (TextView)view.findViewById(R.id.alert_dialogue_body);
			alertMeassage.setText(message);
			alertMeassage.setTextSize(17.0f);
			alertMeassage.setPadding(10, 10, 10, 10);
			alertMeassage.setGravity(Gravity.CENTER_VERTICAL);
			
			Button alertOk = (Button)view.findViewById(R.id.buttonAlertOk);
			Button alertCancel = (Button)view.findViewById(R.id.buttonAlertCancel);
			//alertOk.setOnClickListener((android.view.View.OnClickListener) alertOkListener);	
			
			AlertDialog alertDialog = builder.create();
			alertDialog.show();
		
				
		//builder.setView(view);	
		//builder.setTitle(title);
		builder.setPositiveButton(activity.getString(android.R.string.ok),
				listener1);
		builder.setNegativeButton(activity.getString(android.R.string.cancel),
				listener2);
		//builder.setCancelable(false);
		//builder.create().show();
			
	
	}
	
	
		
	private static boolean isValidWindow(Activity activity) {
		if (activity == null || activity.isFinishing()
				|| activity.getWindow() == null
				|| !activity.getWindow().isActive())
			return false;
		return true;
	}

	public static void showLoginOptionFrag(
			FragmentManager childFragmentManager, Activity activity) {

	}

	public static void showForgotPasswordFrag(
			FragmentManager childFragmentManager) {
		Log.d(TAG, "ForgotPassFragment()");
		// if (fwdFrag == null) {
		fwdFrag = new ForgotPassFragment();
		// } else
		if (fwdFrag.isVisible()) {
			return;
		}
		if (!fwdFrag.isVisible()) {

			fwdFrag.setCancelable(true);
			fwdFrag.show(childFragmentManager,
					ForgotPassFragment.class.getName());
		}
	}

	public static String getHtmlData(Context context, String data) {
		String head = "<head><style>@font-face {font-family: 'Raleway-Regular';src: url('file://"
				+ context.getFilesDir().getAbsolutePath()
				+ "/Raleway-Regular.otf');}body {font-family: 'Raleway-Regular';}</style></head>";

		String htmlData = "<html>" + head + "<body>" + data + "</body></html>";
		return htmlData;
	}

	public static String getHtmlDatawithFont(Context context, String data) {

		String head = "<head><style type=\"text/css\">@font-face {font-family: 'MyFont';src: url(\"file:///android_asset/fonts/BREESERIF-REGULAR.OTF\")}body {font-family: 'MyFont';font-size: medium;text-align: justify;}</style></head>";

		String htmlData = "<html>" + head + "<body>" + data + "</body></html>";
		return htmlData;
	}

	public static int convertDip2Pixels(Context context, float dip) {
		Log.d(TAG, "convertDip2Pixels");
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				dip, context.getResources().getDisplayMetrics());
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
