package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.navdraweritem.NavDrawerItem;

public class NavDrawerListAdapter extends BaseAdapter{
	private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
    int test;
    
    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems, int position){
        this.context = context;
        this.navDrawerItems = navDrawerItems;
        this.test = position;
    }
 
    @Override
    public int getCount() {
        return navDrawerItems.size();
    }
 
    @Override
    public Object getItem(int position) {       
        return navDrawerItems.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
        
        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
          
        imgIcon.setImageResource(navDrawerItems.get(position).getIcon());        
        txtTitle.setText(navDrawerItems.get(position).getTitle());
        
        if (position == test) {
			txtTitle.setTextColor(Color.parseColor("#daa030"));
			imgIcon.setImageResource(setImage(position));
		} else {
			txtTitle.setTextColor(Color.parseColor("#ffffff"));
			notifyDataSetChanged(); 
		}
        
        return convertView;
    }
    private int setImage(int test2) {
		switch (test2) {
		case 0:
			
		case 1:
			return R.drawable.myaccount_sel;			
		case 2:
			return R.drawable.orderyourdishes_sel;			
		case 3:
			return R.drawable.livechat_sel;			
		case 4:
			return R.drawable.promotions_sel;		
		case 5:
			return R.drawable.u3ancredit_sel;			
		case 6:
			return R.drawable.giftvoucher_sel;
		case 7:
			return R.drawable.consultation_sel;
		case 8:
			return R.drawable.add_restuarant_sel;
		case 9:
			return R.drawable.feedback_sel;
		case 10:
			return R.drawable.terms_sel;
		case 11:
			return R.drawable.faq_sel;
		case 12:
			return R.drawable.privacy_sel;	
		case 13:
			return R.drawable.social_responsibility_sel;	
		case 14:
			return R.drawable.socialmedia_sel;	
		case 15:
			
		}		
		return 35;
	}
   
}
