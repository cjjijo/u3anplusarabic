package com.mawaqaa.u3an.adapters;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.fragments.EditAddressFrag;
import com.mawaqaa.u3an.interfaces.OnItemRemovedListener;
import com.mawaqaa.u3an.interfaces.OnMakeAsPrimary;

public class AddressListingAdapter extends BaseAdapter {
	BaseActivity context;

	ArrayList<HashMap<String, String>> arrayList;
	HashMap<String, String> map;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	int pos;
	boolean prim = false;
	String isPrimary = null;
	OnMakeAsPrimary makeprimary;
	OnItemRemovedListener removeListen;

	public AddressListingAdapter(BaseActivity context,
			ArrayList<HashMap<String, String>> arrayList,
			OnMakeAsPrimary makePrimary, OnItemRemovedListener removeListener) {
		this.context = context;
		this.arrayList = arrayList;
		this.makeprimary = makePrimary;
		this.removeListen = removeListener;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arrayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return arrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		final RecordHolder holder;
		map = arrayList.get(position);
		String areaid;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.addresslistsingleitem, null);
			context.setupUI(view, false);
			holder = new RecordHolder();
			holder.area = (TextView) view.findViewById(R.id.area);
			holder.streat = (TextView) view.findViewById(R.id.streat);
			holder.arrow = (ImageView) view.findViewById(R.id.arrow);
			holder.delete = (ImageView) view.findViewById(R.id.delete);
			holder.primary = (ImageView) view.findViewById(R.id.primary);
			holder.primarytext = (TextView) view.findViewById(R.id.asprimary);
			view.setTag(holder);
		} else {
			holder = (RecordHolder) view.getTag();
		}
		String Area = arrayList.get(position).get(AppConstants.areaName);
		String Streat = arrayList.get(position).get(AppConstants.street);
		areaid = arrayList.get(position).get(AppConstants.AreaId);
		final String prf = arrayList.get(position)
				.get(AppConstants.profileName);
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			// fosdummyy = Activity.openFileOutput(FILENAMEAUTHKEY,
			// Context.MODE_PRIVATE);
			fisdummy = context.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());

		} catch (Exception e) {
		}

		Log.e("profile name..........", "" + prf);
		holder.streat.setText(" " + Streat);
		/* holder.streat.setSelected(true); */
		holder.arrow.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Fragment deliveryFra = new EditAddressFrag();
				String Area = arrayList.get(position).get(AppConstants.Block);
				Log.e("Bloack", "" + Area);
				String Streat = arrayList.get(position)
						.get(AppConstants.street);
				Log.e("Streat", "" + Streat);

				Bundle bundle = new Bundle();
				bundle.putString(AppConstants.AreaId, arrayList.get(position)
						.get(AppConstants.AreaId));
				bundle.putString(AppConstants.areaName, arrayList.get(position)
						.get(AppConstants.areaName));
				bundle.putString(AppConstants.Block, arrayList.get(position)
						.get(AppConstants.Block));
				bundle.putString(AppConstants.Buildingno,
						arrayList.get(position).get(AppConstants.Buildingno));
				bundle.putString(
						AppConstants.Extradirections,
						arrayList.get(position).get(
								AppConstants.Extradirections));
				bundle.putString(AppConstants.street, arrayList.get(position)
						.get(AppConstants.street));
				bundle.putString(AppConstants.id,
						arrayList.get(position).get(AppConstants.id));
				bundle.putString(AppConstants.isprimary, arrayList
						.get(position).get(AppConstants.isprimary));
				bundle.putString(AppConstants.judda, arrayList.get(position)
						.get(AppConstants.judda));
				bundle.putString(AppConstants.profileName,
						arrayList.get(position).get(AppConstants.profileName));
				bundle.putString(AppConstants.type, arrayList.get(position)
						.get(AppConstants.type));
				bundle.putString(AppConstants.floor, arrayList.get(position)
						.get(AppConstants.floor));
				bundle.putString(AppConstants.suite, arrayList.get(position)
						.get(AppConstants.suite));
				deliveryFra.setArguments(bundle);
				context.pushFragments(deliveryFra, false, true);
			}

		});

		holder.delete.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// new doAddToCart().execute();
				Log.e("Position", "" + position);
				if (Boolean.parseBoolean(arrayList.get(position).get(
						AppConstants.isprimary))) {
				} else {
					removeListen.ItemCheckComple(arrayList.get(position).get(
							AppConstants.id));
					Log.e("removeritem", "delete  "
							+ arrayList.get(position).get(AppConstants.id));
				}
			}
		});

		holder.primary.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				makeprimary.makeAsPrimaryComplete(arrayList.get(position).get(
						AppConstants.id));
			}
		});

		Log.e("isPrimary", "" + prim);
		if (Boolean.parseBoolean(arrayList.get(position).get(
				AppConstants.isprimary))) {
			holder.primary.setVisibility(View.INVISIBLE);
			holder.primarytext.setVisibility(View.VISIBLE);
			holder.delete.setVisibility(View.INVISIBLE);
		} else {
			holder.primarytext.setVisibility(View.INVISIBLE);
			holder.primary.setVisibility(View.VISIBLE);
			holder.delete.setVisibility(View.VISIBLE);
		}
		return view;

	}

	public class RecordHolder {
		TextView area, streat;
		ImageView arrow;
		ImageView delete;
		ImageView primary;
		TextView primarytext;

	}

}
