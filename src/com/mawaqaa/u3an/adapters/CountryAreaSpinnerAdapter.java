package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.data.SingleAreaCountry;

public class CountryAreaSpinnerAdapter extends BaseAdapter {
	ArrayList<SingleAreaCountry> countries;
	private LayoutInflater inflater;
	BaseActivity context;

	public CountryAreaSpinnerAdapter(BaseActivity _con,
			ArrayList<SingleAreaCountry> countries) {
		context = _con;
		this.countries = countries;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return countries.size();
	}

	@Override
	public SingleAreaCountry getItem(int position) {

		return countries.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_layout, null);
			context.setupUI(v, false);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.item_category);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		SingleAreaCountry singleAreaCountry = countries.get(position);
		if (singleAreaCountry.isCityName) {
			holder.name.setTextColor(context.getResources().getColor(
					R.color.red_font_color));
			holder.name.setText(singleAreaCountry.areaName);
		} else if (countries.get(position).areaName != null) {
			holder.name.setText(singleAreaCountry.areaName);
			holder.name.setTextColor(Color.BLACK);
		}

		return v;
	}


	public class ViewHolder {
		public TextView name;

	}
}