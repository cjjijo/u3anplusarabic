package com.mawaqaa.u3an.adapters;

import java.util.List;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;

public class FilterSearchAdapter extends BaseAdapter {
	List<String> arrFilter;
	private LayoutInflater inflater;
	BaseActivity context;
	
	public FilterSearchAdapter(BaseActivity _con, List<String> arrFilter) {
		context = _con;
		this.arrFilter = arrFilter;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return arrFilter.size();
	}

	@Override
	public Object getItem(int position) {
		return arrFilter.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_layout, null);
			context.setupUI(v, false);
			holder = new ViewHolder();
			holder.nameFilter = (TextView) v.findViewById(R.id.item_category);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}		
		holder.nameFilter.setText(arrFilter.get(position));
		holder.nameFilter.setTextColor(Color.BLACK);
		return v;
	}
	
		public class ViewHolder {
			public TextView nameFilter;	
		}
	}