package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.squareup.picasso.Picasso;

public class GiftVoucherAdapter extends BaseAdapter{
	BaseActivity context;
	ArrayList<HashMap<String, String>>arraylist;
	HashMap<String, String>map=new HashMap<String, String>();
	public GiftVoucherAdapter(BaseActivity context,ArrayList<HashMap<String, String>>arraylist)
	{
		this.context=context;
		this.arraylist=arraylist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rootview=convertView;
		RecordHolder holder;
		if (rootview==null) {
			LayoutInflater inflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rootview=inflater.inflate(R.layout.giftvouchersingleitem, null);
			context.setupUI(rootview, false);
			holder=new RecordHolder();
			holder.gift=(ImageView) rootview.findViewById(R.id.imageView_giftpic);
			rootview.setTag(holder);
		}
		else {
			holder=(RecordHolder) rootview.getTag();
		}
			map=arraylist.get(position);
			String image=map.get(AppConstants.GiftVoucherImage);
			Log.e("image", ""+image);
			if (image.startsWith("http")) {
				Picasso.with(context).load(removeWhiteSpace(image)).error(R.drawable.default_food)
			     .into(holder.gift);
			} else {
				holder.gift
				     .setBackgroundResource(R.drawable.default_food);
			}
		
		return rootview;
	}
	private String removeWhiteSpace(String string) {
		String dummyString  =  string.replaceAll(" ", "%20");
		return dummyString;
		
	}
	public class RecordHolder
	{
		ImageView gift;
	}

}
