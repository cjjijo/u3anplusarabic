package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.OnRatingbarChanged;

public class MyOrdersthirdPageAdapter extends BaseAdapter {
	private ArrayList<HashMap<String, String>> arraylist;
	BaseActivity act;
	private LayoutInflater inflater;
	JSONObject objectJson;
	OnRatingbarChanged rateBarChecked;;

	public MyOrdersthirdPageAdapter(BaseActivity activity, ArrayList<HashMap<String, String>> arraylist,
			JSONObject childObj, OnRatingbarChanged rateBarChecked) {
		this.act = activity;
		this.arraylist = arraylist;
		this.objectJson = childObj;
		inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Log.e("count ", "" + arraylist.size());
		this.rateBarChecked = rateBarChecked;

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder vh;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.myorderthird_listitem, parent, false);
			act.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.name = (TextView) convertView.findViewById(R.id.name);
			vh.spcl = (TextView) convertView.findViewById(R.id.spcl);
			vh.qt = (TextView) convertView.findViewById(R.id.qtty);
			vh.price = (TextView) convertView.findViewById(R.id.price);
			vh.tprice = (TextView) convertView.findViewById(R.id.tprice);
			vh.content = (LinearLayout) convertView.findViewById(R.id.totalcontent);
			vh.total = (TextView) convertView.findViewById(R.id.total);
			vh.subtotal = (TextView) convertView.findViewById(R.id.sub_total);
			vh.deliverycharg = (TextView) convertView.findViewById(R.id.delivery_charge);
			vh.divider = convertView.findViewById(R.id.divider);
			vh.ratebar = (RatingBar) convertView.findViewById(R.id.ratingBar1);
			vh.gnRqst=(TextView) convertView.findViewById(R.id.gen_rqst);
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		vh.name.setText(arraylist.get(position).get(AppConstants.get_cart_info_ItemName));
		vh.spcl.setText(arraylist.get(position).get(AppConstants.SpecialRequest));
		vh.qt.setText(arraylist.get(position).get(AppConstants.ItemQuantity));
		vh.price.setText(arraylist.get(position).get(AppConstants.ORDERED_ITEM_PRICE));
		String price = arraylist.get(position).get(AppConstants.ItemQuantity);
		String quantity = arraylist.get(position).get(AppConstants.ORDERED_ITEM_PRICE);
		
		double tot = Double.parseDouble(price) * Double.parseDouble(quantity);
		vh.tprice.setText("KD " + tot);
		vh.ratebar.setOnRatingBarChangeListener(null);
		vh.ratebar.setRating(Integer.parseInt(arraylist.get(position).get(AppConstants.Rating)));
		vh.ratebar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
				
					rateBarChecked.ItemRatingComple("Item", arraylist.get(position).get(AppConstants.get_cart_info_ItemId),String.valueOf(rating));
				
				

			}
		});
		try {
			vh.deliverycharg.setText("KD " + objectJson.getString(AppConstants.delivery_charge_json_obj));
			vh.subtotal.setText("KD " + objectJson.getString(AppConstants.SubTotal));
			vh.total.setText("KD " + objectJson.getString(AppConstants.Total));
			vh.gnRqst.setText(objectJson.getString(AppConstants.Gen_reqst));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.e("position", "" + position);
		if (position != arraylist.size() - 1) {
			vh.content.setVisibility(View.GONE);
			vh.divider.setVisibility(View.GONE);
		} else {
			vh.content.setVisibility(View.VISIBLE);
			vh.divider.setVisibility(View.VISIBLE);
		}
		return convertView;
	}

	private class ViewHolder {

		TextView name;
		TextView spcl;
		TextView qt;
		TextView price;
		TextView tprice;
		TextView subtotal;
		TextView deliverycharg;
		TextView total;
		LinearLayout content;
		View divider;
		RatingBar ratebar;
		TextView gnRqst;

	}
}
