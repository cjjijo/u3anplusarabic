package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.squareup.picasso.Picasso;

public class MostSellingCuisinesAdapter extends BaseAdapter {
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	/* ImageLoaderOne imageLoader; */
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();
	Fragment fr;

	BaseActivity context;

	Boolean tag = false;

	/* list.add(textview.getText().toString()); */
	public MostSellingCuisinesAdapter(BaseActivity context1,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context1;
		data = arraylist;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables...
		ViewHolder vh;
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(
					R.layout.most_selling_cuisine_details, parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.flag = (ImageView) convertView
					.findViewById(R.id.imageView_cuisinepivhpic);
			vh.namTextView = (TextView) convertView
					.findViewById(R.id.dish_name_cuisine);

			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		// Get the position
		resultp = data.get(position);

		/*
		 * String imagString = resultp
		 * .get(AppConstants.all_restaurants_resturant_logo);
		 */
// jijo 5-1-16
		String imagString = "data";

		String dishName = resultp.get(AppConstants.get_cusines_country_name);

		if (imagString.startsWith("http")) {
			Picasso.with(context).load(removeWhiteSpace(imagString))
					.error(R.drawable.default_food).into(vh.flag);
		} else {
			vh.flag.setBackgroundResource(R.drawable.default_food);
		}

		vh.namTextView.setText(dishName);
		vh.namTextView.setSelected(true);

		return convertView;
	}

	private static class ViewHolder {

		ImageView flag;
		TextView namTextView;
	}

	private String removeWhiteSpace(String string) {

		String dummyString = string.replaceAll(" ", "%20");
		return dummyString;

	}

}
