package com.mawaqaa.u3an.adapters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.MenuItemDetail;
import com.mawaqaa.u3an.data.MenuItemDetail.MenuItemChoiceDetails;
import com.mawaqaa.u3an.data.SelectedChoiceItems;
import com.mawaqaa.u3an.db.OpenHelper;
import com.mawaqaa.u3an.fragments.LoginOptionFragment;
import com.mawaqaa.u3an.fragments.RestaurantMenuInnerSectionFragment;
import com.mawaqaa.u3an.fragments.ResturentMenuItemDialogueFragment;
import com.mawaqaa.u3an.interfaces.OnItemCheckedCompleted;
import com.mawaqaa.u3an.interfaces.UpdateCartCount;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.squareup.picasso.Picasso;

public class RestaurantMenuItemAdapter extends BaseAdapter {

	private static final String TAG = "RestaurantMenuItemAdapter";

	private LayoutInflater inflater;
	private ArrayList<MenuItemDetail> menuItemDetails;
	private BaseActivity baseActivity;
	private String FILENAMEAUTHKEY = "authentication_key.txt";
	private String header;
	OnItemCheckedCompleted listenerComment;
	ResturentMenuItemDialogueFragment fwdFrag;
	private String area_ID, restaurant_ID, ItemChoiceIds, ItemChoiceQty,
			ItemId, Quantity, UserName;
	String userName;
	boolean shouldSelectChoice;
	Context mContex;
	

	String restaurantName, restaurantLogo;
	String deviceID;
	OpenHelper db_Obj;
	String minimum_Amount;
	String ratinString, sectID, sectName;
	UpdateCartCount updateCountOnFragment;
	int idForChoiceSelection;
	
	Fragment fragment;
	
	NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
	DecimalFormat df = (DecimalFormat)nf;

	/* list.add(textview.getText().toString()); */
	public RestaurantMenuItemAdapter(BaseActivity baseActivity,
			ArrayList<MenuItemDetail> menuItemDetails, String header,
			String restaurant_ID, String area_ID, String restaurant_Name,
			String restaurant_Logo, String mimimum_amount, String rating,
			String sectionID, String sectionName, UpdateCartCount update) {
		this.baseActivity = baseActivity;
		this.menuItemDetails = menuItemDetails;
		this.header = header;
		this.area_ID = area_ID;
		this.restaurant_ID = restaurant_ID;
		this.restaurantName = restaurant_Name;
		this.restaurantLogo = restaurant_Logo;
		this.minimum_Amount = mimimum_amount;
		this.ratinString = rating;
		this.sectID = sectionID;
		this.sectName = sectionName;
		this.updateCountOnFragment = update;
		//this.mContex = context;
		inflater = (LayoutInflater) baseActivity
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return menuItemDetails.size();
	}

	@Override
	public MenuItemDetail getItem(int position) {
		return menuItemDetails.get(position);
	}

	@Override
	public long getItemId(int position) {
		long itemId = 0;
		try {
			itemId = Long.parseLong(menuItemDetails.get(position).ItemId);
		} catch (Exception exception) {

		}
		return itemId;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables...

		final ViewHolder vh;
		final MenuItemDetail menuItemDetailss = menuItemDetails.get(position);
		if (convertView == null) {
			convertView = inflater.inflate(
					R.layout.restuarantmenu_details_singleitem, parent, false);
			baseActivity.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.m_dishImg = (ImageView) convertView
					.findViewById(R.id.imageView_dishpic);
			vh.m_dishName = (TextView) convertView.findViewById(R.id.dish_name);
			vh.m_dishName.setSelected(true);
			vh.m_dishDesc = (TextView) convertView.findViewById(R.id.dish_desc);
			vh.arrow = (Button) convertView.findViewById(R.id.arrow);
			vh.m_dishRating = (RatingBar) convertView
					.findViewById(R.id.resturant_rating);
			vh.m_dishPrice = (TextView) convertView
					.findViewById(R.id.dish_price);
			vh.m_dishPrice.setSelected(true);
//*************************************************************************************************************	
			vh.m_dishCountRemove = (Button) convertView.findViewById(R.id.dish_count_remove);
			vh.m_DishOrderingCount = (EditText) convertView
					.findViewById(R.id.dish_count);
			vh.m_dishCountAdd = (Button) convertView.findViewById(R.id.dish_count_add);
			vh.m_OrderBtn = (Button) convertView
					.findViewById(R.id.order_dish_btn);
			vh.addtofav = (TextView) convertView.findViewById(R.id.addto_fav);
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}
//:::::::::::::::::::::::::::::::::*****************&&&&&&&&&&&&&&********************:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		vh.m_dishCountRemove.setFocusable(false);
		vh.m_dishCountAdd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//txtQty.setText(""+Integer.parseInt(txtQty.getText())+1)
				vh.m_dishCountRemove.setFocusable(true);
				int count = Integer.parseInt(vh.m_DishOrderingCount.getText().toString());
				if(count<99){
					vh.m_DishOrderingCount.setText(""+(Integer.parseInt(vh.m_DishOrderingCount.getText().toString())+1));
				}else{
					vh.m_dishCountAdd.setFocusable(false);
					vh.m_DishOrderingCount.setText(""+99);
				}
				
			}
		});
		vh.m_dishCountRemove.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//txtQty.setText(""+Integer.parseInt(txtQty.getText())+1)
				vh.m_dishCountAdd.setFocusable(true);
				int count = Integer.parseInt(vh.m_DishOrderingCount.getText().toString());
				if(count>1){
					vh.m_DishOrderingCount.setText(""+(Integer.parseInt(vh.m_DishOrderingCount.getText().toString())-1));
				}else{
					vh.m_dishCountRemove.setFocusable(false);
					vh.m_DishOrderingCount.setText(""+1);
				}
				
			}
		});
		
//::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::		
		final MenuItemDetail menuItemDetail = menuItemDetails.get(position);
		ArrayList<MenuItemChoiceDetails> menuItemChoiceDetails = new ArrayList<MenuItemChoiceDetails>();
		/* menuItemDetail.MenuItemChoiceDetails.size(); */
		Log.e("menuItemChoiceDetails........", ""
				+ menuItemDetail.MenuItemChoiceDetails);

		if (menuItemDetail.MenuItemChoiceDetails != null
				&& menuItemDetail.MenuItemChoiceDetails.size() > 0) {
			Log.e("Menu items ",
					"" + menuItemDetail.MenuItemChoiceDetails.size());
			vh.arrow.setVisibility(View.VISIBLE);
			shouldSelectChoice = checkMinitems(menuItemDetail.MenuItemChoiceDetails);

		} else {
			vh.arrow.setVisibility(View.INVISIBLE);
			shouldSelectChoice = false;

		}
		
		if (shouldSelectChoice) {
			Log.e("Entered Here", "msg..........................................................................................");
			vh.m_OrderBtn.setId(1);
		}else {
			Log.e("Entered Here", "msggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg");
			vh.m_OrderBtn.setId(0);
		}
		
		Log.e("should select", "" + shouldSelectChoice);
		vh.arrow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shouldSelectChoice = false;
				fwdFrag = new ResturentMenuItemDialogueFragment(baseActivity,
						listenerComment);
				if (fwdFrag.isVisible()) {
					return;
				}
				if (!fwdFrag.isVisible()) {
					fwdFrag.setAdObj(menuItemDetail.MenuItemChoiceDetails);
					fwdFrag.show(baseActivity.getFragmentManager(),
							"myDialogFragment");
				}
			}
		});
		if (URLUtil.isValidUrl(menuItemDetail.itemImgUrl)
				&& menuItemDetail.itemImgUrl.length() > 5) {
			Picasso.with(baseActivity)
					.load(removeData(menuItemDetail.itemImgUrl))
					.error(R.drawable.default_food).into(vh.m_dishImg);
			
		} else {
			vh.m_dishImg.setBackgroundResource(R.drawable.default_food);
		}
		if (PrefUtil.isUserSignedIn(baseActivity)) {
			vh.addtofav.setVisibility(View.VISIBLE);
		} else {
			vh.addtofav.setVisibility(View.GONE);
		}
		vh.m_dishName.setText(menuItemDetail.name);
		vh.m_dishDesc.setText(menuItemDetail.description);
		double price = 0.000;
		
		
		try {
			price = Double.valueOf(menuItemDetail.price);
		} catch (Exception exception) {
			Log.e(TAG, "Exception in range minimum amount parsing : "
					+ exception.getMessage());
		}
		//String strPrice = baseActivity.getString(R.string.to_kd, df.format(price));
		df.applyPattern("###,##0.000");
		vh.m_dishPrice.setText(baseActivity.getResources().getString(R.string.to_kd)+" "+df.format(price));

		float rating = 0;
		try {
			rating = Float.valueOf(menuItemDetail.rating);
		} catch (Exception exception) {
			Log.e(TAG,
					"Exception in rating converting : "
							+ exception.getMessage());
		}
		ItemChoiceIds = "";
		vh.m_dishRating.setRating(rating);
		listenerComment = new OnItemCheckedCompleted() {
			@Override
			public void ItemCheckComple(ArrayList<SelectedChoiceItems> items,
					boolean b) {
				shouldSelectChoice = false;
				ArrayList<String> newarray = new ArrayList<String>();
				for (int i = 0; i < items.size(); i++) {
					for (int j = 0; j < items.get(i).addredItems.size(); j++) {
						if (!items.get(i).addredItems.get(j).equals("null"))
							newarray.add(items.get(i).addredItems.get(j));
					}
				}
				Log.e("last array size", "" + newarray.size());
				Log.e("last array elements", "" + newarray.toString());
				if (newarray.size() > 0) {
					ItemChoiceIds = newarray.toString().replaceAll("\\[", "")
							.replaceAll("\\]", "").trim();
				}
			}
		};

		vh.m_OrderBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				
				idForChoiceSelection =  vh.m_OrderBtn.getId();
				
				Log.e("Get id............######............", vh.m_OrderBtn.getId()+" ");
				// Check Login status.....
				userName = baseActivity.readFile();
				
//***************************************************************************************************************		
				
				String qua = vh.m_DishOrderingCount.getText().toString();
				Log.e("quantity.......##########.........", "zxx" + qua);
				try{
				int quan = Integer.parseInt(vh.m_DishOrderingCount.getText().toString());
				if (!(quan == 0)
						&& !vh.m_DishOrderingCount.getText().toString()
								.equals("")) {
					Log.d("inside first if.......##########.........", "zxx" + quan);
					Log.w("inside first if.......##########.........", "zxx" + quan);
					Log.i("inside first if.......##########.........", "zxx" + quan);
					if (userName == null || userName.equals("")) {
						String quantity = vh.m_DishOrderingCount.getText()
								.toString();
						String iteemid = menuItemDetailss.ItemId;
						String dishname = menuItemDetailss.name;
						String price = menuItemDetailss.price;
						Log.e("item_id...........", ""+iteemid);
						Log.e("price.............", ""+price);
						Log.e("quantity..........", ""+quantity);
					
						db_Obj = new OpenHelper(baseActivity);
						db_Obj.InsertItemDetailsToCart(restaurant_ID,
								restaurantName, restaurantLogo, dishname,
								"dish_Image", price, "ItemChoiceIds.trim()",
								quantity, iteemid, quantity, minimum_Amount,
								price);
						
						deviceID = Secure.getString(
								baseActivity.getContentResolver(),
								Secure.ANDROID_ID);
						baseActivity.popFragments();
						android.app.Fragment fragment = new LoginOptionFragment(
								BaseActivity.getBaseActivity(),area_ID,ItemChoiceIds.trim(),quantity,iteemid,restaurant_ID,deviceID,restaurantName,restaurantLogo,minimum_Amount,sectID,sectName,ratinString,idForChoiceSelection);
						((LoginOptionFragment) fragment).show(
								baseActivity.getFragmentManager(), "Settings");
					
					} else if (userName.equals("Guest")) {
						Log.e("order submit", "order submitt");
						deviceID = Secure.getString(
								baseActivity.getContentResolver(),
								Secure.ANDROID_ID);

						ItemId = menuItemDetailss.ItemId;
						Quantity = vh.m_DishOrderingCount.getText().toString();
						Log.e("The data...else if......", "restaurant_ID"
								+ restaurant_ID + "area_ID" + area_ID
								+ "menuItemDetailss.Item" + "" + "Id"
								+ menuItemDetailss.ItemId
								+ "vh.m_DishOrderingCount.getText().toString()"
								+ vh.m_DishOrderingCount.getText().toString());
						baseActivity.WriteDataToFile("Guest");
						if (menuItemDetail.MenuItemChoiceDetails != null
								&& menuItemDetail.MenuItemChoiceDetails.size() > 0) {							
							if (shouldSelectChoice) {
								Log.d("itemmmmmmmmmmmmmmmmmmm", ItemChoiceIds.toString());
								Toast.makeText(baseActivity,baseActivity.getResources().getString(R.string.select_choice),
										Toast.LENGTH_SHORT).show();		
								shouldSelectChoice = false;		
							} else {
								new doAddToTempCart().execute();								
							}
						} else {
							new doAddToTempCart().execute();
						}
					} else {
						ItemId = menuItemDetailss.ItemId;
						Quantity = vh.m_DishOrderingCount.getText().toString();
						Log.e("The data.....else....", "restaurant_ID"
								+ restaurant_ID + "area_ID" + area_ID
								+ "menuItemDetailss.Item" + "" + "Id"
								+ menuItemDetailss.ItemId
								+ "vh.m_DishOrderingCount.getText().toString()"
								+ vh.m_DishOrderingCount.getText().toString());
						if (menuItemDetail.MenuItemChoiceDetails != null
								&& menuItemDetail.MenuItemChoiceDetails.size() > 0) {
							if (shouldSelectChoice) {
								Toast.makeText(baseActivity,
										baseActivity.getResources().getString(R.string.select_choice),
										Toast.LENGTH_SHORT).show();	
								shouldSelectChoice = false;
							} else {
								Log.d("add to cart", "11111111");	
								new doAddToCart().execute();	
							}
						} else {
							Log.d("itemmmmmmmmmmmmmmmmmmm", ""+ItemChoiceIds.toString());
							new doAddToCart().execute();
						}
					}
				} else {
					Toast.makeText(baseActivity, baseActivity.getResources().getString(R.string.order),
							Toast.LENGTH_SHORT).show();	
				}	
				}catch(Exception e){
					Toast.makeText(baseActivity, baseActivity.getResources().getString(R.string.order), Toast.LENGTH_SHORT).show();
				}
			}
		});
			
		vh.addtofav.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				userName = baseActivity.readFile();
				baseActivity.startSpinwheel(false, true);
				try {
					RequestQueue queue = VolleyUtils.getRequestQueue();
					JSONObject jobj = new JSONObject();
					jobj.accumulate("ItemId", menuItemDetailss.ItemId);
					jobj.accumulate("UserId", userName);
					JsonObjectRequest myReq = new JsonObjectRequest(
							Method.POST, AppConstants.addtoFavUrl, jobj,
							addTofavSuccessListener(), addTofavErrorListener());
					queue.add(myReq);
					Log.e(TAG, "ReqSuccessListener" + jobj.toString());
				} catch (Exception e) {				
					e.printStackTrace();
				}
			}
		});
		return convertView;
	}             

	private boolean checkMinitems(
			ArrayList<MenuItemChoiceDetails> menuItemChoiceDetails) {
		for (int i = 0; i < menuItemChoiceDetails.size(); i++) {
			if (Integer.parseInt(menuItemChoiceDetails.get(i).minItem) > 0) {
				return true;
			}
		}
		return false;
	}

	private class ViewHolder {
		ImageView m_dishImg;
		TextView m_dishName;
		TextView m_dishDesc;
		RatingBar m_dishRating;
		TextView m_dishPrice;
		//TextView m_dishCountRemove;
		Button m_dishCountRemove;
		EditText m_DishOrderingCount;
		//TextView m_dishCountAdd;
		Button m_dishCountAdd;
		Button m_OrderBtn;
		Button arrow;
		TextView addtofav;;
	}
 
	// AsynchTask for posting the value to the server.....
	public class doAddToCart extends AsyncTask<Void, String, Void> {
		String result;
		JSONObject jObj = null;
		InputStream istream;

		
		public doAddToCart() {
		}
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			baseActivity.startSpinwheel(false, true);
			//Toast.makeText(baseActivity, "add to cart --pre", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				JSONObject objJson = new JSONObject();
				objJson.accumulate("AreaId", area_ID);
				objJson.accumulate("ItemChoiceIds", ItemChoiceIds.trim());
				objJson.accumulate("ItemChoiceQty", Quantity);
				objJson.accumulate("ItemId", ItemId);
				objJson.accumulate("Quantity", Quantity);
				objJson.accumulate("RestaurantId", restaurant_ID);
				objJson.accumulate("Username", userName);
				
				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost httppost = new HttpPost(AppConstants.ADDTO_CART_URL);
				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				Log.e("Login Request", ""+objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();
					jObj = new JSONObject(result);
					Log.i("Login Result", ""+result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.e("The response is", ""+this.result);
			//Toast.makeText(baseActivity, "add to cart --post", Toast.LENGTH_SHORT).show();
			baseActivity.stopSpinWheel();
			if (this.result.contains("Success")) {		
				Toast.makeText(baseActivity, R.string.successfully_submit,
						Toast.LENGTH_SHORT).show();		
				updateCountOnFragment.updateCount();
			} else {
				Toast.makeText(baseActivity,
						baseActivity.getResources().getString(R.string.cannot_order),
						Toast.LENGTH_SHORT).show();
			}

		}
	}

	// Update the cart count AsynchTask for posting the value to the server.....
	public class doUpdateCartCount extends AsyncTask<Void, String, Void> {
		String result;
		JSONObject jObj = null;
		InputStream istream;

		public doUpdateCartCount() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			baseActivity.startSpinwheel(false, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			fragment = RestaurantMenuInnerSectionFragment.newInstance();
			Locale current = fragment.getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();

			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				objJson.accumulate("userId", userName);
				Log.e("doUpdateCartCount", ""+objJson.toString());

				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);

				HttpClient httpClient = new DefaultHttpClient(params1);
				
				HttpPost httppost;
				if (Utilities.doemailValidation(userName)) {
					httppost = new HttpPost(AppConstants.cart_count_URL);
				} else {
					httppost = new HttpPost(AppConstants.temp_cart_count_URL);
				}

				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				Log.e("Login Request", ""+objJson.toString());

				httppost.setEntity(entity);

				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);
					Log.i("Login Result", ""+result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.e("The response is", ""+this.result);
			baseActivity.stopSpinWheel();

			if (this.result.contains("Success")) {

			} else {
				Toast.makeText(baseActivity,
						baseActivity.getResources().getString(R.string.cannot_order),
						Toast.LENGTH_SHORT).show();
			}

		}
	}

	private ErrorListener addTofavErrorListener() {
		return new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.e(TAG, "ReqErrorListener" + error.toString());
				baseActivity.stopSpinWheel();
				// @TODO JPJ handle the error
			}
		};
	}

	private Listener<JSONObject> addTofavSuccessListener() {
		return new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.e(TAG, "ReqSuccessListener" + response.toString());
				baseActivity.stopSpinWheel();
				Toast.makeText(baseActivity, R.string.addtofavsucess,
						Toast.LENGTH_SHORT).show();
			}
		};
	}

	// Add to temp: cart(Order as guest)
	// AsynchTask for posting the value to the server.....
	public class doAddToTempCart extends AsyncTask<Void, String, Void> {
		String result;
		JSONObject jObj = null;
		InputStream istream;

		public doAddToTempCart() {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			baseActivity.startSpinwheel(false, true);
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				JSONObject objJson = new JSONObject();
				objJson.accumulate("AreaId", area_ID);
				objJson.accumulate("ItemChoiceIds", ItemChoiceIds.trim());
				objJson.accumulate("ItemChoiceQty", Quantity);
				objJson.accumulate("ItemId", ItemId);
				objJson.accumulate("Quantity", Quantity);
				objJson.accumulate("RestaurantId", restaurant_ID);
				objJson.accumulate("Username", deviceID);

				HttpParams params1 = new BasicHttpParams();
				HttpProtocolParams.setVersion(params1, HttpVersion.HTTP_1_1);
				HttpProtocolParams.setContentCharset(params1, "UTF-8");
				params1.setBooleanParameter("http.protocol.expect-continue",
						false);
				HttpClient httpClient = new DefaultHttpClient(params1);
				HttpPost httppost = new HttpPost(AppConstants.addToTempCartURL);
				StringEntity entity = new StringEntity(objJson.toString(),
						HTTP.UTF_8);
				Log.i("Login Request", ""+objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();
				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;
					istream.close();
					jObj = new JSONObject(result);
					Log.i("Login Result", ""+result);
				} else {
					result = null;
					Log.i("Input Stream", "Null");
				}

			} catch (JSONException e) {
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			Log.e("The response is", ""+this.result);
			baseActivity.stopSpinWheel();
			if (this.result.contains("Success")) {
				
				// kdjks
				/*
				 * Fragment cartFragment = new U3AnFoodCartFragment(); Bundle
				 * data = new Bundle(); data.putString("UserName", deviceID);
				 * data.putString("RestaurantID", restaurant_ID);
				 * data.putString("Area_ID", area_ID);
				 * data.putString("restaurant_id", restaurant_ID);
				 * data.putString("restaurant_name", restaurantName);
				 * data.putString("restaurant_rating", ratinString);
				 * data.putString("Header", header);
				 * data.putString("restaurant_logo", restaurantLogo);
				 * data.putString("menuItemId", "" + sectID);
				 * data.putString("menuItemName", sectName);
				 * data.putString("minimum_Amount", minimum_Amount);
				 * cartFragment.setArguments(data);
				 * baseActivity.pushFragments(cartFragment, false, true);
				 */
				
				Toast.makeText(baseActivity, R.string.successfully_submit,
						Toast.LENGTH_SHORT).show();
				
				// baseActivity.popFragments();
				// Fragment fragment = new RestaurantMenuInnerSectionFragment();
				// Bundle bundle = new Bundle();
				// bundle.putString("Area_ID", area_ID);
				// bundle.putString("restaurant_id", restaurant_ID);
				// bundle.putString("restaurant_name", restaurantName);
				// bundle.putString("restaurant_rating", ratinString);
				// bundle.putString("Header", header);
				// bundle.putString("restaurant_logo", restaurantLogo);
				// bundle.putString("menuItemId", "" + sectID);
				// bundle.putString("menuItemName", sectName);
				// bundle.putString("minimum_Amount", minimum_Amount);
				// fragment.setArguments(bundle);
				// baseActivity.pushFragments(fragment, false, true);
				
				updateCountOnFragment.updateCount();
			} else {
				Toast.makeText(baseActivity,
						baseActivity.getResources().getString(R.string.cannot_order),
						Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	private String removeData(String temp){
		temp = temp.replaceAll(" ", "%20");
		return temp;
	}
}
