package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.squareup.picasso.Picasso;

public class PromotionInnerAdapter extends BaseAdapter {
	BaseActivity context;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	String imagestring, name, price;

	public PromotionInnerAdapter(BaseActivity context,
			ArrayList<HashMap<String, String>> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View rootview = convertView;
		RecordHolder holder;
		if (rootview == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rootview = inflater.inflate(R.layout.promotionsinnersingleitem,
					null);
			context.setupUI(rootview, false);
			holder = new RecordHolder();
			holder.imageitem = (ImageView) rootview
					.findViewById(R.id.imageView_item);
			holder.item = (TextView) rootview.findViewById(R.id.dish_name);
			holder.price = (TextView) rootview.findViewById(R.id.price);

			rootview.setTag(holder);
		} else {
			holder = (RecordHolder) rootview.getTag();
		}
		resultp = data.get(position);
		imagestring = resultp.get(AppConstants.Thumbnail);
		name = resultp.get(AppConstants.Name);
		price = resultp.get(AppConstants.Price);
		if (imagestring.startsWith("http://")) {
			Picasso.with(context).load(removeWhiteSpace(imagestring))
					.error(R.drawable.default_food).into(holder.imageitem);
		} else {
			holder.imageitem
					.setBackgroundResource(R.drawable.default_food);
		}

		if (name != null) {
			holder.item.setText(name);
		} else {
			holder.item.setText(context.getResources().getString(R.string.nodata));
			/*if(PrefUtil.getAppLanguage(context)=="en"){
				holder.item.setText("no data");
			}else{
				holder.item.setText("لا يوجد نتائج");
			}*/
		}
		/*if(PrefUtil.getAppLanguage(context)=="en"){
			holder.price.setText("KD " + price);
		}else{
			holder.price.setText("ك.د " + price);
		}*/
		holder.price.setText(context.getResources().getString(R.string.KD) + price);
		return rootview;
	}

	public class RecordHolder {
		ImageView imageitem;
		TextView item;
		TextView price;
	}

	private String removeWhiteSpace(String string) {

		String dummyString = string.replaceAll(" ", "%20");
		return dummyString;

	}
}
