package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.OnItemRemovedListener;
import com.squareup.picasso.Picasso;

public class MyFavouriteAdapter extends BaseAdapter {
	private ArrayList<HashMap<String, String>> arraylist;
	OnItemRemovedListener listener;
	BaseActivity act;
	private LayoutInflater inflater;

	public MyFavouriteAdapter(BaseActivity activity, ArrayList<HashMap<String, String>> arraylist2,
			OnItemRemovedListener removeListener) {
		this.arraylist = arraylist2;
		this.listener = removeListener;
		this.act = activity;
		inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {

		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {

		return null;
	}

	@Override
	public long getItemId(int position) {

		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder vh;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.mayfavourite_griditem, parent, false);
			act.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.m_dishImg = (ImageView) convertView.findViewById(R.id.imageView_dishpic);
			vh.m_dishName = (TextView) convertView.findViewById(R.id.dish_name);
			vh.m_dishName.setSelected(true);
			vh.m_dishDesc = (TextView) convertView.findViewById(R.id.dish_desc);
			vh.remove = (Button) convertView.findViewById(R.id.delete);
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		String imageUrl = arraylist.get(position).get(AppConstants.Thumbnail);

		if (imageUrl.trim().length() > 0) {
			if (URLUtil.isValidUrl(imageUrl)) {
				Picasso.with(act).load(imageUrl).placeholder(R.drawable.default_food)
						.error(R.drawable.default_food).into(vh.m_dishImg);
			} else {
				Picasso.with(act).load(R.drawable.default_food).placeholder(R.drawable.default_food)
						.error(R.drawable.default_food).into(vh.m_dishImg);
			}
		} else {
			Picasso.with(act).load(R.drawable.default_food).placeholder(R.drawable.default_food)
					.error(R.drawable.default_food).into(vh.m_dishImg);
		}
		vh.m_dishName.setText(arraylist.get(position).get(AppConstants.Name));
		vh.m_dishDesc.setText(arraylist.get(position).get(AppConstants.most_selling_dishes_description));
		vh.remove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				listener.ItemCheckComple(arraylist.get(position).get(AppConstants.get_cart_info_ItemId));
			}
		});
		return convertView;
	}

	private class ViewHolder {
		ImageView m_dishImg;
		TextView m_dishName;
		TextView m_dishDesc;
		Button remove;

	}
}
