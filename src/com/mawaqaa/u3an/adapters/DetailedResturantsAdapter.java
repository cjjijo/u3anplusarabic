package com.mawaqaa.u3an.adapters;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.data.Restaurant;
import com.squareup.picasso.Picasso;

public class DetailedResturantsAdapter extends BaseAdapter {
	private static final String TAG = "DetailedResturantsAdapter";
	private ArrayList<Restaurant> restaurants;

	private BaseActivity context;
	
	public DetailedResturantsAdapter(BaseActivity context,
			ArrayList<Restaurant> restaurants) {
		this.context = context;
		this.restaurants = restaurants;
	}

	@Override
	public int getCount() {
		try {
			return restaurants.size();
		} catch (Exception exception) {
			Log.e(TAG, "Exception : " + exception.getMessage());
			return 0;
		}
	}

	@Override
	public Restaurant getItem(int position) {
		try {
			return restaurants.get(position);
		} catch (Exception exception) {
			Log.e(TAG, "Exception : " + exception.getMessage());
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		try {
			return restaurants.get(position).restaurantId;
		} catch (Exception exception) {
			Log.e(TAG, "Exception : " + exception.getMessage());
			return 0;
		}
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables...
		ViewHolder vh;
		Restaurant restaurant;
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.detailedsearch_singleitem,
					parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.m_imgRestaurantLogo = (ImageView) convertView
					.findViewById(R.id.rest_logo);
			vh.m_txtRestaurantName = (TextView) convertView
					.findViewById(R.id.rest_name);
			vh.m_txtRestaurantRange = (TextView) convertView
					.findViewById(R.id.resturant_range);
			vh.m_txtRestaurantStatus = (TextView) convertView
					.findViewById(R.id.resturant_status);
			vh.m_txtRestaurantAvailableTime = (TextView) convertView
					.findViewById(R.id.avialable_time);
			vh.m_txtRestaurantDelivaryTime = (TextView) convertView
					.findViewById(R.id.delivary_time);

			vh.m_restaurantratingBar = (RatingBar) convertView
					.findViewById(R.id.resturant_rating);

			vh.m_img_knet = (ImageView) convertView.findViewById(R.id.knet);
			vh.m_img_cc = (ImageView) convertView.findViewById(R.id.cr_card);
			vh.m_img_cash = (ImageView) convertView.findViewById(R.id.cash);
			
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		// Get the position
		restaurant = restaurants.get(position);
		Log.d("jijoooooooooRestaurant", restaurant.restaurantStatus+"");	

		if (URLUtil.isValidUrl(restaurant.restaurantLogoUrl)
				&& restaurant.restaurantLogoUrl.length() > 5) {
			Picasso.with(context).load(removeWhiteSpace(restaurant.restaurantLogoUrl)).error(R.drawable.default_food)
					.into(vh.m_imgRestaurantLogo);
		} else {
			vh.m_imgRestaurantLogo
					.setBackgroundResource(R.drawable.default_food);
		}
		
		vh.m_txtRestaurantName.setText(restaurant.restaurantName);
		Log.w(TAG, "restaurant.minAmount==="+restaurant.minAmount);
		double range = 0.000;
		
		try {
			range = Double.valueOf(restaurant.minAmount);
		} catch (Exception exception) {
			Log.e(TAG, "Exception in range minimum amount parsing : "
					+ exception.getMessage());

		}
		//anju
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.ENGLISH);
		DecimalFormat df = (DecimalFormat)nf;
		df.applyPattern("###,##0.000");
		String formatedRangeStr = df.format(range);
		vh.m_txtRestaurantRange.setText(context.getResources().getString(R.string.min_amount_range)+" "+formatedRangeStr);
		
		
		if (restaurant.restaurantStatus != null) {
			Log.d("jijoooooooooRestaurant", restaurant.restaurantStatus+"");			
			if (restaurant.restaurantStatus.equalsIgnoreCase(context.getResources().getString(R.string.open)))
				vh.m_txtRestaurantStatus.setTextColor(context.getResources()
						.getColor(R.color.green_font_color));
			else
				vh.m_txtRestaurantStatus.setTextColor(context.getResources()
						.getColor(R.color.red_font_color));

			vh.m_txtRestaurantStatus.setText(restaurant.restaurantStatus);
		} else {

			/*
			 * vh.m_txtRestaurantStatus.setText("CLOSED");
			 * vh.m_txtRestaurantStatus.setTextColor(context.getResources()
			 * .getColor(R.color.red_font_color));
			 */
		}
		vh.m_txtRestaurantAvailableTime.setText(restaurant.workingHour);
		
		vh.m_txtRestaurantDelivaryTime.setText(Html.fromHtml(restaurant.deliveryTime));

		float rating = 0;
		try {
			rating = Float.valueOf(restaurant.rating);
		} catch (Exception exception) {
			Log.e(TAG,
					"Exception in rating converting : "
							+ exception.getMessage());
		}

		vh.m_restaurantratingBar.setRating(rating);

		vh.m_img_knet.setVisibility(restaurant.acceptsKNet ? View.VISIBLE
				: View.GONE);
		vh.m_img_cc.setVisibility(restaurant.acceptsCC ? View.VISIBLE
				: View.GONE);
		vh.m_img_cash.setVisibility(restaurant.acceptsCash ? View.VISIBLE
				: View.GONE);
		
		vh.m_txtRestaurantRange.setSelected(true);
		vh.m_txtRestaurantName.setSelected(true);
		vh.m_txtRestaurantAvailableTime.setSelected(true);

		return convertView;
	}

	private static class ViewHolder {
		ImageView m_imgRestaurantLogo;
		TextView m_txtRestaurantName;
		TextView m_txtRestaurantRange;
		TextView m_txtRestaurantStatus;
		TextView m_txtRestaurantAvailableTime;
		TextView m_txtRestaurantDelivaryTime;
		RatingBar m_restaurantratingBar;		
		ImageView m_img_knet;
		ImageView m_img_cc;
		ImageView m_img_cash;

	}
private String removeWhiteSpace(String string){
		
		String dummyString  =  string.replaceAll(" ", "%20");
		return dummyString;
		
	}
}
