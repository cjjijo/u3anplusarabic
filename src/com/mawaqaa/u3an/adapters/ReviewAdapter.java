package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;

public class ReviewAdapter extends BaseAdapter {
	LayoutInflater inflater;
	BaseActivity context;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();

	public ReviewAdapter(BaseActivity context,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context;
		data = arraylist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;
		ViewHolder holder;
		if (view == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.reviewsingleitem, parent, false);
			context.setupUI(view, false);
			holder = new ViewHolder();
			holder.By = (TextView) view.findViewById(R.id.by);
			holder.At = (TextView) view.findViewById(R.id.time);
			holder.textReview = (TextView) view.findViewById(R.id.testreview);
			view.setTag(holder);
		} else {
			holder = (ViewHolder) view.getTag();
		}
		resultp = data.get(position);
		String createon = resultp.get(AppConstants.createOn);

		String rev = resultp.get(AppConstants.Review);
		String username = resultp.get(AppConstants.Username);
		holder.By.setText(username);
		holder.At.setText(createon);
		holder.textReview.setText(rev);
		return view;
	}

	public class ViewHolder {
		TextView By;
		TextView At;
		TextView textReview;
	}

}
