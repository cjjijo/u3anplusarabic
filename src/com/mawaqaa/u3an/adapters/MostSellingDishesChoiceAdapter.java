package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.fragments.RestaurantMenuFragment;
import com.squareup.picasso.Picasso;

public class MostSellingDishesChoiceAdapter extends BaseAdapter {
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	/* ImageLoaderOne imageLoader; */
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();
	Fragment fr;

	BaseActivity context;
	Boolean tag = false;

	String restaurant_ID, restaurant_logo, restaurant_Name, areaID,
			restaurant_Rating, header, minimum_Amount;

	/* list.add(textview.getText().toString()); */
	public MostSellingDishesChoiceAdapter(BaseActivity context1,
			ArrayList<HashMap<String, String>> arraylist, String restaurant_id,
			String restaurantlogo, String restaurantName, String areaid,
			String restaurantRating, String header_, String minimumAmount) {
		this.context = context1;
		data = arraylist;
		restaurant_ID = restaurant_id;
		restaurant_logo = restaurantlogo;
		restaurant_Name = restaurantName;
		areaID = areaid;
		restaurant_Rating = restaurantRating;
		header = header_;
		minimum_Amount = minimumAmount;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables...


		ViewHolder vh;
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.most_selling_single_item,
					parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.flag = (ImageView) convertView
					.findViewById(R.id.imageView_dishpic);
			vh.namTextView = (TextView) convertView
					.findViewById(R.id.dish_name_most_selling);
			vh.namTextView.setSelected(true);
			vh.value1 = (TextView) convertView
					.findViewById(R.id.resturant_name);
			vh.value1.setSelected(true);
			vh.value2 = (TextView) convertView
					.findViewById(R.id.resturant_status);
			vh.nextButton = (Button)convertView.findViewById(R.id.moreButton);
			
			vh.nextButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {					
					Fragment restaurantMenuFragment = new RestaurantMenuFragment();
					Bundle params1 = new Bundle();
					params1.putString("Area_ID", areaID);
					params1.putString("restaurant_id", restaurant_ID);
					params1.putString("restaurant_logo", restaurant_logo);
					params1.putString("restaurant_name", restaurant_Name);
					params1.putString("restaurant_rating", restaurant_Rating);
					params1.putString("Header", header);
					params1.putString("minimum_Amount", minimum_Amount);
					restaurantMenuFragment.setArguments(params1);
					context.pushFragments(restaurantMenuFragment, false, true);
				}
			});
			
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		// Get the position
		resultp = data.get(position);

		String imagString = resultp.get(AppConstants.most_selling_dishes_image);

		String dishName = resultp.get(AppConstants.most_selling_dishes_dishname);
		String resturantName = resultp.get(AppConstants.most_selling_dishes_description);
		String resturantPrice = resultp.get(AppConstants.most_selling_dishes_price);

		
		
		if (imagString.startsWith("http")) {
			Picasso.with(context).load(removeWhiteSpace(imagString)).error(R.drawable.default_food)
		     .into(vh.flag);
		} else {
			vh.flag
			     .setBackgroundResource(R.drawable.default_food);
		}

		vh.namTextView.setText(dishName);
		vh.namTextView.setSelected(true);
		vh.value1.setText(resturantName);
		
		
		double range = 0.000;
		try {
			range = Double.valueOf(resturantPrice);
		} catch (Exception exception) {			
		} 
	     vh.value2.setText(context.getString(R.string.to_kd, range));
		return convertView;
	}

	private String removeWhiteSpace(String string) {
		String dummyString = string.replaceAll(" ", "%20");
		return dummyString;
	}
	
	private static class ViewHolder {
		ImageView flag;
		TextView namTextView;
		TextView value1;
		TextView value2;
		Button nextButton;

	}
}
