package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.interfaces.OnListItemClicked;
import com.mawaqaa.u3an.interfaces.OnRatingbarChanged;

public class MyOrdersSecondPageAdapter extends BaseAdapter {
	private ArrayList<HashMap<String, String>> arraylist;
	BaseActivity act;
	private LayoutInflater inflater;
	OnListItemClicked clicked;
	OnRatingbarChanged rateBarChecked;

	public MyOrdersSecondPageAdapter(BaseActivity activity, ArrayList<HashMap<String, String>> list,
			OnListItemClicked clickedItem, OnRatingbarChanged rateBarChecked) {
		this.arraylist = list;
		this.act = activity;
		inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.clicked = clickedItem;
		this.rateBarChecked = rateBarChecked;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder vh;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.myordersecond_listitem, parent, false);
			act.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.name = (TextView) convertView.findViewById(R.id.name);
			vh.time = (TextView) convertView.findViewById(R.id.time);
			vh.ratBar = (RatingBar) convertView.findViewById(R.id.ratingBar1);
			vh.go = (Button) convertView.findViewById(R.id.button1);
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		vh.name.setText(arraylist.get(position).get(AppConstants.Restaurant));
		vh.time.setText(arraylist.get(position).get(AppConstants.pDate) + "\n"
				+ arraylist.get(position).get(AppConstants.delivery_time_json_obj));
		vh.ratBar.setOnRatingBarChangeListener(null);
		vh.ratBar.setRating(Integer.parseInt(arraylist.get(position).get(AppConstants.all_restaurants_rating)));

		vh.ratBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
			@Override
			public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

				rateBarChecked.ItemRatingComple("Restaurant",
						arraylist.get(position).get(AppConstants.all_restaurants_id), String.valueOf(rating));

			}
		});
		vh.go.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				clicked.onItemClicked(position);

			}
		});
		return convertView;
	}

	private class ViewHolder {

		TextView name;
		TextView time;
		RatingBar ratBar;
		Button go;

	}
}
