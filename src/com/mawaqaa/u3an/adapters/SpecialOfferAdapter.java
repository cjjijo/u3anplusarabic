package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.squareup.picasso.Picasso;

public class SpecialOfferAdapter extends BaseAdapter {
	private static final String TAG = "SpecialOfferAdapter";
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	/* ImageLoaderOne imageLoader; */
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();
	Fragment fr;

	BaseActivity context;

	Boolean tag = false;

	/* list.add(textview.getText().toString()); */
	public SpecialOfferAdapter(BaseActivity context1,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context1;
		data = arraylist;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables...
        Float rating = (float) 0;
		ViewHolder vh;
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.special_offer_single_item,
					parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.flag = (ImageView) convertView
					.findViewById(R.id.rest_logo_specialoffer);
			vh.namTextView = (TextView) convertView
					.findViewById(R.id.rest_name_special_offers);
			vh.priceTextView = (TextView) convertView
					.findViewById(R.id.rest_details_special_offers);
			vh.descriptionTextView = (TextView) convertView
					.findViewById(R.id.resturant_range_special_offers);
            vh.moreButton  =  (Button)convertView.findViewById(R.id.buttonMore_special_offers);
            vh.m_restaurantratingBar = (RatingBar) convertView
					.findViewById(R.id.resturant_rating_special_offers);
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		// Get the position
		resultp = data.get(position);

		/*
		 * String imagString = resultp
		 * .get(AppConstants.all_restaurants_resturant_logo);
		 */

		String imagString = resultp.get(AppConstants.special_offers_thumbnail);
		String restaurant_Name = resultp.get(AppConstants.special_offers_name);
		String restaurant_desc = resultp.get(AppConstants.special_offers_description);
		String price = resultp.get(AppConstants.special_offers_price);
        String ratingString = resultp.get(AppConstants.special_offers_rating);

        try {
			rating = Float.valueOf(ratingString);
		} catch (Exception exception) {
			Log.e(TAG,
					"Exception in rating converting : "
							+ exception.getMessage());

		}
        vh.m_restaurantratingBar.setRating(rating);
        if (imagString.startsWith("http")) {
			Picasso.with(context).load(removeWhiteSpace(imagString)).error(R.drawable.default_food)
		     .into(vh.flag);
		} else {
			vh.flag
			     .setBackgroundResource(R.drawable.default_food);
		}

		vh.namTextView.setText(restaurant_Name);
		vh.namTextView.setSelected(true);
		vh.priceTextView.setText(price);

		return convertView;
	}
	private String removeWhiteSpace(String string){
		
		String dummyString  =  string.replaceAll(" ", "");
		return dummyString;
		
	}
	private static class ViewHolder {

		ImageView flag;
		TextView namTextView;
		TextView descriptionTextView;
		RatingBar m_restaurantratingBar;
		TextView priceTextView;
		Button moreButton;
	}
}
