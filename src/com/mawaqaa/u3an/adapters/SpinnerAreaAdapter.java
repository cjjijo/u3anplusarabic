package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.data.SingleAreaCountry;

public class SpinnerAreaAdapter extends BaseAdapter{

	ArrayList<SingleAreaCountry> cat;
	private LayoutInflater inflater;
	BaseActivity context;
	
	public SpinnerAreaAdapter(BaseActivity _con, ArrayList<SingleAreaCountry> list) {
		context = _con;
		this.cat = list;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return cat.size();
	}

	@Override
	public Object getItem(int position) {

		return cat.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_adapter, null);
			context.setupUI(v, false);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.item_category);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
// Anju
		//holder.name.setText(cat.get(position).areaName);
		if(cat.get(position).areaName!=null){
			   holder.name.setText(cat.get(position).areaName);
		}else{
		   holder.name.setText("");
		  
		}
		Log.e("Area Name..............................", ""+cat.get(position).areaName);
		holder.name.setTextColor(Color.BLACK);
		return v;
	}

		public class ViewHolder {
			public TextView name;
	
		}
	}