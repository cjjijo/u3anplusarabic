package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;

public class BrandSpinnerAdpater extends BaseAdapter {
	ArrayList<String> cat;
	private LayoutInflater inflater;
	BaseActivity activity;
	
	public BrandSpinnerAdpater(BaseActivity activity, ArrayList<String> list) {
		this.activity = activity;
		this.cat = list;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return cat.size();
	}

	@Override
	public Object getItem(int position) {
		return cat.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_layout, null);
			activity.setupUI(v, true);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.item_category);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.name.setText(cat.get(position).toLowerCase());
		holder.name.setTextColor(Color.WHITE);
		return v;
	}
	
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_dropdowm_item, null);
			activity.setupUI(v, true);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.dropdown_text);
			v.setTag(holder);
			
		} else {
			holder = (ViewHolder) v.getTag();
		}
		holder.name.setText(cat.get(position).toLowerCase());
		holder.name.setTextColor(Color.WHITE);
		return v;
	}

	                                                     	
		public class ViewHolder {
			public TextView name;
	
		}
	}