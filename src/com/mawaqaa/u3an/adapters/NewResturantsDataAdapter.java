package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.squareup.picasso.Picasso;

public class NewResturantsDataAdapter extends BaseAdapter{

	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	/* ImageLoaderOne imageLoader; */
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();
	Fragment fr;

	BaseActivity context;

	Boolean tag = false;
	public NewResturantsDataAdapter(BaseActivity context1,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context1;
		data = arraylist;
	}
	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		ViewHolder vh;
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.allresturant_singleitem,
					parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.flag = (ImageView) convertView
					.findViewById(R.id.imageView_restpivhpic);
			vh.namTextView = (TextView) convertView
					.findViewById(R.id.dish_name_allrest);
			vh.value1 = (TextView) convertView
					.findViewById(R.id.resturant_status_all_rest);

			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		// Get the position
		resultp = data.get(position);

		String imagString = resultp
				.get(AppConstants.all_restaurants_resturant_logo);

		String dishName = resultp
				.get(AppConstants.all_restaurants_resturant_name);
		String resturantStatus = resultp
				.get(AppConstants.all_restaurants_resturant_status);
		if (imagString.length() > 3) {
			Picasso.with(context).load(removeWhiteSpace(imagString)).error(R.drawable.imagenotavailable)
		     .into(vh.flag);
		} else {
			vh.flag
		     .setBackgroundResource(R.drawable.imagenotavailable);
		}
		
		vh.namTextView.setText(dishName);
		vh.namTextView.setSelected(true);
		if(PrefUtil.getAppLanguage(context).equals("en")){
			if (resturantStatus != null
					&& resturantStatus.equalsIgnoreCase(AppConstants.OPEN))
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.green_font_color));
			else
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.red_font_color));
		}else{
			if (resturantStatus != null
					&& resturantStatus.equalsIgnoreCase(context.getResources().getString(R.string.open)))
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.green_font_color));
			else
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.red_font_color));
		}
		
		vh.value1.setText(resturantStatus);

		return convertView;
	}
	
	private static class ViewHolder {
		ImageView flag;
		TextView namTextView;
		TextView value1;
	}
	private String removeWhiteSpace(String string){
		
		String dummyString  =  string.replaceAll(" ", "");
		return dummyString;
		
	}
}
