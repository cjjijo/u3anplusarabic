package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;

public class MyOrdersAdapter extends BaseAdapter {
	private ArrayList<HashMap<String, String>> arraylist;
	BaseActivity act;
	private LayoutInflater inflater;

	public MyOrdersAdapter(BaseActivity activity, ArrayList<HashMap<String, String>> arraylist2) {
		this.arraylist = arraylist2;
		this.act = activity;
		inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arraylist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder vh;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.myorder_listitem, parent, false);
			act.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.date = (TextView) convertView.findViewById(R.id.date);
			vh.total = (TextView) convertView.findViewById(R.id.total);
			vh.area = (TextView) convertView.findViewById(R.id.area);
			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		vh.date.setText(arraylist.get(position).get(AppConstants.pDate));
		vh.area.setText(arraylist.get(position).get(AppConstants.DeliveryArea)+","+arraylist.get(position).get(AppConstants.DeliveryBlock));
		vh.total.setText(R.string.KD+arraylist.get(position).get(AppConstants.Total));
		
		return convertView;
	}

	private class ViewHolder {

		TextView date;
		TextView area;
		TextView total;

	}
}
