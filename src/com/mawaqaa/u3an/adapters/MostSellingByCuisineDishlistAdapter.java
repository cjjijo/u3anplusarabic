package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.squareup.picasso.Picasso;

public class MostSellingByCuisineDishlistAdapter extends BaseAdapter {
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	/* ImageLoaderOne imageLoader; */
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();
	Fragment fr;

	BaseActivity context;
	Boolean tag = false;

	/* list.add(textview.getText().toString()); */
	public MostSellingByCuisineDishlistAdapter(BaseActivity context1,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context1;
		data = arraylist;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables...

		ViewHolder vh;
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			convertView = inflater.inflate(R.layout.mostselling_singleitem,
					parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.flag = (ImageView) convertView
					.findViewById(R.id.imageView_dishpic);
			vh.namTextView = (TextView) convertView
					.findViewById(R.id.dish_name);
			vh.namTextView.setSelected(true);
			vh.value1 = (TextView) convertView
					.findViewById(R.id.resturant_name);
			vh.value1.setSelected(true);
			vh.value2 = (TextView) convertView
					.findViewById(R.id.resturant_status);

			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}
		
		// Get the position
		resultp = data.get(position);
		String imagString = resultp
				.get(AppConstants.most_selling_by_cuisines_dish_thumbnail);

		String dishName = resultp
				.get(AppConstants.most_selling_by_cuisines_restaurant_name);
		String resturantName = resultp
				.get(AppConstants.most_selling_by_cuisines_dishname);
		String resturantStatus = resultp
				.get(AppConstants.most_selling_by_cuisines_restaurant_status);
// Jijo
		if (imagString.startsWith("http")) {
			Picasso.with(context).load(removeWhiteSpace(imagString))
					.error(R.drawable.default_food).into(vh.flag);
		} else {
			vh.flag.setBackgroundResource(R.drawable.default_food);
		}

		vh.namTextView.setText(dishName);
		vh.namTextView.setSelected(true);
		vh.value1.setText(resturantName);
		if(PrefUtil.getAppLanguage(context).equals("en")){
			if (resturantStatus != null
					&& resturantStatus.equalsIgnoreCase(AppConstants.OPEN))
				vh.value2.setTextColor(context.getResources().getColor(
						R.color.green_font_color));
			else
				vh.value2.setTextColor(context.getResources().getColor(
						R.color.red_font_color));
		}else{
			if (resturantStatus != null
					&& resturantStatus.equalsIgnoreCase(context.getResources().getString(R.string.open)))
				vh.value2.setTextColor(context.getResources().getColor(
						R.color.green_font_color));
			else
				vh.value2.setTextColor(context.getResources().getColor(
						R.color.red_font_color));
		}
		
		vh.value2.setText(resturantStatus);
		return convertView;
	}

	private static class ViewHolder {
		ImageView flag;
		TextView namTextView;
		TextView value1;
		TextView value2;
	}

	private String removeWhiteSpace(String string) {
		String dummyString = string.replaceAll(" ", "%20");
		return dummyString;

	}
}
