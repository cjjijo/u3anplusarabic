package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;

public class SpinnerAdapter extends BaseAdapter{

	ArrayList<String> cat;
	private LayoutInflater inflater;
	BaseActivity context;
	
	public SpinnerAdapter(BaseActivity _con, ArrayList<String> list) {
		context = _con;
		this.cat = list;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return cat.size();
	}

	@Override
	public Object getItem(int position) {

		return cat.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_adapter, null);
			context.setupUI(v, false);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.item_category);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		if (cat.size()>0) {
			holder.name.setText(cat.get(position));
		}
		holder.name.setTextColor(Color.BLACK);
		return v;
	}

		public class ViewHolder {
			public TextView name;
	
		}
	}