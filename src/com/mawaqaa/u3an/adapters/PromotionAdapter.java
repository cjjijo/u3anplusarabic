package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.squareup.picasso.Picasso;

public class PromotionAdapter extends BaseAdapter{
BaseActivity activity;

ArrayList<HashMap<String, String>>data;
HashMap<String, String>map=new HashMap<String,String>();
String resturentname,restuarent_id,imagestring,status;
public PromotionAdapter(BaseActivity context,ArrayList<HashMap<String, String>>data)
{
	this.activity=context;
	this.data=data;
}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rootview=convertView;
		RecordHolder holder;
		if (rootview==null) {
			LayoutInflater inflater=(LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			rootview=inflater.inflate(R.layout.promotionsingleitem, null);
			activity.setupUI(rootview, false);
			holder=new RecordHolder();
			holder.restuarentImage=(ImageView) rootview.findViewById(R.id.imageView_restpivhpic);
			holder.restuarentname=(TextView) rootview.findViewById(R.id.dish_name_allrest);
			holder.status=(TextView) rootview.findViewById(R.id.resturant_status_all_rest);
			
		rootview.setTag(holder);	
		}
		else {
			holder=(RecordHolder) rootview.getTag();
		}
		map=data.get(position);
		resturentname=map.get(AppConstants.RestaurantName);
		Log.e("resturentname",""+ resturentname);
		 restuarent_id=map.get(AppConstants.RestaurantId);
		 Log.e("resturentid",""+ restuarent_id);
		 imagestring=map.get(AppConstants.RestaurantLogo);
		 Log.e("imagestring",""+ imagestring);
		 status=map.get(AppConstants.status);
		 Log.e("status",""+ status);
		if (imagestring.length() > 3) {
			Picasso.with(activity).load(removeWhiteSpace(imagestring))
					.error(R.drawable.default_food).into(holder.restuarentImage);
		} else {
			holder.restuarentImage.setBackgroundResource(R.drawable.default_food);
		}
		
// Jijo
		holder.restuarentname.setText(resturentname);
		if(PrefUtil.getAppLanguage(activity).equals("en")){
			if (status != null
					&& status.equalsIgnoreCase(AppConstants.OPEN))
				holder.status.setTextColor(activity.getResources().getColor(
						R.color.green_font_color));
			else
				holder.status.setTextColor(activity.getResources().getColor(
						R.color.red_font_color));
		}else{
			if (status != null
					&& status.equalsIgnoreCase(activity.getResources().getString(R.string.open)))
				holder.status.setTextColor(activity.getResources().getColor(
						R.color.green_font_color));
			else
				holder.status.setTextColor(activity.getResources().getColor(
						R.color.red_font_color));
		}
		

		holder.status.setText(status);
		
				return rootview;
	}
	public class RecordHolder
	{
		ImageView restuarentImage;
		TextView restuarentname;
		TextView status;
	} 
private String removeWhiteSpace(String string){
		
		String dummyString  =  string.replaceAll(" ", "%20");
		return dummyString;
		
	}
}
