package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.squareup.picasso.Picasso;

public class AllResturantsDataAdapter extends BaseAdapter {
	LayoutInflater inflater;
	ArrayList<HashMap<String, String>> data;
	HashMap<String, String> resultp = new HashMap<String, String>();
	ArrayList<String> list = new ArrayList<String>();
	Fragment fr;

	BaseActivity context;

	Boolean tag = false;

	public AllResturantsDataAdapter(BaseActivity context1,
			ArrayList<HashMap<String, String>> arraylist) {
		this.context = context1;
		data = arraylist;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// Declare Variables.....
		ViewHolder vh;
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.all_restaurant_single_item,
					parent, false);
			context.setupUI(convertView, false);
			vh = new ViewHolder();
			vh.flag = (ImageView) convertView
					.findViewById(R.id.imageView_restpivhpic);
			vh.namTextView = (TextView) convertView
					.findViewById(R.id.dish_name_allrest);
			vh.value1 = (TextView) convertView
					.findViewById(R.id.resturant_status_all_rest);
			vh.badgeTextView = (TextView) convertView.findViewById(R.id.textView1badge);
						
			vh.badgeBg = (ImageButton) convertView.findViewById(R.id.badgenumber);
			

			convertView.setTag(vh);

		} else {
			vh = (ViewHolder) convertView.getTag();
		}

		// Get the position
		resultp = data.get(position);

		String imagString = resultp
				.get(AppConstants.all_restaurants_resturant_logo);
		String dishName = resultp
				.get(AppConstants.all_restaurants_resturant_name);
		String resturantName = resultp
				.get(AppConstants.all_restaurants_resturant_status);
		//String badgeNumber = resultp.get(AppConstants.all_restaurants_resturant_sort_order);
		if (imagString.length() > 3) {
			Picasso.with(context).load(removeWhiteSpace(imagString))
				.error(R.drawable.default_food).into(vh.flag);
		} else {
			vh.flag.setBackgroundResource(R.drawable.default_food);
		}

		vh.namTextView.setText(dishName);
		vh.namTextView.setSelected(true);
	//	Jijo
		vh.badgeTextView.setVisibility(View.INVISIBLE);
		vh.badgeBg.setVisibility(View.INVISIBLE);
		//vh.badgeBg.setVisibility(View.GONE);
		if(PrefUtil.getAppLanguage(context).equals("en")){
			if (resturantName != null
					&& resturantName.equalsIgnoreCase(AppConstants.OPEN))
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.green_font_color));
			else
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.red_font_color));
		}else{
			if (resturantName != null
					&& resturantName.equalsIgnoreCase(context.getResources().getString(R.string.open)))
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.green_font_color));
			else
				vh.value1.setTextColor(context.getResources().getColor(
						R.color.red_font_color));
		}
		
		vh.value1.setText(resturantName);
		return convertView;
	}

	private static class ViewHolder {
		ImageView flag;
		TextView namTextView;
		TextView value1;
		TextView badgeTextView;
		ImageButton  badgeBg;
	}

	private String removeWhiteSpace(String string) {
		String dummyString = string.replaceAll(" ", "%20");
		return dummyString;
	}
}
