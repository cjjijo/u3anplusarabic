package com.mawaqaa.u3an.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mawaqaa.u3an.BaseActivity;
import com.mawaqaa.u3an.R;
import com.mawaqaa.u3an.data.Cuisine;

public class CuisineSpinnerAdapter extends BaseAdapter {
	ArrayList<Cuisine> cuisines;
	private LayoutInflater inflater;
	BaseActivity context;
	
	public CuisineSpinnerAdapter(BaseActivity _con, ArrayList<Cuisine> cuisines) {
		context = _con;
		this.cuisines = cuisines;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	@Override
	public int getCount() {
		return cuisines.size();
	}

	@Override
	public Object getItem(int position) {
		return cuisines.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		ViewHolder holder;
		if (convertView == null) {
			v = inflater.inflate(R.layout.spinner_layout, null);
			context.setupUI(v, false);
			holder = new ViewHolder();
			holder.name = (TextView) v.findViewById(R.id.item_category);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}	
		//Anju
		if(cuisines.size()!=0){
			holder.name.setText(cuisines.get(position).strCuisineName);
			holder.name.setTextColor(Color.BLACK);
		}else{
			holder.name.setText("");
		}
		return v;
	}
	
		public class ViewHolder {
			public TextView name;	
		}
	}