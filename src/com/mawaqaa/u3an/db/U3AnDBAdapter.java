package com.mawaqaa.u3an.db;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.CartItem;
import com.mawaqaa.u3an.data.Restaurant;

public class U3AnDBAdapter {
	
	private static final String TAG = "U3AnDBAdapter";

	private DatabaseHelper mDbHelper;
	private SQLiteDatabase mDb;

	private static final String DATABASE_NAME = "U3Andb";

	private static final int DATABASE_VERSION = 1;
	private final Context mContext;
	private static final String RESTAURANT_TABLE = "U3AN_RESTAURANT";
	private static final String RESTAURANT_COVERAGE_TIMING_TABLE = "U3AN_RESTAURANT_COVERAGE_TIMING";
	
	private static final String COUNTRY_AREA_TABLE = "U3AN_COUNTRY_AREA";
	private static final String LOCAL_CART_DETAILS_TABLE = "U3AN_LOCAL_CART";

	
	//------------------- Table Schema ---------------------------------//
	
	private static final String ACCEPTS_CC = "AcceptsCC";
	private static final String ACCEPTS_CASH = "AcceptsCash";
	private static final String ACCEPTS_KNET = "AcceptsKNET";
	private static final String CUISINE = "Cuisine";
	private static final String DELIVERY_CHARGES = "DeliveryCharges";
	private static final String DELIVERY_TIME = "DeliveryTime";
	private static final String MIN_AMOUNT = "MinAmount";
	
	private static final String MINIMUM_AMOUNT = "MinimumAmount";
	private static final String RATING = "Rating";
	private static final String RESTAURANT_ID = "RestaurantId";
	private static final String RESTAURANT_LOGO_URL = "RestaurantLogo";
	private static final String RESTAURANT_NAME = "RestaurantName";
	private static final String RESTAURANT_STATUS = "RestaurantStatus";
	private static final String SUMMARY = "Summary";
	
	private static final String WORKING_HOUR = "Workinghour";
	private static final String U3AN_CHARGE = "u3anCharge";

	private static final String CREATE_RESTAURANT_TABLE = "CREATE TABLE "
			+ RESTAURANT_TABLE + 
			" ("
				+ RESTAURANT_ID 	 + " INTEGER PRIMARY KEY on conflict replace, "
				+ ACCEPTS_CC + " BOOL, " 
				+ ACCEPTS_CASH + " BOOL, "
				+ ACCEPTS_KNET + " BOOL, "
				+ CUISINE + " TEXT, "
				+ DELIVERY_CHARGES + " REAL, "
				+ DELIVERY_TIME + " TEXT, "
				+ MIN_AMOUNT + " TEXT, "
				+ MINIMUM_AMOUNT + " TEXT, "
				+ RATING + " TEXT, "
				+ RESTAURANT_LOGO_URL + " TEXT, "
				+ RESTAURANT_NAME + " TEXT, "
				+ RESTAURANT_STATUS + " TEXT, "
				+ SUMMARY + " TEXT, "
				+ WORKING_HOUR + " TEXT, "
				+ U3AN_CHARGE + " REAL "
			+ "); ";
	
	
	private static final String CREATE_COUNTRY_AREA_TABLE = "CREATE TABLE "
			+ COUNTRY_AREA_TABLE + 
			" ("
				+ AppConstants.areas_area_id 	 + " TEXT PRIMARY KEY on conflict replace, "
				+ AppConstants.areas_area_name + " TEXT, "
				+ AppConstants.area_by_city_id + " TEXT, "
				+ AppConstants.area_by_city_name + " TEXT "
			+ "); ";
	
	private static final String CREATE_LOCAL_CART_DETAILS_TABLE = "CREATE TABLE "
			+ LOCAL_CART_DETAILS_TABLE + 
			" ("
				+ AppConstants.ORDERED_ITEM_ID 	 + " INTEGER PRIMARY KEY on conflict replace, "
				+ AppConstants.ORDERED_ITEM_NAME + " TEXT, "
				+ AppConstants.ORDERED_ITEM_PRICE + " TEXT, "
				+ AppConstants.ORDERED_ITEM_QUANTITY + " TEXT, "
				+ AppConstants.ORDERED_ITEM_SPECIAL_REQUEST + " TEXT "
			+ "); ";
	
	
		

	private static class DatabaseHelper extends SQLiteOpenHelper {

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);

		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_RESTAURANT_TABLE);
			db.execSQL(CREATE_COUNTRY_AREA_TABLE);
			db.execSQL(CREATE_LOCAL_CART_DETAILS_TABLE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.execSQL("DROP TABLE IF EXISTS " + RESTAURANT_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + COUNTRY_AREA_TABLE);
			db.execSQL("DROP TABLE IF EXISTS " + LOCAL_CART_DETAILS_TABLE);
			onCreate(db);
		}

	}

	public U3AnDBAdapter(Context context) {
		this.mContext = context;
	}

	public U3AnDBAdapter open() throws SQLException {
		mDbHelper = new DatabaseHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	
	public void insertOrUpdate(ArrayList<Restaurant> restaurants){		
		 try{			 
			 for(Restaurant restaurant : restaurants){
					Cursor cursor = mDb.rawQuery("SELECT * FROM "
							+ RESTAURANT_TABLE + " WHERE "+ RESTAURANT_ID + "=?", new String[]{""+restaurant.restaurantId});
					
					if(cursor == null || cursor.getCount() == 0){
						//INsert if no Album details available
						ContentValues values = new ContentValues();
						values.put(RESTAURANT_ID,restaurant.restaurantId);
						values.put(ACCEPTS_CC,restaurant.acceptsCC);
						values.put(ACCEPTS_CASH,restaurant.acceptsCash);
						values.put(ACCEPTS_KNET,restaurant.acceptsKNet);
						values.put(CUISINE,restaurant.cuisine);
						values.put(DELIVERY_CHARGES,restaurant.deliveryCharge);
						//values.put(ASSET_OWNER,"");
						values.put(DELIVERY_TIME,restaurant.deliveryTime);
						values.put(MIN_AMOUNT ,restaurant.minAmount);
						values.put(MINIMUM_AMOUNT ,restaurant.minimumAmount);
						values.put(RATING ,restaurant.rating);
						values.put(RESTAURANT_LOGO_URL ,restaurant.restaurantLogoUrl);
						values.put(RESTAURANT_NAME ,restaurant.restaurantName);
						
						values.put(RESTAURANT_STATUS ,restaurant.restaurantStatus);
						values.put(SUMMARY ,restaurant.summary);
						values.put(WORKING_HOUR ,restaurant.workingHour);
						values.put(U3AN_CHARGE ,restaurant.u3anCharge);
											
						mDb.insertWithOnConflict(RESTAURANT_TABLE, null, values, SQLiteDatabase.CONFLICT_IGNORE);
						//updatedAssetIds.add(asset.getAssetId());
					}
					else{
						//Update the exsting one
						ContentValues values = new ContentValues();
						values.put(RESTAURANT_ID,restaurant.restaurantId);
						values.put(ACCEPTS_CC,restaurant.acceptsCC);
						values.put(ACCEPTS_CASH,restaurant.acceptsCash);
						values.put(ACCEPTS_KNET,restaurant.acceptsKNet);
						values.put(CUISINE,restaurant.cuisine);
						values.put(DELIVERY_CHARGES,restaurant.deliveryCharge);
						//values.put(ASSET_OWNER,"");
						values.put(DELIVERY_TIME,restaurant.deliveryTime);
						values.put(MIN_AMOUNT ,restaurant.minAmount);
						values.put(MINIMUM_AMOUNT ,restaurant.minimumAmount);
						values.put(RATING ,restaurant.rating);
						values.put(RESTAURANT_LOGO_URL ,restaurant.restaurantLogoUrl);
						values.put(RESTAURANT_NAME ,restaurant.restaurantName);
						
						values.put(RESTAURANT_STATUS ,restaurant.restaurantStatus);
						values.put(SUMMARY ,restaurant.summary);
						values.put(WORKING_HOUR ,restaurant.workingHour);
						values.put(U3AN_CHARGE ,restaurant.u3anCharge);
						
						mDb.update(RESTAURANT_TABLE, values, RESTAURANT_ID+"=?", new String[]{""+restaurant.restaurantId});
						//updatedAssetIds.add(asset.getAssetId());
						
					}
					
			 }
			
			Log.d("DBINSERTTEST", " Addding Albuminfo  Finshed ---");
		 }catch (Exception e) {
			 Log.e("DBINSERTTEST", " !!!!!!!!!!!!!! !!!!! excettion Addding notification---"+e.getLocalizedMessage());
				e.printStackTrace();
		 }	
		 
	}
	
	
	/*public void insertOrUpdateCountryArea(Country country) {

		
		if(country== null)
			return;
		
		
		Log.d("DBINSERTTEST", " Addding CountryArea  Start ---");


			String sql = "INSERT INTO " + COUNTRY_AREA_TABLE + "("
					+ AppConstants.areas_area_id + ","
					+ AppConstants.areas_area_name + ","
					+ AppConstants.area_by_city_id + ","
					+ AppConstants.area_by_city_name + ")"
					+ "VALUES (?, ?, ?, ?)";

			mDb.beginTransaction();
			SQLiteStatement stmt = mDb.compileStatement(sql);

			String updateSql = "UPDATE " + COUNTRY_AREA_TABLE + " SET "
					+ AppConstants.areas_area_id + " = ?1, "
					+ AppConstants.areas_area_name + " = ?2, "
					+ AppConstants.area_by_city_id + " = ?3, "
					+ AppConstants.area_by_city_name + " = ?4 " + " WHERE  "
					+ AppConstants.areas_area_id + " = ?5 ";

			SQLiteStatement updateStmt = mDb.compileStatement(updateSql);
			Cursor cursor;

			try {

				cursor = mDb.rawQuery("SELECT * FROM " + COUNTRY_AREA_TABLE
						+ " WHERE " + AppConstants.areas_area_id + "=?",
						new String[] { country.areaId });

				if (cursor != null && cursor.getCount() > 0
						&& cursor.moveToFirst()) { // Album entry is already
													// available .So update if
													// needed

					// long modifiedMillisecs =
					// cursor.getLong(cursor.getColumnIndex(DATE_OF_MODIFICATION));

					// if(album.getLastModifiedDate().getTime() >
					// modifiedMillisecs) {

					// if(fwNotification.getItemId() != null)
					updateStmt.bindString(1, country.areaId);

					// if(fwNotification.getNotiType() != null)
					updateStmt.bindString(2, country.areaName);
					// if(fwNotification.getPayLoad() != null)
					updateStmt.bindString(3, country.cityId);
					// if(fwNotification.getFrom() != null)
					updateStmt.bindString(4, country.cityName);
					
					updateStmt.bindString(5, country.areaId);

					Log.d("DBUPDATE", " Addding nofication---");

					updateStmt.executeUpdateDelete();
					updateStmt.clearBindings();
					
					 * } else{ continue; }
					 
				}

				else {
					// if(fwNotification.getItemId() != null)
					stmt.bindString(1, country.areaId);

					// if(fwNotification.getNotiType() != null)
					stmt.bindString(2, country.areaName);
					// if(fwNotification.getPayLoad() != null)
					stmt.bindString(3, country.cityId);
					// if(fwNotification.getFrom() != null)
					stmt.bindString(4, country.cityName);

					Log.d("DBINSERTTEST", " Addding notification---");

					stmt.execute();
					stmt.clearBindings();
				}

				mDb.setTransactionSuccessful();
				mDb.endTransaction();

				Log.d("DBINSERTTEST", " Addding Albuminfo  Finshed ---");
			} catch (Exception e) {
				Log.e("DBINSERTTEST",
						" !!!!!!!!!!!!!! !!!!! excettion Addding notification---"
								+ e.getLocalizedMessage());
				e.printStackTrace();
			}
	}
	
	
	public ArrayList<SingleAreaCountry> getCountryAreaSpinnerData(){
		ArrayList<SingleAreaCountry> cities = getCountryAreaCities();
		Log.d(TAG, "getCountryAreaSpinnerData : City Size :"+cities.size());
		ArrayList<SingleAreaCountry> spinnerAreaDatas = new ArrayList<SingleAreaCountry>();
		for(SingleAreaCountry city : cities){
			spinnerAreaDatas.add(city);
			ArrayList<SingleAreaCountry> areasByCity = getAreasByCity(city.cityId);
			spinnerAreaDatas.addAll(areasByCity);
			
		}
		return spinnerAreaDatas;
		
		
	}
	
	
	private ArrayList<SingleAreaCountry> getAreasByCity(String cityId) {

		// in this country , will only contain city id and city name
		
		Cursor cursor = null;
		ArrayList<SingleAreaCountry> cities = null;
		try {

			cursor = mDb.rawQuery("SELECT * FROM "
					+ COUNTRY_AREA_TABLE
			 + " WHERE " +AppConstants.area_by_city_id + " = ?" ,
					new String[] {cityId});
			if (cursor == null)
				return null;
			cities = new ArrayList<SingleAreaCountry>();
			while (cursor.moveToNext()) {
				SingleAreaCountry country = new SingleAreaCountry();
				String cityName = cursor.getString(cursor
						.getColumnIndex(AppConstants.area_by_city_name));
				String cityID = cursor.getString(cursor
						.getColumnIndex(AppConstants.area_by_city_id));
				String areaName = cursor.getString(cursor
						.getColumnIndex(AppConstants.areas_area_name));
				String areaId = cursor.getString(cursor
						.getColumnIndex(AppConstants.areas_area_id));
					country.cityId = cityID;
					country.cityName = cityName;
					country.areaId = areaId;
					country.areaName = areaName;
					
				cities.add(country);	

			}
		} catch (Exception exception) {
			Log.e("DB getAreasByCity :", exception.getMessage());

		} finally {
			if (cursor != null)
				cursor.close();
		}

		// cursor.close();

		return cities;
	}
	
		*/

	/*public ArrayList<SingleAreaCountry> getCountryAreaCities(){
		// in this country , will only contain city id and city name
		
		Cursor cursor = null;
		ArrayList<SingleAreaCountry> cities = null;
		try {

			cursor = mDb.rawQuery("SELECT " + AppConstants.area_by_city_name + " , "+AppConstants.area_by_city_id + " FROM "
					+ COUNTRY_AREA_TABLE
			 + " ORDER BY " +DATE_OF_MODIFICATION + " DESC" ,
					new String[] {});
			if (cursor == null)
				return null;
			cities = new ArrayList<SingleAreaCountry>();
			while (cursor.moveToNext()) {
				SingleAreaCountry country = new SingleAreaCountry();
				String cityName = cursor.getString(cursor
						.getColumnIndex(AppConstants.area_by_city_name));
				String cityId = cursor.getString(cursor
						.getColumnIndex(AppConstants.area_by_city_id));
					country.cityId = cityId;
					country.areaName = cityName;
					
				cities.add(country);	

			}
		} catch (Exception exception) {
			Log.e("DB getCountryAreaCities :", exception.getMessage());

		} finally {
			if (cursor != null)
				cursor.close();
		}

		// cursor.close();

		return cities;

		
	}
	*/
	
	

	
	
	public void removeRestaurant(int restaurantID){
		mDb.delete(RESTAURANT_TABLE, RESTAURANT_ID + " =?", new String[]{""+restaurantID});
	}
	
	
	public void removeAllRestaurant(){
		//mDb.delete(RESTAURANT_TABLE, RESTAURANT_ID + " =?", new String[]{""+restaurantID});
		mDb.execSQL("delete * from "+ RESTAURANT_TABLE);
	}

	
	
	
	
	public Restaurant getRestaurant(int restaurantID) {

		Cursor cursor = mDb.rawQuery("SELECT * FROM " + RESTAURANT_TABLE
				+ " WHERE " + RESTAURANT_ID + " =? ", new String[] { ""+restaurantID });
		if (cursor == null)
			return null;

		ArrayList<Restaurant> restaurants = getRestaurantList(cursor);
		Log.d("DB", "Restaurant Size:"+restaurants.size());
		if (cursor != null)
			cursor.close();
		if (restaurants != null && restaurants.size() > 0)
			return restaurants.get(0);
		return null;
	}
	
	
	public ArrayList<Restaurant> getAllRestaurants() {

		Cursor cursor = mDb.rawQuery("SELECT * FROM " + RESTAURANT_TABLE
		/* + " ORDER BY " +DATE_OF_MODIFICATION + " DESC" */, new String[] {});
		if (cursor == null)
			return null;

		ArrayList<Restaurant> restaurants = getRestaurantList(cursor);

		// cursor.close();

		return restaurants;

	}
	
	
	public ArrayList<String> getAllRestaurantsNames() {
		Cursor cursor = null;
		ArrayList<String> restaurants = null;
		try {

			cursor = mDb.rawQuery("SELECT " + RESTAURANT_NAME + " FROM "
					+ RESTAURANT_TABLE
			/* + " ORDER BY " +DATE_OF_MODIFICATION + " DESC" */,
					new String[] {});
			if (cursor == null)
				return null;
			restaurants = new ArrayList<String>();
			while (cursor.moveToNext()) {

				String restName = cursor.getString(cursor
						.getColumnIndex(RESTAURANT_NAME));
				if (restName != null)
					restaurants.add(restName);

			}
		} catch (Exception exception) {
			Log.e("DB GetRestaurant :", exception.getMessage());

		} finally {
			if (cursor != null)
				cursor.close();
		}

		// cursor.close();

		return restaurants;

	}
	


	public  int getRestaurantCount(){		
		
		Cursor cursor = mDb.rawQuery("SELECT * FROM "	+ RESTAURANT_TABLE 
				/*+" WHERE " +IS_READ +" =? " + " ORDER BY " +DATE_OF_MODIFICATION + " DESC"*/, 
				new String[]{/*UNREAD*/});  // 0 for unread 1 for read
		if(cursor == null)
			return 0;
		return cursor.getCount();
	}
	
	
	
	
	private ArrayList<Restaurant> getRestaurantList(Cursor cursor){		
		
		ArrayList<Restaurant> restaurants  = new ArrayList<Restaurant>();
		while(cursor.moveToNext()){
			Restaurant restaurant = new Restaurant();	
			restaurant.restaurantId = cursor.getInt(cursor.getColumnIndex(RESTAURANT_ID));
			
			restaurant.acceptsCC = cursor.getInt(cursor
					.getColumnIndex(ACCEPTS_CC)) == 1 ? true : false;
			
			restaurant.acceptsCash = cursor.getInt(cursor.getColumnIndex(ACCEPTS_CASH)) == 1 ? true :false;
			
			restaurant.acceptsKNet = cursor.getInt(cursor.getColumnIndex(ACCEPTS_KNET)) == 1 ? true : false;
			
			restaurant.cuisine = cursor.getString(cursor.getColumnIndex(CUISINE));
			
			restaurant.deliveryCharge = cursor.getDouble(cursor.getColumnIndex(DELIVERY_CHARGES));
			
			restaurant.deliveryTime = cursor.getString(cursor.getColumnIndex(DELIVERY_TIME));
			
			restaurant.minAmount = cursor.getString(cursor.getColumnIndex(MIN_AMOUNT));
			restaurant.minimumAmount = cursor.getString(cursor.getColumnIndex(MINIMUM_AMOUNT));
			restaurant.rating = cursor.getString(cursor.getColumnIndex(RATING));
			restaurant.restaurantLogoUrl = cursor.getString(cursor.getColumnIndex(RESTAURANT_LOGO_URL));
			restaurant.restaurantName = cursor.getString(cursor.getColumnIndex(RESTAURANT_NAME));
			restaurant.restaurantStatus = cursor.getString(cursor.getColumnIndex(RESTAURANT_STATUS));
			restaurant.summary = cursor.getString(cursor.getColumnIndex(SUMMARY));
			restaurant.workingHour = cursor.getString(cursor.getColumnIndex(WORKING_HOUR));
			restaurant.u3anCharge = cursor.getInt(cursor.getColumnIndex(U3AN_CHARGE));
			
			
			restaurants.add(restaurant);
		}		
		
		
		return restaurants;
	}
	
	
	
	
	
	
	/******************************cart details*********************************/
	
public void insertOrUpdateCartItem(CartItem cartItem){
		
		Log.d("DBINSERTTEST", " Addding to cart  Start ---");		
	
		String sql = "INSERT INTO "+LOCAL_CART_DETAILS_TABLE+ "("
				+AppConstants.ORDERED_ITEM_ID +","
				+AppConstants.ORDERED_ITEM_NAME + ","
				+AppConstants.ORDERED_ITEM_PRICE +","
				+AppConstants.ORDERED_ITEM_QUANTITY + ","
				+AppConstants.ORDERED_ITEM_SPECIAL_REQUEST 
				+")"+ "VALUES (?, ?, ?, ?, ?)";
				
				mDb.beginTransaction();
				SQLiteStatement stmt = mDb.compileStatement(sql);
				
		String updateSql = "UPDATE "+LOCAL_CART_DETAILS_TABLE +" SET "				
				+AppConstants.ORDERED_ITEM_ID +" = ?1, "
				+AppConstants.ORDERED_ITEM_NAME +" = ?2, "
				+AppConstants.ORDERED_ITEM_PRICE +" = ?3, "
				+AppConstants.ORDERED_ITEM_QUANTITY +" = ?4, "
				+AppConstants.ORDERED_ITEM_SPECIAL_REQUEST +" = ?5 "
				+" WHERE  " +AppConstants.ORDERED_ITEM_ID +" = ?6 ";
								
				SQLiteStatement updateStmt = mDb.compileStatement(updateSql);
				Cursor cursor;
				
		 try{
			
			
					cursor = mDb.rawQuery("SELECT * FROM "
							+ LOCAL_CART_DETAILS_TABLE + " WHERE "+ AppConstants.ORDERED_ITEM_ID + "=?", new String[]{""+cartItem.itemId});
					
					if(cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) { //Album entry is already available .So update if needed
						
						//long modifiedMillisecs = cursor.getLong(cursor.getColumnIndex(DATE_OF_MODIFICATION));
						
						//if(album.getLastModifiedDate().getTime() > modifiedMillisecs) {		
							
						    //if(fwNotification.getItemId() != null)
						    	updateStmt.bindLong(1, cartItem.itemId);
						    						    	
						    //if(fwNotification.getNotiType() != null)
						    	updateStmt.bindString(2, cartItem.itemName);
						    //if(fwNotification.getPayLoad() != null)
						    	updateStmt.bindString(3, cartItem.itemPrice);		    
						    //if(fwNotification.getFrom() != null)
						    	updateStmt.bindString(4,cartItem.itemQty);
						    //if(fwNotification.getWakeLockId() != null)
						    	updateStmt.bindString(5, cartItem.itemSpecialRequest);
						    //if(fwNotification.isRead() != null)
						    	updateStmt.bindLong(6, cartItem.itemId);
						   
							
						   	Log.d("DBUPDATE", " Addding nofication---");
							
						   	updateStmt.executeUpdateDelete();
						   	updateStmt.clearBindings();
						/*}
						else{
							continue;
						}*/
					}
					
					else{		
						//if(fwNotification.getItemId() != null)
						stmt.bindLong(1, cartItem.itemId);
				    						    	
				    //if(fwNotification.getNotiType() != null)
						stmt.bindString(2, cartItem.itemName);
				    //if(fwNotification.getPayLoad() != null)
						stmt.bindString(3, cartItem.itemPrice);		    
				    //if(fwNotification.getFrom() != null)
						stmt.bindString(4,cartItem.itemQty);
				    //if(fwNotification.getWakeLockId() != null)
						stmt.bindString(5, cartItem.itemSpecialRequest);

					   	
					   	Log.d("DBINSERTTEST", " Addding notification---");
						
						stmt.execute();
						stmt.clearBindings();
					}
			
	
			mDb.setTransactionSuccessful();
			mDb.endTransaction();
			
			Log.d("DBINSERTTEST", " Addding Albuminfo  Finshed ---");
		 }catch (Exception e) {
			 Log.e("DBINSERTTEST", " !!!!!!!!!!!!!! !!!!! excettion Addding notification---"+e.getLocalizedMessage());
				e.printStackTrace();
		 }	

	}
	

   
	
	public SQLiteDatabase getDb() {
		synchronized(this) {
			return mDb;
		}
	}
	
	
	public  void wipeOffData(){
		Log.d("WipingData","Wiping Data started");
		try{
			mDb.execSQL("delete  from "+ RESTAURANT_TABLE);	
		}catch(Exception ex){
			Log.d("WipingData","Wiping Data Failed");
		}
		Log.d("WipingData","Wiping Data success");
		
	}
	
	
}
