package com.mawaqaa.u3an.db;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class OpenHelper extends SQLiteOpenHelper {
	static final String dbname = "signupdatadatabase1";
	static final String Tablename = "signup_data";

	static final String key_id = "key_id";
	static final String email = "email";
	static final String password = "password";
	static final String first_name = "first_name";
	static final String last_name = "last_name";
	static final String phone = "phone";
	static final String home_phone = "home_phone";

	static final String Tablenamestage2 = "Tablenamestage2";
	static final String address_Type = "address_Type";
	static final String key_id_ = "key_id_";
	static final String Area = "Area";
	static final String ProfileName = "ProfileName";
	static final String Block = "Block";
	static final String Judda = "Judda";
	static final String Street = "Street";
	static final String HouseNo = "HouseNo";
	static final String Floor = "Floor";
	static final String Apartment = "Apartment";
	static final String Directions = "Directions";

	static final String tablenameCartData = "tablenameCartData";
	static final String key_id_cart = "key_id_cart";
	static final String restaurant_ID = "restaurant_ID";
	static final String restaurant_name = "restaurant_name";
	static final String restaurant_thumbnail = "restaurant_thumbnail";
	static final String dishname = "dishname";
	static final String dish_image = "dish_image";
	static final String AreaId = "AreaId";
	static final String ItemChoiceIds = "ItemChoiceIds";
	static final String ItemChoiceQty = "ItemChoiceQty";
	static final String ItemId = "ItemId";
	static final String Quantity = "Quantity";
	static final String price = "price";
	static final String minimumAmount = "minimumAmount";

	static final String tablenameConfirmDelivery = "tablenameConfirmDelivery";
	static final String key_id_ConfirmDelivery = "key_id_ConfirmDelivery";
	static final String tablenameAddressType = "tablenameAddressType";
	static final String tablenameArea = "tablenameArea";
	static final String tablenameBlock = "tablenameBlock";
	static final String tablenameStreet = "tablenameStreet";
	static final String tablenamejudda = "tablenamejudda";
	static final String tablenameFloor = "tablenameFloor";
	static final String tablenameApartment = "tablenameApartment";
	static final String tablenameDirection = "tablenameDirection";

	public OpenHelper(Context context) {
		super(context, dbname, null, 33);
		Log.e("DataBase", "opened");
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + Tablename + "(" + key_id
				+ " INTEGER NOT NULL PRIMARY KEY, " + email + " TEXT  , "
				+ password + " TEXT, " + first_name + " TEXT, " + last_name
				+ " TEXT, " + phone + " TEXT, " + home_phone + " TEXT)");

		db.execSQL("CREATE TABLE " + Tablenamestage2 + "(" + key_id_
				+ " INTEGER NOT NULL PRIMARY KEY, " + Area + " TEXT  , "
				+ ProfileName + " TEXT, " + address_Type + " TEXT, " + Block
				+ " TEXT, " + Judda + " TEXT, " + Street + " TEXT, " + HouseNo
				+ " TEXT,  " + Floor + " TEXT,  " + Apartment + " TEXT,  "
				+ Directions + " TEXT)");

		db.execSQL("CREATE TABLE " + tablenameCartData + "(" + key_id_cart
				+ " INTEGER NOT NULL PRIMARY KEY, " + restaurant_ID
				+ " TEXT  , " + restaurant_name + " TEXT, "
				+ restaurant_thumbnail + " TEXT, " + dishname + " TEXT, "
				+ dish_image + " TEXT, " + AreaId + " TEXT, " + ItemChoiceIds
				+ " TEXT,  " + ItemChoiceQty + " TEXT,  " + minimumAmount
				+ " TEXT, " + price + " TEXT,  " + ItemId + " TEXT,  "
				+ Quantity + " TEXT)");

		db.execSQL("CREATE TABLE " + tablenameConfirmDelivery + "("
				+ key_id_ConfirmDelivery + " INTEGER NOT NULL PRIMARY KEY, "
				+ tablenameAddressType + " TEXT  , " + tablenameArea
				+ " TEXT, " + tablenameBlock + " TEXT, " + tablenameStreet
				+ " TEXT, " + tablenamejudda + " TEXT, " + tablenameFloor
				+ " TEXT, " + tablenameApartment + " TEXT,  "
				+ tablenameDirection + " TEXT)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {

	}

	/************************* Confirm order table *****************************************/
	// Insert into the table
	public void InsertItemDetailsToConfirmDelivery(String AddressType,
			String area, String block, String street, String judda,
			String floor, String apartment, String direction) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(tablenameAddressType, AddressType);
		cv.put(tablenameArea, area);
		cv.put(tablenameBlock, block);
		cv.put(tablenameStreet, street);
		cv.put(tablenamejudda, judda);
		cv.put(tablenameFloor, floor);
		cv.put(tablenameApartment, apartment);
		cv.put(tablenameDirection, direction);

		db.insert(tablenameConfirmDelivery, tablenameAddressType, cv);
		db.close();
	}

	// Retrieve data from database
	public ArrayList<HashMap<String, String>> order_Confirmation_Data() {

		SQLiteDatabase db = getReadableDatabase();
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

		Cursor cursor = db.rawQuery("SELECT  " + key_id_ConfirmDelivery
				+ " as _id ," + tablenameAddressType + ", " + tablenameArea
				+ "," + tablenameBlock + ", " + tablenameStreet + ", "
				+ tablenamejudda + ", " + tablenameFloor + ", "
				+ tablenameApartment + ", " + tablenameDirection + " from "
				+ tablenameConfirmDelivery, new String[] {});

		if (cursor != null) {
			if (cursor.moveToFirst()) // movies first column
			{
				do {
					HashMap<String, String> map = new HashMap<String, String>();
					String fnam = cursor.getString(cursor
							.getColumnIndex("tablenameAddressType"));
					String lnam = cursor.getString(cursor
							.getColumnIndex("tablenameArea"));
					String fnam1 = cursor.getString(cursor
							.getColumnIndex("tablenameBlock"));
					String lnam1 = cursor.getString(cursor
							.getColumnIndex("tablenameStreet"));
					String lnam2 = cursor.getString(cursor
							.getColumnIndex("tablenamejudda"));
					String lnam3 = cursor.getString(cursor
							.getColumnIndex("tablenameFloor"));
					String lnam4 = cursor.getString(cursor
							.getColumnIndex("tablenameApartment"));
					String lnam5 = cursor.getString(cursor
							.getColumnIndex("tablenameDirection"));

					map.put(tablenameAddressType, fnam);
					map.put(tablenameArea, lnam);
					map.put(tablenameBlock, fnam1);
					map.put(tablenameStreet, lnam1);
					map.put(tablenamejudda, lnam2);
					map.put(tablenameFloor, lnam3);
					map.put(tablenameApartment, lnam4);
					map.put(tablenameDirection, lnam5);

					list.add(map);

					Log.e("Done", "Added to database");
				} while (cursor.moveToNext()); // move to next row
			}
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}

	/************************* Confirm order table *****************************************/

	/************************* CART TABLE OPERATIONS ***********************************/

	// Insert into the table
	public void InsertItemDetailsToCart(String restaurantID,
			String restaurant_Name, String restaurant_Thumbnail,
			String dishName, String dish_Image, String area_ID,
			String item_Choice_ID, String item_Choice_Quantity, String item_ID,
			String quantity, String minimumamnt, String price1) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(restaurant_ID, restaurantID);
		cv.put(restaurant_name, restaurant_Name);
		cv.put(restaurant_thumbnail, restaurant_Thumbnail);
		cv.put(dishname, dishName);
		cv.put(dish_image, dish_Image);
		cv.put(AreaId, area_ID);
		cv.put(ItemChoiceIds, item_Choice_ID);
		cv.put(ItemChoiceQty, item_Choice_Quantity);
		cv.put(minimumAmount, minimumamnt);
		cv.put(price, price1);
		cv.put(ItemId, item_ID);
		cv.put(Quantity, quantity);

		db.insert(tablenameCartData, restaurant_ID, cv);
		db.close();
	}

	// Retrieve data from database
	public ArrayList<HashMap<String, String>> cart_Data() {

		SQLiteDatabase db = getReadableDatabase();
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

		Cursor cursor = db.rawQuery("SELECT  " + key_id_cart + " as _id ,"
				+ restaurant_name + ", " + restaurant_ID + ","
				+ restaurant_thumbnail + ", " + dishname + ", " + dish_image
				+ ", " + AreaId + ", " + ItemChoiceIds + ", " + ItemChoiceQty
				+ ", " + minimumAmount + ", " + price + ", " + ItemId + ", "
				+ Quantity + " from " + tablenameCartData + " ORDER BY "
				+ restaurant_ID + " ASC", new String[] {});

		if (cursor != null) {
			if (cursor.moveToFirst()) // movies first column
			{
				do {
					HashMap<String, String> map = new HashMap<String, String>();
					String fnam = cursor.getString(cursor
							.getColumnIndex("restaurant_ID"));
					String lnam = cursor.getString(cursor
							.getColumnIndex("restaurant_name"));
					String fnam1 = cursor.getString(cursor
							.getColumnIndex("restaurant_thumbnail"));
					String lnam1 = cursor.getString(cursor
							.getColumnIndex("dishname"));
					String lnam2 = cursor.getString(cursor
							.getColumnIndex("dish_image"));
					String lnam3 = cursor.getString(cursor
							.getColumnIndex("AreaId"));
					String lnam4 = cursor.getString(cursor
							.getColumnIndex("ItemChoiceIds"));
					String lnam5 = cursor.getString(cursor
							.getColumnIndex("ItemChoiceQty"));
					String lnamdup = cursor.getString(cursor
							.getColumnIndex("minimumAmount"));
					String lnamPrice = cursor.getString(cursor
							.getColumnIndex("price"));
					String lnam6 = cursor.getString(cursor
							.getColumnIndex("ItemId"));
					String lnam7 = cursor.getString(cursor
							.getColumnIndex("Quantity"));

					map.put(restaurant_ID, fnam);
					map.put(restaurant_name, lnam);
					map.put(restaurant_thumbnail, fnam1);
					map.put(dishname, lnam1);
					map.put(dish_image, lnam2);
					map.put(AreaId, lnam3);
					map.put(ItemChoiceIds, lnam4);
					map.put(ItemChoiceQty, lnam5);
					map.put(minimumAmount, lnamdup);
					map.put(price, lnamPrice);
					map.put(ItemId, lnam6);
					map.put(Quantity, lnam7);

					list.add(map);

					Log.e("Done", "Added to database");
				} while (cursor.moveToNext()); // move to next row
			}
		}
		if (cursor != null && !cursor.isClosed()) {
			cursor.close();
		}
		return list;
	}

	/************************* CART TABLE OPERATIONS ***********************************/

	public void DataInsertionStageTwo(String area, String profilename,
			String addressType, String block, String judda, String street,
			String houseNo, String floor, String apartment, String direction) {

		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();

		cv.put(Area, area);
		cv.put(ProfileName, profilename);
		cv.put(address_Type, addressType);
		cv.put(Block, block);
		cv.put(Judda, judda);
		cv.put(Street, street);
		cv.put(HouseNo, houseNo);
		cv.put(Floor, floor);
		cv.put(Apartment, apartment);
		cv.put(Directions, direction);

		db.insert(Tablenamestage2, Area, cv);
		db.close();

		Log.e("DB", "Inset_User OK" + addressType);
	}

	// Data Insertion here
	public void DetailsInsertion(String email1, String password1,
			String first_Name, String last_Name, String phone_num,
			String homephone) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(email, email1);
		cv.put(password, password1);
		cv.put(first_name, first_Name);
		cv.put(last_name, last_Name);
		cv.put(phone, phone_num);
		cv.put(home_phone, homephone);

		db.insert(Tablename, email1, cv);
		db.close();

		Log.e("DB", "Inset_User OK");
	}

	// Data second stage retrieve here
	public ArrayList<HashMap<String, String>> sign_up_Data_SecondStage_Retrieval() {
		SQLiteDatabase db = getReadableDatabase();
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();

		Cursor cursor = db.rawQuery("SELECT  " + key_id_ + " as _id,  " + Area
				+ ", " + ProfileName + ", " + address_Type + ", " + Block
				+ ", " + Judda + ", " + Street + ", " + HouseNo + ", " + Floor
				+ ", " + Apartment + ", " + Directions + " from "
				+ Tablenamestage2, new String[] {});

		if (cursor != null) {
			if (cursor.moveToFirst()) // movies first column
			{
				do {
					HashMap<String, String> map = new HashMap<String, String>();					
					String fnam = cursor.getString(cursor
							.getColumnIndex("Area"));
					String lnam = cursor.getString(cursor
							.getColumnIndex("ProfileName"));
					String fnam1 = cursor.getString(cursor
							.getColumnIndex("address_Type"));
					String lnam1 = cursor.getString(cursor
							.getColumnIndex("Block"));
					String lnam2 = cursor.getString(cursor
							.getColumnIndex("Judda"));
					String lnam3 = cursor.getString(cursor
							.getColumnIndex("Street"));
					String lnam4 = cursor.getString(cursor
							.getColumnIndex("HouseNo"));
					String lnam5 = cursor.getString(cursor
							.getColumnIndex("Floor"));
					String lnam6 = cursor.getString(cursor
							.getColumnIndex("Apartment"));
					String lnam7 = cursor.getString(cursor
							.getColumnIndex("Directions"));
					map.put(Area, fnam);
					map.put(ProfileName, lnam);
					map.put(address_Type, fnam1);
					map.put(Block, lnam1);
					map.put(Judda, lnam2);
					map.put(Street, lnam3);
					map.put(HouseNo, lnam4);
					map.put(Floor, lnam5);
					map.put(Apartment, lnam6);
					map.put(Directions, lnam7);
					list.add(map);
					Log.e("Done", "Added to database");
				} while (cursor.moveToNext()); // move to next row
			}
		}
		if (cursor != null && !cursor.isClosed())

		{
			cursor.close();
		}
		return list;
	}

	// Image Data retrieve here
	public ArrayList<HashMap<String, String>> temporary_Contact_Records() {
		SQLiteDatabase db = getReadableDatabase();
		ArrayList<HashMap<String, String>> list = new ArrayList<HashMap<String, String>>();
		Cursor cursor = db.rawQuery("SELECT  " + key_id + " as _id,  " + email
				+ ", " + password + ", " + first_name + ", " + last_name + ", "
				+ phone + ", " + home_phone + " from " + Tablename,
				new String[] {});
		if (cursor != null) {
			if (cursor.moveToFirst()) // movies first column
			{
				do {
					HashMap<String, String> map = new HashMap<String, String>();

					String fnam = cursor.getString(cursor
							.getColumnIndex("email"));
					String lnam = cursor.getString(cursor
							.getColumnIndex("password"));
					String lnam1 = cursor.getString(cursor
							.getColumnIndex("first_name"));
					String lnam2 = cursor.getString(cursor
							.getColumnIndex("last_name"));
					String lnam3 = cursor.getString(cursor
							.getColumnIndex("phone"));
					String lnam4 = cursor.getString(cursor
							.getColumnIndex("home_phone"));

					map.put(email, fnam);
					map.put(password, lnam);
					map.put(first_name, lnam1);
					map.put(last_name, lnam2);
					map.put(phone, lnam3);
					map.put(home_phone, lnam4);

					list.add(map);
				} while (cursor.moveToNext()); // move to next row
			}
		}
		if (cursor != null && !cursor.isClosed())
		{ 
			cursor.close();
		}
		return list;
	}

	// Delete Audio Row
	public void Deletrow(String rolls) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Tablename, email + "=?", new String[] { rolls });
		db.close();
		Log.e("Delete", "this token row" + rolls);
	}

	// Delete Audio Row
	public void DeletrowSecondStage(String rolls) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(Tablenamestage2, ProfileName + "=?", new String[] { rolls });
		db.close();
		Log.e("Delete", "this token row" + rolls);
	}
}