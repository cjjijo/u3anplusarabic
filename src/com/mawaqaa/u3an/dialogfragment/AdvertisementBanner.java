package com.mawaqaa.u3an.dialogfragment;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.mawaqaa.u3an.R;

public class AdvertisementBanner extends DialogFragment{
	public static AdvertisementBanner newInstance() {

		String title = "My Advertisement";

		AdvertisementBanner f = new AdvertisementBanner();
		Bundle args = new Bundle();
		args.putString("title", title);
		f.setArguments(args);		
		return f;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		final Dialog myDialog = new Dialog(getActivity());
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.advertisement);
/*
		Button facebookButton = (Button) myDialog
				.findViewById(R.id.facebookbtnsocial1);
		Button twitterButton = (Button) myDialog
				.findViewById(R.id.twitterbtnsocial1);
		Button instagramButton = (Button) myDialog
				.findViewById(R.id.instagrambtnsocial1);
	
		TextView facebookText = (TextView) myDialog
				.findViewById(R.id.textview1social);
		TextView twitterText = (TextView) myDialog
				.findViewById(R.id.textview2social);
		TextView instagramText = (TextView) myDialog
				.findViewById(R.id.textview3social);*/
	/*	
		facebookButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseActivity) getActivity()).fbSelected();
			}
		});

		twitterButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseActivity) getActivity()).twitterSelected();
			}
		});

		instagramButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseActivity) getActivity()).instaGram();
			}
		});

		facebookText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseActivity) getActivity()).fbSelected();
			}
		});

		twitterText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseActivity) getActivity()).twitterSelected();
			}
		});

		instagramText.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				((BaseActivity) getActivity()).instaGram();
			}
		});*/

		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;
		myDialog.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		myDialog.getWindow().setLayout(
				getActivity().getResources().getDisplayMetrics().widthPixels,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		myDialog.show();
		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;

		return myDialog;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	//	setDialogPosition();
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private void setDialogPosition() {
		SharedPreferences sp = getActivity().getSharedPreferences("lan",
				Context.MODE_PRIVATE);
		String lan = sp.getString("lan", "en");

		Window window = this.getDialog().getWindow();
		
		if (lan.equals("en"))
			window.setGravity(Gravity.BOTTOM | Gravity.LEFT);
		else
			window.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
		
		WindowManager.LayoutParams params = window.getAttributes();
	//	params.y = dpToPx(-10);
		window.setAttributes(params);
	}

	private int dpToPx(int dp) {
		DisplayMetrics metrics = getActivity().getResources()
				.getDisplayMetrics();
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				metrics);
	}
}
