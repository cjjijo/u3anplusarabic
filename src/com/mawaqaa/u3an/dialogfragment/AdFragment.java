package com.mawaqaa.u3an.dialogfragment;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.mawaqaa.u3an.R;

public class AdFragment extends DialogFragment {

	private String TAG = "FashionWorld::AdFragment";
	private Bitmap adImage;
	private String Adlink;
	private String strAdTime;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setStyle(STYLE_NO_FRAME, 0);
		if (getDialog() == null)
			return;
		getDialog().getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			// fragListener = (FragListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement OnHeadlineSelectedListener");
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		Window window = getDialog().getWindow();
		WindowManager.LayoutParams params = window.getAttributes();
		params.dimAmount = 0.5f; // dim only a little bit
		params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		window.setAttributes(params);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView()");

		View view = inflater.inflate(R.layout.fragment_ad, container, false);
		ImageView adImageview = (ImageView) view.findViewById(R.id.ad_image);
		adImageview.setImageBitmap(adImage);
		view.findViewById(R.id.close_btn).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						if (getDialog() != null && getDialog().isShowing())
							getDialog().dismiss();
					}
				});
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					if (Adlink != null) {
						Intent browserIntent = new Intent(Intent.ACTION_VIEW,
								Uri.parse(Adlink));
						startActivity(browserIntent);
					}
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		});
		getDialog().setCanceledOnTouchOutside(false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.d(TAG, "onActivityCreated()");
		super.onActivityCreated(savedInstanceState);
		getDialog().getWindow().getAttributes().windowAnimations = R.style.dialogueAnim;
	};

	@Override
	public void onResume() {
		super.onResume();
		// Utilities.brandAdSHowed(getActivity());
		startTimer();
	}

	public void setAdObj(Bitmap b, String AdLink, String adTime) {
		this.adImage = b;
		this.Adlink = AdLink;
		strAdTime = adTime;
	}

	private void startTimer() {
		int adTime = 0;
		if (strAdTime != null && !TextUtils.isEmpty(strAdTime))
			adTime = Integer.parseInt(strAdTime);

		new Handler().postDelayed(adClosingrunnable, adTime * 1000);
	}

	private Runnable adClosingrunnable = new Runnable() {
		@Override
		public void run() {
			Log.d(TAG, "adClosingrunnable");
			if (getDialog() != null && getDialog().isShowing())
				getDialog().dismiss();
		}
	};

}
