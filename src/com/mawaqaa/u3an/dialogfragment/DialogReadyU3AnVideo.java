package com.mawaqaa.u3an.dialogfragment;

import com.mawaqaa.u3an.R;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutCompat.LayoutParams;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.VideoView;

public class DialogReadyU3AnVideo extends DialogFragment {
	private String TAG = "DialogReadyU3AnVideo";
	private VideoView videoViewMenuReadyUan;
	private DisplayMetrics dm;
	private SurfaceView sur_View;
	private MediaController media_Controller;
	private String uriPath;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// String title = getArguments().getString("title");
		final Dialog myDialog = new Dialog(getActivity());
		myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myDialog.setContentView(R.layout.layout_dialog_show_menu_ready_video);
		
		videoViewMenuReadyUan = (VideoView) myDialog.findViewById(R.id.videoViewMenuReadyUan);
		uriPath = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.you3an_run2small;

		videoViewMenuReadyUan.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				// Do whatever u need to do here
				myDialog.dismiss();
			}
		});

		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;
		myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		myDialog.getWindow().setLayout(getActivity().getResources().getDisplayMetrics().widthPixels,
				ViewGroup.LayoutParams.WRAP_CONTENT);
		myDialog.show();
		myDialog.getWindow().getAttributes().windowAnimations = R.style.dialoganimation;
		playVideo();
		return myDialog;
	}

	private void playVideo() {

		media_Controller = new MediaController(getActivity());
		dm = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
		int height = dm.heightPixels;
		int width = dm.widthPixels;
		// videoViewSplash.setMinimumWidth(width);
		// videoViewSplash.setMinimumHeight(height);
		// videoViewSplash.setMediaController(media_Controller);

		MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.u3anrun);
		videoViewMenuReadyUan.setVideoPath(uriPath);
// Jijo Video
		/*RelativeLayout.LayoutParams videoviewlp = new RelativeLayout.LayoutParams(500, 500);
	    videoviewlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
	    videoviewlp.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
	    videoviewlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
	    videoViewReadyUan.setLayoutParams(videoviewlp);
	    videoViewReadyUan.invalidate();*/
//		
		videoViewMenuReadyUan.start();
		mp.start();
	
	}

	@Override
	public void onResume() {
		super.onResume();
		Window window = getDialog().getWindow();
		window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		window.setGravity(Gravity.CENTER);
		// TODO:
	}
	/*
	 * @Override public void onStart() { super.onStart(); Window window =
	 * getDialog().getWindow(); WindowManager.LayoutParams params =
	 * window.getAttributes(); params.dimAmount = 0.2f; // dim only a little bit
	 * params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND; params.width
	 * = 750; params.height = 1000;
	 * 
	 * params.width = WindowManager.LayoutParams.WRAP_CONTENT; params.height =
	 * WindowManager.LayoutParams.WRAP_CONTENT; window.setAttributes(params);
	 * //window.setLayout(750, 1000); }
	 */

}
