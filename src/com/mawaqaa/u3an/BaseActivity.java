package com.mawaqaa.u3an;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.json.JSONObject;

import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.data.Restaurant;
import com.mawaqaa.u3an.db.U3AnDBAdapter;
import com.mawaqaa.u3an.fragments.AllRestaurantsFragment;
import com.mawaqaa.u3an.fragments.BaseFragment;
import com.mawaqaa.u3an.fragments.HomeFragment;
import com.mawaqaa.u3an.fragments.KnetForGiftvoucher;
import com.mawaqaa.u3an.fragments.LiveChatFragment;
import com.mawaqaa.u3an.fragments.PaymentKNETPage;
import com.mawaqaa.u3an.utilities.Utilities;
import com.mawaqaa.u3an.volley.VolleyUtils;
import com.mawaqaa.u3an.volley.u3annResponse;

public class BaseActivity extends ActionBarActivity {

	public BaseFragment baseFragment;
	private static final String TAG = "u3anBaseActivity";

	private static BaseActivity baseActivity;
	private U3AnDBAdapter mDb;
	public String FILENAMEAUTHKEY = "authentication_key.txt";
	String FILENAMENOTIFICATION = "notificationLog.txt";

	public BaseActivity currentActivity;
	String FILENAMELOG = "keeplogin.txt";

	
	
	public enum DISPLAY_MODE {
		GRID, LIST
	}

	public DISPLAY_MODE curr_Disp_Mode = DISPLAY_MODE.GRID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		baseActivity = this;
		VolleyUtils.init(this);
		mDb = new U3AnDBAdapter(this);
		mDb.open();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mDb.close();
	}

	public void setupUI(View view, boolean isHeading) {
		if (!(view instanceof EditText)) {
			view.setOnTouchListener(new OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					Utilities.hide_keyboard(BaseActivity.this);
					return false;
				}
			});
		}

		if ((view instanceof TextView)) {
			((TextView) view).setTypeface(isHeading ? getHedingTypeFace()
					: getRegularTypeFace());
		}
		if ((view instanceof Button)) {
			((Button) view).setTypeface(isHeading ? getHedingTypeFace()
					: getRegularTypeFace());
		}

		// If a layout container, iterate over children and seed recursion.
		if (view instanceof ViewGroup) {
			for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
				View innerView = ((ViewGroup) view).getChildAt(i);
				setupUI(innerView, isHeading);
			}
		}
	}

	Typeface getRegularTypeFace() {
		return Typeface.createFromAsset(getAssets(),
				getString(R.string.regular_typeface));
	}

	Typeface getBoldTypeFace() {
		return Typeface.createFromAsset(getAssets(),
				getString(R.string.bold_typeface));
	}

	Typeface getHedingTypeFace() {
		return Typeface.createFromAsset(getAssets(),
				getString(R.string.menubar_typeface));
	}

	public BaseActivity getCurrActivity() {
		return currentActivity;
	}
//********************************************************************************************
	
	
	
	public void pushFragments(Fragment fragment, boolean shouldAnimate,
			boolean shouldAdd) {
		FragmentManager manager = getSupportFragmentManager();
		String backStateName = fragment.getClass().getName();
		Log.d(TAG, "backStateName :" + backStateName);
		if (baseFragment != null)
			Log.d(TAG, "baseFragment :" + baseFragment.getClass().getName());
	//Jijo	
		
		/*Fragment currentFragment = manager.findFragmentByTag(TAG);
	    FragmentTransaction fragTransaction = manager.beginTransaction();
	    fragTransaction.detach(currentFragment);
	    fragTransaction.attach(currentFragment);
	    fragTransaction.commit();*/
		
		
		if (fragment instanceof AllRestaurantsFragment ) {
			   boolean fragmentPopped =
			   manager.popBackStackImmediate(backStateName, 0);
			   if (!fragmentPopped) { // fragment not in back stack, create it.
			   FragmentTransaction ft = manager.beginTransaction();
			   if (shouldAnimate)
			    ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
			   ft.replace(R.id.fragment_container, fragment);
			   if (shouldAdd)
			    ft.addToBackStack(backStateName);
			   ft.commit();
			   manager.executePendingTransactions();
		}}
/////		
		else if (isNeedTransaction(backStateName)) {
			boolean fragmentPopped = manager.popBackStackImmediate(
					backStateName, 0);

			if (!fragmentPopped) { // fragment not in back stack, create it.
				FragmentTransaction ft = manager.beginTransaction();
				if (shouldAnimate)
					ft.setCustomAnimations(R.anim.slide_in_right,
							R.anim.slide_out_left);
				ft.replace(R.id.fragment_container, fragment);
				if (shouldAdd)
					ft.addToBackStack(backStateName);
				ft.commit();
				manager.executePendingTransactions();
			} else
				Log.d(TAG, "fragmentPopped :" + fragmentPopped);
		} else {
			Log.d(TAG, "already in same fragment no need to call it again :");
		}
	}
	
//***********************************************************************************************************
	public void pushFragments4LanSwitch(Fragment fragment, boolean shouldAnimate) {
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		if (shouldAnimate)
			ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
		ft.replace(R.id.fragment_container, fragment);
		ft.commit();
	}

	public void popFragments(Fragment frag) {
		Log.e("Enterd here", "Inside pop fragment");

		try {
			FragmentManager manager = getSupportFragmentManager();
			FragmentTransaction ft = manager.beginTransaction();
			String fragName = frag.getClass().getName();
			manager.popBackStack(fragName,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			ft.remove(frag);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void WriteDataToFile(String content) {

		FileOutputStream fosdummyy;
		try {
			fosdummyy = this.openFileOutput(FILENAMEAUTHKEY,
					Context.MODE_PRIVATE);
			fosdummyy.write(content.getBytes());
			fosdummyy.close();
		} catch (Exception e) {

		}
	}

	public String readFile() {
		String data = null;
		try {
			FileInputStream fis = baseActivity.openFileInput(FILENAMEAUTHKEY);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			data = br.readLine();
		} catch (Exception e) {
		}
		return data;
	}

	private boolean isNeedTransaction(String backStateName) {
		boolean needTransaction = true;
		if (baseFragment != null) {
			String baseFrag = baseFragment.getClass().getName();
			if (baseFrag.equals(backStateName)) {

				needTransaction = false;
			}
			/*
			 * there is an exceptional case, if we are in login status page, we
			 * are using child fragment in it, so the "babtainBaseFragment" will
			 * be child Fragment. But we are not adding the child fragment to
			 * the stack for handling back press. adding a work around to
			 * handling this case.
			 */
			/*
			 * else if ((baseFrag.equals(LoginFragment.class.getName()) ||
			 * baseFrag.equals(RegisterFragment.class.getName()) || baseFrag
			 * .equals(ProfileFragment.class.getName())) && backStateName
			 * .equals(LoginStatusFragment.class.getName())) { needTransaction =
			 * false; }
			 */
			else
				needTransaction = true;
		}
		return needTransaction;
	}

	public void popFragments() {
		super.onBackPressed();
	}

	/* showing and dismissing Dialogue Fragment */
	public void showDialogueFragment(DialogFragment dialogFragment) {
		Log.d(TAG, "showDialogueFragment()");
		if (dialogFragment.isVisible()) {
			return;
		}
		String tag = dialogFragment.getClass().getName();
		FragmentManager manager = getSupportFragmentManager();
		if (!dialogFragment.isVisible()) {
			dialogFragment.setCancelable(true);
			dialogFragment.show(manager, tag);
		}
	}

	public void dismissDialogueFrag(DialogFragment dialogFragment) {
		Log.d(TAG, "dismissDialogueFrag()");
		if (dialogFragment != null && dialogFragment.isVisible())
			dialogFragment.dismiss();
	}

	/**************************** DB methods ****************************/

	public void insertOrUpdateRestaurants(ArrayList<Restaurant> restaurants) {
		mDb.insertOrUpdate(restaurants);
	}

	public void removeRestaurantById(int restaurantID) {
		mDb.removeRestaurant(restaurantID);
	}

	public Restaurant getRestaurantById(int restaurantId) {
		return mDb.getRestaurant(restaurantId);
	}

	public ArrayList<Restaurant> getAllRestaurants() {
		return mDb.getAllRestaurants();
	}

	public ArrayList<String> getAllRestaurantnames() {
		return mDb.getAllRestaurantsNames();
	}

	public int getRestaurantCount() {
		return mDb.getRestaurantCount();
	}

	/*
	 * public void insertSingleAreaCountry(SingleAreaCountry singleAreaCountry)
	 * { mDb.insertOrUpdateCountryArea(singleAreaCountry); }
	 * 
	 * 
	 * public ArrayList<SingleAreaCountry> getCountryAreaSpinnerData() { return
	 * mDb.getCountryAreaSpinnerData(); }
	 */

	/*
	 * Imagine if you wanted to get an image selected using ImagePicker intent
	 * to the fragment. Of course I could have created a public function in that
	 * fragment, and called it from the activity. But couldn't resist myself.
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*
		 * switch (requestCode) { case RC_SIGN_IN: if (resultCode == RESULT_OK)
		 * { // If the error resolution was successful we should continue //
		 * processing errors. mSignInProgress = STATE_SIGN_IN; } else { // If
		 * the error resolution was not successful or the user // canceled, //
		 * we should stop processing errors. mSignInProgress = STATE_DEFAULT;
		 * 
		 * }
		 * 
		 * if (!mGoogleApiClient.isConnecting()) { // If Google Play services
		 * resolved the issue with a dialog then // onStart is not called so we
		 * need to re-attempt connection // here. mGoogleApiClient.connect(); }
		 * break; default: super.onActivityResult(requestCode, resultCode,
		 * data); Session.getActiveSession().onActivityResult(this, requestCode,
		 * resultCode, data); break;
		 * 
		 * }
		 */
	}

	public static BaseActivity getBaseActivity() {
		return baseActivity;
	}

	public static void finishAndRestart(String data) {
		baseActivity.finish();
		Intent intent = new Intent(baseActivity, MainActivity.class);
		intent.putExtra("notificationMsg", data);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		baseActivity.startActivity(intent);
	}

	public void serviceResponseSuccess(u3annResponse babtainResponse) {
		String reqUrl = babtainResponse.mReqUrl;
		Log.d(TAG, "serviceResponseSuccess" + reqUrl);
		switch (reqUrl) {
		case AppConstants.get_cusines_url:
			baseFragment
					.onCusinesDataLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.areas_url:
			baseFragment
					.onCountryLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.most_selling_url:
			baseFragment
					.onMostSellingDishesLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.all_restaurants_url:
			baseFragment
					.onAllRestaurantsDataLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.most_selling_restaurants_url:
			baseFragment
					.onMostSellingDishesByRestaurantsLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.area_by_restaurants_url:
			baseFragment
					.onGetAreaByRestaurantsLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.restaurant_details_url:
			baseFragment
					.onRestaurantDetailsLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.login_url:
			baseFragment
					.onLoginDataLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.forget_password_url:
			baseFragment
					.onForgetPasswordDataLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.most_selling_dishes_data:
			baseFragment
					.onMostSellingDishesDataLoadedSuccessfull(babtainResponse.jsonObject);
			break;

		case AppConstants.most_selling_by_cuisines_url:
			baseFragment
					.onMostSellingDishesByCuisinesLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.get_restuarant_by_area:
			baseFragment.onGotResponseByArea(babtainResponse.jsonObject);
			break;

		case AppConstants.new_restaurant_url:
			baseFragment
					.onNewRestaurantsLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.get_restaurant_advance_search_url:
			baseFragment
					.onAdvanceSeachRestaurantSuccessFully(babtainResponse.jsonObject);
			break;

		case AppConstants.most_selling_get_restaurant_menu_url:
			baseFragment
					.onRestaurantMenuLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.placeQuickOrder_URL:
			baseFragment.PlaceQuickSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.cart_count_URL:
			baseFragment.cartCountSuccess(babtainResponse.jsonObject);
			break;

		case AppConstants.get_restaurant_menu_sections_url:

			baseFragment
					.onRestaurantMenuSectionsLoadedSuccessfully(babtainResponse.jsonObject);
			break;

		case AppConstants.sign_up_url:
			baseFragment.SignupSuccess(babtainResponse.jsonObject);
			break;

		case AppConstants.add_restaurant_url:
			baseFragment
					.restaurantAddedSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.get_review_url:
			baseFragment.onNewReviewSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.get_order_count_url:
			baseFragment.onOrderCountGot(babtainResponse.jsonObject);
			break;
		case AppConstants.get_order_details:
			baseFragment.onOrderdetailSuccesfully(babtainResponse.jsonObject);
		case AppConstants.ADDTO_CART_URL:
			baseFragment.addToCartSuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.GET_CART_INFO_URL:
			baseFragment.cartLoadedSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.get_pagedetails_url:
			baseFragment.pageLoadSuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.REMOVE_CART_ITEM_URL:
			baseFragment.removeCartItemSuccess(babtainResponse.jsonObject);

		case AppConstants.REMOVE_TEMP_CART_ITEM_URL:
			baseFragment.removetempCartItemSuccess(babtainResponse.jsonObject);
		case AppConstants.all_restuarent_promotions:
			baseFragment.LoadPromotionsSuccessfully(babtainResponse.jsonObject);
		case AppConstants.get_restuarent_promotions:
			baseFragment
					.LoadPromotionsbyresturentSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.Feedback_frm:
			baseFragment.FeedbackSumbitSuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.getCustomerAddressURL:
			baseFragment.getCustomerAddressSuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.uancredit:
			baseFragment.LoadcrediuanSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.get_GiftVoucher:
			baseFragment.LoadGiftVoucherSuccesfully(babtainResponse.jsonObject);
			break;
		case AppConstants.buy_GiftVoucher:
			baseFragment
					.LoadBuyGiftVoucherSuccesfully(babtainResponse.jsonObject);
			break;
		case AppConstants.getMyFavUrl:
			baseFragment
					.MyFavouriteLoadedSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.removeFromMyFavUrl:
			baseFragment
					.removedFromFavouriteSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.uancreditintervals:
			baseFragment
					.LoadcreditintealsuanSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.deleveryinformation:
			baseFragment.LoaddeleverySuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.myOrdersUrl:
			baseFragment
					.onMyOrdersLoadedSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.chnge_password:
			baseFragment.changePasswordSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.AccountInformation:
			baseFragment.EditAccountSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.postRating:
			baseFragment
					.onItemorRestaurantratingSuccessfully(babtainResponse.jsonObject);
			break;
		case AppConstants.MakeAsPrimary:
			baseFragment.MakeAsPrimarySuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.REMOVE_Address:
			baseFragment.onAddressRemovalSuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.placeOrder_URL:
			baseFragment.placeOrderSuccess(babtainResponse.jsonObject);
			break;
		case AppConstants.EditAddress:
			baseFragment.editAddressSuccess(babtainResponse.jsonObject);
			break;

		case AppConstants.temp_cart_count_URL:
			baseFragment.tempCartCountSuccess(babtainResponse.jsonObject);
			break;

		case AppConstants.AdvertisementBannerUrl:
			baseFragment
					.onAdvertisementLoadedSuccessfully(babtainResponse.jsonObject);
			break;
		default:
			break;
		}
	}

	public void serviceResponseError(u3annResponse babtainResponse) {
		String reqUrl = babtainResponse.mReqUrl;
		Log.d(TAG, "serviceResponseError" + reqUrl);

		switch (reqUrl) {
		case AppConstants.get_cusines_url:
			baseFragment.onCusinesLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.cart_count_URL:
			baseFragment.cartCountFail(babtainResponse.jsonObject);
			break;
		case AppConstants.temp_cart_count_URL:
			baseFragment.tempCartCountFail(babtainResponse.jsonObject);
			break;
		case AppConstants.areas_url:
			baseFragment.onCountryLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.most_selling_url:
			baseFragment
					.onMostSellingDishesLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.all_restaurants_url:
			baseFragment
					.onAllresturantDataLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.most_selling_restaurants_url:
			baseFragment
					.onMostSellingDishesByRestaurantsLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.area_by_restaurants_url:
			baseFragment
					.onGetAreaByRestaurantsLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.restaurant_details_url:
			baseFragment
					.onRestaurantDetailsLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.login_url:
			baseFragment.onLoginDataLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.forget_password_url:
			baseFragment
					.onForgetPasswordDataLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.most_selling_dishes_data:
			baseFragment
					.onMostSellingdishesDataLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.most_selling_by_cuisines_url:
			baseFragment
					.onMostSellingDishesByCuisinesLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.get_restuarant_by_area:
			baseFragment
					.onGettingResponseByAreaFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.new_restaurant_url:
			baseFragment
					.onNewRestaurantsLoadedFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.get_restaurant_advance_search_url:
			baseFragment
					.onAdvanceSeachRestaurantFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.most_selling_get_restaurant_menu_url:
			baseFragment
					.onRestaurantMenuLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.placeQuickOrder_URL:
			baseFragment.PlaceQuickFailed(babtainResponse.jsonObject);
			break;

		case AppConstants.get_restaurant_menu_sections_url:
			baseFragment
					.onRestaurantMenuSectionsLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.sign_up_url:
			baseFragment.SignupFail(babtainResponse.jsonObject);
			break;

		case AppConstants.add_restaurant_url:
			baseFragment.addingRestaurantFail(babtainResponse.jsonObject);
			break;

		case AppConstants.get_review_url:
			baseFragment.onNewReviewFail(babtainResponse.jsonObject);
			break;
		case AppConstants.get_order_count_url:
			baseFragment.onOrderCountGettingFail(babtainResponse.jsonObject);
			break;
		case AppConstants.get_order_details:
			baseFragment.onOrderdetailFail(babtainResponse.jsonObject);
			break;
		case AppConstants.ADDTO_CART_URL:
			baseFragment.addToCartFail(babtainResponse.jsonObject);
			break;
		case AppConstants.GET_CART_INFO_URL:
			baseFragment.cartLoadingFailed(babtainResponse.jsonObject);
			break;
		case AppConstants.get_pagedetails_url:
			baseFragment.pageLoadFail(babtainResponse.jsonObject);
			break;
		case AppConstants.REMOVE_CART_ITEM_URL:
			baseFragment.removeCartItemFail(babtainResponse.jsonObject);
			break;
		case AppConstants.REMOVE_TEMP_CART_ITEM_URL:
			baseFragment.removetempCartItemFail(babtainResponse.jsonObject);
			break;
		case AppConstants.all_restuarent_promotions:
			baseFragment.LoadPromotionsFaill(babtainResponse.jsonObject);
			break;
		case AppConstants.get_restuarent_promotions:
			baseFragment
					.LoadPromotionsbyresturentfail(babtainResponse.jsonObject);
			break;
		case AppConstants.Feedback_frm:
			baseFragment.FeedbackSumbitFail(babtainResponse.jsonObject);
			break;
		case AppConstants.getCustomerAddressURL:
			baseFragment.getCustomerAddressFail(babtainResponse.jsonObject);
			break;
		case AppConstants.uancredit:
			baseFragment.Loadcrediuanfail(babtainResponse.jsonObject);
			break;
		case AppConstants.get_GiftVoucher:
			baseFragment.LoadGiftVoucherFail(babtainResponse.jsonObject);
			break;
		case AppConstants.buy_GiftVoucher:
			baseFragment.LoadBuyGiftVoucherFail(babtainResponse.jsonObject);
			break;
		case AppConstants.getMyFavUrl:
			baseFragment.MyFavouriteLoadingFail(babtainResponse.jsonObject);
			break;
		case AppConstants.removeFromMyFavUrl:
			baseFragment.removedFromFavouriteFail(babtainResponse.jsonObject);
			break;
		case AppConstants.uancreditintervals:
			baseFragment.Loadcreditintealuanfail(babtainResponse.jsonObject);
			break;
		case AppConstants.deleveryinformation:
			baseFragment.Loaddeleveryfail(babtainResponse.jsonObject);
			break;
		case AppConstants.myOrdersUrl:
			baseFragment.onMyOrdersLoadFail(babtainResponse.jsonObject);
			break;
		case AppConstants.chnge_password:
			baseFragment.changePasswordfail(babtainResponse.jsonObject);
			break;
		case AppConstants.AccountInformation:
			baseFragment.EditAccountfail(babtainResponse.jsonObject);
			break;
		case AppConstants.postRating:
			baseFragment
					.onItemorRestaurantratingLoadFail(babtainResponse.jsonObject);
			break;
		case AppConstants.MakeAsPrimary:
			baseFragment.MakeAsPrimaryFail(babtainResponse.jsonObject);
			break;
		case AppConstants.REMOVE_Address:
			baseFragment.onAddressRemovalFail(babtainResponse.jsonObject);
			break;
		case AppConstants.EditAddress:
			baseFragment.editAddressFail(babtainResponse.jsonObject);
			break;
		case AppConstants.AdvertisementBannerUrl:
			baseFragment
					.onAdvertisementLoadingFailed(babtainResponse.jsonObject);
			break;
		default:
			break;
		}
	}

	public void advanceSearchRestarants(JSONObject jsonObject) {
		baseFragment.advanceSearchRestarants(jsonObject);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		Log.d(TAG, "Back Buttn Pressed in BaseActivity");
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction trans = fm.beginTransaction();
		ArrayList<Fragment> list = null;
		Fragment fr;

		Log.e(TAG, "" + fm.getBackStackEntryCount());
		
		String referenceDataFromFile = readData();
		if (referenceDataFromFile.equals("No Data")) {
			
			if (fm.getFragments().size() > 0) {
				list = (ArrayList<Fragment>) fm.getFragments();
				Log.e(TAG, "the data" + list.size());
				for (int i = 0; i < list.size(); i++) {
					fr = list.get(i);
					Log.d("jijooooooo", "BackStack entered");
					if (fr instanceof LiveChatFragment) {
					}

					if (fr instanceof KnetForGiftvoucher) {
						Log.e("Page", "Gift Voucher");
						trans.remove(fr);
						trans.commit();
						fm.popBackStack();
					}

					if (fr instanceof PaymentKNETPage) {
						Log.e("Page", "PaymentKNETPage");
						trans.remove(fr);
						trans.commit();
						fm.popBackStack();
					}
					if (fm.getBackStackEntryCount() < 1) {
						Log.e(TAG, "i am here");

						/*
						 * try { String DATA = readDatas();
						 * Log.e("The data received", DATA); if
						 * (DATA.equals("keeplogin")) {
						 * 
						 * } else { clearuserid();
						 * PrefUtil.setUserSignedOut(this);
						 * PrefUtil.setFirstname(this, ""); clearuserid();
						 * 
						 * PrefUtil.setAuthkey(this, ""); this.logout();
						 * this.WriteDataToFile(""); } } catch (Exception e) {
						 * 
						 * }
						 */

						System.exit(0);
					}
				}

			} else {
				System.exit(0);
			}
		} else {
			Fragment fragment = new HomeFragment();
			pushFragments(fragment, false, true);
		}
	}

	private String readData() {
		String data = "No Data";
		try {
			FileInputStream fis = openFileInput(FILENAMENOTIFICATION);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			data = br.readLine();
		} catch (Exception e) {
		}
		return data;
	}

	private Dialog spinWheelDialog;
	Handler spinWheelTimer = new Handler(); // Handler to post a runnable that
	// can dismiss spin wheel after a specific time
	public static final int SPINWHEEL_LIFE_TIME = 2000; /*
														 * Dismiss spin wheel
														 * after 5 seconds
														 */

	/*
	 * Creates a dialog with a spin wheel to block the UI elements
	 * 
	 * @param setDefaultLifetime : set this to true if to dismiss the spin wheel
	 * after SPINWHEEL_LIFE_TIME Setting setDefaultLifetime false will not
	 * dismiss the spin wheel.Need to issue stopSpinWheel command
	 */
	public void startSpinwheel(boolean setDefaultLifetime, boolean isCancelable) {
		// Log.d(TAG, "startSpinwheel"+getCurrentActivity().getClass() );
		// If already showing no need to create.
		if (spinWheelDialog != null && spinWheelDialog.isShowing())
			return;
		spinWheelDialog = new Dialog(this, R.style.wait_spinner_style);
		ProgressBar progressBar = new ProgressBar(this);
		LayoutParams layoutParams = new LayoutParams(android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
		spinWheelDialog.addContentView(progressBar, layoutParams);
		spinWheelDialog.setCancelable(isCancelable);
		spinWheelDialog.show();
		// start timer for SPINWHEEL_LIFE_TIME
		spinWheelTimer.removeCallbacks(dismissSpinner);
		if (setDefaultLifetime) // If requested for default dismiss time.
			spinWheelTimer.postAtTime(dismissSpinner,
					SystemClock.uptimeMillis() + SPINWHEEL_LIFE_TIME);

		spinWheelDialog.setCanceledOnTouchOutside(false);
	}

	public void startSpinwheel(boolean isCancelable, int layoutid,
			int timeOutSec) {
		startSpinwheel(true, isCancelable);
		spinWheelTimer.removeCallbacks(dismissSpinner);
		spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis()
				+ timeOutSec);
		spinWheelDialog.setContentView(layoutid);
	}

	/*
	 * Closes the spin wheel dialog
	 */
	public void stopSpinWheel() {
		// Log.d(TAG, "stopSpinWheel"+getCurrentActivity().getClass());
		if (spinWheelDialog != null)
			try {
				spinWheelDialog.dismiss();
			} catch (IllegalArgumentException e) {
				Log.e(TAG,
						"Parent is died while trying to dismiss spin wheel dialog ");
			}
		spinWheelDialog = null;
	}

	/* Dismiss the spin wheel */
	Runnable dismissSpinner = new Runnable() {
		@Override
		public void run() {
			stopSpinWheel();
		}
	};

	// Callback for spin wheel dismiss
	protected void onSpinWheelDismissed() {
		Log.d(TAG, "Spin wheel disconnected");
	}

	public void logout() {
		if (baseFragment != null)
			baseFragment.pushFragments4LanSwitch();
	}

	public void SwitchLanguage() {
		if (baseFragment != null){
			Log.d("Jijooooooooooo", "reached here");
			baseFragment.LanSwitchTrigered();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		U3an.activityResumed();
	}

	@Override
	protected void onPause() {
		super.onPause();
		U3an.activityPaused();
	}

	private String readDatas() {
		String dummyCallData = null;
		// reading the file
		try {
			FileInputStream fis = this.openFileInput(FILENAMELOG);
			InputStreamReader in = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(in);
			String data = br.readLine();
			dummyCallData = data;
		} catch (Exception e) {

		}
		return dummyCallData;
	}

	private void clearuserid() {
		FileOutputStream fosdummyy;
		FileInputStream fisdummy;
		try {
			fisdummy = this.openFileInput(FILENAMEAUTHKEY);
			StringBuffer fileContent = new StringBuffer("");
			byte[] buffer = new byte[1024];
			int n = 0;
			while ((n = fisdummy.read(buffer)) != -1) {
				fileContent.append(new String(buffer, 0, n));
			}
			FILENAMEAUTHKEY = fileContent.toString();
			Log.e("auth key", "" + FILENAMEAUTHKEY);
			// fosdummyy.write(key.getBytes());
		} catch (Exception e) {
		}
		Log.e("auth key............", "" + FILENAMEAUTHKEY);
		String data = "";
		try {
			fosdummyy = this.openFileOutput(FILENAMEAUTHKEY,
					Context.MODE_PRIVATE);
			fosdummyy.write(data.getBytes());
			fosdummyy.close();
		} catch (Exception e) {
			Log.e("The last stage", e.getMessage());
		}
	}

	public void fbSelected() {
		try {
			String facebookUrl = "https://www.facebook.com/dairamcom";

			Uri uri = Uri.parse("fb://facewebmodal/f?href=" + facebookUrl);
			// Uri uri = Uri.parse("fb://facewebmodal/f?href=" +
			// "fb://profile/344207749051029");
			Log.e("FB URI in Try", "" + uri);
			startActivity(new Intent(Intent.ACTION_VIEW, uri));

		} catch (Exception e) {// or else open in browser instead
			String facebookUrl = "https://www.facebook.com/dairamcom";
			Log.e("FB URI in Catch", "" + facebookUrl);
			startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookUrl)));
		}
	}

	public void twitterSelected() {
		Uri uri = Uri.parse("https://twitter.com/dairam_com");
		Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

		likeIng.setPackage("com.twitter.android");

		try {
			startActivity(likeIng);

		} catch (Exception e) {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("https://twitter.com/dairam_com")));
		}
	}

	public void instaGram() {
		Uri uri = Uri.parse("http://instagram.com/dairam_com");
		Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);
		likeIng.setPackage("com.instagram.android");
		try {
			startActivity(likeIng);
		} catch (ActivityNotFoundException e) {
			startActivity(new Intent(Intent.ACTION_VIEW,
					Uri.parse("http://instagram.com/dairam_com  ")));
		}
	}
}
