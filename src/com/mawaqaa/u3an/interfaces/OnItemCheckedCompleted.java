package com.mawaqaa.u3an.interfaces;

import java.util.ArrayList;

import com.mawaqaa.u3an.data.SelectedChoiceItems;

public interface OnItemCheckedCompleted {

	void ItemCheckComple(ArrayList<SelectedChoiceItems> items, boolean b);

}
