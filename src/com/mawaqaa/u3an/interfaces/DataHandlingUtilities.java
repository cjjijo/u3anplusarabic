package com.mawaqaa.u3an.interfaces;

public interface DataHandlingUtilities {

	void hideToolBar();

	void showToolBar();

	void defaultBottomBar();

	void mostSellingItemBottomBar();

	void allResturantsBottomBar();
	
	void newResturantsBottomBar();
	
	
	//Interfaces for side bar
	// void onOrderYourDishes();
	
}
