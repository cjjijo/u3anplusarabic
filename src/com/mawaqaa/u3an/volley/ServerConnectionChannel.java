package com.mawaqaa.u3an.volley;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.mawaqaa.u3an.BaseActivity;

public class ServerConnectionChannel {

	private static final String TAG = "ServerConnectionChannel";
	private static final int BABTAIN_MAX_RETRIES = 5;
	private static final float BABTAIN_BACKOFF_MULT = 5;

	
	public ServerConnectionChannel() {
	}

	public void doSendJsonRequest(u3annRequest babtainRequest) {
		RequestQueue queue = VolleyUtils.getRequestQueue();
		try {
			JSONObject jsonObject = babtainRequest.jsonObject;
			
			u3annJsonRequest myReq = new u3annJsonRequest(
					babtainRequest.method, babtainRequest.mReqUrl,
					jsonObject, createReqSuccessListener(babtainRequest),
					createReqErrorListener(babtainRequest)){
				protected Map<String, String> getParams()
						throws AuthFailureError {
					Map<String, String> params = new HashMap<String, String>
					();
					params.put("Content-Type", "text/json");
					return params;
				}
			};
			myReq.setRetryPolicy(new DefaultRetryPolicy(
				     DefaultRetryPolicy.DEFAULT_TIMEOUT_MS , 
				     BABTAIN_MAX_RETRIES, 
				                 BABTAIN_BACKOFF_MULT));
			//myReq.setHeader("Cache-Control", "no-cache");
			//myReq.setHeader("Content-Type", "text/json");			
			queue.add(myReq);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	private ErrorListener createReqErrorListener(final u3annRequest babtainRequest) {
		return new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				Log.d(TAG, "ReqErrorListener"+error.getMessage());
				u3annResponse batainResponse = new u3annResponse();
				batainResponse.mReqUrl = babtainRequest.mReqUrl;
				BaseActivity.getBaseActivity().serviceResponseError(batainResponse);
			}
		};
	}

	private Listener<JSONObject> createReqSuccessListener(final u3annRequest babtainRequest) {
		return new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				Log.d(TAG, "ReqSuccessListener :"+babtainRequest.mReqUrl);
				u3annResponse batainResponse = new u3annResponse();
				batainResponse.mReqUrl = babtainRequest.mReqUrl;
				batainResponse.jsonObject = response;
				BaseActivity.getBaseActivity().serviceResponseSuccess(batainResponse);
			}
		};
	}

}
