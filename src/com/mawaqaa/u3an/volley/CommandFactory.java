package com.mawaqaa.u3an.volley;

import org.json.JSONObject;

import android.util.Log;

import com.android.volley.Request.Method;

public class CommandFactory {
	
	private static final String TAG = "CommandFactory";

	public void sendgetCommand(String url) {

		ServerConnectionChannel serverConnectionChannel = VolleyUtils
				.getServerConnectionChannel();
		serverConnectionChannel.doSendJsonRequest(createGetRequest(url));

	}
	
	public void sendPostCommand(String url, JSONObject jsonObject) {
		Log.d(TAG, "sendPostCommand()");
		Log.d(TAG, "URL :"+url);
		Log.d(TAG, "Post jsonObject :"+jsonObject);
		ServerConnectionChannel serverConnectionChannel = VolleyUtils
				.getServerConnectionChannel();
		serverConnectionChannel.doSendJsonRequest(createPostRequest(url, jsonObject));

	}

	/* method for get method */
	private u3annRequest createGetRequest(String url) {
		u3annRequest babtainRequest = new u3annRequest();
		babtainRequest.method = Method.GET;
		babtainRequest.mReqUrl = url;
		return babtainRequest;

	}

	/* method for post method */
	private u3annRequest createPostRequest(String url, JSONObject jsonObject) {
		u3annRequest babtainRequest = new u3annRequest();
		babtainRequest.method = Method.POST;
		babtainRequest.mReqUrl = url;
		babtainRequest.jsonObject = jsonObject;

		return babtainRequest;
	}

}
