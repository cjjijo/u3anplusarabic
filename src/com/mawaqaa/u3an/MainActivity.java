package com.mawaqaa.u3an;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mawaqaa.u3an.adapters.NavDrawerListAdapter;
import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.dialogfragment.AdvertisementBanner;
import com.mawaqaa.u3an.dialogfragment.MyDialogFragmentSocialmedia;
import com.mawaqaa.u3an.fragments.AddRestaurantFragment;
import com.mawaqaa.u3an.fragments.AllRestaurantsFragment;
import com.mawaqaa.u3an.fragments.ConsultationFragment;
import com.mawaqaa.u3an.fragments.FaqFragment;
import com.mawaqaa.u3an.fragments.FeedBackFragment;
import com.mawaqaa.u3an.fragments.FragmentGiftVoucher;
import com.mawaqaa.u3an.fragments.HomeFragment;
import com.mawaqaa.u3an.fragments.LiveChatFragment;
import com.mawaqaa.u3an.fragments.LoginFragment;
import com.mawaqaa.u3an.fragments.MostSellingDishesFragments;
import com.mawaqaa.u3an.fragments.MostSellingRestaurantsDetails;
import com.mawaqaa.u3an.fragments.MyAccountLandingPage;
import com.mawaqaa.u3an.fragments.NewRestaurantsFragment;
import com.mawaqaa.u3an.fragments.PrivacyFragment;
import com.mawaqaa.u3an.fragments.PromotionsFragment;
import com.mawaqaa.u3an.fragments.SettingsFragment;
import com.mawaqaa.u3an.fragments.SocialResponsibilityFragment;
import com.mawaqaa.u3an.fragments.TermsAndConditionFrag;
import com.mawaqaa.u3an.fragments.UanCreditFragment;
import com.mawaqaa.u3an.interfaces.DataHandlingUtilities;
import com.mawaqaa.u3an.interfaces.LogoutListener;
import com.mawaqaa.u3an.interfaces.onLanguageChangeListener;
import com.mawaqaa.u3an.navdraweritem.NavDrawerItem;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.squareup.picasso.Picasso;

@SuppressWarnings("deprecation")
public class MainActivity extends BaseActivity implements
		DataHandlingUtilities, OnClickListener, LogoutListener,
		onLanguageChangeListener {

	static int  languageSwitching;
	public static Bitmap image;
	private Toolbar toolbar;
	ImageButton backButton, homeButton;
	TextView headerTextView;
	Spinner sortingSpinner;
	ImageButton mostSellingDishesButton, allRestaurantsButton,
			newRestaurantsButton, moreButton;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	String bannerImage, restaurantID;
	public static String adsbannerImage;

	// nav drawer title
	private CharSequence mDrawerTitle;
	
	// used to store app title
	private CharSequence mTitle;

	String valueFromNotification;// = getResources().getString(R.string.nodata);

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	private Boolean isNetworkavailable;
	JSONObject jsonObject;
	BaseActivity activity;
	ImageView bannerImageView;

	MyDialogFragmentSocialmedia socialmedia;
	AdvertisementBanner adBanner;
	Bundle savedInstanceState;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.savedInstanceState = savedInstanceState;
		
		activity = BaseActivity.getBaseActivity();
		setLanguage(PrefUtil.getAppLanValue(activity));
		setContentView(R.layout.activity_main);
		valueFromNotification = getResources().getString(R.string.nodata);
		/*Locale current = getResources().getConfiguration().locale;
		String sDefSystemLanguage = current.getLanguage();*/
		//if(sDefSystemLanguage.equals("en")){
			setToolBar();
		/*}else{
			Log.d("Jijoo", sDefSystemLanguage+":Reached");
			//setToolBarAr();
			setToolBar();
		}*/

		//setToolBar();
		
		/*StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
	      StrictMode.setThreadPolicy(policy);*/

		isNetworkavailable = isNetworkAvailable();
		Log.e("The data received", isNetworkavailable + "");
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			valueFromNotification = extras.getString("notificationMsg");
		}
		
		// initialize views
		mostSellingDishesButton = (ImageButton) findViewById(R.id.most_selling);
		allRestaurantsButton = (ImageButton) findViewById(R.id.all_restaurants);
		newRestaurantsButton = (ImageButton) findViewById(R.id.new_restaurants);
		moreButton = (ImageButton) findViewById(R.id.more);

		mostSellingDishesButton.setOnClickListener(this);
		allRestaurantsButton.setOnClickListener(this);
		newRestaurantsButton.setOnClickListener(this);
		moreButton.setOnClickListener(this);

		mTitle = mDrawerTitle = getTitle();
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

		// navigation drawer icons from resources
		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.drawer_list);
		
		LayoutInflater inflater = getLayoutInflater();
		ViewGroup header = (ViewGroup) inflater.inflate(R.layout.listheader,
				mDrawerList, false);

		bannerImageView = (ImageView) header.findViewById(R.id.headerImageView);
		mDrawerList.addHeaderView(header, null, false);
		

		// Adding OnClick Action for header
		header.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				/*Locale current = getResources().getConfiguration().locale;
				String sDefSystemLanguage = current.getLanguage();
				if(sDefSystemLanguage.equals("en")){
					PrefUtil.setFragHeading(getApplicationContext(),getResources().getString(R.string.all_restaurent));
				}else{
					PrefUtil.setFragHeading(getApplicationContext(),"جميع المطاعم");
				}*/
				PrefUtil.setFragHeading(getApplicationContext(),getResources().getString(R.string.all_restaurent));
				if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_AR)){
					mDrawerLayout.closeDrawer(Gravity.LEFT);
				}else{
					mDrawerLayout.closeDrawer(Gravity.RIGHT);
				}
				Fragment allRestaurantsDetailsFragment = new MostSellingRestaurantsDetails();
				Bundle params1 = new Bundle();
				params1.putString("Header", "Promotion");
				params1.putString(AppConstants.all_restaurants_id, restaurantID);
				allRestaurantsDetailsFragment.setArguments(params1);
				activity.pushFragments(allRestaurantsDetailsFragment, false,
						true);
			}
		});

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
		navDrawerItems = new ArrayList<NavDrawerItem>();
		// adding navigation drawer items to array.....
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
				.getResourceId(0, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
				.getResourceId(1, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
				.getResourceId(2, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
				.getResourceId(3, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
				.getResourceId(4, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
				.getResourceId(5, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
				.getResourceId(6, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons
				.getResourceId(7, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[8], navMenuIcons
				.getResourceId(8, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[9], navMenuIcons
				.getResourceId(9, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[10], navMenuIcons
				.getResourceId(10, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[11], navMenuIcons
				.getResourceId(11, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[12], navMenuIcons
				.getResourceId(12, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[13], navMenuIcons
				.getResourceId(13, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[14], navMenuIcons
				.getResourceId(14, -1)));
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[15], navMenuIcons
				.getResourceId(15, -1)));

		// Recycle the typed array.....
		navMenuIcons.recycle();

		// setting the nav drawer list adapter.....
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems, 20);
		mDrawerList.setAdapter(adapter);

		// enabling action bar application icon and behaving it as toggle
		// button.....
		/*
		 * getActionBar().setDisplayHomeAsUpEnabled(true);
		 * getActionBar().setHomeButtonEnabled(true);
		 */
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.more, R.string.drawer_open, R.string.drawer_close) {
// Jijo
			/* Called when a drawer has settled in a completely closed state. */
			@Override
			public void onDrawerClosed(View view) {
				super.onDrawerClosed(view);
				if(PrefUtil.getAppLanguage(activity).equals("en"))
					moreButton.setBackgroundResource(R.drawable.more);
				else
					moreButton.setBackgroundResource(R.drawable.more_icon_ar);
			}

			/* Called when a drawer has settled in a completely open state. */
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if(PrefUtil.getAppLanguage(activity).equals("en"))
					moreButton.setBackgroundResource(R.drawable.more_sel);
				else
					moreButton.setBackgroundResource(R.drawable.more_icon_sel_ar);
				if (isNetworkAvailable()) {
					
					new getBannerFromTheservice(true).execute();
					
				}
			}
		};
		
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		Log.e("inside mDrawerLayout..........","++++++++++"+ mDrawerToggle);
		if (savedInstanceState == null) {
		}
	}

	private void setLanguage(String lang) {
			
		//String languageToLoad = lang;
		Locale locale = new Locale(lang);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		getResources().updateConfiguration(config,
				getResources().getDisplayMetrics());
		activity.SwitchLanguage();
	}
	
	// AsyncTask for loading data....
	
	public class getBannerFromTheservice extends AsyncTask<Void, String, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;
		InputStream istream;

		public getBannerFromTheservice(Boolean progressVisibilty) {
			this.progressVisibiltyflag = progressVisibilty;
		}

		@Override
		protected void onPreExecute() {
			showProgressbar();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();

			try {
				
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(AppConstants.banner_url);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();
					jObj = new JSONObject(result);

				} else {
					result = null;
				}

			} catch (JSONException e) {
				Log.e("Data", "JSONException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				Log.e("Data",
						"UnsupportedEncodingException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				Log.e("Data", "ClientProtocolException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				Log.e("Data", "IOException Error:" + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			hideProgressbar();
			try {
				bannerImage = jObj.getString("BannerImage");
				restaurantID = jObj.getString("RetaurantId");

				Picasso.with(getApplicationContext()).load(bannerImage)
						.error(R.drawable.imagenotavailable)
						.into(bannerImageView);

				Log.e("The banner Image ",""+ bannerImage.toString());
			} catch (Exception e) {

			}
		}
	}

	private void showProgressbar() {
		activity.startSpinwheel(false, false);
	}

	private void hideProgressbar() {
		activity.stopSpinWheel();
	}

	private void setToolBar() {
		if(languageSwitching == 2){
			languageSwitching = 1;
		}else{
			new getAdsBannerFromTheservice(true).execute();
		}
		
		toolbar = (Toolbar) findViewById(R.id.actionbar_toolbar);
		LayoutInflater inflater = getLayoutInflater();
		View row = inflater.inflate(R.layout.main_actionbar, toolbar);
		backButton = (ImageButton) row.findViewById(R.id.backButton_main);
		headerTextView = (TextView) row.findViewById(R.id.title);
		sortingSpinner = (Spinner) row.findViewById(R.id.category_spinner);
		//homeButton = (ImageButton) row.findViewById(R.id.imgHomeButton);
		backButton.setOnClickListener(backButtonClicked);
		headerTextView.setOnClickListener(headerTextviewClicked);
		setSupportActionBar(toolbar);
		
		if (isNetworkAvailable()) {
		} else {
			Toast.makeText(getApplicationContext(), R.string.no_network,
					Toast.LENGTH_SHORT).show();
		}
		try {
			Log.e("Value from notification", ""+valueFromNotification);
			if (valueFromNotification.startsWith("2")) {
				WriteDataToFile();
				Fragment myDetailFragment = new NewRestaurantsFragment();
				pushFragments(myDetailFragment, false, true);
			} else if (valueFromNotification.startsWith("1")) {
				WriteDataToFile();
				Fragment myDetailFragment = new PromotionsFragment();
				pushFragments(myDetailFragment, false, true);
			} else {
				Fragment myDetailFragment = new HomeFragment();
				pushFragments(myDetailFragment, false, true);

			}
		} catch (Exception e) {

		}
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//setToolBar() ;
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	private void WriteDataToFile() {
		String data = "Notification";
		FileOutputStream fosdummyy;
		try {
			fosdummyy = openFileOutput(FILENAMENOTIFICATION,
					Context.MODE_PRIVATE);
			fosdummyy.write(data.getBytes());
			fosdummyy.close();
		} catch (Exception e) {
		}
	}

	private OnClickListener backButtonClicked = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		}
	};

	private OnClickListener headerTextviewClicked = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		}
	};

	@Override
	public void hideToolBar() {
		toolbar.setVisibility(View.GONE);
	}

	@Override
	public void showToolBar() {
		toolbar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.most_selling:
			adapter = new NavDrawerListAdapter(getApplicationContext(),
					navDrawerItems, 25);
			mDrawerList.setAdapter(adapter);
			if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_AR)){
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}else{
				mDrawerLayout.closeDrawer(Gravity.RIGHT);
			}
			isNetworkavailable = isNetworkAvailable();
			if (isNetworkavailable == true) {
				Fragment mostSellingDishesFragments = new MostSellingDishesFragments();
				pushFragments(mostSellingDishesFragments, false, true);
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.all_restaurants:
			adapter = new NavDrawerListAdapter(getApplicationContext(),
					navDrawerItems, 25);
			mDrawerList.setAdapter(adapter);
			if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_AR)){
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}else{
				mDrawerLayout.closeDrawer(Gravity.RIGHT);
			}
			isNetworkavailable = isNetworkAvailable();
			if (isNetworkavailable == true) {
				Fragment allResturantsFragment = new AllRestaurantsFragment();
				pushFragments(allResturantsFragment, false, true);
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;
			
		case R.id.new_restaurants:
			adapter = new NavDrawerListAdapter(getApplicationContext(),
					navDrawerItems, 25);
			mDrawerList.setAdapter(adapter);
			isNetworkavailable = isNetworkAvailable();
			if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_AR)){
				mDrawerLayout.closeDrawer(Gravity.LEFT);
			}else{
				mDrawerLayout.closeDrawer(Gravity.RIGHT);
			}
			Fragment newRestaurantsFragment = new NewRestaurantsFragment();
			pushFragments(newRestaurantsFragment, false, true);
			break;
		case R.id.more:
			handleMoreButtonClickEvent();
			break;
		}
	}
// Jijo
	private void handleMoreButtonClickEvent() {
		Log.d("PrefUtil.getAppLanValue(activity)==",""+ PrefUtil.getAppLanValue(activity));
		Log.e("AppConstants.STR_AR==", ""+AppConstants.STR_AR);
		try {
			if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_AR)){
				//mDrawerLayout.setDrawerListener(mDrawerToggle);
				Log.e("inside if=", ""+AppConstants.STR_AR);
				//moreButton.setBackgroundResource(R.drawable.more);
				//mDrawerLayout.openDrawer(Gravity.LEFT | Gravity.BOTTOM);
				if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
					Log.e("inside if===if==",""+ AppConstants.STR_AR);
					moreButton.setBackgroundResource(R.drawable.more_icon_sel_ar);
					mDrawerLayout.closeDrawer(Gravity.LEFT);
					Log.d("jijo", "dfdfdfdfdfdfdfdfdf");
				} else {
					moreButton.setBackgroundResource(R.drawable.more_icon_ar);
					mDrawerLayout.openDrawer(Gravity.LEFT | Gravity.BOTTOM);
				}
			}else if (PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_EN)) {
				
			
				if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
					moreButton.setBackgroundResource(R.drawable.more_sel);
					mDrawerLayout.closeDrawer(Gravity.RIGHT);
				} else {
					moreButton.setBackgroundResource(R.drawable.more);
					mDrawerLayout.openDrawer(Gravity.RIGHT | Gravity.BOTTOM);
				}
			}
		} catch (Exception e) {
			System.out.println("Here it caught an exception" + e);
			e.printStackTrace();
		}
	}

	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected navigation drawer item
			displayView(position);
		}
	}
//Jijo
	private void displayView(int position) {
		
		if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_AR)){
			moreButton.setBackgroundResource(R.drawable.more_icon_sel_ar);
			mDrawerLayout.closeDrawer(Gravity.LEFT);
		}else{
			moreButton.setBackgroundResource(R.drawable.more_sel);
			mDrawerLayout.closeDrawer(Gravity.RIGHT);
		}
		// update the main content by replacing fragments
		Fragment fragment = null;

		switch (position) {
		case 0: // Language
			// fragment = new DeleveryInformation();
			break;

		case 1: // language

		/*	Toast.makeText(activity, "Language switching not available now",
					Toast.LENGTH_SHORT).show();
				
*/			String language = PrefUtil.getAppLanValue(activity);
			/*if(language.equalsIgnoreCase("en"))
				language = "Arabic";
			else
				language = "English";
			Utilities.showAlert4LanguageSwitch(this,getResources().getString(R.string.alert4_language_switch)+" "+getResources().getString(R.string.language), 
														getResources().getString(R.string.language_change_alert),okClicked,cancelClicked,language);	*/
			showAlert4LanguageSwitch();
			break;
		case 2: // U3an Credit
			if (isNetworkAvailable() == true) {
				if (PrefUtil.isUserSignedIn(getApplicationContext()))
					fragment = new MyAccountLandingPage();
				else {
					Toast.makeText(getApplicationContext(),
							R.string.access_u3an_credit,
							Toast.LENGTH_SHORT).show();
					fragment = new LoginFragment();
				}
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 3: // Order your Dishes
			if (isNetworkAvailable() == true) {
				fragment = new HomeFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 4: // Live Chat
			if (isNetworkAvailable() == true) {
				fragment = new LiveChatFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 5: // Promotions
			if (isNetworkAvailable() == true) {
				fragment = new PromotionsFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 6: // U3an Credit
			if (isNetworkAvailable() == true) {
				if (PrefUtil.isUserSignedIn(getApplicationContext()))
					fragment = new UanCreditFragment();
				else {
					Toast.makeText(getApplicationContext(),
							R.string.access_u3an_credit,
							Toast.LENGTH_SHORT).show();
					fragment = new LoginFragment();
				}
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 7: // Gift Voucher
			if (isNetworkAvailable() == true) {
				if (PrefUtil.isUserSignedIn(getApplicationContext()))
					fragment = new FragmentGiftVoucher();
				else {
					Toast.makeText(getApplicationContext(),
							R.string.access_gift_voucher,
							Toast.LENGTH_SHORT).show();
					fragment = new LoginFragment();
				}
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 8: // Consultations
			if (isNetworkAvailable() == true) {
				fragment = new ConsultationFragment();
			} else {
				Toast.makeText(getApplicationContext(),R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 9: // Add Restaurant
			if (isNetworkAvailable() == true) {
				fragment = new AddRestaurantFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 10: // Feedback
			if (isNetworkAvailable() == true) {
				fragment = new FeedBackFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 11: // Terms and conditions
			if (isNetworkAvailable() == true) {
				fragment = new TermsAndConditionFrag();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 12:// FAQ's
			if (isNetworkAvailable() == true) {
				fragment = new FaqFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 13: // Privacy
			if (isNetworkAvailable() == true) {
				fragment = new PrivacyFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 14: // Social Responsibility
			if (isNetworkAvailable() == true) {
				fragment = new SocialResponsibilityFragment();
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;

		case 15: // Social Responsibility
			socialmedia = MyDialogFragmentSocialmedia.newInstance();
			socialmedia.show(getFragmentManager(), "myDialogFragment");
			break;

		case 16: // Settings
			if (isNetworkAvailable() == true) {
				fragment = new SettingsFragment(getBaseActivity());
			} else {
				Toast.makeText(getApplicationContext(), R.string.no_network,
						Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}

		if (fragment != null)
			if (fragment instanceof SettingsFragment) {
				((SettingsFragment) fragment).show(getSupportFragmentManager(),
						"Settings");
			} else {
				adapter = new NavDrawerListAdapter(getApplicationContext(),
						navDrawerItems, position - 1);
				mDrawerList.setAdapter(adapter);
				pushFragments(fragment, false, true);
			}
	}

	private void showAlert4LanguageSwitch() {
	
		final Dialog dialog = new Dialog(activity);

		//tell the Dialog to use the dialog.xml as it's layout description
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.alert_dialogue_title);
		//dialog.setTitle(getResources().getString(R.string.language_change_alert));
		TextView txtHead = (TextView) dialog.findViewById(R.id.alert_dialogue_head);
		/*txtHead.setText(getResources().getString(R.string.language_change_alert));
		txtHead.setTextSize(20.0f);
		txtHead.setPadding(10, 10, 10, 10);
		txtHead.setGravity(Gravity.CENTER);*/

		TextView txtBody = (TextView) dialog.findViewById(R.id.alert_dialogue_body);
		txtBody.setText(getResources().getString(R.string.alert4_language_switch)+" "+getResources().getString(R.string.language));
		txtBody.setPadding(10, 40, 10, 40);

		Button dialogButtonOk = (Button) dialog.findViewById(R.id.buttonAlertOk);
		Button dialogButtonCancel = (Button)dialog.findViewById(R.id.buttonAlertCancel);

		dialogButtonOk.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setLanguageSwitching();
			}
		});
		dialogButtonCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();
		dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	
}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggles
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

//Jijo
	@Override
	public void defaultBottomBar() {
		if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_EN)){
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuart);
			moreButton.setBackgroundResource(R.drawable.more);
		}else{
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes_ar);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants_ar);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuarants_ar);
			moreButton.setBackgroundResource(R.drawable.more_icon_ar);
		}
		
	}

	@Override
	public void onBackPressed() {
		if (mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
			mDrawerLayout.closeDrawer(Gravity.RIGHT);
		else if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
			finish();
		} else {
			super.onBackPressed();
		}
	}

	/*
	 * @Override public void onBackPressed() { if
	 * (mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
	 * mDrawerLayout.closeDrawer(Gravity.RIGHT); else if
	 * (getSupportFragmentManager().getBackStackEntryCount() == 0) { finish(); }
	 * else { super.onBackPressed(); } }
	 */
//Jijo
	@Override
	public void mostSellingItemBottomBar() {
		if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_EN)){
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes_sel);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuart);
			moreButton.setBackgroundResource(R.drawable.more);
		}else{
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes_sel_ar);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants_ar);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuarants_ar);
			moreButton.setBackgroundResource(R.drawable.more_icon_ar);
		}
		
	}

	@Override
	public void allResturantsBottomBar() {
		if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_EN)){
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants_sel);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuart);
			moreButton.setBackgroundResource(R.drawable.more);
		}else{
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes_ar);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants_sel_ar);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuarants_ar);
			moreButton.setBackgroundResource(R.drawable.more_icon_ar);
		}
		
	}

	@Override
	public void newResturantsBottomBar() {
		if(PrefUtil.getAppLanValue(activity).equals(AppConstants.STR_EN)){
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuart_sel);
			moreButton.setBackgroundResource(R.drawable.more);
		}else{
			mostSellingDishesButton
			.setBackgroundResource(R.drawable.mostsellingdishes_ar);
			allRestaurantsButton.setBackgroundResource(R.drawable.allrestaurants_ar);
			newRestaurantsButton.setBackgroundResource(R.drawable.newrestuarants_sel_ar);
			moreButton.setBackgroundResource(R.drawable.more_icon_ar);
		}
		
	}
//
	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	@Override
	public void reCreateFrag(Fragment fragment) {
		popFragments();
		pushFragments(fragment, false, true);
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public void OnLanguageChange(Fragment fr) {
		popFragments();

		// if (fragment instanceof CategorylistingFragment)
		// PrefUtils.setBrandId(this, AppConstants.RECREATE_BRAND_ID);
		pushFragments(fr, false, true);

	}

		
// Jijo 10-12-15	
	
	private DialogInterface.OnClickListener okClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
			//popFragments();
			setLanguageSwitching();
		}
	};

	private DialogInterface.OnClickListener cancelClicked = new DialogInterface.OnClickListener() {
		@Override
		public void onClick(DialogInterface dialog, int which) {
		}
	};
	public void setLanguageSwitching(){
		languageSwitching =2;
		Log.e("Current language",PrefUtil.getAppLanValue(activity)+"languageSwitching....=="+languageSwitching);
		
		 if (PrefUtil.isAppLanEnglish(activity)) {
			 Log.e("Language switch ", "AppLanEnglish");
			 PrefUtil.setAppLanguage(activity, PrefUtil.PREF_APP_LAN_AR);
			 setLanguage(PrefUtil.getAppLanValue(activity));
			 //this.onCreate(savedInstanceState);
			 /*setContentView(R.layout.activity_main);
			 setToolBar();*/
			 Intent intent = new Intent(this,MainActivity.class);
			 this.finish();
			 startActivity(intent);
			// finish();
		 } else {
			 Log.e("Language switch", "AppLanArabic");
			 PrefUtil.setAppLanguage(activity, PrefUtil.PREF_APP_LAN_EN);
			 setLanguage(PrefUtil.getAppLanValue(activity)); 
			 //this.onCreate(savedInstanceState);
			// setContentView(R.layout.activity_main);
			// setToolBar();
			 Intent intent = new Intent(this,MainActivity.class);
			 this.finish();
			 startActivity(intent);
			 //finish();
		 }
	}
	// AsyncTask for loading data....
	public class getAdsBannerFromTheservice extends
			AsyncTask<Void, String, Void> {
		String type, result;
		boolean postStatus = false;
		JSONObject jObj = null;
		Boolean progressVisibiltyflag;
		InputStream istream;

		public getAdsBannerFromTheservice(Boolean progressVisibilty) {
			this.progressVisibiltyflag = progressVisibilty;
		}

		@Override
		protected void onPreExecute() {
			showProgressbar();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) {
			Locale current = getResources().getConfiguration().locale;
			String sDefSystemLanguage = current.getLanguage();
			try {
				JSONObject objJson = new JSONObject();
				if(sDefSystemLanguage.equals("en")){
					objJson.accumulate("locale", "en-US");
				}else{
					objJson.accumulate("locale", "ar-KW");
				}
				HttpClient httpClient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost(AppConstants.adsbanner_url);
				StringEntity entity = new StringEntity(objJson.toString());
				httppost.setEntity(entity);
				httppost.setHeader("Accept", "application/json");
				httppost.setHeader("Content-type", "application/json");
				HttpResponse response = httpClient.execute(httppost);
				istream = response.getEntity().getContent();

				if (istream != null) {
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(istream));
					String line = "";
					result = "";
					while ((line = bufferedReader.readLine()) != null)
						result += line;

					istream.close();
					jObj = new JSONObject(result);
					adsbannerImage = jObj.getString("BannerImage");
					try {
					      URL url = new URL(adsbannerImage);
					      image = BitmapFactory.decodeStream(url.openConnection().getInputStream());					      
				    } catch(Exception e){
				    	e.printStackTrace();
				    }
					

				} else {
					result = null;
				}

			} catch (JSONException e) {
				Log.e("Data", "JSONException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (UnsupportedEncodingException e) {
				Log.e("Data",
						"UnsupportedEncodingException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				Log.e("Data", "ClientProtocolException Error:" + e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
				Log.e("Data", "IOException Error:" + e.getMessage());
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			hideProgressbar();
			try {
				adsbannerImage = jObj.getString("BannerImage");
			/*	Log.e("The advertisement banner Image ",
						adsbannerImage.toString());*/
				PrefUtil.setAds(getApplicationContext(), true);
				
			} catch (Exception e) {

			}
		}
	}

}
