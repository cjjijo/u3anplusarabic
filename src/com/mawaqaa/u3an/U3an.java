package com.mawaqaa.u3an;

import android.app.Application;

public class U3an extends Application {

	public static U3an u3an;

	public static boolean isActivityVisible() {
		return activityVisible;
	}

	public static void activityResumed() {
		activityVisible = true;
	}

	public static void activityPaused() {
		activityVisible = false;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		u3an = this;
	}

	private static boolean activityVisible;

	public static U3an getApplication() {
		return u3an;
	}

	public static void exitApplication() {
		System.exit(1);
	}
}
