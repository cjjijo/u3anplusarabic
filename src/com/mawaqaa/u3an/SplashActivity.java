package com.mawaqaa.u3an;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.mawaqaa.u3an.appconstants.AppConstants;
import com.mawaqaa.u3an.gcm.GCMHelper;
import com.mawaqaa.u3an.utilities.PrefUtil;
import com.mawaqaa.u3an.R;

public class SplashActivity extends Activity {
	protected int _splashTime = 8000;

	private Thread splashTread;
	private VideoView videoViewSplash;
	private DisplayMetrics dm;
	private SurfaceView sur_View;
	private MediaController media_Controller;
	private String uriPath;
	public String FILENAMEREGKEY = "registrationID.txt";

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		videoViewSplash = (VideoView) findViewById(R.id.videoViewSplash);
		/*
		 * uriPath = "android.resource://com.android.AndroidVideoPlayer/" +
		 * R.raw.splash_video_knife_small;
		 */
		uriPath = "android.resource://" + getPackageName() + "/" + R.raw.splash_video_knife_small;
		Log.d("SplashActivity", "onCreate");
		try {
			getProjectKeyHash();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		final SplashActivity sPlashScreen = this;
		gcmRegisteration();
		playVideo();

		splashTread = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						// wait 5 sec
						wait(_splashTime);
					}
				} catch (InterruptedException e) {
				} finally {
					finish();
					Boolean isnetWorkAvailable = isNetworkAvailable();
					// start a new activity
					Intent i = new Intent();
					i.setClass(sPlashScreen, MainActivity.class);
					startActivity(i);
				}
			}
		};
		splashTread.start();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			synchronized (splashTread) {
				splashTread.notifyAll();
			}
		}
		return true;
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	public void getProjectKeyHash() throws NoSuchAlgorithmException {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo("com.mawaqaa.u3an", PackageManager.GET_SIGNATURES);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		for (Signature signature : info.signatures) {
			MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(signature.toByteArray());
			Log.d("KeyHash:", "KeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
		}
	}

	public void gcmRegisteration() {
		String data = "q";
		try {
			data = readFromFile();
		} catch (Exception e) {
		}
		if (data.equals("q")) {
			PrefUtil.setpushNotificationEnabled(getApplicationContext());
			GCMHelper gcmHelper = new GCMHelper(this, AppConstants.Add_Device_Token);//
			gcmHelper.gcmRegisteration();
		} else {

		}
	}

	private String readFromFile() {
		String ret = "q";
		try {
			InputStream inputStream = this.openFileInput(FILENAMEREGKEY);

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
				BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();
				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.e("login activity", "File not found: " + e.toString());
		} catch (IOException e) {
			Log.e("login activity", "Can not read file: " + e.toString());
		}
		return ret;
	}

	private void playVideo() {

		/*
		 * Uri uri = Uri.parse(uriPath); videoViewSplash.setVideoURI(uri);
		 * videoViewSplash.requestFocus(); videoViewSplash.start();
		 */

		/*
		 * media_Controller = new MediaController(this); dm = new
		 * DisplayMetrics();
		 * this.getWindowManager().getDefaultDisplay().getMetrics(dm); int
		 * height = dm.heightPixels; int width = dm.widthPixels;
		 */

		/*
		 * DisplayMetrics metrics = new DisplayMetrics();
		 * getWindowManager().getDefaultDisplay().getMetrics(metrics);
		 * android.widget.FrameLayout.LayoutParams params =
		 * (android.widget.FrameLayout.LayoutParams)
		 * videoViewSplash.getLayoutParams(); params.width =
		 * metrics.widthPixels; params.height = metrics.heightPixels;
		 * params.leftMargin = 0; videoViewSplash.setLayoutParams(params);
		 */
		// videoViewSplash.setMinimumWidth(width);
		// videoViewSplash.setMinimumHeight(height);
		// videoViewSplash.setMediaController(media_Controller);
		// videoViewSplash.setMediaController(new MediaController(this));

		MediaPlayer mp = MediaPlayer.create(this, R.raw.forkandknife);
		videoViewSplash.setVideoPath(uriPath);

		mp.start();
		videoViewSplash.start();

	}

	@Override
	public void onBackPressed() {

	}

}
